Create database SFCI
use SFCI

create table Usuarios(

)

create table Clientes(
ID_Cliente int identity(1,1) primary key not null,
PNom_C nvarchar(20) not null,
SNom_C nvarchar(20),
PApe_C nvarchar(20) not null,
SApe_C nvarchar(20),
Dir_C nvarchar(70) not null,
TelC char(8) check (TelC like '[2|5|7|8][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
)

Create table Contacto(
ID_Cont int identity (1,1) primary key not null,
PNom_Cont nvarchar(20) not null,
SNom_Cont nvarchar(20) not null,
PApe_Cont nvarchar(20) not null,
SApe_Cont nvarchar(20) not null,
Movil_Cont char(8) check (TelC like '[5|7|8][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
)

create table Proveedor(
ID_prov int identity primary key not null,
Nombre_Prov nvarchar(45) not null,
Dir_Prov nvarchar(70) not null,
Tel_Prov char(8) check (TelProv like '[2|5|7|8][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
Correo_Prov nvarchar(45),
URL_Prov nvarchar(45),
ID_Cont int foreign key references Contacto (ID_Cont)
)

create table Productos(
Cod_Prod char(4) primary key not null,
Nom_Prod nvarchar(50) not null,
Desc_Prod nvarchar(50) not null,
Fecha_Elab datetime,
Fecha_Venc datetime,
Precio float not null,
Cantidad int not null,
ID_prov char(4) foreign key references Proveedor(ID_prov)
)

create table Pedidos(
ID_Pedido int primary key not null,
FechaPed datetime default getdate(),
FechaEnt datetime,
--PAut nvarchar(45) not null,
)

create table Detalle_Ped(
ID_Detalle_Ped int foreign key references Pedidos(ID_Pedido) not null,
Cod_prod char(4) foreign key references Productos(Cod_prod) not null,
Cantp int not null,
Subtp float,
primary key (ID_Pedido, Cod_prod)
)

create table Ventas(
ID_Cliente int foreign key references Clientes(ID_Cliente) not null,
ID_Venta int identity(1,1) primary key not null,
FechaVenta datetime default getdate() not null,
Total float
)

create table Detalle_Venta(
ID_Detalle_Venta int foreign key references Ventas(ID_Venta) not null,
Cod_prod char(4) foreign key references Productos(Cod_prod) not null,
cantidad int not null,
Sub_TVenta float,
primary key (ID_Venta, Cod_prod)
)

create table Orden_Compras(
N_Orden_Compras char(4) primary key not null,
Fecha_Orden_Compras datetime default getdate(),
ID_Prov char(4) foreign key references Proveedor(ID_Prov),
Total_Compras float
)

create table Detalle_Orden_Compra(
N_Orden_Compra char(4) foreign key references Orden_Compras(N_Orden_Compras) not null,
Cod_Prod char(4) foreign key references Productos(Cod_Prod) not null,
Cant_Compra int not null,
Precio_Compra float not null,
Subt_Compra float,
primary key (N_Orden_Compras, Cod_Prod)
)

create table Dev_Cliente(
ID_Devolucion int primary key not null,
FechaDev datetime default getdate() not null,
ID_Cliente int foreign key references Clientes(ID_Cliente) not null,
ID_Venta int foreign key references Ventas(ID_Venta) not null,
total_Dev float
)

create table Det_DevCliente(
ID_Devolucion int foreign key references Dev_Cliente(ID_Devolucion) not null,
Cod_prod char(4) foreign key references Productos(Cod_prod) not null,
Cantidad int not null,
Subtd float,
primary key (ID_Devolucion, Cod_prod)
)

create table Dev_Proveedor(
N_Dev_Prov char(4) primary key not null,
FechaOrdenD datetime default getdate(),
ID_Prov char(4) foreign key references Proveedor(ID_Prov),
Total_Dev float
)

create table Detalle_Dev_Prov(
N_Dev char(4) foreign key references Dev_Prov(N_Dev) not null,
Cod_Prod char(4) foreign key references Productos(Cod_Prod) not null,
Cantd int not null,
Preciod float not null,
Sub_TDev float,
primary key (N_Dev, Cod_Prod)
)

Create table Factura(
N_Fac int primary key not null,
)