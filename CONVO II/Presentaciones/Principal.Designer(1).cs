﻿namespace Pulpería_Lovo
{
    partial class Principal
    {
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation2 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.Panel_Contenido = new System.Windows.Forms.Panel();
            this.Panel_de_Contenido = new System.Windows.Forms.Panel();
            this.Panel_Men = new System.Windows.Forms.Panel();
            this.Menu_Plegable = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.Sub_Panel_Opciones = new System.Windows.Forms.Panel();
            this.bunifuFlatButton5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton11 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Ventas_Prin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Sub_Panel_Cmpras = new System.Windows.Forms.Panel();
            this.Btn_Registra_Compra = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Proveedor_Prin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Creditos_prov = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Pagos_prov = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Sub_Panel_ventas = new System.Windows.Forms.Panel();
            this.Btn_Caja = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Clientes_Prin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Creditos_Ventas = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Cobro_ventas = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_RealizarVenta = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Compras = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Opciones_Prin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Sub_Panel_Productos = new System.Windows.Forms.Panel();
            this.Btn_Categoría_PrinDes = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Catalogo_PrinDes = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Productos_Prin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Reportes_Prin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Lb_Pulperia_Lovo_Prin = new System.Windows.Forms.Label();
            this.Linea_Menu = new Bunifu.Framework.UI.BunifuSeparator();
            this.Btn_Servicios_Prin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Logo_Principal = new System.Windows.Forms.PictureBox();
            this.Panel_PrincipalTop = new System.Windows.Forms.Panel();
            this.Btn_Maximizar_Prin = new System.Windows.Forms.PictureBox();
            this.Btn_Restaurar_Prin = new System.Windows.Forms.PictureBox();
            this.Btn_Minimizar_Prin = new System.Windows.Forms.PictureBox();
            this.Btn_Salir_Prin = new System.Windows.Forms.PictureBox();
            this.Lb_Menú_Prin = new System.Windows.Forms.Label();
            this.Btn_Menu_Prin = new System.Windows.Forms.PictureBox();
            this.panel_Menu = new System.Windows.Forms.Panel();
            this.Curva = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.Ani_MP_Amplia = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.Ani_MP_Reduce = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.Mov_Principal = new Bunifu.UI.WinForms.BunifuFormDock();
            this.Fecha_Hora_Prin = new System.Windows.Forms.Timer(this.components);
            this.Panel_SubMenu_Servicios = new System.Windows.Forms.Panel();
            this.Btn_Servicio_Recargas = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Servicio_Bac = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Panel_Contenido.SuspendLayout();
            this.Panel_Men.SuspendLayout();
            this.Menu_Plegable.SuspendLayout();
            this.Sub_Panel_Opciones.SuspendLayout();
            this.Sub_Panel_Cmpras.SuspendLayout();
            this.Sub_Panel_ventas.SuspendLayout();
            this.Sub_Panel_Productos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo_Principal)).BeginInit();
            this.Panel_PrincipalTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Maximizar_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Restaurar_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Minimizar_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Salir_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Menu_Prin)).BeginInit();
            this.Panel_SubMenu_Servicios.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel_Contenido
            // 
            this.Panel_Contenido.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Panel_Contenido.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel_Contenido.Controls.Add(this.Panel_de_Contenido);
            this.Panel_Contenido.Controls.Add(this.Panel_Men);
            this.Panel_Contenido.Controls.Add(this.Panel_PrincipalTop);
            this.Ani_MP_Reduce.SetDecoration(this.Panel_Contenido, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Panel_Contenido, BunifuAnimatorNS.DecorationType.None);
            this.Panel_Contenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Contenido.Location = new System.Drawing.Point(0, 0);
            this.Panel_Contenido.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Panel_Contenido.Name = "Panel_Contenido";
            this.Panel_Contenido.Size = new System.Drawing.Size(1400, 1100);
            this.Panel_Contenido.TabIndex = 3;
            this.Panel_Contenido.TabStop = true;
            // 
            // Panel_de_Contenido
            // 
            this.Panel_de_Contenido.BackColor = System.Drawing.Color.DimGray;
            this.Ani_MP_Reduce.SetDecoration(this.Panel_de_Contenido, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Panel_de_Contenido, BunifuAnimatorNS.DecorationType.None);
            this.Panel_de_Contenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_de_Contenido.Location = new System.Drawing.Point(325, 80);
            this.Panel_de_Contenido.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Panel_de_Contenido.Name = "Panel_de_Contenido";
            this.Panel_de_Contenido.Size = new System.Drawing.Size(1075, 1020);
            this.Panel_de_Contenido.TabIndex = 7;
            // 
            // Panel_Men
            // 
            this.Panel_Men.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.Panel_Men.Controls.Add(this.Menu_Plegable);
            this.Ani_MP_Reduce.SetDecoration(this.Panel_Men, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Panel_Men, BunifuAnimatorNS.DecorationType.None);
            this.Panel_Men.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Men.Location = new System.Drawing.Point(0, 80);
            this.Panel_Men.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Panel_Men.Name = "Panel_Men";
            this.Panel_Men.Size = new System.Drawing.Size(325, 1020);
            this.Panel_Men.TabIndex = 6;
            // 
            // Menu_Plegable
            // 
            this.Menu_Plegable.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Menu_Plegable.BackgroundImage")));
            this.Menu_Plegable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Menu_Plegable.Controls.Add(this.Panel_SubMenu_Servicios);
            this.Menu_Plegable.Controls.Add(this.Sub_Panel_Opciones);
            this.Menu_Plegable.Controls.Add(this.Btn_Ventas_Prin);
            this.Menu_Plegable.Controls.Add(this.Sub_Panel_Cmpras);
            this.Menu_Plegable.Controls.Add(this.Sub_Panel_ventas);
            this.Menu_Plegable.Controls.Add(this.Btn_Compras);
            this.Menu_Plegable.Controls.Add(this.Btn_Opciones_Prin);
            this.Menu_Plegable.Controls.Add(this.Sub_Panel_Productos);
            this.Menu_Plegable.Controls.Add(this.Btn_Productos_Prin);
            this.Menu_Plegable.Controls.Add(this.Btn_Reportes_Prin);
            this.Menu_Plegable.Controls.Add(this.Lb_Pulperia_Lovo_Prin);
            this.Menu_Plegable.Controls.Add(this.Linea_Menu);
            this.Menu_Plegable.Controls.Add(this.Btn_Servicios_Prin);
            this.Menu_Plegable.Controls.Add(this.Logo_Principal);
            this.Ani_MP_Reduce.SetDecoration(this.Menu_Plegable, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Menu_Plegable, BunifuAnimatorNS.DecorationType.None);
            this.Menu_Plegable.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Menu_Plegable.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.Menu_Plegable.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.Menu_Plegable.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Menu_Plegable.Location = new System.Drawing.Point(5, 6);
            this.Menu_Plegable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Menu_Plegable.Name = "Menu_Plegable";
            this.Menu_Plegable.Quality = 10;
            this.Menu_Plegable.Size = new System.Drawing.Size(312, 1004);
            this.Menu_Plegable.TabIndex = 0;
            // 
            // Sub_Panel_Opciones
            // 
            this.Sub_Panel_Opciones.Controls.Add(this.bunifuFlatButton5);
            this.Sub_Panel_Opciones.Controls.Add(this.bunifuFlatButton11);
            this.Ani_MP_Reduce.SetDecoration(this.Sub_Panel_Opciones, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Sub_Panel_Opciones, BunifuAnimatorNS.DecorationType.None);
            this.Sub_Panel_Opciones.Location = new System.Drawing.Point(112, 586);
            this.Sub_Panel_Opciones.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Sub_Panel_Opciones.Name = "Sub_Panel_Opciones";
            this.Sub_Panel_Opciones.Size = new System.Drawing.Size(312, 159);
            this.Sub_Panel_Opciones.TabIndex = 14;
            this.Sub_Panel_Opciones.Visible = false;
            // 
            // bunifuFlatButton5
            // 
            this.bunifuFlatButton5.Activecolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton5.BorderRadius = 0;
            this.bunifuFlatButton5.ButtonText = "      Cerrar Sesión";
            this.bunifuFlatButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton5.DisabledColor = System.Drawing.Color.White;
            this.bunifuFlatButton5.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton5.Iconimage")));
            this.bunifuFlatButton5.Iconimage_right = null;
            this.bunifuFlatButton5.Iconimage_right_Selected = null;
            this.bunifuFlatButton5.Iconimage_Selected = null;
            this.bunifuFlatButton5.IconMarginLeft = 0;
            this.bunifuFlatButton5.IconMarginRight = 0;
            this.bunifuFlatButton5.IconRightVisible = true;
            this.bunifuFlatButton5.IconRightZoom = 0D;
            this.bunifuFlatButton5.IconVisible = true;
            this.bunifuFlatButton5.IconZoom = 90D;
            this.bunifuFlatButton5.IsTab = false;
            this.bunifuFlatButton5.Location = new System.Drawing.Point(23, 74);
            this.bunifuFlatButton5.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.bunifuFlatButton5.Name = "bunifuFlatButton5";
            this.bunifuFlatButton5.Normalcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.bunifuFlatButton5.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.bunifuFlatButton5.selected = false;
            this.bunifuFlatButton5.Size = new System.Drawing.Size(289, 57);
            this.bunifuFlatButton5.TabIndex = 11;
            this.bunifuFlatButton5.Text = "      Cerrar Sesión";
            this.bunifuFlatButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton5.Textcolor = System.Drawing.Color.LightGray;
            this.bunifuFlatButton5.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton11
            // 
            this.bunifuFlatButton11.Activecolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton11.BackColor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton11.BorderRadius = 0;
            this.bunifuFlatButton11.ButtonText = "     Cambiar de Cuenta";
            this.bunifuFlatButton11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.bunifuFlatButton11, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.bunifuFlatButton11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton11.DisabledColor = System.Drawing.Color.White;
            this.bunifuFlatButton11.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton11.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton11.Iconimage")));
            this.bunifuFlatButton11.Iconimage_right = null;
            this.bunifuFlatButton11.Iconimage_right_Selected = null;
            this.bunifuFlatButton11.Iconimage_Selected = null;
            this.bunifuFlatButton11.IconMarginLeft = 0;
            this.bunifuFlatButton11.IconMarginRight = 0;
            this.bunifuFlatButton11.IconRightVisible = true;
            this.bunifuFlatButton11.IconRightZoom = 0D;
            this.bunifuFlatButton11.IconVisible = true;
            this.bunifuFlatButton11.IconZoom = 90D;
            this.bunifuFlatButton11.IsTab = false;
            this.bunifuFlatButton11.Location = new System.Drawing.Point(23, 5);
            this.bunifuFlatButton11.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.bunifuFlatButton11.Name = "bunifuFlatButton11";
            this.bunifuFlatButton11.Normalcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton11.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.bunifuFlatButton11.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.bunifuFlatButton11.selected = false;
            this.bunifuFlatButton11.Size = new System.Drawing.Size(289, 57);
            this.bunifuFlatButton11.TabIndex = 8;
            this.bunifuFlatButton11.Text = "     Cambiar de Cuenta";
            this.bunifuFlatButton11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton11.Textcolor = System.Drawing.Color.LightGray;
            this.bunifuFlatButton11.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Btn_Ventas_Prin
            // 
            this.Btn_Ventas_Prin.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Ventas_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Ventas_Prin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Ventas_Prin.BorderRadius = 0;
            this.Btn_Ventas_Prin.ButtonText = "          Ventas";
            this.Btn_Ventas_Prin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Ventas_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Ventas_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Ventas_Prin.DisabledColor = System.Drawing.Color.White;
            this.Btn_Ventas_Prin.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Ventas_Prin.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Ventas_Prin.Iconimage")));
            this.Btn_Ventas_Prin.Iconimage_right = null;
            this.Btn_Ventas_Prin.Iconimage_right_Selected = null;
            this.Btn_Ventas_Prin.Iconimage_Selected = null;
            this.Btn_Ventas_Prin.IconMarginLeft = 0;
            this.Btn_Ventas_Prin.IconMarginRight = 0;
            this.Btn_Ventas_Prin.IconRightVisible = true;
            this.Btn_Ventas_Prin.IconRightZoom = 0D;
            this.Btn_Ventas_Prin.IconVisible = true;
            this.Btn_Ventas_Prin.IconZoom = 90D;
            this.Btn_Ventas_Prin.IsTab = false;
            this.Btn_Ventas_Prin.Location = new System.Drawing.Point(19, 196);
            this.Btn_Ventas_Prin.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Ventas_Prin.Name = "Btn_Ventas_Prin";
            this.Btn_Ventas_Prin.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Ventas_Prin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Ventas_Prin.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Ventas_Prin.selected = false;
            this.Btn_Ventas_Prin.Size = new System.Drawing.Size(289, 57);
            this.Btn_Ventas_Prin.TabIndex = 6;
            this.Btn_Ventas_Prin.Text = "          Ventas";
            this.Btn_Ventas_Prin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Ventas_Prin.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Ventas_Prin.TextFont = new System.Drawing.Font("Lucida Bright", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ventas_Prin.Click += new System.EventHandler(this.Btn_Ventas_Prin_Click);
            // 
            // Sub_Panel_Cmpras
            // 
            this.Sub_Panel_Cmpras.Controls.Add(this.Btn_Registra_Compra);
            this.Sub_Panel_Cmpras.Controls.Add(this.Btn_Proveedor_Prin);
            this.Sub_Panel_Cmpras.Controls.Add(this.Btn_Creditos_prov);
            this.Sub_Panel_Cmpras.Controls.Add(this.Btn_Pagos_prov);
            this.Ani_MP_Reduce.SetDecoration(this.Sub_Panel_Cmpras, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Sub_Panel_Cmpras, BunifuAnimatorNS.DecorationType.None);
            this.Sub_Panel_Cmpras.Location = new System.Drawing.Point(23, 683);
            this.Sub_Panel_Cmpras.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Sub_Panel_Cmpras.Name = "Sub_Panel_Cmpras";
            this.Sub_Panel_Cmpras.Size = new System.Drawing.Size(311, 286);
            this.Sub_Panel_Cmpras.TabIndex = 16;
            this.Sub_Panel_Cmpras.Visible = false;
            // 
            // Btn_Registra_Compra
            // 
            this.Btn_Registra_Compra.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Registra_Compra.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Registra_Compra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Registra_Compra.BorderRadius = 0;
            this.Btn_Registra_Compra.ButtonText = "          Compras";
            this.Btn_Registra_Compra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Registra_Compra, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Registra_Compra, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Registra_Compra.DisabledColor = System.Drawing.Color.White;
            this.Btn_Registra_Compra.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Registra_Compra.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Registra_Compra.Iconimage")));
            this.Btn_Registra_Compra.Iconimage_right = null;
            this.Btn_Registra_Compra.Iconimage_right_Selected = null;
            this.Btn_Registra_Compra.Iconimage_Selected = null;
            this.Btn_Registra_Compra.IconMarginLeft = 0;
            this.Btn_Registra_Compra.IconMarginRight = 0;
            this.Btn_Registra_Compra.IconRightVisible = true;
            this.Btn_Registra_Compra.IconRightZoom = 0D;
            this.Btn_Registra_Compra.IconVisible = true;
            this.Btn_Registra_Compra.IconZoom = 90D;
            this.Btn_Registra_Compra.IsTab = false;
            this.Btn_Registra_Compra.Location = new System.Drawing.Point(19, 7);
            this.Btn_Registra_Compra.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Registra_Compra.Name = "Btn_Registra_Compra";
            this.Btn_Registra_Compra.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Registra_Compra.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Registra_Compra.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Registra_Compra.selected = false;
            this.Btn_Registra_Compra.Size = new System.Drawing.Size(289, 57);
            this.Btn_Registra_Compra.TabIndex = 15;
            this.Btn_Registra_Compra.Text = "          Compras";
            this.Btn_Registra_Compra.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Registra_Compra.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Registra_Compra.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Registra_Compra.Click += new System.EventHandler(this.Btn_Registra_Compra_Click);
            // 
            // Btn_Proveedor_Prin
            // 
            this.Btn_Proveedor_Prin.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Proveedor_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Proveedor_Prin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Proveedor_Prin.BorderRadius = 0;
            this.Btn_Proveedor_Prin.ButtonText = "          Proveedor";
            this.Btn_Proveedor_Prin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Proveedor_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Proveedor_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Proveedor_Prin.DisabledColor = System.Drawing.Color.White;
            this.Btn_Proveedor_Prin.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Proveedor_Prin.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Proveedor_Prin.Iconimage")));
            this.Btn_Proveedor_Prin.Iconimage_right = null;
            this.Btn_Proveedor_Prin.Iconimage_right_Selected = null;
            this.Btn_Proveedor_Prin.Iconimage_Selected = null;
            this.Btn_Proveedor_Prin.IconMarginLeft = 0;
            this.Btn_Proveedor_Prin.IconMarginRight = 0;
            this.Btn_Proveedor_Prin.IconRightVisible = true;
            this.Btn_Proveedor_Prin.IconRightZoom = 0D;
            this.Btn_Proveedor_Prin.IconVisible = true;
            this.Btn_Proveedor_Prin.IconZoom = 90D;
            this.Btn_Proveedor_Prin.IsTab = false;
            this.Btn_Proveedor_Prin.Location = new System.Drawing.Point(19, 78);
            this.Btn_Proveedor_Prin.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Proveedor_Prin.Name = "Btn_Proveedor_Prin";
            this.Btn_Proveedor_Prin.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Proveedor_Prin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Proveedor_Prin.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Proveedor_Prin.selected = false;
            this.Btn_Proveedor_Prin.Size = new System.Drawing.Size(289, 57);
            this.Btn_Proveedor_Prin.TabIndex = 16;
            this.Btn_Proveedor_Prin.Text = "          Proveedor";
            this.Btn_Proveedor_Prin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Proveedor_Prin.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Proveedor_Prin.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Proveedor_Prin.Click += new System.EventHandler(this.Btn_Proveedor_Prin_Click);
            // 
            // Btn_Creditos_prov
            // 
            this.Btn_Creditos_prov.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Creditos_prov.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Creditos_prov.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Creditos_prov.BorderRadius = 0;
            this.Btn_Creditos_prov.ButtonText = "          Creditos";
            this.Btn_Creditos_prov.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Creditos_prov, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Creditos_prov, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Creditos_prov.DisabledColor = System.Drawing.Color.White;
            this.Btn_Creditos_prov.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Creditos_prov.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Creditos_prov.Iconimage")));
            this.Btn_Creditos_prov.Iconimage_right = null;
            this.Btn_Creditos_prov.Iconimage_right_Selected = null;
            this.Btn_Creditos_prov.Iconimage_Selected = null;
            this.Btn_Creditos_prov.IconMarginLeft = 0;
            this.Btn_Creditos_prov.IconMarginRight = 0;
            this.Btn_Creditos_prov.IconRightVisible = true;
            this.Btn_Creditos_prov.IconRightZoom = 0D;
            this.Btn_Creditos_prov.IconVisible = true;
            this.Btn_Creditos_prov.IconZoom = 90D;
            this.Btn_Creditos_prov.IsTab = false;
            this.Btn_Creditos_prov.Location = new System.Drawing.Point(19, 218);
            this.Btn_Creditos_prov.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Creditos_prov.Name = "Btn_Creditos_prov";
            this.Btn_Creditos_prov.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Creditos_prov.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Creditos_prov.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Creditos_prov.selected = false;
            this.Btn_Creditos_prov.Size = new System.Drawing.Size(289, 57);
            this.Btn_Creditos_prov.TabIndex = 13;
            this.Btn_Creditos_prov.Text = "          Creditos";
            this.Btn_Creditos_prov.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Creditos_prov.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Creditos_prov.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Btn_Pagos_prov
            // 
            this.Btn_Pagos_prov.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Pagos_prov.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Pagos_prov.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Pagos_prov.BorderRadius = 0;
            this.Btn_Pagos_prov.ButtonText = "          Pagos";
            this.Btn_Pagos_prov.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Pagos_prov, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Pagos_prov, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Pagos_prov.DisabledColor = System.Drawing.Color.White;
            this.Btn_Pagos_prov.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Pagos_prov.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Pagos_prov.Iconimage")));
            this.Btn_Pagos_prov.Iconimage_right = null;
            this.Btn_Pagos_prov.Iconimage_right_Selected = null;
            this.Btn_Pagos_prov.Iconimage_Selected = null;
            this.Btn_Pagos_prov.IconMarginLeft = 0;
            this.Btn_Pagos_prov.IconMarginRight = 0;
            this.Btn_Pagos_prov.IconRightVisible = true;
            this.Btn_Pagos_prov.IconRightZoom = 0D;
            this.Btn_Pagos_prov.IconVisible = true;
            this.Btn_Pagos_prov.IconZoom = 90D;
            this.Btn_Pagos_prov.IsTab = false;
            this.Btn_Pagos_prov.Location = new System.Drawing.Point(19, 148);
            this.Btn_Pagos_prov.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Pagos_prov.Name = "Btn_Pagos_prov";
            this.Btn_Pagos_prov.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Pagos_prov.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Pagos_prov.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Pagos_prov.selected = false;
            this.Btn_Pagos_prov.Size = new System.Drawing.Size(289, 57);
            this.Btn_Pagos_prov.TabIndex = 12;
            this.Btn_Pagos_prov.Text = "          Pagos";
            this.Btn_Pagos_prov.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Pagos_prov.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Pagos_prov.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Sub_Panel_ventas
            // 
            this.Sub_Panel_ventas.Controls.Add(this.Btn_Caja);
            this.Sub_Panel_ventas.Controls.Add(this.Btn_Clientes_Prin);
            this.Sub_Panel_ventas.Controls.Add(this.Btn_Creditos_Ventas);
            this.Sub_Panel_ventas.Controls.Add(this.Btn_Cobro_ventas);
            this.Sub_Panel_ventas.Controls.Add(this.btn_RealizarVenta);
            this.Ani_MP_Reduce.SetDecoration(this.Sub_Panel_ventas, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Sub_Panel_ventas, BunifuAnimatorNS.DecorationType.None);
            this.Sub_Panel_ventas.Location = new System.Drawing.Point(52, 641);
            this.Sub_Panel_ventas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Sub_Panel_ventas.Name = "Sub_Panel_ventas";
            this.Sub_Panel_ventas.Size = new System.Drawing.Size(311, 359);
            this.Sub_Panel_ventas.TabIndex = 17;
            this.Sub_Panel_ventas.Visible = false;
            // 
            // Btn_Caja
            // 
            this.Btn_Caja.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Caja.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Caja.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Caja.BorderRadius = 0;
            this.Btn_Caja.ButtonText = "          Caja";
            this.Btn_Caja.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Caja, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Caja, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Caja.DisabledColor = System.Drawing.Color.White;
            this.Btn_Caja.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Caja.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Caja.Iconimage")));
            this.Btn_Caja.Iconimage_right = null;
            this.Btn_Caja.Iconimage_right_Selected = null;
            this.Btn_Caja.Iconimage_Selected = null;
            this.Btn_Caja.IconMarginLeft = 0;
            this.Btn_Caja.IconMarginRight = 0;
            this.Btn_Caja.IconRightVisible = true;
            this.Btn_Caja.IconRightZoom = 0D;
            this.Btn_Caja.IconVisible = true;
            this.Btn_Caja.IconZoom = 90D;
            this.Btn_Caja.IsTab = false;
            this.Btn_Caja.Location = new System.Drawing.Point(17, 284);
            this.Btn_Caja.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Caja.Name = "Btn_Caja";
            this.Btn_Caja.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Caja.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Caja.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Caja.selected = false;
            this.Btn_Caja.Size = new System.Drawing.Size(289, 57);
            this.Btn_Caja.TabIndex = 16;
            this.Btn_Caja.Text = "          Caja";
            this.Btn_Caja.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Caja.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Caja.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Btn_Clientes_Prin
            // 
            this.Btn_Clientes_Prin.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Clientes_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Clientes_Prin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Clientes_Prin.BorderRadius = 0;
            this.Btn_Clientes_Prin.ButtonText = "          Clientes";
            this.Btn_Clientes_Prin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Clientes_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Clientes_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Clientes_Prin.DisabledColor = System.Drawing.Color.White;
            this.Btn_Clientes_Prin.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Clientes_Prin.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Clientes_Prin.Iconimage")));
            this.Btn_Clientes_Prin.Iconimage_right = null;
            this.Btn_Clientes_Prin.Iconimage_right_Selected = null;
            this.Btn_Clientes_Prin.Iconimage_Selected = null;
            this.Btn_Clientes_Prin.IconMarginLeft = 0;
            this.Btn_Clientes_Prin.IconMarginRight = 0;
            this.Btn_Clientes_Prin.IconRightVisible = true;
            this.Btn_Clientes_Prin.IconRightZoom = 0D;
            this.Btn_Clientes_Prin.IconVisible = true;
            this.Btn_Clientes_Prin.IconZoom = 90D;
            this.Btn_Clientes_Prin.IsTab = false;
            this.Btn_Clientes_Prin.Location = new System.Drawing.Point(17, 78);
            this.Btn_Clientes_Prin.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Clientes_Prin.Name = "Btn_Clientes_Prin";
            this.Btn_Clientes_Prin.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Clientes_Prin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Clientes_Prin.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Clientes_Prin.selected = false;
            this.Btn_Clientes_Prin.Size = new System.Drawing.Size(289, 57);
            this.Btn_Clientes_Prin.TabIndex = 15;
            this.Btn_Clientes_Prin.Text = "          Clientes";
            this.Btn_Clientes_Prin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Clientes_Prin.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Clientes_Prin.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Clientes_Prin.Click += new System.EventHandler(this.Btn_Clientes_Prin_Click);
            // 
            // Btn_Creditos_Ventas
            // 
            this.Btn_Creditos_Ventas.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Creditos_Ventas.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Creditos_Ventas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Creditos_Ventas.BorderRadius = 0;
            this.Btn_Creditos_Ventas.ButtonText = "          Creditos";
            this.Btn_Creditos_Ventas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Creditos_Ventas, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Creditos_Ventas, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Creditos_Ventas.DisabledColor = System.Drawing.Color.White;
            this.Btn_Creditos_Ventas.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Creditos_Ventas.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Creditos_Ventas.Iconimage")));
            this.Btn_Creditos_Ventas.Iconimage_right = null;
            this.Btn_Creditos_Ventas.Iconimage_right_Selected = null;
            this.Btn_Creditos_Ventas.Iconimage_Selected = null;
            this.Btn_Creditos_Ventas.IconMarginLeft = 0;
            this.Btn_Creditos_Ventas.IconMarginRight = 0;
            this.Btn_Creditos_Ventas.IconRightVisible = true;
            this.Btn_Creditos_Ventas.IconRightZoom = 0D;
            this.Btn_Creditos_Ventas.IconVisible = true;
            this.Btn_Creditos_Ventas.IconZoom = 90D;
            this.Btn_Creditos_Ventas.IsTab = false;
            this.Btn_Creditos_Ventas.Location = new System.Drawing.Point(17, 215);
            this.Btn_Creditos_Ventas.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Creditos_Ventas.Name = "Btn_Creditos_Ventas";
            this.Btn_Creditos_Ventas.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Creditos_Ventas.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Creditos_Ventas.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Creditos_Ventas.selected = false;
            this.Btn_Creditos_Ventas.Size = new System.Drawing.Size(289, 57);
            this.Btn_Creditos_Ventas.TabIndex = 13;
            this.Btn_Creditos_Ventas.Text = "          Creditos";
            this.Btn_Creditos_Ventas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Creditos_Ventas.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Creditos_Ventas.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Btn_Cobro_ventas
            // 
            this.Btn_Cobro_ventas.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Cobro_ventas.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Cobro_ventas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Cobro_ventas.BorderRadius = 0;
            this.Btn_Cobro_ventas.ButtonText = "          Cobros";
            this.Btn_Cobro_ventas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Cobro_ventas, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Cobro_ventas, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Cobro_ventas.DisabledColor = System.Drawing.Color.White;
            this.Btn_Cobro_ventas.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Cobro_ventas.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Cobro_ventas.Iconimage")));
            this.Btn_Cobro_ventas.Iconimage_right = null;
            this.Btn_Cobro_ventas.Iconimage_right_Selected = null;
            this.Btn_Cobro_ventas.Iconimage_Selected = null;
            this.Btn_Cobro_ventas.IconMarginLeft = 0;
            this.Btn_Cobro_ventas.IconMarginRight = 0;
            this.Btn_Cobro_ventas.IconRightVisible = true;
            this.Btn_Cobro_ventas.IconRightZoom = 0D;
            this.Btn_Cobro_ventas.IconVisible = true;
            this.Btn_Cobro_ventas.IconZoom = 90D;
            this.Btn_Cobro_ventas.IsTab = false;
            this.Btn_Cobro_ventas.Location = new System.Drawing.Point(17, 146);
            this.Btn_Cobro_ventas.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Cobro_ventas.Name = "Btn_Cobro_ventas";
            this.Btn_Cobro_ventas.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Cobro_ventas.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Cobro_ventas.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Cobro_ventas.selected = false;
            this.Btn_Cobro_ventas.Size = new System.Drawing.Size(289, 57);
            this.Btn_Cobro_ventas.TabIndex = 12;
            this.Btn_Cobro_ventas.Text = "          Cobros";
            this.Btn_Cobro_ventas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Cobro_ventas.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Cobro_ventas.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btn_RealizarVenta
            // 
            this.btn_RealizarVenta.Activecolor = System.Drawing.Color.Transparent;
            this.btn_RealizarVenta.BackColor = System.Drawing.Color.Transparent;
            this.btn_RealizarVenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_RealizarVenta.BorderRadius = 0;
            this.btn_RealizarVenta.ButtonText = "          Realizar Venta";
            this.btn_RealizarVenta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.btn_RealizarVenta, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.btn_RealizarVenta, BunifuAnimatorNS.DecorationType.None);
            this.btn_RealizarVenta.DisabledColor = System.Drawing.Color.White;
            this.btn_RealizarVenta.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_RealizarVenta.Iconimage = null;
            this.btn_RealizarVenta.Iconimage_right = null;
            this.btn_RealizarVenta.Iconimage_right_Selected = null;
            this.btn_RealizarVenta.Iconimage_Selected = null;
            this.btn_RealizarVenta.IconMarginLeft = 0;
            this.btn_RealizarVenta.IconMarginRight = 0;
            this.btn_RealizarVenta.IconRightVisible = true;
            this.btn_RealizarVenta.IconRightZoom = 0D;
            this.btn_RealizarVenta.IconVisible = true;
            this.btn_RealizarVenta.IconZoom = 90D;
            this.btn_RealizarVenta.IsTab = false;
            this.btn_RealizarVenta.Location = new System.Drawing.Point(17, 7);
            this.btn_RealizarVenta.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.btn_RealizarVenta.Name = "btn_RealizarVenta";
            this.btn_RealizarVenta.Normalcolor = System.Drawing.Color.Transparent;
            this.btn_RealizarVenta.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.btn_RealizarVenta.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.btn_RealizarVenta.selected = false;
            this.btn_RealizarVenta.Size = new System.Drawing.Size(289, 57);
            this.btn_RealizarVenta.TabIndex = 9;
            this.btn_RealizarVenta.Text = "          Realizar Venta";
            this.btn_RealizarVenta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_RealizarVenta.Textcolor = System.Drawing.Color.LightGray;
            this.btn_RealizarVenta.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RealizarVenta.Click += new System.EventHandler(this.BunifuFlatButton2_Click);
            // 
            // Btn_Compras
            // 
            this.Btn_Compras.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Compras.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Compras.BorderRadius = 0;
            this.Btn_Compras.ButtonText = "          Compras";
            this.Btn_Compras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Compras, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Compras, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Compras.DisabledColor = System.Drawing.Color.White;
            this.Btn_Compras.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Compras.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Compras.Iconimage")));
            this.Btn_Compras.Iconimage_right = null;
            this.Btn_Compras.Iconimage_right_Selected = null;
            this.Btn_Compras.Iconimage_Selected = null;
            this.Btn_Compras.IconMarginLeft = 0;
            this.Btn_Compras.IconMarginRight = 0;
            this.Btn_Compras.IconRightVisible = true;
            this.Btn_Compras.IconRightZoom = 0D;
            this.Btn_Compras.IconVisible = true;
            this.Btn_Compras.IconZoom = 90D;
            this.Btn_Compras.IsTab = false;
            this.Btn_Compras.Location = new System.Drawing.Point(19, 268);
            this.Btn_Compras.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Compras.Name = "Btn_Compras";
            this.Btn_Compras.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Compras.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Compras.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Compras.selected = false;
            this.Btn_Compras.Size = new System.Drawing.Size(289, 57);
            this.Btn_Compras.TabIndex = 7;
            this.Btn_Compras.Text = "          Compras";
            this.Btn_Compras.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Compras.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Compras.TextFont = new System.Drawing.Font("Lucida Bright", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Compras.Click += new System.EventHandler(this.Btn_Compras_Click);
            // 
            // Btn_Opciones_Prin
            // 
            this.Btn_Opciones_Prin.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Opciones_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Opciones_Prin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Opciones_Prin.BorderRadius = 0;
            this.Btn_Opciones_Prin.ButtonText = "          Opciones";
            this.Btn_Opciones_Prin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Opciones_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Opciones_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Opciones_Prin.DisabledColor = System.Drawing.Color.White;
            this.Btn_Opciones_Prin.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Opciones_Prin.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Opciones_Prin.Iconimage")));
            this.Btn_Opciones_Prin.Iconimage_right = null;
            this.Btn_Opciones_Prin.Iconimage_right_Selected = null;
            this.Btn_Opciones_Prin.Iconimage_Selected = null;
            this.Btn_Opciones_Prin.IconMarginLeft = 0;
            this.Btn_Opciones_Prin.IconMarginRight = 0;
            this.Btn_Opciones_Prin.IconRightVisible = true;
            this.Btn_Opciones_Prin.IconRightZoom = 0D;
            this.Btn_Opciones_Prin.IconVisible = true;
            this.Btn_Opciones_Prin.IconZoom = 90D;
            this.Btn_Opciones_Prin.IsTab = false;
            this.Btn_Opciones_Prin.Location = new System.Drawing.Point(19, 484);
            this.Btn_Opciones_Prin.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Opciones_Prin.Name = "Btn_Opciones_Prin";
            this.Btn_Opciones_Prin.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Opciones_Prin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Opciones_Prin.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Opciones_Prin.selected = false;
            this.Btn_Opciones_Prin.Size = new System.Drawing.Size(289, 57);
            this.Btn_Opciones_Prin.TabIndex = 12;
            this.Btn_Opciones_Prin.Text = "          Opciones";
            this.Btn_Opciones_Prin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Opciones_Prin.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Opciones_Prin.TextFont = new System.Drawing.Font("Lucida Bright", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Opciones_Prin.Click += new System.EventHandler(this.Btn_Opciones_Prin_Click);
            // 
            // Sub_Panel_Productos
            // 
            this.Sub_Panel_Productos.Controls.Add(this.Btn_Categoría_PrinDes);
            this.Sub_Panel_Productos.Controls.Add(this.Btn_Catalogo_PrinDes);
            this.Ani_MP_Reduce.SetDecoration(this.Sub_Panel_Productos, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Sub_Panel_Productos, BunifuAnimatorNS.DecorationType.None);
            this.Sub_Panel_Productos.Location = new System.Drawing.Point(93, 612);
            this.Sub_Panel_Productos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Sub_Panel_Productos.Name = "Sub_Panel_Productos";
            this.Sub_Panel_Productos.Size = new System.Drawing.Size(311, 153);
            this.Sub_Panel_Productos.TabIndex = 13;
            this.Sub_Panel_Productos.Visible = false;
            // 
            // Btn_Categoría_PrinDes
            // 
            this.Btn_Categoría_PrinDes.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Categoría_PrinDes.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Categoría_PrinDes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Categoría_PrinDes.BorderRadius = 0;
            this.Btn_Categoría_PrinDes.ButtonText = "         Categoría";
            this.Btn_Categoría_PrinDes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Categoría_PrinDes, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Categoría_PrinDes, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Categoría_PrinDes.DisabledColor = System.Drawing.Color.White;
            this.Btn_Categoría_PrinDes.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Categoría_PrinDes.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Categoría_PrinDes.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Categoría_PrinDes.Iconimage")));
            this.Btn_Categoría_PrinDes.Iconimage_right = null;
            this.Btn_Categoría_PrinDes.Iconimage_right_Selected = null;
            this.Btn_Categoría_PrinDes.Iconimage_Selected = null;
            this.Btn_Categoría_PrinDes.IconMarginLeft = 0;
            this.Btn_Categoría_PrinDes.IconMarginRight = 0;
            this.Btn_Categoría_PrinDes.IconRightVisible = true;
            this.Btn_Categoría_PrinDes.IconRightZoom = 0D;
            this.Btn_Categoría_PrinDes.IconVisible = true;
            this.Btn_Categoría_PrinDes.IconZoom = 90D;
            this.Btn_Categoría_PrinDes.IsTab = false;
            this.Btn_Categoría_PrinDes.Location = new System.Drawing.Point(19, 76);
            this.Btn_Categoría_PrinDes.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Categoría_PrinDes.Name = "Btn_Categoría_PrinDes";
            this.Btn_Categoría_PrinDes.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Categoría_PrinDes.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Categoría_PrinDes.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Categoría_PrinDes.selected = false;
            this.Btn_Categoría_PrinDes.Size = new System.Drawing.Size(289, 57);
            this.Btn_Categoría_PrinDes.TabIndex = 10;
            this.Btn_Categoría_PrinDes.Text = "         Categoría";
            this.Btn_Categoría_PrinDes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Btn_Categoría_PrinDes.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Categoría_PrinDes.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Categoría_PrinDes.Click += new System.EventHandler(this.Btn_Categoría_PrinDes_Click);
            // 
            // Btn_Catalogo_PrinDes
            // 
            this.Btn_Catalogo_PrinDes.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Catalogo_PrinDes.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Catalogo_PrinDes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Catalogo_PrinDes.BorderRadius = 0;
            this.Btn_Catalogo_PrinDes.ButtonText = "     Cátalogo Producto";
            this.Btn_Catalogo_PrinDes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Catalogo_PrinDes, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Catalogo_PrinDes, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Catalogo_PrinDes.DisabledColor = System.Drawing.Color.White;
            this.Btn_Catalogo_PrinDes.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Catalogo_PrinDes.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Catalogo_PrinDes.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Catalogo_PrinDes.Iconimage")));
            this.Btn_Catalogo_PrinDes.Iconimage_right = null;
            this.Btn_Catalogo_PrinDes.Iconimage_right_Selected = null;
            this.Btn_Catalogo_PrinDes.Iconimage_Selected = null;
            this.Btn_Catalogo_PrinDes.IconMarginLeft = 0;
            this.Btn_Catalogo_PrinDes.IconMarginRight = 0;
            this.Btn_Catalogo_PrinDes.IconRightVisible = true;
            this.Btn_Catalogo_PrinDes.IconRightZoom = 0D;
            this.Btn_Catalogo_PrinDes.IconVisible = true;
            this.Btn_Catalogo_PrinDes.IconZoom = 90D;
            this.Btn_Catalogo_PrinDes.IsTab = false;
            this.Btn_Catalogo_PrinDes.Location = new System.Drawing.Point(19, 6);
            this.Btn_Catalogo_PrinDes.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Catalogo_PrinDes.Name = "Btn_Catalogo_PrinDes";
            this.Btn_Catalogo_PrinDes.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Catalogo_PrinDes.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Catalogo_PrinDes.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Catalogo_PrinDes.selected = false;
            this.Btn_Catalogo_PrinDes.Size = new System.Drawing.Size(289, 57);
            this.Btn_Catalogo_PrinDes.TabIndex = 8;
            this.Btn_Catalogo_PrinDes.Text = "     Cátalogo Producto";
            this.Btn_Catalogo_PrinDes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Btn_Catalogo_PrinDes.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Catalogo_PrinDes.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Catalogo_PrinDes.Click += new System.EventHandler(this.Btn_Catalogo_PrinDes_Click);
            // 
            // Btn_Productos_Prin
            // 
            this.Btn_Productos_Prin.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Productos_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Productos_Prin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Productos_Prin.BorderRadius = 0;
            this.Btn_Productos_Prin.ButtonText = "          Productos";
            this.Btn_Productos_Prin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Productos_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Productos_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Productos_Prin.DisabledColor = System.Drawing.Color.White;
            this.Btn_Productos_Prin.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Productos_Prin.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Productos_Prin.Iconimage")));
            this.Btn_Productos_Prin.Iconimage_right = null;
            this.Btn_Productos_Prin.Iconimage_right_Selected = null;
            this.Btn_Productos_Prin.Iconimage_Selected = null;
            this.Btn_Productos_Prin.IconMarginLeft = 0;
            this.Btn_Productos_Prin.IconMarginRight = 0;
            this.Btn_Productos_Prin.IconRightVisible = true;
            this.Btn_Productos_Prin.IconRightZoom = 0D;
            this.Btn_Productos_Prin.IconVisible = true;
            this.Btn_Productos_Prin.IconZoom = 90D;
            this.Btn_Productos_Prin.IsTab = false;
            this.Btn_Productos_Prin.Location = new System.Drawing.Point(19, 124);
            this.Btn_Productos_Prin.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Productos_Prin.Name = "Btn_Productos_Prin";
            this.Btn_Productos_Prin.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Productos_Prin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Productos_Prin.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Productos_Prin.selected = false;
            this.Btn_Productos_Prin.Size = new System.Drawing.Size(289, 57);
            this.Btn_Productos_Prin.TabIndex = 5;
            this.Btn_Productos_Prin.Text = "          Productos";
            this.Btn_Productos_Prin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Productos_Prin.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Productos_Prin.TextFont = new System.Drawing.Font("Lucida Bright", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Productos_Prin.Click += new System.EventHandler(this.Btn_Productos_Click);
            // 
            // Btn_Reportes_Prin
            // 
            this.Btn_Reportes_Prin.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Reportes_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Reportes_Prin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Reportes_Prin.BorderRadius = 0;
            this.Btn_Reportes_Prin.ButtonText = "          Reportes";
            this.Btn_Reportes_Prin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Reportes_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Reportes_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Reportes_Prin.DisabledColor = System.Drawing.Color.White;
            this.Btn_Reportes_Prin.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Reportes_Prin.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Reportes_Prin.Iconimage")));
            this.Btn_Reportes_Prin.Iconimage_right = null;
            this.Btn_Reportes_Prin.Iconimage_right_Selected = null;
            this.Btn_Reportes_Prin.Iconimage_Selected = null;
            this.Btn_Reportes_Prin.IconMarginLeft = 0;
            this.Btn_Reportes_Prin.IconMarginRight = 0;
            this.Btn_Reportes_Prin.IconRightVisible = true;
            this.Btn_Reportes_Prin.IconRightZoom = 0D;
            this.Btn_Reportes_Prin.IconVisible = true;
            this.Btn_Reportes_Prin.IconZoom = 90D;
            this.Btn_Reportes_Prin.IsTab = false;
            this.Btn_Reportes_Prin.Location = new System.Drawing.Point(19, 412);
            this.Btn_Reportes_Prin.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Reportes_Prin.Name = "Btn_Reportes_Prin";
            this.Btn_Reportes_Prin.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Reportes_Prin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Reportes_Prin.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Reportes_Prin.selected = false;
            this.Btn_Reportes_Prin.Size = new System.Drawing.Size(289, 57);
            this.Btn_Reportes_Prin.TabIndex = 11;
            this.Btn_Reportes_Prin.Text = "          Reportes";
            this.Btn_Reportes_Prin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Reportes_Prin.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Reportes_Prin.TextFont = new System.Drawing.Font("Lucida Bright", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Lb_Pulperia_Lovo_Prin
            // 
            this.Lb_Pulperia_Lovo_Prin.AutoSize = true;
            this.Lb_Pulperia_Lovo_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Ani_MP_Amplia.SetDecoration(this.Lb_Pulperia_Lovo_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Lb_Pulperia_Lovo_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Lb_Pulperia_Lovo_Prin.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Pulperia_Lovo_Prin.ForeColor = System.Drawing.Color.White;
            this.Lb_Pulperia_Lovo_Prin.Location = new System.Drawing.Point(88, 38);
            this.Lb_Pulperia_Lovo_Prin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lb_Pulperia_Lovo_Prin.Name = "Lb_Pulperia_Lovo_Prin";
            this.Lb_Pulperia_Lovo_Prin.Size = new System.Drawing.Size(180, 27);
            this.Lb_Pulperia_Lovo_Prin.TabIndex = 4;
            this.Lb_Pulperia_Lovo_Prin.Text = "Pulperia Lovo";
            this.Lb_Pulperia_Lovo_Prin.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Linea_Menu
            // 
            this.Linea_Menu.BackColor = System.Drawing.Color.Transparent;
            this.Ani_MP_Reduce.SetDecoration(this.Linea_Menu, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Linea_Menu, BunifuAnimatorNS.DecorationType.None);
            this.Linea_Menu.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(209)))), ((int)(((byte)(215)))));
            this.Linea_Menu.LineThickness = 1;
            this.Linea_Menu.Location = new System.Drawing.Point(17, 82);
            this.Linea_Menu.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Linea_Menu.Name = "Linea_Menu";
            this.Linea_Menu.Size = new System.Drawing.Size(281, 22);
            this.Linea_Menu.TabIndex = 0;
            this.Linea_Menu.Transparency = 255;
            this.Linea_Menu.Vertical = false;
            // 
            // Btn_Servicios_Prin
            // 
            this.Btn_Servicios_Prin.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Servicios_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Servicios_Prin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Servicios_Prin.BorderRadius = 0;
            this.Btn_Servicios_Prin.ButtonText = "          Servicios";
            this.Btn_Servicios_Prin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Servicios_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Servicios_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Servicios_Prin.DisabledColor = System.Drawing.Color.White;
            this.Btn_Servicios_Prin.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Servicios_Prin.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Servicios_Prin.Iconimage")));
            this.Btn_Servicios_Prin.Iconimage_right = null;
            this.Btn_Servicios_Prin.Iconimage_right_Selected = null;
            this.Btn_Servicios_Prin.Iconimage_Selected = null;
            this.Btn_Servicios_Prin.IconMarginLeft = 0;
            this.Btn_Servicios_Prin.IconMarginRight = 0;
            this.Btn_Servicios_Prin.IconRightVisible = true;
            this.Btn_Servicios_Prin.IconRightZoom = 0D;
            this.Btn_Servicios_Prin.IconVisible = true;
            this.Btn_Servicios_Prin.IconZoom = 90D;
            this.Btn_Servicios_Prin.IsTab = false;
            this.Btn_Servicios_Prin.Location = new System.Drawing.Point(19, 340);
            this.Btn_Servicios_Prin.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Servicios_Prin.Name = "Btn_Servicios_Prin";
            this.Btn_Servicios_Prin.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Servicios_Prin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Servicios_Prin.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Servicios_Prin.selected = false;
            this.Btn_Servicios_Prin.Size = new System.Drawing.Size(289, 57);
            this.Btn_Servicios_Prin.TabIndex = 10;
            this.Btn_Servicios_Prin.Text = "          Servicios";
            this.Btn_Servicios_Prin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_Servicios_Prin.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Servicios_Prin.TextFont = new System.Drawing.Font("Lucida Bright", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Logo_Principal
            // 
            this.Logo_Principal.BackColor = System.Drawing.Color.Transparent;
            this.Ani_MP_Amplia.SetDecoration(this.Logo_Principal, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Logo_Principal, BunifuAnimatorNS.DecorationType.None);
            this.Logo_Principal.Image = ((System.Drawing.Image)(resources.GetObject("Logo_Principal.Image")));
            this.Logo_Principal.Location = new System.Drawing.Point(12, 23);
            this.Logo_Principal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Logo_Principal.Name = "Logo_Principal";
            this.Logo_Principal.Size = new System.Drawing.Size(59, 55);
            this.Logo_Principal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Logo_Principal.TabIndex = 3;
            this.Logo_Principal.TabStop = false;
            // 
            // Panel_PrincipalTop
            // 
            this.Panel_PrincipalTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Maximizar_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Restaurar_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Minimizar_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Salir_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Lb_Menú_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Menu_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.panel_Menu);
            this.Ani_MP_Reduce.SetDecoration(this.Panel_PrincipalTop, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Panel_PrincipalTop, BunifuAnimatorNS.DecorationType.None);
            this.Panel_PrincipalTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_PrincipalTop.Location = new System.Drawing.Point(0, 0);
            this.Panel_PrincipalTop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Panel_PrincipalTop.Name = "Panel_PrincipalTop";
            this.Panel_PrincipalTop.Size = new System.Drawing.Size(1400, 80);
            this.Panel_PrincipalTop.TabIndex = 5;
            // 
            // Btn_Maximizar_Prin
            // 
            this.Btn_Maximizar_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Maximizar_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Maximizar_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Maximizar_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Maximizar_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Maximizar_Prin.Image")));
            this.Btn_Maximizar_Prin.Location = new System.Drawing.Point(1303, 27);
            this.Btn_Maximizar_Prin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Maximizar_Prin.Name = "Btn_Maximizar_Prin";
            this.Btn_Maximizar_Prin.Size = new System.Drawing.Size(36, 31);
            this.Btn_Maximizar_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Maximizar_Prin.TabIndex = 3;
            this.Btn_Maximizar_Prin.TabStop = false;
            this.Btn_Maximizar_Prin.Click += new System.EventHandler(this.Maximizar_Click);
            // 
            // Btn_Restaurar_Prin
            // 
            this.Btn_Restaurar_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Restaurar_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Restaurar_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Restaurar_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Restaurar_Prin.Image")));
            this.Btn_Restaurar_Prin.Location = new System.Drawing.Point(1303, 27);
            this.Btn_Restaurar_Prin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Restaurar_Prin.Name = "Btn_Restaurar_Prin";
            this.Btn_Restaurar_Prin.Size = new System.Drawing.Size(36, 31);
            this.Btn_Restaurar_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Restaurar_Prin.TabIndex = 3;
            this.Btn_Restaurar_Prin.TabStop = false;
            this.Btn_Restaurar_Prin.Visible = false;
            this.Btn_Restaurar_Prin.Click += new System.EventHandler(this.Restaurar_Click);
            // 
            // Btn_Minimizar_Prin
            // 
            this.Btn_Minimizar_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Minimizar_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Minimizar_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Minimizar_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Minimizar_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Minimizar_Prin.Image")));
            this.Btn_Minimizar_Prin.Location = new System.Drawing.Point(1261, 27);
            this.Btn_Minimizar_Prin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Minimizar_Prin.Name = "Btn_Minimizar_Prin";
            this.Btn_Minimizar_Prin.Size = new System.Drawing.Size(36, 31);
            this.Btn_Minimizar_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Minimizar_Prin.TabIndex = 3;
            this.Btn_Minimizar_Prin.TabStop = false;
            this.Btn_Minimizar_Prin.Click += new System.EventHandler(this.Minimizar_Click);
            // 
            // Btn_Salir_Prin
            // 
            this.Btn_Salir_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Salir_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Salir_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Salir_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Salir_Prin.ErrorImage = null;
            this.Btn_Salir_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Salir_Prin.Image")));
            this.Btn_Salir_Prin.Location = new System.Drawing.Point(1345, 27);
            this.Btn_Salir_Prin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Salir_Prin.Name = "Btn_Salir_Prin";
            this.Btn_Salir_Prin.Size = new System.Drawing.Size(29, 31);
            this.Btn_Salir_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btn_Salir_Prin.TabIndex = 4;
            this.Btn_Salir_Prin.TabStop = false;
            this.Btn_Salir_Prin.Click += new System.EventHandler(this.Salir_Click);
            // 
            // Lb_Menú_Prin
            // 
            this.Lb_Menú_Prin.AutoSize = true;
            this.Ani_MP_Amplia.SetDecoration(this.Lb_Menú_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Lb_Menú_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Lb_Menú_Prin.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Menú_Prin.ForeColor = System.Drawing.Color.White;
            this.Lb_Menú_Prin.Location = new System.Drawing.Point(91, 32);
            this.Lb_Menú_Prin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lb_Menú_Prin.Name = "Lb_Menú_Prin";
            this.Lb_Menú_Prin.Size = new System.Drawing.Size(87, 27);
            this.Lb_Menú_Prin.TabIndex = 3;
            this.Lb_Menú_Prin.Text = "MENU";
            this.Lb_Menú_Prin.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Btn_Menu_Prin
            // 
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Menu_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Menu_Prin, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Menu_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Menu_Prin.Image")));
            this.Btn_Menu_Prin.Location = new System.Drawing.Point(28, 32);
            this.Btn_Menu_Prin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Menu_Prin.Name = "Btn_Menu_Prin";
            this.Btn_Menu_Prin.Size = new System.Drawing.Size(36, 31);
            this.Btn_Menu_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Menu_Prin.TabIndex = 2;
            this.Btn_Menu_Prin.TabStop = false;
            this.Btn_Menu_Prin.Click += new System.EventHandler(this.BT_Menu_Click);
            // 
            // panel_Menu
            // 
            this.Ani_MP_Reduce.SetDecoration(this.panel_Menu, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.panel_Menu, BunifuAnimatorNS.DecorationType.None);
            this.panel_Menu.Location = new System.Drawing.Point(3, 86);
            this.panel_Menu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel_Menu.Name = "panel_Menu";
            this.panel_Menu.Size = new System.Drawing.Size(300, 820);
            this.panel_Menu.TabIndex = 1;
            // 
            // Curva
            // 
            this.Curva.ElipseRadius = 20;
            this.Curva.TargetControl = this;
            // 
            // Ani_MP_Amplia
            // 
            this.Ani_MP_Amplia.AnimationType = BunifuAnimatorNS.AnimationType.Leaf;
            this.Ani_MP_Amplia.Cursor = null;
            animation2.AnimateOnlyDifferences = true;
            animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
            animation2.LeafCoeff = 1F;
            animation2.MaxTime = 1F;
            animation2.MinTime = 0F;
            animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
            animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
            animation2.MosaicSize = 0;
            animation2.Padding = new System.Windows.Forms.Padding(0);
            animation2.RotateCoeff = 0F;
            animation2.RotateLimit = 0F;
            animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
            animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
            animation2.TimeCoeff = 0F;
            animation2.TransparencyCoeff = 0F;
            this.Ani_MP_Amplia.DefaultAnimation = animation2;
            this.Ani_MP_Amplia.Interval = 5;
            // 
            // Ani_MP_Reduce
            // 
            this.Ani_MP_Reduce.AnimationType = BunifuAnimatorNS.AnimationType.Scale;
            this.Ani_MP_Reduce.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.Ani_MP_Reduce.DefaultAnimation = animation1;
            this.Ani_MP_Reduce.Interval = 5;
            // 
            // Mov_Principal
            // 
            this.Mov_Principal.AllowFormDragging = true;
            this.Mov_Principal.AllowFormDropShadow = true;
            this.Mov_Principal.AllowFormResizing = true;
            this.Mov_Principal.AllowHidingBottomRegion = true;
            this.Mov_Principal.AllowOpacityChangesWhileDragging = false;
            this.Mov_Principal.BorderOptions.BottomBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Principal.BorderOptions.BottomBorder.BorderThickness = 1;
            this.Mov_Principal.BorderOptions.BottomBorder.ShowBorder = true;
            this.Mov_Principal.BorderOptions.LeftBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Principal.BorderOptions.LeftBorder.BorderThickness = 1;
            this.Mov_Principal.BorderOptions.LeftBorder.ShowBorder = true;
            this.Mov_Principal.BorderOptions.RightBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Principal.BorderOptions.RightBorder.BorderThickness = 1;
            this.Mov_Principal.BorderOptions.RightBorder.ShowBorder = true;
            this.Mov_Principal.BorderOptions.TopBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Principal.BorderOptions.TopBorder.BorderThickness = 1;
            this.Mov_Principal.BorderOptions.TopBorder.ShowBorder = true;
            this.Mov_Principal.ContainerControl = this;
            this.Mov_Principal.DockingIndicatorsColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(215)))), ((int)(((byte)(233)))));
            this.Mov_Principal.DockingIndicatorsOpacity = 0.5D;
            this.Mov_Principal.DockingOptions.DockAll = true;
            this.Mov_Principal.DockingOptions.DockBottomLeft = true;
            this.Mov_Principal.DockingOptions.DockBottomRight = true;
            this.Mov_Principal.DockingOptions.DockFullScreen = true;
            this.Mov_Principal.DockingOptions.DockLeft = true;
            this.Mov_Principal.DockingOptions.DockRight = true;
            this.Mov_Principal.DockingOptions.DockTopLeft = true;
            this.Mov_Principal.DockingOptions.DockTopRight = true;
            this.Mov_Principal.FormDraggingOpacity = 0.9D;
            this.Mov_Principal.ParentForm = this;
            this.Mov_Principal.ShowCursorChanges = true;
            this.Mov_Principal.ShowDockingIndicators = true;
            this.Mov_Principal.TitleBarOptions.AllowFormDragging = true;
            this.Mov_Principal.TitleBarOptions.BunifuFormDock = this.Mov_Principal;
            this.Mov_Principal.TitleBarOptions.DoubleClickToExpandWindow = false;
            this.Mov_Principal.TitleBarOptions.TitleBarControl = this.Panel_PrincipalTop;
            this.Mov_Principal.TitleBarOptions.UseBackColorOnDockingIndicators = false;
            // 
            // Fecha_Hora_Prin
            // 
            this.Fecha_Hora_Prin.Enabled = true;
            // 
            // Panel_SubMenu_Servicios
            // 
            this.Panel_SubMenu_Servicios.Controls.Add(this.Btn_Servicio_Recargas);
            this.Panel_SubMenu_Servicios.Controls.Add(this.Btn_Servicio_Bac);
            this.Ani_MP_Reduce.SetDecoration(this.Panel_SubMenu_Servicios, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this.Panel_SubMenu_Servicios, BunifuAnimatorNS.DecorationType.None);
            this.Panel_SubMenu_Servicios.Location = new System.Drawing.Point(135, 558);
            this.Panel_SubMenu_Servicios.Margin = new System.Windows.Forms.Padding(4);
            this.Panel_SubMenu_Servicios.Name = "Panel_SubMenu_Servicios";
            this.Panel_SubMenu_Servicios.Size = new System.Drawing.Size(312, 159);
            this.Panel_SubMenu_Servicios.TabIndex = 15;
            this.Panel_SubMenu_Servicios.Visible = false;
            // 
            // Btn_Servicio_Recargas
            // 
            this.Btn_Servicio_Recargas.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Servicio_Recargas.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Servicio_Recargas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Servicio_Recargas.BorderRadius = 0;
            this.Btn_Servicio_Recargas.ButtonText = "      Recargas";
            this.Btn_Servicio_Recargas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Servicio_Recargas, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Servicio_Recargas, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Servicio_Recargas.DisabledColor = System.Drawing.Color.White;
            this.Btn_Servicio_Recargas.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Servicio_Recargas.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Servicio_Recargas.Iconimage")));
            this.Btn_Servicio_Recargas.Iconimage_right = null;
            this.Btn_Servicio_Recargas.Iconimage_right_Selected = null;
            this.Btn_Servicio_Recargas.Iconimage_Selected = null;
            this.Btn_Servicio_Recargas.IconMarginLeft = 0;
            this.Btn_Servicio_Recargas.IconMarginRight = 0;
            this.Btn_Servicio_Recargas.IconRightVisible = true;
            this.Btn_Servicio_Recargas.IconRightZoom = 0D;
            this.Btn_Servicio_Recargas.IconVisible = true;
            this.Btn_Servicio_Recargas.IconZoom = 90D;
            this.Btn_Servicio_Recargas.IsTab = false;
            this.Btn_Servicio_Recargas.Location = new System.Drawing.Point(23, 74);
            this.Btn_Servicio_Recargas.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Servicio_Recargas.Name = "Btn_Servicio_Recargas";
            this.Btn_Servicio_Recargas.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Servicio_Recargas.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Servicio_Recargas.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Servicio_Recargas.selected = false;
            this.Btn_Servicio_Recargas.Size = new System.Drawing.Size(289, 57);
            this.Btn_Servicio_Recargas.TabIndex = 11;
            this.Btn_Servicio_Recargas.Text = "      Recargas";
            this.Btn_Servicio_Recargas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Btn_Servicio_Recargas.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Servicio_Recargas.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Btn_Servicio_Bac
            // 
            this.Btn_Servicio_Bac.Activecolor = System.Drawing.Color.Transparent;
            this.Btn_Servicio_Bac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Servicio_Bac.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Servicio_Bac.BorderRadius = 0;
            this.Btn_Servicio_Bac.ButtonText = "     Rapi Bac";
            this.Btn_Servicio_Bac.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ani_MP_Amplia.SetDecoration(this.Btn_Servicio_Bac, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Reduce.SetDecoration(this.Btn_Servicio_Bac, BunifuAnimatorNS.DecorationType.None);
            this.Btn_Servicio_Bac.DisabledColor = System.Drawing.Color.White;
            this.Btn_Servicio_Bac.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Servicio_Bac.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Servicio_Bac.Iconimage")));
            this.Btn_Servicio_Bac.Iconimage_right = null;
            this.Btn_Servicio_Bac.Iconimage_right_Selected = null;
            this.Btn_Servicio_Bac.Iconimage_Selected = null;
            this.Btn_Servicio_Bac.IconMarginLeft = 0;
            this.Btn_Servicio_Bac.IconMarginRight = 0;
            this.Btn_Servicio_Bac.IconRightVisible = true;
            this.Btn_Servicio_Bac.IconRightZoom = 0D;
            this.Btn_Servicio_Bac.IconVisible = true;
            this.Btn_Servicio_Bac.IconZoom = 90D;
            this.Btn_Servicio_Bac.IsTab = false;
            this.Btn_Servicio_Bac.Location = new System.Drawing.Point(23, 5);
            this.Btn_Servicio_Bac.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Servicio_Bac.Name = "Btn_Servicio_Bac";
            this.Btn_Servicio_Bac.Normalcolor = System.Drawing.Color.Transparent;
            this.Btn_Servicio_Bac.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(61)))), ((int)(((byte)(70)))));
            this.Btn_Servicio_Bac.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Btn_Servicio_Bac.selected = false;
            this.Btn_Servicio_Bac.Size = new System.Drawing.Size(289, 57);
            this.Btn_Servicio_Bac.TabIndex = 8;
            this.Btn_Servicio_Bac.Text = "     Rapi Bac";
            this.Btn_Servicio_Bac.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Btn_Servicio_Bac.Textcolor = System.Drawing.Color.LightGray;
            this.Btn_Servicio_Bac.TextFont = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1400, 1100);
            this.Controls.Add(this.Panel_Contenido);
            this.Ani_MP_Reduce.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Ani_MP_Amplia.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(867, 736);
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pulpería_Lovo";
            this.Panel_Contenido.ResumeLayout(false);
            this.Panel_Men.ResumeLayout(false);
            this.Menu_Plegable.ResumeLayout(false);
            this.Menu_Plegable.PerformLayout();
            this.Sub_Panel_Opciones.ResumeLayout(false);
            this.Sub_Panel_Cmpras.ResumeLayout(false);
            this.Sub_Panel_ventas.ResumeLayout(false);
            this.Sub_Panel_Productos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Logo_Principal)).EndInit();
            this.Panel_PrincipalTop.ResumeLayout(false);
            this.Panel_PrincipalTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Maximizar_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Restaurar_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Minimizar_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Salir_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Menu_Prin)).EndInit();
            this.Panel_SubMenu_Servicios.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel Panel_Contenido;
        private Bunifu.Framework.UI.BunifuElipse Curva;
        private BunifuAnimatorNS.BunifuTransition Ani_MP_Amplia;
        private BunifuAnimatorNS.BunifuTransition Ani_MP_Reduce;
        private Bunifu.UI.WinForms.BunifuFormDock Mov_Principal;
        private System.Windows.Forms.Timer Fecha_Hora_Prin;
        private System.Windows.Forms.Panel Panel_PrincipalTop;
        private System.Windows.Forms.PictureBox Btn_Maximizar_Prin;
        private System.Windows.Forms.PictureBox Btn_Restaurar_Prin;
        private System.Windows.Forms.PictureBox Btn_Minimizar_Prin;
        private System.Windows.Forms.PictureBox Btn_Salir_Prin;
        private System.Windows.Forms.Label Lb_Menú_Prin;
        private System.Windows.Forms.PictureBox Btn_Menu_Prin;
        private System.Windows.Forms.Panel panel_Menu;
        private System.Windows.Forms.Panel Panel_Men;
        private Bunifu.Framework.UI.BunifuGradientPanel Menu_Plegable;
        public System.Windows.Forms.Panel Sub_Panel_Opciones;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton5;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton11;
        private System.Windows.Forms.Panel Sub_Panel_Cmpras;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Registra_Compra;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Proveedor_Prin;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Creditos_prov;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Pagos_prov;
        private System.Windows.Forms.Panel Sub_Panel_Productos;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Categoría_PrinDes;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Catalogo_PrinDes;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Ventas_Prin;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Compras;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Opciones_Prin;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Productos_Prin;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Reportes_Prin;
        private System.Windows.Forms.Label Lb_Pulperia_Lovo_Prin;
        private Bunifu.Framework.UI.BunifuSeparator Linea_Menu;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Servicios_Prin;
        private System.Windows.Forms.PictureBox Logo_Principal;
        private System.Windows.Forms.Panel Sub_Panel_ventas;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Caja;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Clientes_Prin;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Creditos_Ventas;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Cobro_ventas;
        private Bunifu.Framework.UI.BunifuFlatButton btn_RealizarVenta;
        private System.Windows.Forms.Panel Panel_de_Contenido;
        public System.Windows.Forms.Panel Panel_SubMenu_Servicios;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Servicio_Recargas;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Servicio_Bac;
    }
}

