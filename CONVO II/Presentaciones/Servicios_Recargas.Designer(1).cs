﻿namespace Pulpería_Lovo.Presentaciones
{
    partial class Servicios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Servicios));
            this.Panel_TeclaN_Servicios = new Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel();
            this.Btn_Borrar_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Coma_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N0_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N9_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N8_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N7_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N6_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N5_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N4_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N3_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N2_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N1_Servicios = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Res_Desc_Servicios = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Desc_Servicios = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Res_SubT_Servicios = new Guna.UI.WinForms.GunaLabel();
            this.Btn_SubT_Servicios = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Crédito_Servicios = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Efectivo_Servicios = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_ResT_Servicios = new Guna.UI.WinForms.GunaLabel();
            this.Btb_Cantidad_Servicios = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Total_Servicios = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Des_Servicios = new Bunifu.Framework.UI.BunifuThinButton2();
            this.bunifuRadioButton1 = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.Lb_Movistar_SerRec = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Claro_SerRec = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Recargas_SerRec = new Guna.UI.WinForms.GunaLabel();
            this.RB_Claro_SerRec = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.RB_Movistar_SerRec = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.PB_Claro_SerRec = new System.Windows.Forms.PictureBox();
            this.PB_Monistar_SerRec = new System.Windows.Forms.PictureBox();
            this.Panel_TeclaN_Servicios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Claro_SerRec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Monistar_SerRec)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel_TeclaN_Servicios
            // 
            this.Panel_TeclaN_Servicios.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel_TeclaN_Servicios.BorderColor = System.Drawing.Color.Silver;
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_Borrar_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_Coma_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N0_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N9_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N8_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N7_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N6_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N5_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N4_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N3_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N2_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_N1_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_Res_Desc_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_Desc_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_Res_SubT_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_SubT_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_Crédito_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_Efectivo_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_ResT_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btb_Cantidad_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_Total_Servicios);
            this.Panel_TeclaN_Servicios.Controls.Add(this.Btn_Des_Servicios);
            this.Panel_TeclaN_Servicios.ForeColor = System.Drawing.Color.Gainsboro;
            this.Panel_TeclaN_Servicios.Location = new System.Drawing.Point(44, 164);
            this.Panel_TeclaN_Servicios.Name = "Panel_TeclaN_Servicios";
            this.Panel_TeclaN_Servicios.PanelColor = System.Drawing.Color.Empty;
            this.Panel_TeclaN_Servicios.ShadowDept = 2;
            this.Panel_TeclaN_Servicios.ShadowTopLeftVisible = false;
            this.Panel_TeclaN_Servicios.Size = new System.Drawing.Size(571, 559);
            this.Panel_TeclaN_Servicios.TabIndex = 75;
            // 
            // Btn_Borrar_Servicios
            // 
            this.Btn_Borrar_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Borrar_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Borrar_Servicios.Image")));
            this.Btn_Borrar_Servicios.ImageActive = null;
            this.Btn_Borrar_Servicios.Location = new System.Drawing.Point(212, 373);
            this.Btn_Borrar_Servicios.Name = "Btn_Borrar_Servicios";
            this.Btn_Borrar_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_Borrar_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Borrar_Servicios.TabIndex = 82;
            this.Btn_Borrar_Servicios.TabStop = false;
            this.Btn_Borrar_Servicios.Zoom = 10;
            // 
            // Btn_Coma_Servicios
            // 
            this.Btn_Coma_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Coma_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Coma_Servicios.Image")));
            this.Btn_Coma_Servicios.ImageActive = null;
            this.Btn_Coma_Servicios.Location = new System.Drawing.Point(24, 373);
            this.Btn_Coma_Servicios.Name = "Btn_Coma_Servicios";
            this.Btn_Coma_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_Coma_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Coma_Servicios.TabIndex = 81;
            this.Btn_Coma_Servicios.TabStop = false;
            this.Btn_Coma_Servicios.Zoom = 10;
            // 
            // Btn_N0_Servicios
            // 
            this.Btn_N0_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N0_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N0_Servicios.Image")));
            this.Btn_N0_Servicios.ImageActive = null;
            this.Btn_N0_Servicios.Location = new System.Drawing.Point(118, 373);
            this.Btn_N0_Servicios.Name = "Btn_N0_Servicios";
            this.Btn_N0_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N0_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N0_Servicios.TabIndex = 80;
            this.Btn_N0_Servicios.TabStop = false;
            this.Btn_N0_Servicios.Zoom = 10;
            // 
            // Btn_N9_Servicios
            // 
            this.Btn_N9_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N9_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N9_Servicios.Image")));
            this.Btn_N9_Servicios.ImageActive = null;
            this.Btn_N9_Servicios.Location = new System.Drawing.Point(212, 280);
            this.Btn_N9_Servicios.Name = "Btn_N9_Servicios";
            this.Btn_N9_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N9_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N9_Servicios.TabIndex = 79;
            this.Btn_N9_Servicios.TabStop = false;
            this.Btn_N9_Servicios.Zoom = 10;
            // 
            // Btn_N8_Servicios
            // 
            this.Btn_N8_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N8_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N8_Servicios.Image")));
            this.Btn_N8_Servicios.ImageActive = null;
            this.Btn_N8_Servicios.Location = new System.Drawing.Point(118, 280);
            this.Btn_N8_Servicios.Name = "Btn_N8_Servicios";
            this.Btn_N8_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N8_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N8_Servicios.TabIndex = 78;
            this.Btn_N8_Servicios.TabStop = false;
            this.Btn_N8_Servicios.Zoom = 10;
            // 
            // Btn_N7_Servicios
            // 
            this.Btn_N7_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N7_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N7_Servicios.Image")));
            this.Btn_N7_Servicios.ImageActive = null;
            this.Btn_N7_Servicios.Location = new System.Drawing.Point(24, 280);
            this.Btn_N7_Servicios.Name = "Btn_N7_Servicios";
            this.Btn_N7_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N7_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N7_Servicios.TabIndex = 77;
            this.Btn_N7_Servicios.TabStop = false;
            this.Btn_N7_Servicios.Zoom = 10;
            // 
            // Btn_N6_Servicios
            // 
            this.Btn_N6_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N6_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N6_Servicios.Image")));
            this.Btn_N6_Servicios.ImageActive = null;
            this.Btn_N6_Servicios.Location = new System.Drawing.Point(212, 187);
            this.Btn_N6_Servicios.Name = "Btn_N6_Servicios";
            this.Btn_N6_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N6_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N6_Servicios.TabIndex = 76;
            this.Btn_N6_Servicios.TabStop = false;
            this.Btn_N6_Servicios.Zoom = 10;
            // 
            // Btn_N5_Servicios
            // 
            this.Btn_N5_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N5_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N5_Servicios.Image")));
            this.Btn_N5_Servicios.ImageActive = null;
            this.Btn_N5_Servicios.Location = new System.Drawing.Point(118, 187);
            this.Btn_N5_Servicios.Name = "Btn_N5_Servicios";
            this.Btn_N5_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N5_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N5_Servicios.TabIndex = 75;
            this.Btn_N5_Servicios.TabStop = false;
            this.Btn_N5_Servicios.Zoom = 10;
            // 
            // Btn_N4_Servicios
            // 
            this.Btn_N4_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N4_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N4_Servicios.Image")));
            this.Btn_N4_Servicios.ImageActive = null;
            this.Btn_N4_Servicios.Location = new System.Drawing.Point(24, 187);
            this.Btn_N4_Servicios.Name = "Btn_N4_Servicios";
            this.Btn_N4_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N4_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N4_Servicios.TabIndex = 74;
            this.Btn_N4_Servicios.TabStop = false;
            this.Btn_N4_Servicios.Zoom = 10;
            // 
            // Btn_N3_Servicios
            // 
            this.Btn_N3_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N3_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N3_Servicios.Image")));
            this.Btn_N3_Servicios.ImageActive = null;
            this.Btn_N3_Servicios.Location = new System.Drawing.Point(212, 94);
            this.Btn_N3_Servicios.Name = "Btn_N3_Servicios";
            this.Btn_N3_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N3_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N3_Servicios.TabIndex = 73;
            this.Btn_N3_Servicios.TabStop = false;
            this.Btn_N3_Servicios.Zoom = 10;
            // 
            // Btn_N2_Servicios
            // 
            this.Btn_N2_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N2_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N2_Servicios.Image")));
            this.Btn_N2_Servicios.ImageActive = null;
            this.Btn_N2_Servicios.Location = new System.Drawing.Point(118, 94);
            this.Btn_N2_Servicios.Name = "Btn_N2_Servicios";
            this.Btn_N2_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N2_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N2_Servicios.TabIndex = 72;
            this.Btn_N2_Servicios.TabStop = false;
            this.Btn_N2_Servicios.Zoom = 10;
            // 
            // Btn_N1_Servicios
            // 
            this.Btn_N1_Servicios.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N1_Servicios.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N1_Servicios.Image")));
            this.Btn_N1_Servicios.ImageActive = null;
            this.Btn_N1_Servicios.Location = new System.Drawing.Point(24, 94);
            this.Btn_N1_Servicios.Name = "Btn_N1_Servicios";
            this.Btn_N1_Servicios.Size = new System.Drawing.Size(94, 93);
            this.Btn_N1_Servicios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N1_Servicios.TabIndex = 71;
            this.Btn_N1_Servicios.TabStop = false;
            this.Btn_N1_Servicios.Zoom = 10;
            // 
            // Btn_Res_Desc_Servicios
            // 
            this.Btn_Res_Desc_Servicios.AutoSize = true;
            this.Btn_Res_Desc_Servicios.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Res_Desc_Servicios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Res_Desc_Servicios.Location = new System.Drawing.Point(450, 373);
            this.Btn_Res_Desc_Servicios.Name = "Btn_Res_Desc_Servicios";
            this.Btn_Res_Desc_Servicios.Size = new System.Drawing.Size(79, 22);
            this.Btn_Res_Desc_Servicios.TabIndex = 70;
            this.Btn_Res_Desc_Servicios.Text = "TOTAL";
            // 
            // Btn_Desc_Servicios
            // 
            this.Btn_Desc_Servicios.AutoSize = true;
            this.Btn_Desc_Servicios.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Desc_Servicios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Desc_Servicios.Location = new System.Drawing.Point(312, 373);
            this.Btn_Desc_Servicios.Name = "Btn_Desc_Servicios";
            this.Btn_Desc_Servicios.Size = new System.Drawing.Size(140, 22);
            this.Btn_Desc_Servicios.TabIndex = 69;
            this.Btn_Desc_Servicios.Text = "DESCUENTO :";
            // 
            // Btn_Res_SubT_Servicios
            // 
            this.Btn_Res_SubT_Servicios.AutoSize = true;
            this.Btn_Res_SubT_Servicios.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Res_SubT_Servicios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Res_SubT_Servicios.Location = new System.Drawing.Point(450, 332);
            this.Btn_Res_SubT_Servicios.Name = "Btn_Res_SubT_Servicios";
            this.Btn_Res_SubT_Servicios.Size = new System.Drawing.Size(79, 22);
            this.Btn_Res_SubT_Servicios.TabIndex = 68;
            this.Btn_Res_SubT_Servicios.Text = "TOTAL";
            // 
            // Btn_SubT_Servicios
            // 
            this.Btn_SubT_Servicios.AutoSize = true;
            this.Btn_SubT_Servicios.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_SubT_Servicios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_SubT_Servicios.Location = new System.Drawing.Point(312, 332);
            this.Btn_SubT_Servicios.Name = "Btn_SubT_Servicios";
            this.Btn_SubT_Servicios.Size = new System.Drawing.Size(132, 22);
            this.Btn_SubT_Servicios.TabIndex = 67;
            this.Btn_SubT_Servicios.Text = "SUB TOTAL :";
            // 
            // Btn_Crédito_Servicios
            // 
            this.Btn_Crédito_Servicios.ActiveBorderThickness = 2;
            this.Btn_Crédito_Servicios.ActiveCornerRadius = 30;
            this.Btn_Crédito_Servicios.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Crédito_Servicios.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Servicios.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Servicios.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Servicios.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Crédito_Servicios.BackgroundImage")));
            this.Btn_Crédito_Servicios.ButtonText = "Crédito";
            this.Btn_Crédito_Servicios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Crédito_Servicios.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Crédito_Servicios.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Servicios.IdleBorderThickness = 2;
            this.Btn_Crédito_Servicios.IdleCornerRadius = 20;
            this.Btn_Crédito_Servicios.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Crédito_Servicios.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Servicios.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Servicios.Location = new System.Drawing.Point(243, 479);
            this.Btn_Crédito_Servicios.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Crédito_Servicios.Name = "Btn_Crédito_Servicios";
            this.Btn_Crédito_Servicios.Size = new System.Drawing.Size(196, 59);
            this.Btn_Crédito_Servicios.TabIndex = 23;
            this.Btn_Crédito_Servicios.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Efectivo_Servicios
            // 
            this.Btn_Efectivo_Servicios.ActiveBorderThickness = 2;
            this.Btn_Efectivo_Servicios.ActiveCornerRadius = 30;
            this.Btn_Efectivo_Servicios.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Efectivo_Servicios.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Servicios.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Servicios.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Servicios.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Efectivo_Servicios.BackgroundImage")));
            this.Btn_Efectivo_Servicios.ButtonText = "Efectivo";
            this.Btn_Efectivo_Servicios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Efectivo_Servicios.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Efectivo_Servicios.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Servicios.IdleBorderThickness = 2;
            this.Btn_Efectivo_Servicios.IdleCornerRadius = 20;
            this.Btn_Efectivo_Servicios.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Efectivo_Servicios.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Servicios.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Servicios.Location = new System.Drawing.Point(24, 479);
            this.Btn_Efectivo_Servicios.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Efectivo_Servicios.Name = "Btn_Efectivo_Servicios";
            this.Btn_Efectivo_Servicios.Size = new System.Drawing.Size(196, 59);
            this.Btn_Efectivo_Servicios.TabIndex = 21;
            this.Btn_Efectivo_Servicios.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_ResT_Servicios
            // 
            this.Btn_ResT_Servicios.AutoSize = true;
            this.Btn_ResT_Servicios.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_ResT_Servicios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_ResT_Servicios.Location = new System.Drawing.Point(202, 22);
            this.Btn_ResT_Servicios.Name = "Btn_ResT_Servicios";
            this.Btn_ResT_Servicios.Size = new System.Drawing.Size(157, 43);
            this.Btn_ResT_Servicios.TabIndex = 66;
            this.Btn_ResT_Servicios.Text = "TOTAL";
            // 
            // Btb_Cantidad_Servicios
            // 
            this.Btb_Cantidad_Servicios.ActiveBorderThickness = 2;
            this.Btb_Cantidad_Servicios.ActiveCornerRadius = 30;
            this.Btb_Cantidad_Servicios.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btb_Cantidad_Servicios.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Servicios.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Servicios.BackColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Servicios.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btb_Cantidad_Servicios.BackgroundImage")));
            this.Btb_Cantidad_Servicios.ButtonText = "Cantidad";
            this.Btb_Cantidad_Servicios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btb_Cantidad_Servicios.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btb_Cantidad_Servicios.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Servicios.IdleBorderThickness = 2;
            this.Btb_Cantidad_Servicios.IdleCornerRadius = 20;
            this.Btb_Cantidad_Servicios.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btb_Cantidad_Servicios.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Servicios.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Servicios.Location = new System.Drawing.Point(347, 128);
            this.Btb_Cantidad_Servicios.Margin = new System.Windows.Forms.Padding(5);
            this.Btb_Cantidad_Servicios.Name = "Btb_Cantidad_Servicios";
            this.Btb_Cantidad_Servicios.Size = new System.Drawing.Size(196, 59);
            this.Btb_Cantidad_Servicios.TabIndex = 22;
            this.Btb_Cantidad_Servicios.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Total_Servicios
            // 
            this.Btn_Total_Servicios.AutoSize = true;
            this.Btn_Total_Servicios.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Total_Servicios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Total_Servicios.Location = new System.Drawing.Point(16, 22);
            this.Btn_Total_Servicios.Name = "Btn_Total_Servicios";
            this.Btn_Total_Servicios.Size = new System.Drawing.Size(180, 43);
            this.Btn_Total_Servicios.TabIndex = 19;
            this.Btn_Total_Servicios.Text = "TOTAL :";
            // 
            // Btn_Des_Servicios
            // 
            this.Btn_Des_Servicios.ActiveBorderThickness = 2;
            this.Btn_Des_Servicios.ActiveCornerRadius = 30;
            this.Btn_Des_Servicios.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Des_Servicios.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Servicios.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Servicios.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Servicios.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Des_Servicios.BackgroundImage")));
            this.Btn_Des_Servicios.ButtonText = "Descuento";
            this.Btn_Des_Servicios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Des_Servicios.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Des_Servicios.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Servicios.IdleBorderThickness = 2;
            this.Btn_Des_Servicios.IdleCornerRadius = 20;
            this.Btn_Des_Servicios.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Des_Servicios.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Servicios.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Servicios.Location = new System.Drawing.Point(347, 197);
            this.Btn_Des_Servicios.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Des_Servicios.Name = "Btn_Des_Servicios";
            this.Btn_Des_Servicios.Size = new System.Drawing.Size(196, 59);
            this.Btn_Des_Servicios.TabIndex = 20;
            this.Btn_Des_Servicios.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuRadioButton1
            // 
            this.bunifuRadioButton1.BorderThickness = 1;
            this.bunifuRadioButton1.Checked = true;
            this.bunifuRadioButton1.Location = new System.Drawing.Point(-19, -19);
            this.bunifuRadioButton1.Name = "bunifuRadioButton1";
            this.bunifuRadioButton1.OutlineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuRadioButton1.OutlineColorUnchecked = System.Drawing.Color.Gray;
            this.bunifuRadioButton1.RadioColor = System.Drawing.Color.DodgerBlue;
            this.bunifuRadioButton1.Size = new System.Drawing.Size(21, 21);
            this.bunifuRadioButton1.TabIndex = 76;
            this.bunifuRadioButton1.Text = null;
            // 
            // Lb_Movistar_SerRec
            // 
            this.Lb_Movistar_SerRec.AutoSize = true;
            this.Lb_Movistar_SerRec.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Movistar_SerRec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Movistar_SerRec.Location = new System.Drawing.Point(197, 97);
            this.Lb_Movistar_SerRec.Name = "Lb_Movistar_SerRec";
            this.Lb_Movistar_SerRec.Size = new System.Drawing.Size(119, 27);
            this.Lb_Movistar_SerRec.TabIndex = 78;
            this.Lb_Movistar_SerRec.Text = "Movistar";
            // 
            // Lb_Claro_SerRec
            // 
            this.Lb_Claro_SerRec.AutoSize = true;
            this.Lb_Claro_SerRec.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Claro_SerRec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Claro_SerRec.Location = new System.Drawing.Point(467, 99);
            this.Lb_Claro_SerRec.Name = "Lb_Claro_SerRec";
            this.Lb_Claro_SerRec.Size = new System.Drawing.Size(76, 27);
            this.Lb_Claro_SerRec.TabIndex = 80;
            this.Lb_Claro_SerRec.Text = "Claro";
            // 
            // Lb_Recargas_SerRec
            // 
            this.Lb_Recargas_SerRec.AutoSize = true;
            this.Lb_Recargas_SerRec.Font = new System.Drawing.Font("Lucida Bright", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Recargas_SerRec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Recargas_SerRec.Location = new System.Drawing.Point(37, 31);
            this.Lb_Recargas_SerRec.Name = "Lb_Recargas_SerRec";
            this.Lb_Recargas_SerRec.Size = new System.Drawing.Size(149, 37);
            this.Lb_Recargas_SerRec.TabIndex = 81;
            this.Lb_Recargas_SerRec.Text = "Recarga";
            // 
            // RB_Claro_SerRec
            // 
            this.RB_Claro_SerRec.BackColor = System.Drawing.Color.Transparent;
            this.RB_Claro_SerRec.BorderThickness = 1;
            this.RB_Claro_SerRec.Checked = false;
            this.RB_Claro_SerRec.Location = new System.Drawing.Point(393, 99);
            this.RB_Claro_SerRec.Name = "RB_Claro_SerRec";
            this.RB_Claro_SerRec.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.RB_Claro_SerRec.OutlineColorUnchecked = System.Drawing.Color.Teal;
            this.RB_Claro_SerRec.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.RB_Claro_SerRec.Size = new System.Drawing.Size(29, 29);
            this.RB_Claro_SerRec.TabIndex = 82;
            this.RB_Claro_SerRec.Text = null;
            // 
            // RB_Movistar_SerRec
            // 
            this.RB_Movistar_SerRec.BackColor = System.Drawing.Color.Transparent;
            this.RB_Movistar_SerRec.BorderThickness = 1;
            this.RB_Movistar_SerRec.Checked = false;
            this.RB_Movistar_SerRec.Location = new System.Drawing.Point(112, 99);
            this.RB_Movistar_SerRec.Name = "RB_Movistar_SerRec";
            this.RB_Movistar_SerRec.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.RB_Movistar_SerRec.OutlineColorUnchecked = System.Drawing.Color.Teal;
            this.RB_Movistar_SerRec.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.RB_Movistar_SerRec.Size = new System.Drawing.Size(29, 29);
            this.RB_Movistar_SerRec.TabIndex = 83;
            this.RB_Movistar_SerRec.Text = null;
            // 
            // PB_Claro_SerRec
            // 
            this.PB_Claro_SerRec.Image = ((System.Drawing.Image)(resources.GetObject("PB_Claro_SerRec.Image")));
            this.PB_Claro_SerRec.Location = new System.Drawing.Point(424, 93);
            this.PB_Claro_SerRec.Name = "PB_Claro_SerRec";
            this.PB_Claro_SerRec.Size = new System.Drawing.Size(40, 38);
            this.PB_Claro_SerRec.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_Claro_SerRec.TabIndex = 84;
            this.PB_Claro_SerRec.TabStop = false;
            // 
            // PB_Monistar_SerRec
            // 
            this.PB_Monistar_SerRec.Image = ((System.Drawing.Image)(resources.GetObject("PB_Monistar_SerRec.Image")));
            this.PB_Monistar_SerRec.Location = new System.Drawing.Point(140, 87);
            this.PB_Monistar_SerRec.Name = "PB_Monistar_SerRec";
            this.PB_Monistar_SerRec.Size = new System.Drawing.Size(66, 50);
            this.PB_Monistar_SerRec.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_Monistar_SerRec.TabIndex = 85;
            this.PB_Monistar_SerRec.TabStop = false;
            // 
            // Servicios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(662, 756);
            this.Controls.Add(this.Lb_Movistar_SerRec);
            this.Controls.Add(this.PB_Monistar_SerRec);
            this.Controls.Add(this.PB_Claro_SerRec);
            this.Controls.Add(this.RB_Movistar_SerRec);
            this.Controls.Add(this.RB_Claro_SerRec);
            this.Controls.Add(this.Lb_Recargas_SerRec);
            this.Controls.Add(this.Lb_Claro_SerRec);
            this.Controls.Add(this.bunifuRadioButton1);
            this.Controls.Add(this.Panel_TeclaN_Servicios);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Servicios";
            this.Text = "Servicios";
            this.Panel_TeclaN_Servicios.ResumeLayout(false);
            this.Panel_TeclaN_Servicios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Claro_SerRec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Monistar_SerRec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel Panel_TeclaN_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Borrar_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Coma_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N0_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N9_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N8_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N7_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N6_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N5_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N4_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N3_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N2_Servicios;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N1_Servicios;
        private Guna.UI.WinForms.GunaLabel Btn_Res_Desc_Servicios;
        private Guna.UI.WinForms.GunaLabel Btn_Desc_Servicios;
        private Guna.UI.WinForms.GunaLabel Btn_Res_SubT_Servicios;
        private Guna.UI.WinForms.GunaLabel Btn_SubT_Servicios;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Crédito_Servicios;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Efectivo_Servicios;
        private Guna.UI.WinForms.GunaLabel Btn_ResT_Servicios;
        private Bunifu.Framework.UI.BunifuThinButton2 Btb_Cantidad_Servicios;
        private Guna.UI.WinForms.GunaLabel Btn_Total_Servicios;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Des_Servicios;
        private Bunifu.UI.WinForms.BunifuRadioButton bunifuRadioButton1;
        private Guna.UI.WinForms.GunaLabel Lb_Movistar_SerRec;
        private Guna.UI.WinForms.GunaLabel Lb_Claro_SerRec;
        private Guna.UI.WinForms.GunaLabel Lb_Recargas_SerRec;
        private Bunifu.UI.WinForms.BunifuRadioButton RB_Claro_SerRec;
        private Bunifu.UI.WinForms.BunifuRadioButton RB_Movistar_SerRec;
        private System.Windows.Forms.PictureBox PB_Claro_SerRec;
        private System.Windows.Forms.PictureBox PB_Monistar_SerRec;
    }
}