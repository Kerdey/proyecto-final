﻿namespace CONVO_II.Presentaciones
{
    partial class Proveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Proveedor));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties9 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties10 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties11 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties12 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            this.Lb_Cancelar_Prove = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Cancelar_Prove = new Bunifu.Framework.UI.BunifuImageButton();
            this.Lb_Editar_Prove = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Nuevo_Prove = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Buscar_Prove = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Eliminar_Prove = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Guardar_Prove = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Nuevo_Prove = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Eliminar_Prove = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Editar_Prove = new Bunifu.Framework.UI.BunifuImageButton();
            this.Lb_ListProveedor_Prove = new Guna.UI.WinForms.GunaLabel();
            this.Txt_Buscar_Prove = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Tabla_Proveedor = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Prove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Prove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Prove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Prove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Prove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Proveedor)).BeginInit();
            this.SuspendLayout();
            // 
            // Lb_Cancelar_Prove
            // 
            this.Lb_Cancelar_Prove.AutoSize = true;
            this.Lb_Cancelar_Prove.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Cancelar_Prove.Location = new System.Drawing.Point(947, 95);
            this.Lb_Cancelar_Prove.Name = "Lb_Cancelar_Prove";
            this.Lb_Cancelar_Prove.Size = new System.Drawing.Size(95, 22);
            this.Lb_Cancelar_Prove.TabIndex = 105;
            this.Lb_Cancelar_Prove.Text = "Cancelar";
            // 
            // Btn_Cancelar_Prove
            // 
            this.Btn_Cancelar_Prove.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Cancelar_Prove.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Cancelar_Prove.Image")));
            this.Btn_Cancelar_Prove.ImageActive = null;
            this.Btn_Cancelar_Prove.Location = new System.Drawing.Point(964, 31);
            this.Btn_Cancelar_Prove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Cancelar_Prove.Name = "Btn_Cancelar_Prove";
            this.Btn_Cancelar_Prove.Size = new System.Drawing.Size(61, 60);
            this.Btn_Cancelar_Prove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Cancelar_Prove.TabIndex = 104;
            this.Btn_Cancelar_Prove.TabStop = false;
            this.Btn_Cancelar_Prove.Zoom = 10;
            // 
            // Lb_Editar_Prove
            // 
            this.Lb_Editar_Prove.AutoSize = true;
            this.Lb_Editar_Prove.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Editar_Prove.Location = new System.Drawing.Point(769, 95);
            this.Lb_Editar_Prove.Name = "Lb_Editar_Prove";
            this.Lb_Editar_Prove.Size = new System.Drawing.Size(67, 22);
            this.Lb_Editar_Prove.TabIndex = 103;
            this.Lb_Editar_Prove.Text = "Editar";
            // 
            // Lb_Nuevo_Prove
            // 
            this.Lb_Nuevo_Prove.AutoSize = true;
            this.Lb_Nuevo_Prove.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nuevo_Prove.Location = new System.Drawing.Point(675, 95);
            this.Lb_Nuevo_Prove.Name = "Lb_Nuevo_Prove";
            this.Lb_Nuevo_Prove.Size = new System.Drawing.Size(73, 22);
            this.Lb_Nuevo_Prove.TabIndex = 102;
            this.Lb_Nuevo_Prove.Text = "Nuevo";
            // 
            // Lb_Buscar_Prove
            // 
            this.Lb_Buscar_Prove.AutoSize = true;
            this.Lb_Buscar_Prove.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Buscar_Prove.Location = new System.Drawing.Point(431, 95);
            this.Lb_Buscar_Prove.Name = "Lb_Buscar_Prove";
            this.Lb_Buscar_Prove.Size = new System.Drawing.Size(75, 22);
            this.Lb_Buscar_Prove.TabIndex = 101;
            this.Lb_Buscar_Prove.Text = "Buscar";
            // 
            // Lb_Eliminar_Prove
            // 
            this.Lb_Eliminar_Prove.AutoSize = true;
            this.Lb_Eliminar_Prove.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Eliminar_Prove.Location = new System.Drawing.Point(851, 95);
            this.Lb_Eliminar_Prove.Name = "Lb_Eliminar_Prove";
            this.Lb_Eliminar_Prove.Size = new System.Drawing.Size(88, 22);
            this.Lb_Eliminar_Prove.TabIndex = 100;
            this.Lb_Eliminar_Prove.Text = "Eliminar";
            // 
            // Btn_Guardar_Prove
            // 
            this.Btn_Guardar_Prove.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Guardar_Prove.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Guardar_Prove.Image")));
            this.Btn_Guardar_Prove.ImageActive = null;
            this.Btn_Guardar_Prove.Location = new System.Drawing.Point(439, 31);
            this.Btn_Guardar_Prove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Guardar_Prove.Name = "Btn_Guardar_Prove";
            this.Btn_Guardar_Prove.Size = new System.Drawing.Size(61, 60);
            this.Btn_Guardar_Prove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Guardar_Prove.TabIndex = 99;
            this.Btn_Guardar_Prove.TabStop = false;
            this.Btn_Guardar_Prove.Zoom = 10;
            // 
            // Btn_Nuevo_Prove
            // 
            this.Btn_Nuevo_Prove.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Nuevo_Prove.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Nuevo_Prove.Image")));
            this.Btn_Nuevo_Prove.ImageActive = null;
            this.Btn_Nuevo_Prove.Location = new System.Drawing.Point(681, 31);
            this.Btn_Nuevo_Prove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Nuevo_Prove.Name = "Btn_Nuevo_Prove";
            this.Btn_Nuevo_Prove.Size = new System.Drawing.Size(61, 60);
            this.Btn_Nuevo_Prove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Nuevo_Prove.TabIndex = 98;
            this.Btn_Nuevo_Prove.TabStop = false;
            this.Btn_Nuevo_Prove.Zoom = 10;
            // 
            // Btn_Eliminar_Prove
            // 
            this.Btn_Eliminar_Prove.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Eliminar_Prove.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar_Prove.Image")));
            this.Btn_Eliminar_Prove.ImageActive = null;
            this.Btn_Eliminar_Prove.Location = new System.Drawing.Point(863, 31);
            this.Btn_Eliminar_Prove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Eliminar_Prove.Name = "Btn_Eliminar_Prove";
            this.Btn_Eliminar_Prove.Size = new System.Drawing.Size(61, 60);
            this.Btn_Eliminar_Prove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Eliminar_Prove.TabIndex = 97;
            this.Btn_Eliminar_Prove.TabStop = false;
            this.Btn_Eliminar_Prove.Zoom = 10;
            // 
            // Btn_Editar_Prove
            // 
            this.Btn_Editar_Prove.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Editar_Prove.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Editar_Prove.Image")));
            this.Btn_Editar_Prove.ImageActive = null;
            this.Btn_Editar_Prove.Location = new System.Drawing.Point(772, 31);
            this.Btn_Editar_Prove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Editar_Prove.Name = "Btn_Editar_Prove";
            this.Btn_Editar_Prove.Size = new System.Drawing.Size(61, 60);
            this.Btn_Editar_Prove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Editar_Prove.TabIndex = 96;
            this.Btn_Editar_Prove.TabStop = false;
            this.Btn_Editar_Prove.Zoom = 10;
            // 
            // Lb_ListProveedor_Prove
            // 
            this.Lb_ListProveedor_Prove.AutoSize = true;
            this.Lb_ListProveedor_Prove.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_ListProveedor_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_ListProveedor_Prove.Location = new System.Drawing.Point(39, 155);
            this.Lb_ListProveedor_Prove.Name = "Lb_ListProveedor_Prove";
            this.Lb_ListProveedor_Prove.Size = new System.Drawing.Size(266, 27);
            this.Lb_ListProveedor_Prove.TabIndex = 95;
            this.Lb_ListProveedor_Prove.Text = "Lista de Proveedores";
            // 
            // Txt_Buscar_Prove
            // 
            this.Txt_Buscar_Prove.AcceptsReturn = false;
            this.Txt_Buscar_Prove.AcceptsTab = false;
            this.Txt_Buscar_Prove.AnimationSpeed = 200;
            this.Txt_Buscar_Prove.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Buscar_Prove.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Buscar_Prove.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Buscar_Prove.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Prove.BackgroundImage")));
            this.Txt_Buscar_Prove.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Prove.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Buscar_Prove.BorderColorHover = System.Drawing.Color.DimGray;
            this.Txt_Buscar_Prove.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Prove.BorderRadius = 9;
            this.Txt_Buscar_Prove.BorderThickness = 3;
            this.Txt_Buscar_Prove.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Buscar_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Prove.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buscar_Prove.DefaultText = "";
            this.Txt_Buscar_Prove.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Buscar_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Prove.HideSelection = true;
            this.Txt_Buscar_Prove.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Prove.IconLeft")));
            this.Txt_Buscar_Prove.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Prove.IconPadding = 10;
            this.Txt_Buscar_Prove.IconRight = null;
            this.Txt_Buscar_Prove.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Prove.Lines = new string[0];
            this.Txt_Buscar_Prove.Location = new System.Drawing.Point(44, 38);
            this.Txt_Buscar_Prove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txt_Buscar_Prove.MaxLength = 32767;
            this.Txt_Buscar_Prove.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Buscar_Prove.Modified = false;
            this.Txt_Buscar_Prove.Multiline = false;
            this.Txt_Buscar_Prove.Name = "Txt_Buscar_Prove";
            stateProperties9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties9.FillColor = System.Drawing.Color.Empty;
            stateProperties9.ForeColor = System.Drawing.Color.Empty;
            stateProperties9.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Prove.OnActiveState = stateProperties9;
            stateProperties10.BorderColor = System.Drawing.Color.Empty;
            stateProperties10.FillColor = System.Drawing.Color.White;
            stateProperties10.ForeColor = System.Drawing.Color.Empty;
            stateProperties10.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Prove.OnDisabledState = stateProperties10;
            stateProperties11.BorderColor = System.Drawing.Color.DimGray;
            stateProperties11.FillColor = System.Drawing.Color.Empty;
            stateProperties11.ForeColor = System.Drawing.Color.Empty;
            stateProperties11.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Prove.OnHoverState = stateProperties11;
            stateProperties12.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties12.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties12.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Prove.OnIdleState = stateProperties12;
            this.Txt_Buscar_Prove.PasswordChar = '\0';
            this.Txt_Buscar_Prove.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Prove.PlaceholderText = "Buscar";
            this.Txt_Buscar_Prove.ReadOnly = false;
            this.Txt_Buscar_Prove.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Buscar_Prove.SelectedText = "";
            this.Txt_Buscar_Prove.SelectionLength = 0;
            this.Txt_Buscar_Prove.SelectionStart = 0;
            this.Txt_Buscar_Prove.ShortcutsEnabled = true;
            this.Txt_Buscar_Prove.Size = new System.Drawing.Size(357, 52);
            this.Txt_Buscar_Prove.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Buscar_Prove.TabIndex = 94;
            this.Txt_Buscar_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Buscar_Prove.TextMarginBottom = 0;
            this.Txt_Buscar_Prove.TextMarginLeft = 5;
            this.Txt_Buscar_Prove.TextMarginTop = 0;
            this.Txt_Buscar_Prove.TextPlaceholder = "Buscar";
            this.Txt_Buscar_Prove.UseSystemPasswordChar = false;
            this.Txt_Buscar_Prove.WordWrap = true;
            // 
            // Tabla_Proveedor
            // 
            this.Tabla_Proveedor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.Tabla_Proveedor.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Tabla_Proveedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Tabla_Proveedor.Location = new System.Drawing.Point(44, 221);
            this.Tabla_Proveedor.Name = "Tabla_Proveedor";
            this.Tabla_Proveedor.RowHeadersWidth = 51;
            this.Tabla_Proveedor.RowTemplate.Height = 24;
            this.Tabla_Proveedor.Size = new System.Drawing.Size(1075, 414);
            this.Tabla_Proveedor.TabIndex = 106;
            // 
            // Proveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1167, 668);
            this.Controls.Add(this.Tabla_Proveedor);
            this.Controls.Add(this.Lb_Cancelar_Prove);
            this.Controls.Add(this.Btn_Cancelar_Prove);
            this.Controls.Add(this.Lb_Editar_Prove);
            this.Controls.Add(this.Lb_Nuevo_Prove);
            this.Controls.Add(this.Lb_Buscar_Prove);
            this.Controls.Add(this.Lb_Eliminar_Prove);
            this.Controls.Add(this.Btn_Guardar_Prove);
            this.Controls.Add(this.Btn_Nuevo_Prove);
            this.Controls.Add(this.Btn_Eliminar_Prove);
            this.Controls.Add(this.Btn_Editar_Prove);
            this.Controls.Add(this.Lb_ListProveedor_Prove);
            this.Controls.Add(this.Txt_Buscar_Prove);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Proveedor";
            this.Text = "Proveedor";
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Prove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Prove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Prove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Prove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Prove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Proveedor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI.WinForms.GunaLabel Lb_Cancelar_Prove;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Cancelar_Prove;
        private Guna.UI.WinForms.GunaLabel Lb_Editar_Prove;
        private Guna.UI.WinForms.GunaLabel Lb_Nuevo_Prove;
        private Guna.UI.WinForms.GunaLabel Lb_Buscar_Prove;
        private Guna.UI.WinForms.GunaLabel Lb_Eliminar_Prove;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Guardar_Prove;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Nuevo_Prove;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Eliminar_Prove;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Editar_Prove;
        private Guna.UI.WinForms.GunaLabel Lb_ListProveedor_Prove;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Buscar_Prove;
        private System.Windows.Forms.DataGridView Tabla_Proveedor;
    }
}