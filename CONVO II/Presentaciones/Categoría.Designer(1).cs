﻿namespace Pulpería_Lovo.Presentaciones
{
    partial class Categoría
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Categoría));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.P_Superior_Cate = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.Lb_Categoría = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.Salir = new System.Windows.Forms.PictureBox();
            this.Mov_C = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Cancelar_Cate = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton4 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton5 = new Bunifu.Framework.UI.BunifuImageButton();
            this.Txt_Buscar_Cate = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Tabla_Categoría = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.Lb_Cancelar_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Guardar_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Editar_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Nuevo_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Eliminar_Cate = new Guna.UI.WinForms.GunaLabel();
            this.P_Superior_Cate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Salir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Cate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Categoría)).BeginInit();
            this.SuspendLayout();
            // 
            // P_Superior_Cate
            // 
            this.P_Superior_Cate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("P_Superior_Cate.BackgroundImage")));
            this.P_Superior_Cate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.P_Superior_Cate.Controls.Add(this.Lb_Categoría);
            this.P_Superior_Cate.Controls.Add(this.Logo);
            this.P_Superior_Cate.Controls.Add(this.Salir);
            this.P_Superior_Cate.Dock = System.Windows.Forms.DockStyle.Top;
            this.P_Superior_Cate.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.P_Superior_Cate.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.P_Superior_Cate.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.P_Superior_Cate.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.P_Superior_Cate.Location = new System.Drawing.Point(0, 0);
            this.P_Superior_Cate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.P_Superior_Cate.Name = "P_Superior_Cate";
            this.P_Superior_Cate.Quality = 10;
            this.P_Superior_Cate.Size = new System.Drawing.Size(662, 41);
            this.P_Superior_Cate.TabIndex = 0;
            // 
            // Lb_Categoría
            // 
            this.Lb_Categoría.AutoSize = true;
            this.Lb_Categoría.BackColor = System.Drawing.Color.Transparent;
            this.Lb_Categoría.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Categoría.ForeColor = System.Drawing.Color.Gainsboro;
            this.Lb_Categoría.Location = new System.Drawing.Point(50, 14);
            this.Lb_Categoría.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Categoría.Name = "Lb_Categoría";
            this.Lb_Categoría.Size = new System.Drawing.Size(86, 18);
            this.Lb_Categoría.TabIndex = 10;
            this.Lb_Categoría.Text = "Categoría";
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(4, 1);
            this.Logo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(38, 39);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Logo.TabIndex = 9;
            this.Logo.TabStop = false;
            // 
            // Salir
            // 
            this.Salir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Salir.BackColor = System.Drawing.Color.Transparent;
            this.Salir.ErrorImage = null;
            this.Salir.Image = ((System.Drawing.Image)(resources.GetObject("Salir.Image")));
            this.Salir.Location = new System.Drawing.Point(626, 6);
            this.Salir.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Salir.Name = "Salir";
            this.Salir.Size = new System.Drawing.Size(22, 25);
            this.Salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Salir.TabIndex = 8;
            this.Salir.TabStop = false;
            this.Salir.Click += new System.EventHandler(this.Salir_Click);
            // 
            // Mov_C
            // 
            this.Mov_C.Fixed = true;
            this.Mov_C.Horizontal = true;
            this.Mov_C.TargetControl = this.P_Superior_Cate;
            this.Mov_C.Vertical = true;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(568, 172);
            this.bunifuImageButton1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(46, 49);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bunifuImageButton1.TabIndex = 24;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            // 
            // Btn_Cancelar_Cate
            // 
            this.Btn_Cancelar_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Cancelar_Cate.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Cancelar_Cate.Image")));
            this.Btn_Cancelar_Cate.ImageActive = null;
            this.Btn_Cancelar_Cate.Location = new System.Drawing.Point(568, 396);
            this.Btn_Cancelar_Cate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Cancelar_Cate.Name = "Btn_Cancelar_Cate";
            this.Btn_Cancelar_Cate.Size = new System.Drawing.Size(46, 49);
            this.Btn_Cancelar_Cate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Cancelar_Cate.TabIndex = 25;
            this.Btn_Cancelar_Cate.TabStop = false;
            this.Btn_Cancelar_Cate.Zoom = 10;
            this.Btn_Cancelar_Cate.Click += new System.EventHandler(this.Btn_Cancelar_Cate_Click);
            // 
            // bunifuImageButton3
            // 
            this.bunifuImageButton3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton3.Image")));
            this.bunifuImageButton3.ImageActive = null;
            this.bunifuImageButton3.Location = new System.Drawing.Point(568, 323);
            this.bunifuImageButton3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bunifuImageButton3.Name = "bunifuImageButton3";
            this.bunifuImageButton3.Size = new System.Drawing.Size(46, 49);
            this.bunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bunifuImageButton3.TabIndex = 26;
            this.bunifuImageButton3.TabStop = false;
            this.bunifuImageButton3.Zoom = 10;
            // 
            // bunifuImageButton4
            // 
            this.bunifuImageButton4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton4.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton4.Image")));
            this.bunifuImageButton4.ImageActive = null;
            this.bunifuImageButton4.Location = new System.Drawing.Point(568, 247);
            this.bunifuImageButton4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bunifuImageButton4.Name = "bunifuImageButton4";
            this.bunifuImageButton4.Size = new System.Drawing.Size(46, 49);
            this.bunifuImageButton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bunifuImageButton4.TabIndex = 27;
            this.bunifuImageButton4.TabStop = false;
            this.bunifuImageButton4.Zoom = 10;
            // 
            // bunifuImageButton5
            // 
            this.bunifuImageButton5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton5.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton5.Image")));
            this.bunifuImageButton5.ImageActive = null;
            this.bunifuImageButton5.Location = new System.Drawing.Point(568, 98);
            this.bunifuImageButton5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bunifuImageButton5.Name = "bunifuImageButton5";
            this.bunifuImageButton5.Size = new System.Drawing.Size(46, 49);
            this.bunifuImageButton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bunifuImageButton5.TabIndex = 28;
            this.bunifuImageButton5.TabStop = false;
            this.bunifuImageButton5.Zoom = 10;
            // 
            // Txt_Buscar_Cate
            // 
            this.Txt_Buscar_Cate.AcceptsReturn = false;
            this.Txt_Buscar_Cate.AcceptsTab = false;
            this.Txt_Buscar_Cate.AnimationSpeed = 200;
            this.Txt_Buscar_Cate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Buscar_Cate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Buscar_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Buscar_Cate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cate.BackgroundImage")));
            this.Txt_Buscar_Cate.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Buscar_Cate.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Buscar_Cate.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(151)))), ((int)(((byte)(158)))));
            this.Txt_Buscar_Cate.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Buscar_Cate.BorderRadius = 9;
            this.Txt_Buscar_Cate.BorderThickness = 3;
            this.Txt_Buscar_Cate.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Buscar_Cate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cate.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buscar_Cate.DefaultText = "";
            this.Txt_Buscar_Cate.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Buscar_Cate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Buscar_Cate.HideSelection = true;
            this.Txt_Buscar_Cate.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cate.IconLeft")));
            this.Txt_Buscar_Cate.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cate.IconPadding = 10;
            this.Txt_Buscar_Cate.IconRight = null;
            this.Txt_Buscar_Cate.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cate.Lines = new string[0];
            this.Txt_Buscar_Cate.Location = new System.Drawing.Point(22, 60);
            this.Txt_Buscar_Cate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Txt_Buscar_Cate.MaxLength = 32767;
            this.Txt_Buscar_Cate.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Buscar_Cate.Modified = false;
            this.Txt_Buscar_Cate.Multiline = false;
            this.Txt_Buscar_Cate.Name = "Txt_Buscar_Cate";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties1.FillColor = System.Drawing.Color.Empty;
            stateProperties1.ForeColor = System.Drawing.Color.Empty;
            stateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cate.OnActiveState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.Empty;
            stateProperties2.FillColor = System.Drawing.Color.White;
            stateProperties2.ForeColor = System.Drawing.Color.Empty;
            stateProperties2.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cate.OnDisabledState = stateProperties2;
            stateProperties3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(151)))), ((int)(((byte)(158)))));
            stateProperties3.FillColor = System.Drawing.Color.Empty;
            stateProperties3.ForeColor = System.Drawing.Color.Empty;
            stateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cate.OnHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties4.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cate.OnIdleState = stateProperties4;
            this.Txt_Buscar_Cate.PasswordChar = '\0';
            this.Txt_Buscar_Cate.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cate.PlaceholderText = "Buscar";
            this.Txt_Buscar_Cate.ReadOnly = false;
            this.Txt_Buscar_Cate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Buscar_Cate.SelectedText = "";
            this.Txt_Buscar_Cate.SelectionLength = 0;
            this.Txt_Buscar_Cate.SelectionStart = 0;
            this.Txt_Buscar_Cate.ShortcutsEnabled = true;
            this.Txt_Buscar_Cate.Size = new System.Drawing.Size(268, 42);
            this.Txt_Buscar_Cate.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Buscar_Cate.TabIndex = 35;
            this.Txt_Buscar_Cate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Buscar_Cate.TextMarginBottom = 0;
            this.Txt_Buscar_Cate.TextMarginLeft = 5;
            this.Txt_Buscar_Cate.TextMarginTop = 0;
            this.Txt_Buscar_Cate.TextPlaceholder = "Buscar";
            this.Txt_Buscar_Cate.UseSystemPasswordChar = false;
            this.Txt_Buscar_Cate.WordWrap = true;
            // 
            // Tabla_Categoría
            // 
            this.Tabla_Categoría.AllowCustomTheming = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Categoría.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Tabla_Categoría.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Tabla_Categoría.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Tabla_Categoría.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.Tabla_Categoría.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Tabla_Categoría.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Tabla_Categoría.ColumnHeadersHeight = 40;
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.Tabla_Categoría.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.Tabla_Categoría.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.SelectionForeColor = System.Drawing.Color.White;
            this.Tabla_Categoría.CurrentTheme.Name = null;
            this.Tabla_Categoría.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.Tabla_Categoría.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Categoría.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Categoría.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Tabla_Categoría.DefaultCellStyle = dataGridViewCellStyle3;
            this.Tabla_Categoría.EnableHeadersVisualStyles = false;
            this.Tabla_Categoría.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Categoría.HeaderBgColor = System.Drawing.Color.Empty;
            this.Tabla_Categoría.HeaderForeColor = System.Drawing.Color.White;
            this.Tabla_Categoría.Location = new System.Drawing.Point(22, 150);
            this.Tabla_Categoría.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Tabla_Categoría.Name = "Tabla_Categoría";
            this.Tabla_Categoría.RowHeadersVisible = false;
            this.Tabla_Categoría.RowHeadersWidth = 51;
            this.Tabla_Categoría.RowTemplate.Height = 40;
            this.Tabla_Categoría.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Tabla_Categoría.Size = new System.Drawing.Size(492, 306);
            this.Tabla_Categoría.TabIndex = 36;
            this.Tabla_Categoría.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            // 
            // Lb_Cancelar_Cate
            // 
            this.Lb_Cancelar_Cate.AutoSize = true;
            this.Lb_Cancelar_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Cancelar_Cate.Location = new System.Drawing.Point(555, 448);
            this.Lb_Cancelar_Cate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Cancelar_Cate.Name = "Lb_Cancelar_Cate";
            this.Lb_Cancelar_Cate.Size = new System.Drawing.Size(75, 17);
            this.Lb_Cancelar_Cate.TabIndex = 89;
            this.Lb_Cancelar_Cate.Text = "Cancelar";
            // 
            // Lb_Guardar_Cate
            // 
            this.Lb_Guardar_Cate.AutoSize = true;
            this.Lb_Guardar_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Guardar_Cate.Location = new System.Drawing.Point(558, 297);
            this.Lb_Guardar_Cate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Guardar_Cate.Name = "Lb_Guardar_Cate";
            this.Lb_Guardar_Cate.Size = new System.Drawing.Size(70, 17);
            this.Lb_Guardar_Cate.TabIndex = 88;
            this.Lb_Guardar_Cate.Text = "Guardar";
            // 
            // Lb_Editar_Cate
            // 
            this.Lb_Editar_Cate.AutoSize = true;
            this.Lb_Editar_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Editar_Cate.Location = new System.Drawing.Point(566, 223);
            this.Lb_Editar_Cate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Editar_Cate.Name = "Lb_Editar_Cate";
            this.Lb_Editar_Cate.Size = new System.Drawing.Size(53, 17);
            this.Lb_Editar_Cate.TabIndex = 94;
            this.Lb_Editar_Cate.Text = "Editar";
            // 
            // Lb_Nuevo_Cate
            // 
            this.Lb_Nuevo_Cate.AutoSize = true;
            this.Lb_Nuevo_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nuevo_Cate.Location = new System.Drawing.Point(563, 148);
            this.Lb_Nuevo_Cate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Nuevo_Cate.Name = "Lb_Nuevo_Cate";
            this.Lb_Nuevo_Cate.Size = new System.Drawing.Size(58, 17);
            this.Lb_Nuevo_Cate.TabIndex = 93;
            this.Lb_Nuevo_Cate.Text = "Nuevo";
            // 
            // Lb_Eliminar_Cate
            // 
            this.Lb_Eliminar_Cate.AutoSize = true;
            this.Lb_Eliminar_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Eliminar_Cate.Location = new System.Drawing.Point(560, 374);
            this.Lb_Eliminar_Cate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Eliminar_Cate.Name = "Lb_Eliminar_Cate";
            this.Lb_Eliminar_Cate.Size = new System.Drawing.Size(68, 17);
            this.Lb_Eliminar_Cate.TabIndex = 92;
            this.Lb_Eliminar_Cate.Text = "Eliminar";
            // 
            // Categoría
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(662, 503);
            this.Controls.Add(this.Lb_Editar_Cate);
            this.Controls.Add(this.Lb_Nuevo_Cate);
            this.Controls.Add(this.Lb_Eliminar_Cate);
            this.Controls.Add(this.Lb_Cancelar_Cate);
            this.Controls.Add(this.Lb_Guardar_Cate);
            this.Controls.Add(this.Tabla_Categoría);
            this.Controls.Add(this.Txt_Buscar_Cate);
            this.Controls.Add(this.bunifuImageButton5);
            this.Controls.Add(this.bunifuImageButton4);
            this.Controls.Add(this.bunifuImageButton3);
            this.Controls.Add(this.Btn_Cancelar_Cate);
            this.Controls.Add(this.bunifuImageButton1);
            this.Controls.Add(this.P_Superior_Cate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Categoría";
            this.Text = "Categoria";
            this.Load += new System.EventHandler(this.Categoría_Load);
            this.P_Superior_Cate.ResumeLayout(false);
            this.P_Superior_Cate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Salir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Cate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Categoría)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuGradientPanel P_Superior_Cate;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Categoría;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.PictureBox Salir;
        private Bunifu.Framework.UI.BunifuDragControl Mov_C;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Cancelar_Cate;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton3;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton4;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton5;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Buscar_Cate;
        private Bunifu.UI.WinForms.BunifuDataGridView Tabla_Categoría;
        private Guna.UI.WinForms.GunaLabel Lb_Cancelar_Cate;
        private Guna.UI.WinForms.GunaLabel Lb_Guardar_Cate;
        private Guna.UI.WinForms.GunaLabel Lb_Editar_Cate;
        private Guna.UI.WinForms.GunaLabel Lb_Nuevo_Cate;
        private Guna.UI.WinForms.GunaLabel Lb_Eliminar_Cate;
    }
}