﻿namespace CONVO_II.Presentaciones
{
    partial class Categoría
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Categoría));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.P_Superior_Cate = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.Lb_Categoría = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.Salir = new System.Windows.Forms.PictureBox();
            this.Mov_C = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.Btn_Editar_Cate = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Cancelar_Cate = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Eliminar_Cate = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Guardar_Cate = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Nuevo_Cate = new Bunifu.Framework.UI.BunifuImageButton();
            this.Txt_Buscar_Cate = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Tabla_Categoría = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.Lb_Cancelar_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Guardar_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Editar_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Nuevo_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Eliminar_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Lb_NomCate_Cate = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Txt_NomCate_Cate = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Buscar_Cate = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Buscar_Cate = new Bunifu.Framework.UI.BunifuImageButton();
            this.P_Superior_Cate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Salir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Cate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Cate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Cate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Cate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Cate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Categoría)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Buscar_Cate)).BeginInit();
            this.SuspendLayout();
            // 
            // P_Superior_Cate
            // 
            this.P_Superior_Cate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("P_Superior_Cate.BackgroundImage")));
            this.P_Superior_Cate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.P_Superior_Cate.Controls.Add(this.Lb_Categoría);
            this.P_Superior_Cate.Controls.Add(this.Logo);
            this.P_Superior_Cate.Controls.Add(this.Salir);
            this.P_Superior_Cate.Dock = System.Windows.Forms.DockStyle.Top;
            this.P_Superior_Cate.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.P_Superior_Cate.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.P_Superior_Cate.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.P_Superior_Cate.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.P_Superior_Cate.Location = new System.Drawing.Point(0, 0);
            this.P_Superior_Cate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.P_Superior_Cate.Name = "P_Superior_Cate";
            this.P_Superior_Cate.Quality = 10;
            this.P_Superior_Cate.Size = new System.Drawing.Size(976, 50);
            this.P_Superior_Cate.TabIndex = 0;
            // 
            // Lb_Categoría
            // 
            this.Lb_Categoría.AutoSize = true;
            this.Lb_Categoría.BackColor = System.Drawing.Color.Transparent;
            this.Lb_Categoría.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Categoría.ForeColor = System.Drawing.Color.Gainsboro;
            this.Lb_Categoría.Location = new System.Drawing.Point(67, 17);
            this.Lb_Categoría.Name = "Lb_Categoría";
            this.Lb_Categoría.Size = new System.Drawing.Size(106, 23);
            this.Lb_Categoría.TabIndex = 10;
            this.Lb_Categoría.Text = "Categoría";
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(5, 1);
            this.Logo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(51, 48);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Logo.TabIndex = 9;
            this.Logo.TabStop = false;
            // 
            // Salir
            // 
            this.Salir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Salir.BackColor = System.Drawing.Color.Transparent;
            this.Salir.ErrorImage = null;
            this.Salir.Image = ((System.Drawing.Image)(resources.GetObject("Salir.Image")));
            this.Salir.Location = new System.Drawing.Point(928, 7);
            this.Salir.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Salir.Name = "Salir";
            this.Salir.Size = new System.Drawing.Size(29, 31);
            this.Salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Salir.TabIndex = 8;
            this.Salir.TabStop = false;
            this.Salir.Click += new System.EventHandler(this.Salir_Click);
            // 
            // Mov_C
            // 
            this.Mov_C.Fixed = true;
            this.Mov_C.Horizontal = true;
            this.Mov_C.TargetControl = this.P_Superior_Cate;
            this.Mov_C.Vertical = true;
            // 
            // Btn_Editar_Cate
            // 
            this.Btn_Editar_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Editar_Cate.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Editar_Cate.Image")));
            this.Btn_Editar_Cate.ImageActive = null;
            this.Btn_Editar_Cate.Location = new System.Drawing.Point(688, 74);
            this.Btn_Editar_Cate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Editar_Cate.Name = "Btn_Editar_Cate";
            this.Btn_Editar_Cate.Size = new System.Drawing.Size(61, 60);
            this.Btn_Editar_Cate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Editar_Cate.TabIndex = 24;
            this.Btn_Editar_Cate.TabStop = false;
            this.Btn_Editar_Cate.Zoom = 10;
            // 
            // Btn_Cancelar_Cate
            // 
            this.Btn_Cancelar_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Cancelar_Cate.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Cancelar_Cate.Image")));
            this.Btn_Cancelar_Cate.ImageActive = null;
            this.Btn_Cancelar_Cate.Location = new System.Drawing.Point(870, 587);
            this.Btn_Cancelar_Cate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Cancelar_Cate.Name = "Btn_Cancelar_Cate";
            this.Btn_Cancelar_Cate.Size = new System.Drawing.Size(61, 60);
            this.Btn_Cancelar_Cate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Cancelar_Cate.TabIndex = 25;
            this.Btn_Cancelar_Cate.TabStop = false;
            this.Btn_Cancelar_Cate.Zoom = 10;
            this.Btn_Cancelar_Cate.Click += new System.EventHandler(this.Btn_Cancelar_Cate_Click);
            // 
            // Btn_Eliminar_Cate
            // 
            this.Btn_Eliminar_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Eliminar_Cate.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar_Cate.Image")));
            this.Btn_Eliminar_Cate.ImageActive = null;
            this.Btn_Eliminar_Cate.Location = new System.Drawing.Point(870, 74);
            this.Btn_Eliminar_Cate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Eliminar_Cate.Name = "Btn_Eliminar_Cate";
            this.Btn_Eliminar_Cate.Size = new System.Drawing.Size(61, 60);
            this.Btn_Eliminar_Cate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Eliminar_Cate.TabIndex = 26;
            this.Btn_Eliminar_Cate.TabStop = false;
            this.Btn_Eliminar_Cate.Zoom = 10;
            // 
            // Btn_Guardar_Cate
            // 
            this.Btn_Guardar_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Guardar_Cate.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Guardar_Cate.Image")));
            this.Btn_Guardar_Cate.ImageActive = null;
            this.Btn_Guardar_Cate.Location = new System.Drawing.Point(775, 74);
            this.Btn_Guardar_Cate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Guardar_Cate.Name = "Btn_Guardar_Cate";
            this.Btn_Guardar_Cate.Size = new System.Drawing.Size(61, 60);
            this.Btn_Guardar_Cate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Guardar_Cate.TabIndex = 27;
            this.Btn_Guardar_Cate.TabStop = false;
            this.Btn_Guardar_Cate.Zoom = 10;
            // 
            // Btn_Nuevo_Cate
            // 
            this.Btn_Nuevo_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Nuevo_Cate.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Nuevo_Cate.Image")));
            this.Btn_Nuevo_Cate.ImageActive = null;
            this.Btn_Nuevo_Cate.Location = new System.Drawing.Point(604, 74);
            this.Btn_Nuevo_Cate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Nuevo_Cate.Name = "Btn_Nuevo_Cate";
            this.Btn_Nuevo_Cate.Size = new System.Drawing.Size(61, 60);
            this.Btn_Nuevo_Cate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Nuevo_Cate.TabIndex = 28;
            this.Btn_Nuevo_Cate.TabStop = false;
            this.Btn_Nuevo_Cate.Zoom = 10;
            // 
            // Txt_Buscar_Cate
            // 
            this.Txt_Buscar_Cate.AcceptsReturn = false;
            this.Txt_Buscar_Cate.AcceptsTab = false;
            this.Txt_Buscar_Cate.AnimationSpeed = 200;
            this.Txt_Buscar_Cate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Buscar_Cate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Buscar_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Buscar_Cate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cate.BackgroundImage")));
            this.Txt_Buscar_Cate.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Buscar_Cate.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Buscar_Cate.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(151)))), ((int)(((byte)(158)))));
            this.Txt_Buscar_Cate.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Buscar_Cate.BorderRadius = 9;
            this.Txt_Buscar_Cate.BorderThickness = 3;
            this.Txt_Buscar_Cate.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Buscar_Cate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cate.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buscar_Cate.DefaultText = "";
            this.Txt_Buscar_Cate.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Buscar_Cate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Buscar_Cate.HideSelection = true;
            this.Txt_Buscar_Cate.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cate.IconLeft")));
            this.Txt_Buscar_Cate.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cate.IconPadding = 10;
            this.Txt_Buscar_Cate.IconRight = null;
            this.Txt_Buscar_Cate.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cate.Lines = new string[0];
            this.Txt_Buscar_Cate.Location = new System.Drawing.Point(29, 74);
            this.Txt_Buscar_Cate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txt_Buscar_Cate.MaxLength = 32767;
            this.Txt_Buscar_Cate.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Buscar_Cate.Modified = false;
            this.Txt_Buscar_Cate.Multiline = false;
            this.Txt_Buscar_Cate.Name = "Txt_Buscar_Cate";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties1.FillColor = System.Drawing.Color.Empty;
            stateProperties1.ForeColor = System.Drawing.Color.Empty;
            stateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cate.OnActiveState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.Empty;
            stateProperties2.FillColor = System.Drawing.Color.White;
            stateProperties2.ForeColor = System.Drawing.Color.Empty;
            stateProperties2.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cate.OnDisabledState = stateProperties2;
            stateProperties3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(151)))), ((int)(((byte)(158)))));
            stateProperties3.FillColor = System.Drawing.Color.Empty;
            stateProperties3.ForeColor = System.Drawing.Color.Empty;
            stateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cate.OnHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties4.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cate.OnIdleState = stateProperties4;
            this.Txt_Buscar_Cate.PasswordChar = '\0';
            this.Txt_Buscar_Cate.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cate.PlaceholderText = "Buscar";
            this.Txt_Buscar_Cate.ReadOnly = false;
            this.Txt_Buscar_Cate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Buscar_Cate.SelectedText = "";
            this.Txt_Buscar_Cate.SelectionLength = 0;
            this.Txt_Buscar_Cate.SelectionStart = 0;
            this.Txt_Buscar_Cate.ShortcutsEnabled = true;
            this.Txt_Buscar_Cate.Size = new System.Drawing.Size(357, 52);
            this.Txt_Buscar_Cate.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Buscar_Cate.TabIndex = 35;
            this.Txt_Buscar_Cate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Buscar_Cate.TextMarginBottom = 0;
            this.Txt_Buscar_Cate.TextMarginLeft = 5;
            this.Txt_Buscar_Cate.TextMarginTop = 0;
            this.Txt_Buscar_Cate.TextPlaceholder = "Buscar";
            this.Txt_Buscar_Cate.UseSystemPasswordChar = false;
            this.Txt_Buscar_Cate.WordWrap = true;
            // 
            // Tabla_Categoría
            // 
            this.Tabla_Categoría.AllowCustomTheming = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Categoría.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Tabla_Categoría.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Tabla_Categoría.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Tabla_Categoría.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.Tabla_Categoría.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Tabla_Categoría.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Tabla_Categoría.ColumnHeadersHeight = 40;
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.Tabla_Categoría.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.Tabla_Categoría.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            this.Tabla_Categoría.CurrentTheme.HeaderStyle.SelectionForeColor = System.Drawing.Color.White;
            this.Tabla_Categoría.CurrentTheme.Name = null;
            this.Tabla_Categoría.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.Tabla_Categoría.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Categoría.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Categoría.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Tabla_Categoría.DefaultCellStyle = dataGridViewCellStyle3;
            this.Tabla_Categoría.EnableHeadersVisualStyles = false;
            this.Tabla_Categoría.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Categoría.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Categoría.HeaderBgColor = System.Drawing.Color.Empty;
            this.Tabla_Categoría.HeaderForeColor = System.Drawing.Color.White;
            this.Tabla_Categoría.Location = new System.Drawing.Point(29, 231);
            this.Tabla_Categoría.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Tabla_Categoría.Name = "Tabla_Categoría";
            this.Tabla_Categoría.RowHeadersVisible = false;
            this.Tabla_Categoría.RowHeadersWidth = 51;
            this.Tabla_Categoría.RowTemplate.Height = 40;
            this.Tabla_Categoría.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Tabla_Categoría.Size = new System.Drawing.Size(885, 343);
            this.Tabla_Categoría.TabIndex = 36;
            this.Tabla_Categoría.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            // 
            // Lb_Cancelar_Cate
            // 
            this.Lb_Cancelar_Cate.AutoSize = true;
            this.Lb_Cancelar_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Cancelar_Cate.Location = new System.Drawing.Point(853, 651);
            this.Lb_Cancelar_Cate.Name = "Lb_Cancelar_Cate";
            this.Lb_Cancelar_Cate.Size = new System.Drawing.Size(95, 22);
            this.Lb_Cancelar_Cate.TabIndex = 89;
            this.Lb_Cancelar_Cate.Text = "Cancelar";
            // 
            // Lb_Guardar_Cate
            // 
            this.Lb_Guardar_Cate.AutoSize = true;
            this.Lb_Guardar_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Guardar_Cate.Location = new System.Drawing.Point(762, 136);
            this.Lb_Guardar_Cate.Name = "Lb_Guardar_Cate";
            this.Lb_Guardar_Cate.Size = new System.Drawing.Size(90, 22);
            this.Lb_Guardar_Cate.TabIndex = 88;
            this.Lb_Guardar_Cate.Text = "Guardar";
            // 
            // Lb_Editar_Cate
            // 
            this.Lb_Editar_Cate.AutoSize = true;
            this.Lb_Editar_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Editar_Cate.Location = new System.Drawing.Point(686, 136);
            this.Lb_Editar_Cate.Name = "Lb_Editar_Cate";
            this.Lb_Editar_Cate.Size = new System.Drawing.Size(67, 22);
            this.Lb_Editar_Cate.TabIndex = 94;
            this.Lb_Editar_Cate.Text = "Editar";
            // 
            // Lb_Nuevo_Cate
            // 
            this.Lb_Nuevo_Cate.AutoSize = true;
            this.Lb_Nuevo_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nuevo_Cate.Location = new System.Drawing.Point(598, 135);
            this.Lb_Nuevo_Cate.Name = "Lb_Nuevo_Cate";
            this.Lb_Nuevo_Cate.Size = new System.Drawing.Size(73, 22);
            this.Lb_Nuevo_Cate.TabIndex = 93;
            this.Lb_Nuevo_Cate.Text = "Nuevo";
            // 
            // Lb_Eliminar_Cate
            // 
            this.Lb_Eliminar_Cate.AutoSize = true;
            this.Lb_Eliminar_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Eliminar_Cate.Location = new System.Drawing.Point(860, 136);
            this.Lb_Eliminar_Cate.Name = "Lb_Eliminar_Cate";
            this.Lb_Eliminar_Cate.Size = new System.Drawing.Size(88, 22);
            this.Lb_Eliminar_Cate.TabIndex = 92;
            this.Lb_Eliminar_Cate.Text = "Eliminar";
            // 
            // Lb_NomCate_Cate
            // 
            this.Lb_NomCate_Cate.AutoSize = true;
            this.Lb_NomCate_Cate.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_NomCate_Cate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_NomCate_Cate.Location = new System.Drawing.Point(25, 172);
            this.Lb_NomCate_Cate.Name = "Lb_NomCate_Cate";
            this.Lb_NomCate_Cate.Size = new System.Drawing.Size(231, 23);
            this.Lb_NomCate_Cate.TabIndex = 96;
            this.Lb_NomCate_Cate.Text = "Nombre de Categoría:";
            // 
            // Txt_NomCate_Cate
            // 
            this.Txt_NomCate_Cate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_NomCate_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_NomCate_Cate.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Txt_NomCate_Cate.HintForeColor = System.Drawing.Color.Empty;
            this.Txt_NomCate_Cate.HintText = "";
            this.Txt_NomCate_Cate.isPassword = false;
            this.Txt_NomCate_Cate.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_NomCate_Cate.LineIdleColor = System.Drawing.Color.DimGray;
            this.Txt_NomCate_Cate.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Txt_NomCate_Cate.LineThickness = 3;
            this.Txt_NomCate_Cate.Location = new System.Drawing.Point(274, 159);
            this.Txt_NomCate_Cate.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Txt_NomCate_Cate.Name = "Txt_NomCate_Cate";
            this.Txt_NomCate_Cate.Size = new System.Drawing.Size(226, 36);
            this.Txt_NomCate_Cate.TabIndex = 95;
            this.Txt_NomCate_Cate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // Lb_Buscar_Cate
            // 
            this.Lb_Buscar_Cate.AutoSize = true;
            this.Lb_Buscar_Cate.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Buscar_Cate.Location = new System.Drawing.Point(410, 130);
            this.Lb_Buscar_Cate.Name = "Lb_Buscar_Cate";
            this.Lb_Buscar_Cate.Size = new System.Drawing.Size(75, 22);
            this.Lb_Buscar_Cate.TabIndex = 98;
            this.Lb_Buscar_Cate.Text = "Buscar";
            // 
            // Btn_Buscar_Cate
            // 
            this.Btn_Buscar_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Buscar_Cate.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Buscar_Cate.Image")));
            this.Btn_Buscar_Cate.ImageActive = null;
            this.Btn_Buscar_Cate.Location = new System.Drawing.Point(418, 66);
            this.Btn_Buscar_Cate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Buscar_Cate.Name = "Btn_Buscar_Cate";
            this.Btn_Buscar_Cate.Size = new System.Drawing.Size(61, 60);
            this.Btn_Buscar_Cate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Buscar_Cate.TabIndex = 97;
            this.Btn_Buscar_Cate.TabStop = false;
            this.Btn_Buscar_Cate.Zoom = 10;
            // 
            // Categoría
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(976, 691);
            this.Controls.Add(this.Lb_Buscar_Cate);
            this.Controls.Add(this.Btn_Buscar_Cate);
            this.Controls.Add(this.Lb_NomCate_Cate);
            this.Controls.Add(this.Txt_NomCate_Cate);
            this.Controls.Add(this.Lb_Editar_Cate);
            this.Controls.Add(this.Lb_Nuevo_Cate);
            this.Controls.Add(this.Lb_Eliminar_Cate);
            this.Controls.Add(this.Lb_Cancelar_Cate);
            this.Controls.Add(this.Lb_Guardar_Cate);
            this.Controls.Add(this.Tabla_Categoría);
            this.Controls.Add(this.Txt_Buscar_Cate);
            this.Controls.Add(this.Btn_Nuevo_Cate);
            this.Controls.Add(this.Btn_Guardar_Cate);
            this.Controls.Add(this.Btn_Eliminar_Cate);
            this.Controls.Add(this.Btn_Cancelar_Cate);
            this.Controls.Add(this.Btn_Editar_Cate);
            this.Controls.Add(this.P_Superior_Cate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Categoría";
            this.Text = " ";
            this.Load += new System.EventHandler(this.Categoría_Load);
            this.P_Superior_Cate.ResumeLayout(false);
            this.P_Superior_Cate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Salir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Cate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Cate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Cate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Cate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Cate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Categoría)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Buscar_Cate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuGradientPanel P_Superior_Cate;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Categoría;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.PictureBox Salir;
        private Bunifu.Framework.UI.BunifuDragControl Mov_C;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Editar_Cate;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Cancelar_Cate;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Eliminar_Cate;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Guardar_Cate;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Nuevo_Cate;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Buscar_Cate;
        private Bunifu.UI.WinForms.BunifuDataGridView Tabla_Categoría;
        private Guna.UI.WinForms.GunaLabel Lb_Cancelar_Cate;
        private Guna.UI.WinForms.GunaLabel Lb_Guardar_Cate;
        private Guna.UI.WinForms.GunaLabel Lb_Editar_Cate;
        private Guna.UI.WinForms.GunaLabel Lb_Nuevo_Cate;
        private Guna.UI.WinForms.GunaLabel Lb_Eliminar_Cate;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_NomCate_Cate;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Txt_NomCate_Cate;
        private Guna.UI.WinForms.GunaLabel Lb_Buscar_Cate;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Buscar_Cate;
    }
}