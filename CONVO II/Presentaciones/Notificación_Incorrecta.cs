﻿using System;
using System.Windows.Forms;

namespace CONVO_II.Presentaciones
{
    public partial class Notificación_Incorrecta : Form
    {
        public Notificación_Incorrecta()
        {
            InitializeComponent();
        }

        private void Btn_Ok_Noti_Inco_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
