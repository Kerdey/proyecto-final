﻿namespace CONVO_II.Presentaciones
{
    partial class Nuevo_Proveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Nuevo_Proveedor));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties9 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties10 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties11 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties12 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            this.PanelSupe_NProv = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.Lb_Nuevo_Proveedor = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.Salir = new System.Windows.Forms.PictureBox();
            this.Lb_Cancelar_Prove = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Guardar_Prove = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Guardar_Prove = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Cancelar_Prove = new Bunifu.Framework.UI.BunifuImageButton();
            this.Lb_PrimNom_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_PrimNom_Prove = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_SegNom_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Txt_SegNom_Prove = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_PrimApe_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_PrimApe_Prove = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_SegApe_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_SegApe_Prove = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Movil_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Movil_Prove = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Empresa_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Empresa_Prove = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Ruc_Empresa_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Ruc_Empresa_Prove = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Dirección_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Txt_Direc_Prove = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Lb_Tel_Emp_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Tel_Emp_Prove = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_URL_Emp_Prove = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_URL_Emp_Prove = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Datos_Contacto = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Lb_Datos_Empresa = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Mov_NProv = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.PanelSupe_NProv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Salir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Prove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Prove)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelSupe_NProv
            // 
            this.PanelSupe_NProv.BackColor = System.Drawing.Color.Gainsboro;
            this.PanelSupe_NProv.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelSupe_NProv.BackgroundImage")));
            this.PanelSupe_NProv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelSupe_NProv.Controls.Add(this.Lb_Nuevo_Proveedor);
            this.PanelSupe_NProv.Controls.Add(this.Logo);
            this.PanelSupe_NProv.Controls.Add(this.Salir);
            this.PanelSupe_NProv.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelSupe_NProv.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.PanelSupe_NProv.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.PanelSupe_NProv.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.PanelSupe_NProv.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.PanelSupe_NProv.Location = new System.Drawing.Point(0, 0);
            this.PanelSupe_NProv.Name = "PanelSupe_NProv";
            this.PanelSupe_NProv.Quality = 10;
            this.PanelSupe_NProv.Size = new System.Drawing.Size(871, 51);
            this.PanelSupe_NProv.TabIndex = 36;
            // 
            // Lb_Nuevo_Proveedor
            // 
            this.Lb_Nuevo_Proveedor.AutoSize = true;
            this.Lb_Nuevo_Proveedor.BackColor = System.Drawing.Color.Transparent;
            this.Lb_Nuevo_Proveedor.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nuevo_Proveedor.ForeColor = System.Drawing.Color.Gainsboro;
            this.Lb_Nuevo_Proveedor.Location = new System.Drawing.Point(67, 17);
            this.Lb_Nuevo_Proveedor.Name = "Lb_Nuevo_Proveedor";
            this.Lb_Nuevo_Proveedor.Size = new System.Drawing.Size(189, 23);
            this.Lb_Nuevo_Proveedor.TabIndex = 10;
            this.Lb_Nuevo_Proveedor.Text = "Nuevo Proveedor";
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(6, 1);
            this.Logo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(50, 48);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Logo.TabIndex = 9;
            this.Logo.TabStop = false;
            // 
            // Salir
            // 
            this.Salir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Salir.BackColor = System.Drawing.Color.Transparent;
            this.Salir.ErrorImage = null;
            this.Salir.Image = ((System.Drawing.Image)(resources.GetObject("Salir.Image")));
            this.Salir.Location = new System.Drawing.Point(826, 9);
            this.Salir.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Salir.Name = "Salir";
            this.Salir.Size = new System.Drawing.Size(30, 31);
            this.Salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Salir.TabIndex = 8;
            this.Salir.TabStop = false;
            this.Salir.Click += new System.EventHandler(this.Salir_Click);
            // 
            // Lb_Cancelar_Prove
            // 
            this.Lb_Cancelar_Prove.AutoSize = true;
            this.Lb_Cancelar_Prove.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Cancelar_Prove.Location = new System.Drawing.Point(754, 541);
            this.Lb_Cancelar_Prove.Name = "Lb_Cancelar_Prove";
            this.Lb_Cancelar_Prove.Size = new System.Drawing.Size(95, 22);
            this.Lb_Cancelar_Prove.TabIndex = 59;
            this.Lb_Cancelar_Prove.Text = "Cancelar";
            // 
            // Lb_Guardar_Prove
            // 
            this.Lb_Guardar_Prove.AutoSize = true;
            this.Lb_Guardar_Prove.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Guardar_Prove.Location = new System.Drawing.Point(656, 541);
            this.Lb_Guardar_Prove.Name = "Lb_Guardar_Prove";
            this.Lb_Guardar_Prove.Size = new System.Drawing.Size(90, 22);
            this.Lb_Guardar_Prove.TabIndex = 58;
            this.Lb_Guardar_Prove.Text = "Guardar";
            // 
            // Btn_Guardar_Prove
            // 
            this.Btn_Guardar_Prove.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Guardar_Prove.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Guardar_Prove.Image")));
            this.Btn_Guardar_Prove.ImageActive = null;
            this.Btn_Guardar_Prove.Location = new System.Drawing.Point(668, 479);
            this.Btn_Guardar_Prove.Name = "Btn_Guardar_Prove";
            this.Btn_Guardar_Prove.Size = new System.Drawing.Size(62, 60);
            this.Btn_Guardar_Prove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Guardar_Prove.TabIndex = 55;
            this.Btn_Guardar_Prove.TabStop = false;
            this.Btn_Guardar_Prove.Zoom = 10;
            // 
            // Btn_Cancelar_Prove
            // 
            this.Btn_Cancelar_Prove.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Cancelar_Prove.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Cancelar_Prove.Image")));
            this.Btn_Cancelar_Prove.ImageActive = null;
            this.Btn_Cancelar_Prove.Location = new System.Drawing.Point(769, 479);
            this.Btn_Cancelar_Prove.Name = "Btn_Cancelar_Prove";
            this.Btn_Cancelar_Prove.Size = new System.Drawing.Size(62, 60);
            this.Btn_Cancelar_Prove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Cancelar_Prove.TabIndex = 54;
            this.Btn_Cancelar_Prove.TabStop = false;
            this.Btn_Cancelar_Prove.Zoom = 10;
            this.Btn_Cancelar_Prove.Click += new System.EventHandler(this.Btn_Cancelar_Prove_Click);
            // 
            // Lb_PrimNom_Prove
            // 
            this.Lb_PrimNom_Prove.AutoSize = true;
            this.Lb_PrimNom_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_PrimNom_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_PrimNom_Prove.Location = new System.Drawing.Point(56, 116);
            this.Lb_PrimNom_Prove.Name = "Lb_PrimNom_Prove";
            this.Lb_PrimNom_Prove.Size = new System.Drawing.Size(177, 23);
            this.Lb_PrimNom_Prove.TabIndex = 69;
            this.Lb_PrimNom_Prove.Text = "Primer Nombre :";
            // 
            // txt_PrimNom_Prove
            // 
            this.txt_PrimNom_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_PrimNom_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PrimNom_Prove.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_PrimNom_Prove.HintForeColor = System.Drawing.Color.Empty;
            this.txt_PrimNom_Prove.HintText = "";
            this.txt_PrimNom_Prove.isPassword = false;
            this.txt_PrimNom_Prove.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_PrimNom_Prove.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_PrimNom_Prove.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_PrimNom_Prove.LineThickness = 3;
            this.txt_PrimNom_Prove.Location = new System.Drawing.Point(240, 107);
            this.txt_PrimNom_Prove.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_PrimNom_Prove.Name = "txt_PrimNom_Prove";
            this.txt_PrimNom_Prove.Size = new System.Drawing.Size(186, 36);
            this.txt_PrimNom_Prove.TabIndex = 68;
            this.txt_PrimNom_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_PrimNom_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_PrimNom_Prove_KeyPress);
            // 
            // Lb_SegNom_Prove
            // 
            this.Lb_SegNom_Prove.AutoSize = true;
            this.Lb_SegNom_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_SegNom_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_SegNom_Prove.Location = new System.Drawing.Point(444, 116);
            this.Lb_SegNom_Prove.Name = "Lb_SegNom_Prove";
            this.Lb_SegNom_Prove.Size = new System.Drawing.Size(198, 23);
            this.Lb_SegNom_Prove.TabIndex = 71;
            this.Lb_SegNom_Prove.Text = "Segundo Nombre :";
            // 
            // Txt_SegNom_Prove
            // 
            this.Txt_SegNom_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_SegNom_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SegNom_Prove.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Txt_SegNom_Prove.HintForeColor = System.Drawing.Color.Empty;
            this.Txt_SegNom_Prove.HintText = "";
            this.Txt_SegNom_Prove.isPassword = false;
            this.Txt_SegNom_Prove.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_SegNom_Prove.LineIdleColor = System.Drawing.Color.DimGray;
            this.Txt_SegNom_Prove.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Txt_SegNom_Prove.LineThickness = 3;
            this.Txt_SegNom_Prove.Location = new System.Drawing.Point(643, 107);
            this.Txt_SegNom_Prove.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Txt_SegNom_Prove.Name = "Txt_SegNom_Prove";
            this.Txt_SegNom_Prove.Size = new System.Drawing.Size(186, 36);
            this.Txt_SegNom_Prove.TabIndex = 70;
            this.Txt_SegNom_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_SegNom_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_SegNom_Prove_KeyPress);
            // 
            // Lb_PrimApe_Prove
            // 
            this.Lb_PrimApe_Prove.AutoSize = true;
            this.Lb_PrimApe_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_PrimApe_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_PrimApe_Prove.Location = new System.Drawing.Point(56, 159);
            this.Lb_PrimApe_Prove.Name = "Lb_PrimApe_Prove";
            this.Lb_PrimApe_Prove.Size = new System.Drawing.Size(180, 23);
            this.Lb_PrimApe_Prove.TabIndex = 73;
            this.Lb_PrimApe_Prove.Text = "Primer Apellido :";
            // 
            // txt_PrimApe_Prove
            // 
            this.txt_PrimApe_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_PrimApe_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PrimApe_Prove.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_PrimApe_Prove.HintForeColor = System.Drawing.Color.Empty;
            this.txt_PrimApe_Prove.HintText = "";
            this.txt_PrimApe_Prove.isPassword = false;
            this.txt_PrimApe_Prove.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_PrimApe_Prove.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_PrimApe_Prove.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_PrimApe_Prove.LineThickness = 3;
            this.txt_PrimApe_Prove.Location = new System.Drawing.Point(240, 150);
            this.txt_PrimApe_Prove.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_PrimApe_Prove.Name = "txt_PrimApe_Prove";
            this.txt_PrimApe_Prove.Size = new System.Drawing.Size(186, 36);
            this.txt_PrimApe_Prove.TabIndex = 72;
            this.txt_PrimApe_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_PrimApe_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_PrimApe_Prove_KeyPress);
            // 
            // Lb_SegApe_Prove
            // 
            this.Lb_SegApe_Prove.AutoSize = true;
            this.Lb_SegApe_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_SegApe_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_SegApe_Prove.Location = new System.Drawing.Point(444, 159);
            this.Lb_SegApe_Prove.Name = "Lb_SegApe_Prove";
            this.Lb_SegApe_Prove.Size = new System.Drawing.Size(201, 23);
            this.Lb_SegApe_Prove.TabIndex = 75;
            this.Lb_SegApe_Prove.Text = "Segundo Apellido :";
            // 
            // txt_SegApe_Prove
            // 
            this.txt_SegApe_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SegApe_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SegApe_Prove.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SegApe_Prove.HintForeColor = System.Drawing.Color.Empty;
            this.txt_SegApe_Prove.HintText = "";
            this.txt_SegApe_Prove.isPassword = false;
            this.txt_SegApe_Prove.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_SegApe_Prove.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_SegApe_Prove.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_SegApe_Prove.LineThickness = 3;
            this.txt_SegApe_Prove.Location = new System.Drawing.Point(643, 150);
            this.txt_SegApe_Prove.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_SegApe_Prove.Name = "txt_SegApe_Prove";
            this.txt_SegApe_Prove.Size = new System.Drawing.Size(186, 36);
            this.txt_SegApe_Prove.TabIndex = 74;
            this.txt_SegApe_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_SegApe_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_SegApe_Prove_KeyPress);
            // 
            // Lb_Movil_Prove
            // 
            this.Lb_Movil_Prove.AutoSize = true;
            this.Lb_Movil_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Movil_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Movil_Prove.Location = new System.Drawing.Point(56, 202);
            this.Lb_Movil_Prove.Name = "Lb_Movil_Prove";
            this.Lb_Movil_Prove.Size = new System.Drawing.Size(77, 23);
            this.Lb_Movil_Prove.TabIndex = 91;
            this.Lb_Movil_Prove.Text = "Movil :";
            // 
            // txt_Movil_Prove
            // 
            this.txt_Movil_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Movil_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Movil_Prove.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Movil_Prove.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Movil_Prove.HintText = "";
            this.txt_Movil_Prove.isPassword = false;
            this.txt_Movil_Prove.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Movil_Prove.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Movil_Prove.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Movil_Prove.LineThickness = 3;
            this.txt_Movil_Prove.Location = new System.Drawing.Point(135, 192);
            this.txt_Movil_Prove.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_Movil_Prove.Name = "txt_Movil_Prove";
            this.txt_Movil_Prove.Size = new System.Drawing.Size(171, 36);
            this.txt_Movil_Prove.TabIndex = 90;
            this.txt_Movil_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Movil_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Movil_Prove_KeyPress);
            // 
            // Lb_Empresa_Prove
            // 
            this.Lb_Empresa_Prove.AutoSize = true;
            this.Lb_Empresa_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Empresa_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Empresa_Prove.Location = new System.Drawing.Point(56, 314);
            this.Lb_Empresa_Prove.Name = "Lb_Empresa_Prove";
            this.Lb_Empresa_Prove.Size = new System.Drawing.Size(109, 23);
            this.Lb_Empresa_Prove.TabIndex = 93;
            this.Lb_Empresa_Prove.Text = "Empresa :";
            // 
            // txt_Empresa_Prove
            // 
            this.txt_Empresa_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Empresa_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Empresa_Prove.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Empresa_Prove.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Empresa_Prove.HintText = "";
            this.txt_Empresa_Prove.isPassword = false;
            this.txt_Empresa_Prove.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Empresa_Prove.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Empresa_Prove.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Empresa_Prove.LineThickness = 3;
            this.txt_Empresa_Prove.Location = new System.Drawing.Point(173, 304);
            this.txt_Empresa_Prove.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_Empresa_Prove.Name = "txt_Empresa_Prove";
            this.txt_Empresa_Prove.Size = new System.Drawing.Size(201, 36);
            this.txt_Empresa_Prove.TabIndex = 92;
            this.txt_Empresa_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Empresa_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Empresa_Prove_KeyPress);
            // 
            // Lb_Ruc_Empresa_Prove
            // 
            this.Lb_Ruc_Empresa_Prove.AutoSize = true;
            this.Lb_Ruc_Empresa_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Ruc_Empresa_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Ruc_Empresa_Prove.Location = new System.Drawing.Point(379, 317);
            this.Lb_Ruc_Empresa_Prove.Name = "Lb_Ruc_Empresa_Prove";
            this.Lb_Ruc_Empresa_Prove.Size = new System.Drawing.Size(64, 23);
            this.Lb_Ruc_Empresa_Prove.TabIndex = 95;
            this.Lb_Ruc_Empresa_Prove.Text = "RUC :";
            // 
            // txt_Ruc_Empresa_Prove
            // 
            this.txt_Ruc_Empresa_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Ruc_Empresa_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Ruc_Empresa_Prove.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Ruc_Empresa_Prove.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Ruc_Empresa_Prove.HintText = "";
            this.txt_Ruc_Empresa_Prove.isPassword = false;
            this.txt_Ruc_Empresa_Prove.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Ruc_Empresa_Prove.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Ruc_Empresa_Prove.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Ruc_Empresa_Prove.LineThickness = 3;
            this.txt_Ruc_Empresa_Prove.Location = new System.Drawing.Point(451, 304);
            this.txt_Ruc_Empresa_Prove.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_Ruc_Empresa_Prove.Name = "txt_Ruc_Empresa_Prove";
            this.txt_Ruc_Empresa_Prove.Size = new System.Drawing.Size(231, 36);
            this.txt_Ruc_Empresa_Prove.TabIndex = 94;
            this.txt_Ruc_Empresa_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Ruc_Empresa_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Ruc_Empresa_Prove_KeyPress);
            // 
            // Lb_Dirección_Prove
            // 
            this.Lb_Dirección_Prove.AutoSize = true;
            this.Lb_Dirección_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Dirección_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Dirección_Prove.Location = new System.Drawing.Point(56, 363);
            this.Lb_Dirección_Prove.Name = "Lb_Dirección_Prove";
            this.Lb_Dirección_Prove.Size = new System.Drawing.Size(118, 23);
            this.Lb_Dirección_Prove.TabIndex = 97;
            this.Lb_Dirección_Prove.Text = "Dirección :";
            // 
            // Txt_Direc_Prove
            // 
            this.Txt_Direc_Prove.AcceptsReturn = false;
            this.Txt_Direc_Prove.AcceptsTab = false;
            this.Txt_Direc_Prove.AnimationSpeed = 200;
            this.Txt_Direc_Prove.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Direc_Prove.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Direc_Prove.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Direc_Prove.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Direc_Prove.BackgroundImage")));
            this.Txt_Direc_Prove.BorderColorActive = System.Drawing.Color.Transparent;
            this.Txt_Direc_Prove.BorderColorDisabled = System.Drawing.Color.Gainsboro;
            this.Txt_Direc_Prove.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Direc_Prove.BorderColorIdle = System.Drawing.Color.Gainsboro;
            this.Txt_Direc_Prove.BorderRadius = 9;
            this.Txt_Direc_Prove.BorderThickness = 2;
            this.Txt_Direc_Prove.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Direc_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Direc_Prove.DefaultFont = new System.Drawing.Font("Lucida Bright", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Direc_Prove.DefaultText = "";
            this.Txt_Direc_Prove.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Direc_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Direc_Prove.HideSelection = true;
            this.Txt_Direc_Prove.IconLeft = null;
            this.Txt_Direc_Prove.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Direc_Prove.IconPadding = 10;
            this.Txt_Direc_Prove.IconRight = null;
            this.Txt_Direc_Prove.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Direc_Prove.Lines = new string[0];
            this.Txt_Direc_Prove.Location = new System.Drawing.Point(191, 353);
            this.Txt_Direc_Prove.MaxLength = 32767;
            this.Txt_Direc_Prove.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Direc_Prove.Modified = false;
            this.Txt_Direc_Prove.Multiline = true;
            this.Txt_Direc_Prove.Name = "Txt_Direc_Prove";
            stateProperties9.BorderColor = System.Drawing.Color.Transparent;
            stateProperties9.FillColor = System.Drawing.Color.Empty;
            stateProperties9.ForeColor = System.Drawing.Color.Empty;
            stateProperties9.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Direc_Prove.OnActiveState = stateProperties9;
            stateProperties10.BorderColor = System.Drawing.Color.Gainsboro;
            stateProperties10.FillColor = System.Drawing.Color.White;
            stateProperties10.ForeColor = System.Drawing.Color.Empty;
            stateProperties10.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Direc_Prove.OnDisabledState = stateProperties10;
            stateProperties11.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties11.FillColor = System.Drawing.Color.Empty;
            stateProperties11.ForeColor = System.Drawing.Color.Empty;
            stateProperties11.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Direc_Prove.OnHoverState = stateProperties11;
            stateProperties12.BorderColor = System.Drawing.Color.Gainsboro;
            stateProperties12.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties12.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Direc_Prove.OnIdleState = stateProperties12;
            this.Txt_Direc_Prove.PasswordChar = '\0';
            this.Txt_Direc_Prove.PlaceholderForeColor = System.Drawing.Color.DarkGray;
            this.Txt_Direc_Prove.PlaceholderText = "Dirección";
            this.Txt_Direc_Prove.ReadOnly = false;
            this.Txt_Direc_Prove.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Direc_Prove.SelectedText = "";
            this.Txt_Direc_Prove.SelectionLength = 0;
            this.Txt_Direc_Prove.SelectionStart = 0;
            this.Txt_Direc_Prove.ShortcutsEnabled = true;
            this.Txt_Direc_Prove.Size = new System.Drawing.Size(638, 55);
            this.Txt_Direc_Prove.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Direc_Prove.TabIndex = 96;
            this.Txt_Direc_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Direc_Prove.TextMarginBottom = 0;
            this.Txt_Direc_Prove.TextMarginLeft = 5;
            this.Txt_Direc_Prove.TextMarginTop = 0;
            this.Txt_Direc_Prove.TextPlaceholder = "Dirección";
            this.Txt_Direc_Prove.UseSystemPasswordChar = false;
            this.Txt_Direc_Prove.WordWrap = true;
            this.Txt_Direc_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Direc_Prove_KeyPress);
            // 
            // Lb_Tel_Emp_Prove
            // 
            this.Lb_Tel_Emp_Prove.AutoSize = true;
            this.Lb_Tel_Emp_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Tel_Emp_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Tel_Emp_Prove.Location = new System.Drawing.Point(56, 426);
            this.Lb_Tel_Emp_Prove.Name = "Lb_Tel_Emp_Prove";
            this.Lb_Tel_Emp_Prove.Size = new System.Drawing.Size(112, 23);
            this.Lb_Tel_Emp_Prove.TabIndex = 99;
            this.Lb_Tel_Emp_Prove.Text = "Teléfono :";
            // 
            // txt_Tel_Emp_Prove
            // 
            this.txt_Tel_Emp_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Tel_Emp_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Tel_Emp_Prove.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Tel_Emp_Prove.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Tel_Emp_Prove.HintText = "";
            this.txt_Tel_Emp_Prove.isPassword = false;
            this.txt_Tel_Emp_Prove.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Tel_Emp_Prove.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Tel_Emp_Prove.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Tel_Emp_Prove.LineThickness = 3;
            this.txt_Tel_Emp_Prove.Location = new System.Drawing.Point(175, 416);
            this.txt_Tel_Emp_Prove.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_Tel_Emp_Prove.Name = "txt_Tel_Emp_Prove";
            this.txt_Tel_Emp_Prove.Size = new System.Drawing.Size(171, 36);
            this.txt_Tel_Emp_Prove.TabIndex = 98;
            this.txt_Tel_Emp_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Tel_Emp_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Tel_Emp_Prove_KeyPress);
            // 
            // Lb_URL_Emp_Prove
            // 
            this.Lb_URL_Emp_Prove.AutoSize = true;
            this.Lb_URL_Emp_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_URL_Emp_Prove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_URL_Emp_Prove.Location = new System.Drawing.Point(364, 426);
            this.Lb_URL_Emp_Prove.Name = "Lb_URL_Emp_Prove";
            this.Lb_URL_Emp_Prove.Size = new System.Drawing.Size(164, 23);
            this.Lb_URL_Emp_Prove.TabIndex = 101;
            this.Lb_URL_Emp_Prove.Text = "Dirección URL :";
            // 
            // txt_URL_Emp_Prove
            // 
            this.txt_URL_Emp_Prove.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_URL_Emp_Prove.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_URL_Emp_Prove.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_URL_Emp_Prove.HintForeColor = System.Drawing.Color.Empty;
            this.txt_URL_Emp_Prove.HintText = "";
            this.txt_URL_Emp_Prove.isPassword = false;
            this.txt_URL_Emp_Prove.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_URL_Emp_Prove.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_URL_Emp_Prove.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_URL_Emp_Prove.LineThickness = 3;
            this.txt_URL_Emp_Prove.Location = new System.Drawing.Point(536, 416);
            this.txt_URL_Emp_Prove.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_URL_Emp_Prove.Name = "txt_URL_Emp_Prove";
            this.txt_URL_Emp_Prove.Size = new System.Drawing.Size(293, 36);
            this.txt_URL_Emp_Prove.TabIndex = 100;
            this.txt_URL_Emp_Prove.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_URL_Emp_Prove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_URL_Emp_Prove_KeyPress);
            // 
            // Lb_Datos_Contacto
            // 
            this.Lb_Datos_Contacto.AutoSize = true;
            this.Lb_Datos_Contacto.Font = new System.Drawing.Font("Lucida Bright", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Datos_Contacto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Datos_Contacto.Location = new System.Drawing.Point(19, 69);
            this.Lb_Datos_Contacto.Name = "Lb_Datos_Contacto";
            this.Lb_Datos_Contacto.Size = new System.Drawing.Size(240, 27);
            this.Lb_Datos_Contacto.TabIndex = 102;
            this.Lb_Datos_Contacto.Text = "Datos del Contacto";
            // 
            // Lb_Datos_Empresa
            // 
            this.Lb_Datos_Empresa.AutoSize = true;
            this.Lb_Datos_Empresa.Font = new System.Drawing.Font("Lucida Bright", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Datos_Empresa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Datos_Empresa.Location = new System.Drawing.Point(19, 264);
            this.Lb_Datos_Empresa.Name = "Lb_Datos_Empresa";
            this.Lb_Datos_Empresa.Size = new System.Drawing.Size(258, 27);
            this.Lb_Datos_Empresa.TabIndex = 103;
            this.Lb_Datos_Empresa.Text = "Datos de la Empresa";
            // 
            // Mov_NProv
            // 
            this.Mov_NProv.Fixed = true;
            this.Mov_NProv.Horizontal = true;
            this.Mov_NProv.TargetControl = this.PanelSupe_NProv;
            this.Mov_NProv.Vertical = true;
            // 
            // Nuevo_Proveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(871, 586);
            this.Controls.Add(this.Lb_Datos_Empresa);
            this.Controls.Add(this.Lb_Datos_Contacto);
            this.Controls.Add(this.Lb_URL_Emp_Prove);
            this.Controls.Add(this.txt_URL_Emp_Prove);
            this.Controls.Add(this.Lb_Tel_Emp_Prove);
            this.Controls.Add(this.txt_Tel_Emp_Prove);
            this.Controls.Add(this.Lb_Dirección_Prove);
            this.Controls.Add(this.Txt_Direc_Prove);
            this.Controls.Add(this.Lb_Ruc_Empresa_Prove);
            this.Controls.Add(this.txt_Ruc_Empresa_Prove);
            this.Controls.Add(this.Lb_Empresa_Prove);
            this.Controls.Add(this.txt_Empresa_Prove);
            this.Controls.Add(this.Lb_Movil_Prove);
            this.Controls.Add(this.txt_Movil_Prove);
            this.Controls.Add(this.Lb_SegApe_Prove);
            this.Controls.Add(this.txt_SegApe_Prove);
            this.Controls.Add(this.Lb_PrimApe_Prove);
            this.Controls.Add(this.txt_PrimApe_Prove);
            this.Controls.Add(this.Lb_SegNom_Prove);
            this.Controls.Add(this.Txt_SegNom_Prove);
            this.Controls.Add(this.Lb_PrimNom_Prove);
            this.Controls.Add(this.txt_PrimNom_Prove);
            this.Controls.Add(this.Lb_Cancelar_Prove);
            this.Controls.Add(this.Lb_Guardar_Prove);
            this.Controls.Add(this.Btn_Guardar_Prove);
            this.Controls.Add(this.Btn_Cancelar_Prove);
            this.Controls.Add(this.PanelSupe_NProv);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Nuevo_Proveedor";
            this.Text = "Nuevo_Proveedor";
            this.PanelSupe_NProv.ResumeLayout(false);
            this.PanelSupe_NProv.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Salir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Prove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Prove)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuGradientPanel PanelSupe_NProv;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Nuevo_Proveedor;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.PictureBox Salir;
        private Guna.UI.WinForms.GunaLabel Lb_Cancelar_Prove;
        private Guna.UI.WinForms.GunaLabel Lb_Guardar_Prove;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Guardar_Prove;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Cancelar_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_PrimNom_Prove;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_PrimNom_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_SegNom_Prove;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Txt_SegNom_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_PrimApe_Prove;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_PrimApe_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_SegApe_Prove;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_SegApe_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Movil_Prove;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Movil_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Empresa_Prove;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Empresa_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Ruc_Empresa_Prove;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Ruc_Empresa_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Dirección_Prove;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Direc_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Tel_Emp_Prove;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Tel_Emp_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_URL_Emp_Prove;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_URL_Emp_Prove;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Datos_Contacto;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Datos_Empresa;
        private Bunifu.Framework.UI.BunifuDragControl Mov_NProv;
    }
}