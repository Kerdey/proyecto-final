﻿using Pulpería_Lovo.Presentaciones;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Pulpería_Lovo
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
            CerrarSubsMenus();
        }

        private void AbrirFormularios<MiForm>() where MiForm : Form, new()
        {
            Form Formularios;
            Formularios = Panel_de_Contenido.Controls.OfType<MiForm>().FirstOrDefault();
            if (Formularios == null)
            {
                if(Panel_de_Contenido.Controls.Count > 0)
                {
                    Panel_de_Contenido.Controls.RemoveAt(0);
                }
                Formularios = new MiForm();
                Formularios.TopLevel = false;
                Formularios.Dock = DockStyle.Fill;
             
                Panel_de_Contenido.Controls.Add(Formularios);
                Panel_de_Contenido.Tag = Formularios;
                Formularios.Show();
                Formularios.BringToFront();

            }
            else
            {
                Formularios.BringToFront();
            }

        }

        private void Restaurar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            Btn_Restaurar_Prin.Visible = false;                             //Se oculta el Boton si no se ha maximido
            Btn_Maximizar_Prin.Visible = true;                            //Se muestra en Boton de maximizar
        }

        ////Metodo para mover la ventana
        //[DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        //private extern static void ReleaseCapture();

        //[DllImport("user32.DLL", EntryPoint = "SendMessage")]
        //private extern static void SendMessage(System.IntPtr hwnd, int wmg, int wparam, int lparam);

        private void Maximizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;             //Maximiza la ventana
            Btn_Maximizar_Prin.Visible = false;                             //No de muestra maximizar en pantalla completa
            Btn_Restaurar_Prin.Visible = true;                              //Se muestra Restaurar estando en pantalla completa
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BT_Menu_Click(object sender, EventArgs e)
        {
            if (Menu_Plegable.Width == 260)     //Si tiene un ancho de 325 *
            {
                Menu_Plegable.Visible = false;             //  Animación del menu plegable que al inicio no sea visible
                Menu_Plegable.Width = 62;                   // * El ancho del panel que contiene el menu se colocara del tamaño deseado
                Panel_Men.Width = 70;           //Para que el menu plegable se contraiga a como queremos
                Linea_Menu.Width = 50;               //Para que la linea del menu tambien disminuya al contraerse el menu
                //this.Lb_Fecha_Prin.Location = new Point(this.Lb_Fecha_Prin.Location.X + 110, this.Lb_Fecha_Prin.Location.Y);
                //this.Lb_Hora_Prin.Location = new Point(this.Lb_Hora_Prin.Location.X + 130, this.Lb_Hora_Prin.Location.Y);
                //this.MarcaAgua_Logo_Prin.Location = new Point(this.MarcaAgua_Logo_Prin.Location.X - 70, this.MarcaAgua_Logo_Prin.Location.Y);


                //Animación
                Ani_MP_Reduce.Show(Menu_Plegable);
            }
            else                                            //sino que vuleva a su tamaño original
            {

                Menu_Plegable.Visible = false;
                Menu_Plegable.Width = 260;
                Panel_Men.Width = 270;
                Linea_Menu.Width = 220;

                //Animación
                Ani_MP_Amplia.Show(Menu_Plegable);
                //this.Lb_Fecha_Prin.Location = new Point(248 - 40, 569 - 38);
                //this.Lb_Hora_Prin.Location = new Point(320 - 20, 605 - 38);
                //this.MarcaAgua_Logo_Prin.Location = new Point(165, 124);// 165; 124


            }
        }

        private void Btn_Productos_Click(object sender, EventArgs e)
        {
            if (Sub_Panel_Productos.Visible == false)
            {
                CerrarSubsMenus();
                Sub_Panel_Productos.Visible = true;

                Sub_Panel_Productos.Location = new Point(1, 150);

                Btn_Ventas_Prin.Location = new Point(14, 159 + 117);
                Btn_Compras.Location = new Point(14, 218 + 117);
                Btn_Servicios_Prin.Location = new Point(14, 276 + 117);
                Btn_Reportes_Prin.Location = new Point(14, 335 + 117);
                Btn_Opciones_Prin.Location = new Point(14, 393 + 117);

            }
            else
            {
                CerrarSubsMenus();
            }
        }

        private void Fecha_Hora_Prin_Tick(object sender, EventArgs e)
        {
            //Lb_Hora_Prin.Text = DateTime.Now.ToString("hh:mm:ss");
            //Lb_Fecha_Prin.Text = DateTime.Now.ToLongDateString();
        }

        private void Lb_Fecha_Prin_Click(object sender, EventArgs e)
        {

        }


        //private void Btn_Productos_Click(object sender, EventArgs e)
        //{
        //    if (Sub_Panel_Productos.Visible == false)
        //    {
        //        CerrarSubsMenus();
        //        Sub_Panel_Productos.Visible = true;
        //        Btn_Ventas_Prin.Location = new Point(5, 306);
        //        Sub_Panel_Productos.Location = new Point(1, 150);
        //        Btn_Compras.Location = new Point(5, 360);
        //        Btn_Reportes_Prin.Location = new Point(5, 414);
        //        Btn_Opciones_Prin.Location = new Point(5, 468);

        //    }
        //    else
        //    {
        //        CerrarSubsMenus();
        //    }
        //}

        private void Panel_Contenido_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BunifuFlatButton8_Click(object sender, EventArgs e)
        {

        }

        private void Btn_Ventas_Prin_Click(object sender, EventArgs e)
        {
            if (Sub_Panel_ventas.Visible == false)
            {
                CerrarSubsMenus();
                Sub_Panel_ventas.Visible = true;
                Sub_Panel_ventas.Location = new Point(1, 208);

                Btn_Compras.Location = new Point(14, 218 + 288);
                Btn_Servicios_Prin.Location = new Point(14, 276 + 288);
                Btn_Reportes_Prin.Location = new Point(14, 335 + 288);
                Btn_Opciones_Prin.Location = new Point(14, 393 + 288);
            }
            else
            {
                CerrarSubsMenus();

            }
        }

        private void Btn_Compras_Click(object sender, EventArgs e)
        {
            if (Sub_Panel_Cmpras.Visible == false)
            {
                CerrarSubsMenus();
                Sub_Panel_Cmpras.Visible = true;
                Sub_Panel_Cmpras.Location = new Point(1, 265);
                Btn_Servicios_Prin.Location = new Point(14, 276 + 225);
                Btn_Reportes_Prin.Location = new Point(14, 335 + 225);
                Btn_Opciones_Prin.Location = new Point(14, 393 + 225);

            }
            else
            {
                CerrarSubsMenus();
            }
        }

        private void BunifuFlatButton10_Click(object sender, EventArgs e)
        {

        }


        private void CerrarSubsMenus()
        {
            Sub_Panel_Productos.Visible = false;
            Sub_Panel_ventas.Visible = false;
            Sub_Panel_Cmpras.Visible = false;
            Sub_Panel_Opciones.Visible = false;


            Btn_Ventas_Prin.Location = new Point(14, 159);
            Btn_Compras.Location = new Point(14, 218);
            Btn_Servicios_Prin.Location = new Point(14, 276);
            Btn_Reportes_Prin.Location = new Point(14, 335);
            Btn_Opciones_Prin.Location = new Point(14, 393);
        }

        private void Btn_Opciones_Prin_Click(object sender, EventArgs e)
        {
         //   4; 388 +

            if (Sub_Panel_Opciones.Visible == false)
            {
                CerrarSubsMenus();
                Sub_Panel_Opciones.Visible = true;
                Sub_Panel_Opciones.Location = new Point(1, 440);

            }
            else
            {
                CerrarSubsMenus();
            }
        }

        //RESIZE METODO PARA REDIMENCIONAR/CAMBIAR TAMAÑO A FORMULARIO EN TIEMPO DE EJECUCION ----------------------------------------------------------
        private int tolerance = 12;
        private const int WM_NCHITTEST = 132;
        private const int HTBOTTOMRIGHT = 17;
        private Rectangle sizeGripRectangle;
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCHITTEST:
                    base.WndProc(ref m);
                    var hitPoint = this.PointToClient(new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16));
                    if (sizeGripRectangle.Contains(hitPoint))
                        m.Result = new IntPtr(HTBOTTOMRIGHT);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        //----------------DIBUJAR RECTANGULO / EXCLUIR ESQUINA PANEL 
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            var region = new Region(new Rectangle(0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height));
            sizeGripRectangle = new Rectangle(this.ClientRectangle.Width - tolerance, this.ClientRectangle.Height - tolerance, tolerance, tolerance);
            region.Exclude(sizeGripRectangle);
            this.Panel_Contenido.Region = region;
            this.Invalidate();
        }
        //----------------COLOR Y GRIP DE RECTANGULO INFERIOR
        protected override void OnPaint(PaintEventArgs e)
        {
            SolidBrush blueBrush = new SolidBrush(Color.FromArgb(244, 244, 244));
            e.Graphics.FillRectangle(blueBrush, sizeGripRectangle);
            //base.OnPaint(e);
            //ControlPaint.DrawSizeGrip(e.Graphics, Color.Transparent, sizeGripRectangle);
        }

   
        private void Btn_Catalogo_PrinDes_Click(object sender, EventArgs e)
        {
            AbrirFormularios<Cátalogo_Prod>();
        }

        private void Btn_Categoría_PrinDes_Click(object sender, EventArgs e)
        {
            AbrirFormularios<Categoría>();
        }

        private void BunifuFlatButton2_Click(object sender, EventArgs e)
        {
            AbrirFormularios<Ventas>();
        }

        private void Btn_Clientes_Prin_Click(object sender, EventArgs e)
        {
            AbrirFormularios<Clientes>();
        }

        private void Btn_Registra_Compra_Click(object sender, EventArgs e)
        {
            AbrirFormularios<Compras>();
        }

        private void Btn_Proveedor_Prin_Click(object sender, EventArgs e)
        {
            //AbrirFormularios<>
        }
    }
}
