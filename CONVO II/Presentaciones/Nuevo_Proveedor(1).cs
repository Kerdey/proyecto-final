﻿using System;
using System.Windows.Forms;

namespace Pulpería_Lovo.Presentaciones
{
    public partial class Nuevo_Proveedor : Form
    {
        public Nuevo_Proveedor()
        {
            InitializeComponent();
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btn_Cancelar_Prove_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Txt_PrimNom_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_SegNom_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_PrimApe_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_SegApe_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Movil_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Número por favor");
                e.Handled = true;                                                                                                                       //Para que no deje q aun con el mensaje se escriban letras
                return;
            }
        }

        private void Txt_Empresa_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Ruc_Empresa_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Número por favor");
                e.Handled = true;                                                                                                                       //Para que no deje q aun con el mensaje se escriban letras
                return;
            }
        }

        private void Txt_Direc_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void Txt_Tel_Emp_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Número por favor");
                e.Handled = true;                                                                                                                       //Para que no deje q aun con el mensaje se escriban letras
                return;
            }
        }

        private void Txt_URL_Emp_Prove_KeyPress(object sender, KeyPressEventArgs e)
        {
         
        }
    }
}
