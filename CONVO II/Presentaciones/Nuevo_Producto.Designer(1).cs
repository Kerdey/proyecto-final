﻿namespace Pulpería_Lovo.Presentaciones
{
    partial class Nuevo_Producto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Nuevo_Producto));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            this.Txt_Cod_NuevoP = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txt_NomProd = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Cod_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Lb_NomProd_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Marca_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Marca = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_PrecioVts_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_PrecioVts_NP = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Categ_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.PSuperior_NP = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.Lb_Nuevo_Prod = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.Salir = new System.Windows.Forms.PictureBox();
            this.Mov_Panel_NP = new Bunifu.UI.WinForms.BunifuFormDock();
            this.Txt__Guardar_NP = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Cancelar_Cate = new Bunifu.Framework.UI.BunifuImageButton();
            this.ComboBox_Und_Medida_NP = new Guna.UI.WinForms.GunaComboBox();
            this.Lb_Und_Med_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Lb_Proveedor_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Lb_Fecha_Regis_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Fecha_Regis_NP = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Fecha_Ven_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Fecha_Ven_NP = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Txt_Descrip_NP = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Lb_Descripción_NP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Lb_Guardar_NP = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Cancelar_NP = new Guna.UI.WinForms.GunaLabel();
            this.txt_Stock_NP = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_ = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.ComboBox_Prove_NPorv = new Guna.UI.WinForms.GunaComboBox();
            this.ComboBox_Cate_NP = new Guna.UI.WinForms.GunaComboBox();
            this.PSuperior_NP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Salir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Txt__Guardar_NP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Cate)).BeginInit();
            this.SuspendLayout();
            // 
            // Txt_Cod_NuevoP
            // 
            this.Txt_Cod_NuevoP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Cod_NuevoP.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Cod_NuevoP.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Txt_Cod_NuevoP.HintForeColor = System.Drawing.Color.Empty;
            this.Txt_Cod_NuevoP.HintText = "";
            this.Txt_Cod_NuevoP.isPassword = false;
            this.Txt_Cod_NuevoP.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Cod_NuevoP.LineIdleColor = System.Drawing.Color.DimGray;
            this.Txt_Cod_NuevoP.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Txt_Cod_NuevoP.LineThickness = 3;
            this.Txt_Cod_NuevoP.Location = new System.Drawing.Point(99, 72);
            this.Txt_Cod_NuevoP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Txt_Cod_NuevoP.Name = "Txt_Cod_NuevoP";
            this.Txt_Cod_NuevoP.Size = new System.Drawing.Size(101, 29);
            this.Txt_Cod_NuevoP.TabIndex = 15;
            this.Txt_Cod_NuevoP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txt_NomProd
            // 
            this.txt_NomProd.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_NomProd.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_NomProd.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_NomProd.HintForeColor = System.Drawing.Color.Empty;
            this.txt_NomProd.HintText = "";
            this.txt_NomProd.isPassword = false;
            this.txt_NomProd.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_NomProd.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_NomProd.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_NomProd.LineThickness = 3;
            this.txt_NomProd.Location = new System.Drawing.Point(213, 115);
            this.txt_NomProd.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_NomProd.Name = "txt_NomProd";
            this.txt_NomProd.Size = new System.Drawing.Size(194, 29);
            this.txt_NomProd.TabIndex = 22;
            this.txt_NomProd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_NomProd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_NomProd_KeyPress);
            // 
            // Lb_Cod_NP
            // 
            this.Lb_Cod_NP.AutoSize = true;
            this.Lb_Cod_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Cod_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Cod_NP.Location = new System.Drawing.Point(26, 78);
            this.Lb_Cod_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Cod_NP.Name = "Lb_Cod_NP";
            this.Lb_Cod_NP.Size = new System.Drawing.Size(75, 18);
            this.Lb_Cod_NP.TabIndex = 23;
            this.Lb_Cod_NP.Text = "Código :";
            // 
            // Lb_NomProd_NP
            // 
            this.Lb_NomProd_NP.AutoSize = true;
            this.Lb_NomProd_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_NomProd_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_NomProd_NP.Location = new System.Drawing.Point(26, 122);
            this.Lb_NomProd_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_NomProd_NP.Name = "Lb_NomProd_NP";
            this.Lb_NomProd_NP.Size = new System.Drawing.Size(191, 18);
            this.Lb_NomProd_NP.TabIndex = 24;
            this.Lb_NomProd_NP.Text = "Nombre del Producto :";
            // 
            // Marca_NP
            // 
            this.Marca_NP.AutoSize = true;
            this.Marca_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Marca_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Marca_NP.Location = new System.Drawing.Point(26, 162);
            this.Marca_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Marca_NP.Name = "Marca_NP";
            this.Marca_NP.Size = new System.Drawing.Size(66, 18);
            this.Marca_NP.TabIndex = 25;
            this.Marca_NP.Text = "Marca :";
            // 
            // txt_Marca
            // 
            this.txt_Marca.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Marca.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Marca.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Marca.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Marca.HintText = "";
            this.txt_Marca.isPassword = false;
            this.txt_Marca.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Marca.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Marca.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Marca.LineThickness = 3;
            this.txt_Marca.Location = new System.Drawing.Point(87, 155);
            this.txt_Marca.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_Marca.Name = "txt_Marca";
            this.txt_Marca.Size = new System.Drawing.Size(154, 29);
            this.txt_Marca.TabIndex = 26;
            this.txt_Marca.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Marca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Marca_KeyPress);
            // 
            // Lb_PrecioVts_NP
            // 
            this.Lb_PrecioVts_NP.AutoSize = true;
            this.Lb_PrecioVts_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_PrecioVts_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_PrecioVts_NP.Location = new System.Drawing.Point(248, 163);
            this.Lb_PrecioVts_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_PrecioVts_NP.Name = "Lb_PrecioVts_NP";
            this.Lb_PrecioVts_NP.Size = new System.Drawing.Size(68, 18);
            this.Lb_PrecioVts_NP.TabIndex = 30;
            this.Lb_PrecioVts_NP.Text = "Precio :";
            // 
            // txt_PrecioVts_NP
            // 
            this.txt_PrecioVts_NP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_PrecioVts_NP.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PrecioVts_NP.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_PrecioVts_NP.HintForeColor = System.Drawing.Color.Empty;
            this.txt_PrecioVts_NP.HintText = "";
            this.txt_PrecioVts_NP.isPassword = false;
            this.txt_PrecioVts_NP.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_PrecioVts_NP.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_PrecioVts_NP.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_PrecioVts_NP.LineThickness = 3;
            this.txt_PrecioVts_NP.Location = new System.Drawing.Point(312, 155);
            this.txt_PrecioVts_NP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_PrecioVts_NP.Name = "txt_PrecioVts_NP";
            this.txt_PrecioVts_NP.Size = new System.Drawing.Size(112, 29);
            this.txt_PrecioVts_NP.TabIndex = 29;
            this.txt_PrecioVts_NP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_PrecioVts_NP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_PrecioVts_NP_KeyPress);
            // 
            // Lb_Categ_NP
            // 
            this.Lb_Categ_NP.AutoSize = true;
            this.Lb_Categ_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Categ_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Categ_NP.Location = new System.Drawing.Point(26, 205);
            this.Lb_Categ_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Categ_NP.Name = "Lb_Categ_NP";
            this.Lb_Categ_NP.Size = new System.Drawing.Size(95, 18);
            this.Lb_Categ_NP.TabIndex = 32;
            this.Lb_Categ_NP.Text = "Categoria :";
            // 
            // PSuperior_NP
            // 
            this.PSuperior_NP.BackColor = System.Drawing.Color.Gainsboro;
            this.PSuperior_NP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PSuperior_NP.BackgroundImage")));
            this.PSuperior_NP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PSuperior_NP.Controls.Add(this.Lb_Nuevo_Prod);
            this.PSuperior_NP.Controls.Add(this.Logo);
            this.PSuperior_NP.Controls.Add(this.Salir);
            this.PSuperior_NP.Dock = System.Windows.Forms.DockStyle.Top;
            this.PSuperior_NP.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.PSuperior_NP.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.PSuperior_NP.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.PSuperior_NP.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.PSuperior_NP.Location = new System.Drawing.Point(0, 0);
            this.PSuperior_NP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PSuperior_NP.Name = "PSuperior_NP";
            this.PSuperior_NP.Quality = 10;
            this.PSuperior_NP.Size = new System.Drawing.Size(620, 41);
            this.PSuperior_NP.TabIndex = 35;
            // 
            // Lb_Nuevo_Prod
            // 
            this.Lb_Nuevo_Prod.AutoSize = true;
            this.Lb_Nuevo_Prod.BackColor = System.Drawing.Color.Transparent;
            this.Lb_Nuevo_Prod.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nuevo_Prod.ForeColor = System.Drawing.Color.Gainsboro;
            this.Lb_Nuevo_Prod.Location = new System.Drawing.Point(50, 14);
            this.Lb_Nuevo_Prod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Nuevo_Prod.Name = "Lb_Nuevo_Prod";
            this.Lb_Nuevo_Prod.Size = new System.Drawing.Size(143, 18);
            this.Lb_Nuevo_Prod.TabIndex = 10;
            this.Lb_Nuevo_Prod.Text = "Nuevo Producto";
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(4, 1);
            this.Logo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(38, 39);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Logo.TabIndex = 9;
            this.Logo.TabStop = false;
            // 
            // Salir
            // 
            this.Salir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Salir.BackColor = System.Drawing.Color.Transparent;
            this.Salir.ErrorImage = null;
            this.Salir.Image = ((System.Drawing.Image)(resources.GetObject("Salir.Image")));
            this.Salir.Location = new System.Drawing.Point(586, 7);
            this.Salir.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Salir.Name = "Salir";
            this.Salir.Size = new System.Drawing.Size(22, 25);
            this.Salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Salir.TabIndex = 8;
            this.Salir.TabStop = false;
            this.Salir.Click += new System.EventHandler(this.Salir_Click_1);
            // 
            // Mov_Panel_NP
            // 
            this.Mov_Panel_NP.AllowFormDragging = true;
            this.Mov_Panel_NP.AllowFormDropShadow = true;
            this.Mov_Panel_NP.AllowFormResizing = true;
            this.Mov_Panel_NP.AllowHidingBottomRegion = true;
            this.Mov_Panel_NP.AllowOpacityChangesWhileDragging = false;
            this.Mov_Panel_NP.BorderOptions.BottomBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Panel_NP.BorderOptions.BottomBorder.BorderThickness = 1;
            this.Mov_Panel_NP.BorderOptions.BottomBorder.ShowBorder = true;
            this.Mov_Panel_NP.BorderOptions.LeftBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Panel_NP.BorderOptions.LeftBorder.BorderThickness = 1;
            this.Mov_Panel_NP.BorderOptions.LeftBorder.ShowBorder = true;
            this.Mov_Panel_NP.BorderOptions.RightBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Panel_NP.BorderOptions.RightBorder.BorderThickness = 1;
            this.Mov_Panel_NP.BorderOptions.RightBorder.ShowBorder = true;
            this.Mov_Panel_NP.BorderOptions.TopBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Panel_NP.BorderOptions.TopBorder.BorderThickness = 1;
            this.Mov_Panel_NP.BorderOptions.TopBorder.ShowBorder = true;
            this.Mov_Panel_NP.ContainerControl = this;
            this.Mov_Panel_NP.DockingIndicatorsColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(215)))), ((int)(((byte)(233)))));
            this.Mov_Panel_NP.DockingIndicatorsOpacity = 0.5D;
            this.Mov_Panel_NP.DockingOptions.DockAll = true;
            this.Mov_Panel_NP.DockingOptions.DockBottomLeft = true;
            this.Mov_Panel_NP.DockingOptions.DockBottomRight = true;
            this.Mov_Panel_NP.DockingOptions.DockFullScreen = true;
            this.Mov_Panel_NP.DockingOptions.DockLeft = true;
            this.Mov_Panel_NP.DockingOptions.DockRight = true;
            this.Mov_Panel_NP.DockingOptions.DockTopLeft = true;
            this.Mov_Panel_NP.DockingOptions.DockTopRight = true;
            this.Mov_Panel_NP.FormDraggingOpacity = 0.9D;
            this.Mov_Panel_NP.ParentForm = this;
            this.Mov_Panel_NP.ShowCursorChanges = true;
            this.Mov_Panel_NP.ShowDockingIndicators = true;
            this.Mov_Panel_NP.TitleBarOptions.AllowFormDragging = true;
            this.Mov_Panel_NP.TitleBarOptions.BunifuFormDock = this.Mov_Panel_NP;
            this.Mov_Panel_NP.TitleBarOptions.DoubleClickToExpandWindow = true;
            this.Mov_Panel_NP.TitleBarOptions.TitleBarControl = this.PSuperior_NP;
            this.Mov_Panel_NP.TitleBarOptions.UseBackColorOnDockingIndicators = false;
            // 
            // Txt__Guardar_NP
            // 
            this.Txt__Guardar_NP.BackColor = System.Drawing.Color.Transparent;
            this.Txt__Guardar_NP.Image = ((System.Drawing.Image)(resources.GetObject("Txt__Guardar_NP.Image")));
            this.Txt__Guardar_NP.ImageActive = null;
            this.Txt__Guardar_NP.Location = new System.Drawing.Point(468, 297);
            this.Txt__Guardar_NP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Txt__Guardar_NP.Name = "Txt__Guardar_NP";
            this.Txt__Guardar_NP.Size = new System.Drawing.Size(46, 49);
            this.Txt__Guardar_NP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Txt__Guardar_NP.TabIndex = 37;
            this.Txt__Guardar_NP.TabStop = false;
            this.Txt__Guardar_NP.Zoom = 10;
            // 
            // Btn_Cancelar_Cate
            // 
            this.Btn_Cancelar_Cate.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Cancelar_Cate.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Cancelar_Cate.Image")));
            this.Btn_Cancelar_Cate.ImageActive = null;
            this.Btn_Cancelar_Cate.Location = new System.Drawing.Point(544, 297);
            this.Btn_Cancelar_Cate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Cancelar_Cate.Name = "Btn_Cancelar_Cate";
            this.Btn_Cancelar_Cate.Size = new System.Drawing.Size(46, 49);
            this.Btn_Cancelar_Cate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Cancelar_Cate.TabIndex = 36;
            this.Btn_Cancelar_Cate.TabStop = false;
            this.Btn_Cancelar_Cate.Zoom = 10;
            this.Btn_Cancelar_Cate.Click += new System.EventHandler(this.Btn_Cancelar_Cate_Click);
            // 
            // ComboBox_Und_Medida_NP
            // 
            this.ComboBox_Und_Medida_NP.BackColor = System.Drawing.Color.Transparent;
            this.ComboBox_Und_Medida_NP.BaseColor = System.Drawing.Color.Gainsboro;
            this.ComboBox_Und_Medida_NP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.ComboBox_Und_Medida_NP.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ComboBox_Und_Medida_NP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Und_Medida_NP.FocusedColor = System.Drawing.Color.LightGray;
            this.ComboBox_Und_Medida_NP.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBox_Und_Medida_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.ComboBox_Und_Medida_NP.FormattingEnabled = true;
            this.ComboBox_Und_Medida_NP.Items.AddRange(new object[] {
            "Libras (Lb.)",
            "Litro (Lt.)",
            "Yarda (Yd.)",
            "Unidad (Und.)"});
            this.ComboBox_Und_Medida_NP.Location = new System.Drawing.Point(472, 77);
            this.ComboBox_Und_Medida_NP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ComboBox_Und_Medida_NP.Name = "ComboBox_Und_Medida_NP";
            this.ComboBox_Und_Medida_NP.OnHoverItemBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.ComboBox_Und_Medida_NP.OnHoverItemForeColor = System.Drawing.Color.Gainsboro;
            this.ComboBox_Und_Medida_NP.Radius = 9;
            this.ComboBox_Und_Medida_NP.Size = new System.Drawing.Size(133, 25);
            this.ComboBox_Und_Medida_NP.TabIndex = 40;
            // 
            // Lb_Und_Med_NP
            // 
            this.Lb_Und_Med_NP.AutoSize = true;
            this.Lb_Und_Med_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Und_Med_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Und_Med_NP.Location = new System.Drawing.Point(312, 82);
            this.Lb_Und_Med_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Und_Med_NP.Name = "Lb_Und_Med_NP";
            this.Lb_Und_Med_NP.Size = new System.Drawing.Size(159, 18);
            this.Lb_Und_Med_NP.TabIndex = 41;
            this.Lb_Und_Med_NP.Text = "Unidad de Medida :";
            // 
            // Lb_Proveedor_NP
            // 
            this.Lb_Proveedor_NP.AutoSize = true;
            this.Lb_Proveedor_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Proveedor_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Proveedor_NP.Location = new System.Drawing.Point(274, 205);
            this.Lb_Proveedor_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Proveedor_NP.Name = "Lb_Proveedor_NP";
            this.Lb_Proveedor_NP.Size = new System.Drawing.Size(103, 18);
            this.Lb_Proveedor_NP.TabIndex = 43;
            this.Lb_Proveedor_NP.Text = "Proveedor :";
            // 
            // Lb_Fecha_Regis_NP
            // 
            this.Lb_Fecha_Regis_NP.AutoSize = true;
            this.Lb_Fecha_Regis_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Fecha_Regis_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Fecha_Regis_NP.Location = new System.Drawing.Point(26, 248);
            this.Lb_Fecha_Regis_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Fecha_Regis_NP.Name = "Lb_Fecha_Regis_NP";
            this.Lb_Fecha_Regis_NP.Size = new System.Drawing.Size(163, 18);
            this.Lb_Fecha_Regis_NP.TabIndex = 45;
            this.Lb_Fecha_Regis_NP.Text = "Fecha de Registro :";
            // 
            // txt_Fecha_Regis_NP
            // 
            this.txt_Fecha_Regis_NP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Fecha_Regis_NP.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Fecha_Regis_NP.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Fecha_Regis_NP.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Fecha_Regis_NP.HintText = "";
            this.txt_Fecha_Regis_NP.isPassword = false;
            this.txt_Fecha_Regis_NP.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Fecha_Regis_NP.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Fecha_Regis_NP.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Fecha_Regis_NP.LineThickness = 3;
            this.txt_Fecha_Regis_NP.Location = new System.Drawing.Point(183, 240);
            this.txt_Fecha_Regis_NP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_Fecha_Regis_NP.Name = "txt_Fecha_Regis_NP";
            this.txt_Fecha_Regis_NP.Size = new System.Drawing.Size(106, 29);
            this.txt_Fecha_Regis_NP.TabIndex = 44;
            this.txt_Fecha_Regis_NP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Fecha_Regis_NP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Fecha_Regis_NP_KeyPress);
            // 
            // Lb_Fecha_Ven_NP
            // 
            this.Lb_Fecha_Ven_NP.AutoSize = true;
            this.Lb_Fecha_Ven_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Fecha_Ven_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Fecha_Ven_NP.Location = new System.Drawing.Point(296, 248);
            this.Lb_Fecha_Ven_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Fecha_Ven_NP.Name = "Lb_Fecha_Ven_NP";
            this.Lb_Fecha_Ven_NP.Size = new System.Drawing.Size(167, 18);
            this.Lb_Fecha_Ven_NP.TabIndex = 47;
            this.Lb_Fecha_Ven_NP.Text = "Fecha Vencimiento :";
            // 
            // txt_Fecha_Ven_NP
            // 
            this.txt_Fecha_Ven_NP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Fecha_Ven_NP.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Fecha_Ven_NP.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Fecha_Ven_NP.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Fecha_Ven_NP.HintText = "";
            this.txt_Fecha_Ven_NP.isPassword = false;
            this.txt_Fecha_Ven_NP.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Fecha_Ven_NP.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Fecha_Ven_NP.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Fecha_Ven_NP.LineThickness = 3;
            this.txt_Fecha_Ven_NP.Location = new System.Drawing.Point(462, 240);
            this.txt_Fecha_Ven_NP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_Fecha_Ven_NP.Name = "txt_Fecha_Ven_NP";
            this.txt_Fecha_Ven_NP.Size = new System.Drawing.Size(102, 29);
            this.txt_Fecha_Ven_NP.TabIndex = 46;
            this.txt_Fecha_Ven_NP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Fecha_Ven_NP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Fecha_Ven_NP_KeyPress);
            // 
            // Txt_Descrip_NP
            // 
            this.Txt_Descrip_NP.AcceptsReturn = false;
            this.Txt_Descrip_NP.AcceptsTab = false;
            this.Txt_Descrip_NP.AnimationSpeed = 200;
            this.Txt_Descrip_NP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Descrip_NP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Descrip_NP.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Descrip_NP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Descrip_NP.BackgroundImage")));
            this.Txt_Descrip_NP.BorderColorActive = System.Drawing.Color.Transparent;
            this.Txt_Descrip_NP.BorderColorDisabled = System.Drawing.Color.Gainsboro;
            this.Txt_Descrip_NP.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Descrip_NP.BorderColorIdle = System.Drawing.Color.Gainsboro;
            this.Txt_Descrip_NP.BorderRadius = 9;
            this.Txt_Descrip_NP.BorderThickness = 2;
            this.Txt_Descrip_NP.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Descrip_NP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Descrip_NP.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Descrip_NP.DefaultText = "";
            this.Txt_Descrip_NP.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Descrip_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Descrip_NP.HideSelection = true;
            this.Txt_Descrip_NP.IconLeft = null;
            this.Txt_Descrip_NP.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Descrip_NP.IconPadding = 10;
            this.Txt_Descrip_NP.IconRight = null;
            this.Txt_Descrip_NP.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Descrip_NP.Lines = new string[0];
            this.Txt_Descrip_NP.Location = new System.Drawing.Point(145, 295);
            this.Txt_Descrip_NP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Txt_Descrip_NP.MaxLength = 32767;
            this.Txt_Descrip_NP.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Descrip_NP.Modified = false;
            this.Txt_Descrip_NP.Multiline = true;
            this.Txt_Descrip_NP.Name = "Txt_Descrip_NP";
            stateProperties1.BorderColor = System.Drawing.Color.Transparent;
            stateProperties1.FillColor = System.Drawing.Color.Empty;
            stateProperties1.ForeColor = System.Drawing.Color.Empty;
            stateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Descrip_NP.OnActiveState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.Gainsboro;
            stateProperties2.FillColor = System.Drawing.Color.White;
            stateProperties2.ForeColor = System.Drawing.Color.Empty;
            stateProperties2.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Descrip_NP.OnDisabledState = stateProperties2;
            stateProperties3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties3.FillColor = System.Drawing.Color.Empty;
            stateProperties3.ForeColor = System.Drawing.Color.Empty;
            stateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Descrip_NP.OnHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.Gainsboro;
            stateProperties4.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Descrip_NP.OnIdleState = stateProperties4;
            this.Txt_Descrip_NP.PasswordChar = '\0';
            this.Txt_Descrip_NP.PlaceholderForeColor = System.Drawing.Color.DarkGray;
            this.Txt_Descrip_NP.PlaceholderText = "Decripción del Producto";
            this.Txt_Descrip_NP.ReadOnly = false;
            this.Txt_Descrip_NP.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Descrip_NP.SelectedText = "";
            this.Txt_Descrip_NP.SelectionLength = 0;
            this.Txt_Descrip_NP.SelectionStart = 0;
            this.Txt_Descrip_NP.ShortcutsEnabled = true;
            this.Txt_Descrip_NP.Size = new System.Drawing.Size(292, 70);
            this.Txt_Descrip_NP.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Descrip_NP.TabIndex = 48;
            this.Txt_Descrip_NP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Descrip_NP.TextMarginBottom = 0;
            this.Txt_Descrip_NP.TextMarginLeft = 5;
            this.Txt_Descrip_NP.TextMarginTop = 0;
            this.Txt_Descrip_NP.TextPlaceholder = "Decripción del Producto";
            this.Txt_Descrip_NP.UseSystemPasswordChar = false;
            this.Txt_Descrip_NP.WordWrap = true;
            this.Txt_Descrip_NP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Descrip_NP_KeyPress);
            // 
            // Lb_Descripción_NP
            // 
            this.Lb_Descripción_NP.AutoSize = true;
            this.Lb_Descripción_NP.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Descripción_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Descripción_NP.Location = new System.Drawing.Point(26, 295);
            this.Lb_Descripción_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Descripción_NP.Name = "Lb_Descripción_NP";
            this.Lb_Descripción_NP.Size = new System.Drawing.Size(112, 18);
            this.Lb_Descripción_NP.TabIndex = 49;
            this.Lb_Descripción_NP.Text = "Descripción :";
            // 
            // Lb_Guardar_NP
            // 
            this.Lb_Guardar_NP.AutoSize = true;
            this.Lb_Guardar_NP.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Guardar_NP.Location = new System.Drawing.Point(459, 347);
            this.Lb_Guardar_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Guardar_NP.Name = "Lb_Guardar_NP";
            this.Lb_Guardar_NP.Size = new System.Drawing.Size(70, 17);
            this.Lb_Guardar_NP.TabIndex = 50;
            this.Lb_Guardar_NP.Text = "Guardar";
            // 
            // Lb_Cancelar_NP
            // 
            this.Lb_Cancelar_NP.AutoSize = true;
            this.Lb_Cancelar_NP.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Cancelar_NP.Location = new System.Drawing.Point(532, 347);
            this.Lb_Cancelar_NP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Cancelar_NP.Name = "Lb_Cancelar_NP";
            this.Lb_Cancelar_NP.Size = new System.Drawing.Size(75, 17);
            this.Lb_Cancelar_NP.TabIndex = 51;
            this.Lb_Cancelar_NP.Text = "Cancelar";
            // 
            // txt_Stock_NP
            // 
            this.txt_Stock_NP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Stock_NP.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Stock_NP.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Stock_NP.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Stock_NP.HintText = "";
            this.txt_Stock_NP.isPassword = false;
            this.txt_Stock_NP.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Stock_NP.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Stock_NP.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Stock_NP.LineThickness = 3;
            this.txt_Stock_NP.Location = new System.Drawing.Point(489, 155);
            this.txt_Stock_NP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_Stock_NP.Name = "txt_Stock_NP";
            this.txt_Stock_NP.Size = new System.Drawing.Size(101, 29);
            this.txt_Stock_NP.TabIndex = 33;
            this.txt_Stock_NP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Stock_NP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Stock_NP_KeyPress);
            // 
            // Lb_
            // 
            this.Lb_.AutoSize = true;
            this.Lb_.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_.Location = new System.Drawing.Point(430, 162);
            this.Lb_.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_.Name = "Lb_";
            this.Lb_.Size = new System.Drawing.Size(62, 18);
            this.Lb_.TabIndex = 34;
            this.Lb_.Text = "Stock :";
            // 
            // ComboBox_Prove_NPorv
            // 
            this.ComboBox_Prove_NPorv.BackColor = System.Drawing.Color.Transparent;
            this.ComboBox_Prove_NPorv.BaseColor = System.Drawing.Color.Gainsboro;
            this.ComboBox_Prove_NPorv.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.ComboBox_Prove_NPorv.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ComboBox_Prove_NPorv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Prove_NPorv.FocusedColor = System.Drawing.Color.LightGray;
            this.ComboBox_Prove_NPorv.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBox_Prove_NPorv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.ComboBox_Prove_NPorv.FormattingEnabled = true;
            this.ComboBox_Prove_NPorv.Location = new System.Drawing.Point(374, 199);
            this.ComboBox_Prove_NPorv.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ComboBox_Prove_NPorv.Name = "ComboBox_Prove_NPorv";
            this.ComboBox_Prove_NPorv.OnHoverItemBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.ComboBox_Prove_NPorv.OnHoverItemForeColor = System.Drawing.Color.Gainsboro;
            this.ComboBox_Prove_NPorv.Radius = 9;
            this.ComboBox_Prove_NPorv.Size = new System.Drawing.Size(133, 25);
            this.ComboBox_Prove_NPorv.TabIndex = 52;
            // 
            // ComboBox_Cate_NP
            // 
            this.ComboBox_Cate_NP.BackColor = System.Drawing.Color.Transparent;
            this.ComboBox_Cate_NP.BaseColor = System.Drawing.Color.Gainsboro;
            this.ComboBox_Cate_NP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.ComboBox_Cate_NP.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ComboBox_Cate_NP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Cate_NP.FocusedColor = System.Drawing.Color.LightGray;
            this.ComboBox_Cate_NP.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBox_Cate_NP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.ComboBox_Cate_NP.FormattingEnabled = true;
            this.ComboBox_Cate_NP.Location = new System.Drawing.Point(118, 199);
            this.ComboBox_Cate_NP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ComboBox_Cate_NP.Name = "ComboBox_Cate_NP";
            this.ComboBox_Cate_NP.OnHoverItemBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.ComboBox_Cate_NP.OnHoverItemForeColor = System.Drawing.Color.Gainsboro;
            this.ComboBox_Cate_NP.Radius = 9;
            this.ComboBox_Cate_NP.Size = new System.Drawing.Size(148, 25);
            this.ComboBox_Cate_NP.TabIndex = 53;
            // 
            // Nuevo_Producto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(620, 394);
            this.Controls.Add(this.ComboBox_Cate_NP);
            this.Controls.Add(this.ComboBox_Prove_NPorv);
            this.Controls.Add(this.Lb_Cancelar_NP);
            this.Controls.Add(this.Lb_Guardar_NP);
            this.Controls.Add(this.Lb_Descripción_NP);
            this.Controls.Add(this.Txt_Descrip_NP);
            this.Controls.Add(this.Lb_Fecha_Ven_NP);
            this.Controls.Add(this.txt_Fecha_Ven_NP);
            this.Controls.Add(this.Lb_Fecha_Regis_NP);
            this.Controls.Add(this.txt_Fecha_Regis_NP);
            this.Controls.Add(this.Lb_Proveedor_NP);
            this.Controls.Add(this.Lb_Und_Med_NP);
            this.Controls.Add(this.ComboBox_Und_Medida_NP);
            this.Controls.Add(this.Txt__Guardar_NP);
            this.Controls.Add(this.Btn_Cancelar_Cate);
            this.Controls.Add(this.PSuperior_NP);
            this.Controls.Add(this.Lb_);
            this.Controls.Add(this.txt_Stock_NP);
            this.Controls.Add(this.Lb_Categ_NP);
            this.Controls.Add(this.Lb_PrecioVts_NP);
            this.Controls.Add(this.txt_PrecioVts_NP);
            this.Controls.Add(this.txt_Marca);
            this.Controls.Add(this.Marca_NP);
            this.Controls.Add(this.Lb_NomProd_NP);
            this.Controls.Add(this.Lb_Cod_NP);
            this.Controls.Add(this.txt_NomProd);
            this.Controls.Add(this.Txt_Cod_NuevoP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Nuevo_Producto";
            this.Text = "Nuevo_Producto";
            this.Load += new System.EventHandler(this.Nuevo_Producto_Load);
            this.PSuperior_NP.ResumeLayout(false);
            this.PSuperior_NP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Salir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Txt__Guardar_NP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_Cate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuMaterialTextbox Txt_Cod_NuevoP;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_NomProd;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Cod_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_NomProd_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Marca_NP;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Marca;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_PrecioVts_NP;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_PrecioVts_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Categ_NP;
        private Bunifu.Framework.UI.BunifuGradientPanel PSuperior_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Nuevo_Prod;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.PictureBox Salir;
        private Bunifu.UI.WinForms.BunifuFormDock Mov_Panel_NP;
        private Bunifu.Framework.UI.BunifuImageButton Txt__Guardar_NP;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Cancelar_Cate;
        private Guna.UI.WinForms.GunaComboBox ComboBox_Und_Medida_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Und_Med_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Proveedor_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Fecha_Ven_NP;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Fecha_Ven_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Fecha_Regis_NP;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Fecha_Regis_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Descripción_NP;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Descrip_NP;
        private Guna.UI.WinForms.GunaLabel Lb_Guardar_NP;
        private Guna.UI.WinForms.GunaLabel Lb_Cancelar_NP;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Stock_NP;
        private Guna.UI.WinForms.GunaComboBox ComboBox_Prove_NPorv;
        private Guna.UI.WinForms.GunaComboBox ComboBox_Cate_NP;
    }
}