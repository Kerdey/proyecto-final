﻿namespace CONVO_II.Presentaciones
{
    partial class Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Clientes));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Lb_Editar_Cliente = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Nuevo_Cliente = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Buscar_Cliente = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Eliminar_Cliente = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Guardar_Cliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Nuevo_Cliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Eliminar_Cliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Editar_Cliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Lb_ListClientes_ = new Guna.UI.WinForms.GunaLabel();
            this.Txt_Buscar_Cliente = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Tabla_Clientes = new Bunifu.UI.WinForms.BunifuDataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Clientes)).BeginInit();
            this.SuspendLayout();
            // 
            // Lb_Editar_Cliente
            // 
            this.Lb_Editar_Cliente.AutoSize = true;
            this.Lb_Editar_Cliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Editar_Cliente.Location = new System.Drawing.Point(774, 107);
            this.Lb_Editar_Cliente.Name = "Lb_Editar_Cliente";
            this.Lb_Editar_Cliente.Size = new System.Drawing.Size(67, 22);
            this.Lb_Editar_Cliente.TabIndex = 101;
            this.Lb_Editar_Cliente.Text = "Editar";
            // 
            // Lb_Nuevo_Cliente
            // 
            this.Lb_Nuevo_Cliente.AutoSize = true;
            this.Lb_Nuevo_Cliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nuevo_Cliente.Location = new System.Drawing.Point(680, 107);
            this.Lb_Nuevo_Cliente.Name = "Lb_Nuevo_Cliente";
            this.Lb_Nuevo_Cliente.Size = new System.Drawing.Size(73, 22);
            this.Lb_Nuevo_Cliente.TabIndex = 100;
            this.Lb_Nuevo_Cliente.Text = "Nuevo";
            // 
            // Lb_Buscar_Cliente
            // 
            this.Lb_Buscar_Cliente.AutoSize = true;
            this.Lb_Buscar_Cliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Buscar_Cliente.Location = new System.Drawing.Point(426, 107);
            this.Lb_Buscar_Cliente.Name = "Lb_Buscar_Cliente";
            this.Lb_Buscar_Cliente.Size = new System.Drawing.Size(75, 22);
            this.Lb_Buscar_Cliente.TabIndex = 99;
            this.Lb_Buscar_Cliente.Text = "Buscar";
            // 
            // Lb_Eliminar_Cliente
            // 
            this.Lb_Eliminar_Cliente.AutoSize = true;
            this.Lb_Eliminar_Cliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Eliminar_Cliente.Location = new System.Drawing.Point(856, 107);
            this.Lb_Eliminar_Cliente.Name = "Lb_Eliminar_Cliente";
            this.Lb_Eliminar_Cliente.Size = new System.Drawing.Size(88, 22);
            this.Lb_Eliminar_Cliente.TabIndex = 98;
            this.Lb_Eliminar_Cliente.Text = "Eliminar";
            // 
            // Btn_Guardar_Cliente
            // 
            this.Btn_Guardar_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Guardar_Cliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Guardar_Cliente.Image")));
            this.Btn_Guardar_Cliente.ImageActive = null;
            this.Btn_Guardar_Cliente.Location = new System.Drawing.Point(434, 42);
            this.Btn_Guardar_Cliente.Name = "Btn_Guardar_Cliente";
            this.Btn_Guardar_Cliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Guardar_Cliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Guardar_Cliente.TabIndex = 97;
            this.Btn_Guardar_Cliente.TabStop = false;
            this.Btn_Guardar_Cliente.Zoom = 10;
            // 
            // Btn_Nuevo_Cliente
            // 
            this.Btn_Nuevo_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Nuevo_Cliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Nuevo_Cliente.Image")));
            this.Btn_Nuevo_Cliente.ImageActive = null;
            this.Btn_Nuevo_Cliente.Location = new System.Drawing.Point(687, 42);
            this.Btn_Nuevo_Cliente.Name = "Btn_Nuevo_Cliente";
            this.Btn_Nuevo_Cliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Nuevo_Cliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Nuevo_Cliente.TabIndex = 96;
            this.Btn_Nuevo_Cliente.TabStop = false;
            this.Btn_Nuevo_Cliente.Zoom = 10;
            // 
            // Btn_Eliminar_Cliente
            // 
            this.Btn_Eliminar_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Eliminar_Cliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar_Cliente.Image")));
            this.Btn_Eliminar_Cliente.ImageActive = null;
            this.Btn_Eliminar_Cliente.Location = new System.Drawing.Point(868, 42);
            this.Btn_Eliminar_Cliente.Name = "Btn_Eliminar_Cliente";
            this.Btn_Eliminar_Cliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Eliminar_Cliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Eliminar_Cliente.TabIndex = 95;
            this.Btn_Eliminar_Cliente.TabStop = false;
            this.Btn_Eliminar_Cliente.Zoom = 10;
            // 
            // Btn_Editar_Cliente
            // 
            this.Btn_Editar_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Editar_Cliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Editar_Cliente.Image")));
            this.Btn_Editar_Cliente.ImageActive = null;
            this.Btn_Editar_Cliente.Location = new System.Drawing.Point(777, 42);
            this.Btn_Editar_Cliente.Name = "Btn_Editar_Cliente";
            this.Btn_Editar_Cliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Editar_Cliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Editar_Cliente.TabIndex = 94;
            this.Btn_Editar_Cliente.TabStop = false;
            this.Btn_Editar_Cliente.Zoom = 10;
            // 
            // Lb_ListClientes_
            // 
            this.Lb_ListClientes_.AutoSize = true;
            this.Lb_ListClientes_.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_ListClientes_.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_ListClientes_.Location = new System.Drawing.Point(35, 162);
            this.Lb_ListClientes_.Name = "Lb_ListClientes_";
            this.Lb_ListClientes_.Size = new System.Drawing.Size(207, 27);
            this.Lb_ListClientes_.TabIndex = 93;
            this.Lb_ListClientes_.Text = "Lista de Clientes";
            // 
            // Txt_Buscar_Cliente
            // 
            this.Txt_Buscar_Cliente.AcceptsReturn = false;
            this.Txt_Buscar_Cliente.AcceptsTab = false;
            this.Txt_Buscar_Cliente.AnimationSpeed = 200;
            this.Txt_Buscar_Cliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Buscar_Cliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Buscar_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Buscar_Cliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cliente.BackgroundImage")));
            this.Txt_Buscar_Cliente.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Cliente.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Buscar_Cliente.BorderColorHover = System.Drawing.Color.DimGray;
            this.Txt_Buscar_Cliente.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Cliente.BorderRadius = 9;
            this.Txt_Buscar_Cliente.BorderThickness = 3;
            this.Txt_Buscar_Cliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Buscar_Cliente.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cliente.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buscar_Cliente.DefaultText = "";
            this.Txt_Buscar_Cliente.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Buscar_Cliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Cliente.HideSelection = true;
            this.Txt_Buscar_Cliente.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cliente.IconLeft")));
            this.Txt_Buscar_Cliente.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cliente.IconPadding = 10;
            this.Txt_Buscar_Cliente.IconRight = null;
            this.Txt_Buscar_Cliente.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cliente.Lines = new string[0];
            this.Txt_Buscar_Cliente.Location = new System.Drawing.Point(40, 50);
            this.Txt_Buscar_Cliente.MaxLength = 32767;
            this.Txt_Buscar_Cliente.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Buscar_Cliente.Modified = false;
            this.Txt_Buscar_Cliente.Multiline = false;
            this.Txt_Buscar_Cliente.Name = "Txt_Buscar_Cliente";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties1.FillColor = System.Drawing.Color.Empty;
            stateProperties1.ForeColor = System.Drawing.Color.Empty;
            stateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cliente.OnActiveState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.Empty;
            stateProperties2.FillColor = System.Drawing.Color.White;
            stateProperties2.ForeColor = System.Drawing.Color.Empty;
            stateProperties2.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cliente.OnDisabledState = stateProperties2;
            stateProperties3.BorderColor = System.Drawing.Color.DimGray;
            stateProperties3.FillColor = System.Drawing.Color.Empty;
            stateProperties3.ForeColor = System.Drawing.Color.Empty;
            stateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cliente.OnHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties4.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cliente.OnIdleState = stateProperties4;
            this.Txt_Buscar_Cliente.PasswordChar = '\0';
            this.Txt_Buscar_Cliente.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cliente.PlaceholderText = "Buscar";
            this.Txt_Buscar_Cliente.ReadOnly = false;
            this.Txt_Buscar_Cliente.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Buscar_Cliente.SelectedText = "";
            this.Txt_Buscar_Cliente.SelectionLength = 0;
            this.Txt_Buscar_Cliente.SelectionStart = 0;
            this.Txt_Buscar_Cliente.ShortcutsEnabled = true;
            this.Txt_Buscar_Cliente.Size = new System.Drawing.Size(357, 52);
            this.Txt_Buscar_Cliente.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Buscar_Cliente.TabIndex = 92;
            this.Txt_Buscar_Cliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Buscar_Cliente.TextMarginBottom = 0;
            this.Txt_Buscar_Cliente.TextMarginLeft = 5;
            this.Txt_Buscar_Cliente.TextMarginTop = 0;
            this.Txt_Buscar_Cliente.TextPlaceholder = "Buscar";
            this.Txt_Buscar_Cliente.UseSystemPasswordChar = false;
            this.Txt_Buscar_Cliente.WordWrap = true;
            // 
            // Tabla_Clientes
            // 
            this.Tabla_Clientes.AllowCustomTheming = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Clientes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Tabla_Clientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Tabla_Clientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Tabla_Clientes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.Tabla_Clientes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Tabla_Clientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Tabla_Clientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Tabla_Clientes.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.Tabla_Clientes.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Clientes.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Clientes.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Clientes.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.Tabla_Clientes.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.Tabla_Clientes.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Clientes.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Clientes.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Clientes.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.Tabla_Clientes.CurrentTheme.HeaderStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            this.Tabla_Clientes.CurrentTheme.HeaderStyle.SelectionForeColor = System.Drawing.Color.White;
            this.Tabla_Clientes.CurrentTheme.Name = null;
            this.Tabla_Clientes.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.Tabla_Clientes.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Clientes.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Clientes.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Clientes.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Tabla_Clientes.DefaultCellStyle = dataGridViewCellStyle3;
            this.Tabla_Clientes.EnableHeadersVisualStyles = false;
            this.Tabla_Clientes.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Clientes.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Clientes.HeaderBgColor = System.Drawing.Color.Empty;
            this.Tabla_Clientes.HeaderForeColor = System.Drawing.Color.White;
            this.Tabla_Clientes.Location = new System.Drawing.Point(40, 219);
            this.Tabla_Clientes.Name = "Tabla_Clientes";
            this.Tabla_Clientes.RowHeadersVisible = false;
            this.Tabla_Clientes.RowHeadersWidth = 51;
            this.Tabla_Clientes.RowTemplate.Height = 40;
            this.Tabla_Clientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Tabla_Clientes.Size = new System.Drawing.Size(864, 479);
            this.Tabla_Clientes.TabIndex = 102;
            this.Tabla_Clientes.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            // 
            // Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1002, 727);
            this.Controls.Add(this.Tabla_Clientes);
            this.Controls.Add(this.Lb_Editar_Cliente);
            this.Controls.Add(this.Lb_Nuevo_Cliente);
            this.Controls.Add(this.Lb_Buscar_Cliente);
            this.Controls.Add(this.Lb_Eliminar_Cliente);
            this.Controls.Add(this.Btn_Guardar_Cliente);
            this.Controls.Add(this.Btn_Nuevo_Cliente);
            this.Controls.Add(this.Btn_Eliminar_Cliente);
            this.Controls.Add(this.Btn_Editar_Cliente);
            this.Controls.Add(this.Lb_ListClientes_);
            this.Controls.Add(this.Txt_Buscar_Cliente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Clientes";
            this.Text = "Clientes";
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Clientes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI.WinForms.GunaLabel Lb_Editar_Cliente;
        private Guna.UI.WinForms.GunaLabel Lb_Nuevo_Cliente;
        private Guna.UI.WinForms.GunaLabel Lb_Buscar_Cliente;
        private Guna.UI.WinForms.GunaLabel Lb_Eliminar_Cliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Guardar_Cliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Nuevo_Cliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Eliminar_Cliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Editar_Cliente;
        private Guna.UI.WinForms.GunaLabel Lb_ListClientes_;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Buscar_Cliente;
        private Bunifu.UI.WinForms.BunifuDataGridView Tabla_Clientes;
    }
}