﻿using Pulpería_Lovo.CAPA_DATOS.Coneccion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pulpería_Lovo.Presentaciones
{
    public partial class Cátalogo_Prod : Form
    {
        public Cátalogo_Prod()
        {
            InitializeComponent();
        }

        private void Catalogo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Btn_Nuevo_Cáta_Click(object sender, EventArgs e)
        {
            Form NP = new Nuevo_Producto();
            NP.Show();
        }

        private void Cátalogo_Prod_Load(object sender, EventArgs e)
        {
            Productos pd = new Productos();
            dgv_Productos.DataSource = pd.ListProd();
           // dgv_Productos.AutoGenerateColumns = true;
        }
    }
}
