﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pulpería_Lovo.Presentaciones
{
    public partial class Notificaciones : Form
    {
        public Notificaciones()
        {
            InitializeComponent();
        }

        private void Btn_Ok_Correcto_Noti_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btn_Ok_Correcto_Noti_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Dispose();
            }
        }
    }
}
