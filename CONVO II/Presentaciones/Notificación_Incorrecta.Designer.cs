﻿namespace CONVO_II.Presentaciones
{
    partial class Notificación_Incorrecta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Notificación_Incorrecta));
            this.Mov_Noti_Incorrecta = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.Redondear_Noti_Inco = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.Chek_Correcto_Noti_Inco = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.Lb_Correcto_Noti_Inco = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Ok_Noti_Inco = new Guna.UI.WinForms.GunaGradientButton();
            ((System.ComponentModel.ISupportInitialize)(this.Chek_Correcto_Noti_Inco)).BeginInit();
            this.SuspendLayout();
            // 
            // Mov_Noti_Incorrecta
            // 
            this.Mov_Noti_Incorrecta.Fixed = true;
            this.Mov_Noti_Incorrecta.Horizontal = true;
            this.Mov_Noti_Incorrecta.TargetControl = this;
            this.Mov_Noti_Incorrecta.Vertical = true;
            // 
            // Redondear_Noti_Inco
            // 
            this.Redondear_Noti_Inco.ElipseRadius = 20;
            this.Redondear_Noti_Inco.TargetControl = this;
            // 
            // Chek_Correcto_Noti_Inco
            // 
            this.Chek_Correcto_Noti_Inco.AllowFocused = false;
            this.Chek_Correcto_Noti_Inco.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Chek_Correcto_Noti_Inco.BackColor = System.Drawing.Color.Transparent;
            this.Chek_Correcto_Noti_Inco.BorderRadius = 50;
            this.Chek_Correcto_Noti_Inco.Image = ((System.Drawing.Image)(resources.GetObject("Chek_Correcto_Noti_Inco.Image")));
            this.Chek_Correcto_Noti_Inco.IsCircle = true;
            this.Chek_Correcto_Noti_Inco.Location = new System.Drawing.Point(76, 20);
            this.Chek_Correcto_Noti_Inco.Name = "Chek_Correcto_Noti_Inco";
            this.Chek_Correcto_Noti_Inco.Size = new System.Drawing.Size(175, 175);
            this.Chek_Correcto_Noti_Inco.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Chek_Correcto_Noti_Inco.TabIndex = 1;
            this.Chek_Correcto_Noti_Inco.TabStop = false;
            this.Chek_Correcto_Noti_Inco.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            // 
            // Lb_Correcto_Noti_Inco
            // 
            this.Lb_Correcto_Noti_Inco.AutoSize = true;
            this.Lb_Correcto_Noti_Inco.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Correcto_Noti_Inco.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Correcto_Noti_Inco.Location = new System.Drawing.Point(51, 210);
            this.Lb_Correcto_Noti_Inco.Name = "Lb_Correcto_Noti_Inco";
            this.Lb_Correcto_Noti_Inco.Size = new System.Drawing.Size(225, 27);
            this.Lb_Correcto_Noti_Inco.TabIndex = 4;
            this.Lb_Correcto_Noti_Inco.Text = "Acción Incorrecta";
            // 
            // Btn_Ok_Noti_Inco
            // 
            this.Btn_Ok_Noti_Inco.AnimationHoverSpeed = 0.07F;
            this.Btn_Ok_Noti_Inco.AnimationSpeed = 0.03F;
            this.Btn_Ok_Noti_Inco.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Ok_Noti_Inco.BaseColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Ok_Noti_Inco.BaseColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.Btn_Ok_Noti_Inco.BorderColor = System.Drawing.Color.Black;
            this.Btn_Ok_Noti_Inco.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Btn_Ok_Noti_Inco.FocusedColor = System.Drawing.Color.Empty;
            this.Btn_Ok_Noti_Inco.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ok_Noti_Inco.ForeColor = System.Drawing.Color.White;
            this.Btn_Ok_Noti_Inco.Image = null;
            this.Btn_Ok_Noti_Inco.ImageSize = new System.Drawing.Size(20, 20);
            this.Btn_Ok_Noti_Inco.Location = new System.Drawing.Point(83, 260);
            this.Btn_Ok_Noti_Inco.Name = "Btn_Ok_Noti_Inco";
            this.Btn_Ok_Noti_Inco.OnHoverBaseColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Ok_Noti_Inco.OnHoverBaseColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.Btn_Ok_Noti_Inco.OnHoverBorderColor = System.Drawing.Color.Black;
            this.Btn_Ok_Noti_Inco.OnHoverForeColor = System.Drawing.Color.White;
            this.Btn_Ok_Noti_Inco.OnHoverImage = null;
            this.Btn_Ok_Noti_Inco.OnPressedColor = System.Drawing.Color.Black;
            this.Btn_Ok_Noti_Inco.Radius = 10;
            this.Btn_Ok_Noti_Inco.Size = new System.Drawing.Size(160, 42);
            this.Btn_Ok_Noti_Inco.TabIndex = 3;
            this.Btn_Ok_Noti_Inco.Text = "OK";
            this.Btn_Ok_Noti_Inco.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Btn_Ok_Noti_Inco.Click += new System.EventHandler(this.Btn_Ok_Noti_Inco_Click);
            // 
            // Notificación_Incorrecta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(325, 319);
            this.Controls.Add(this.Lb_Correcto_Noti_Inco);
            this.Controls.Add(this.Btn_Ok_Noti_Inco);
            this.Controls.Add(this.Chek_Correcto_Noti_Inco);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Notificación_Incorrecta";
            this.Text = "Notificación_Incorrecta";
            ((System.ComponentModel.ISupportInitialize)(this.Chek_Correcto_Noti_Inco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuDragControl Mov_Noti_Incorrecta;
        private Bunifu.Framework.UI.BunifuElipse Redondear_Noti_Inco;
        private Bunifu.UI.WinForms.BunifuPictureBox Chek_Correcto_Noti_Inco;
        private Guna.UI.WinForms.GunaLabel Lb_Correcto_Noti_Inco;
        private Guna.UI.WinForms.GunaGradientButton Btn_Ok_Noti_Inco;
    }
}