﻿namespace CONVO_II.Presentaciones
{
    partial class Servicios_Bac
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Servicios_Bac));
            this.RB_Deposito_SerBac = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.RB_Pago_SerBac = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.Lb_Bac_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Pago_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.Panel_TeclaN_SerBac = new Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel();
            this.Btn_Borrar_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Coma_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N0_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N9_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N8_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N7_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N6_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N5_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N4_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N3_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N2_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N1_SerBac = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Res_Desc_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Desc_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Res_SubT_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.Btn_SubT_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Crédito_SerBac = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Efectivo_SerBac = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_ResT_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.Btb_Cantidad_SerBac = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Total_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Des_SerBac = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Lb_Deposito_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.PB_Retiro_SerBac = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.Lb_Retiro_SerBac = new Guna.UI.WinForms.GunaLabel();
            this.Panel_TeclaN_SerBac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_SerBac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_SerBac)).BeginInit();
            this.SuspendLayout();
            // 
            // RB_Deposito_SerBac
            // 
            this.RB_Deposito_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.RB_Deposito_SerBac.BorderThickness = 1;
            this.RB_Deposito_SerBac.Checked = false;
            this.RB_Deposito_SerBac.Location = new System.Drawing.Point(58, 96);
            this.RB_Deposito_SerBac.Name = "RB_Deposito_SerBac";
            this.RB_Deposito_SerBac.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.RB_Deposito_SerBac.OutlineColorUnchecked = System.Drawing.Color.Teal;
            this.RB_Deposito_SerBac.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.RB_Deposito_SerBac.Size = new System.Drawing.Size(29, 29);
            this.RB_Deposito_SerBac.TabIndex = 91;
            this.RB_Deposito_SerBac.Text = null;
            // 
            // RB_Pago_SerBac
            // 
            this.RB_Pago_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.RB_Pago_SerBac.BorderThickness = 1;
            this.RB_Pago_SerBac.Checked = false;
            this.RB_Pago_SerBac.Location = new System.Drawing.Point(486, 94);
            this.RB_Pago_SerBac.Name = "RB_Pago_SerBac";
            this.RB_Pago_SerBac.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.RB_Pago_SerBac.OutlineColorUnchecked = System.Drawing.Color.Teal;
            this.RB_Pago_SerBac.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.RB_Pago_SerBac.Size = new System.Drawing.Size(29, 29);
            this.RB_Pago_SerBac.TabIndex = 90;
            this.RB_Pago_SerBac.Text = null;
            // 
            // Lb_Bac_SerBac
            // 
            this.Lb_Bac_SerBac.AutoSize = true;
            this.Lb_Bac_SerBac.Font = new System.Drawing.Font("Lucida Bright", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Bac_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Bac_SerBac.Location = new System.Drawing.Point(47, 26);
            this.Lb_Bac_SerBac.Name = "Lb_Bac_SerBac";
            this.Lb_Bac_SerBac.Size = new System.Drawing.Size(158, 37);
            this.Lb_Bac_SerBac.TabIndex = 89;
            this.Lb_Bac_SerBac.Text = "Rapi Bac";
            // 
            // Lb_Pago_SerBac
            // 
            this.Lb_Pago_SerBac.AutoSize = true;
            this.Lb_Pago_SerBac.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Pago_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Pago_SerBac.Location = new System.Drawing.Point(524, 96);
            this.Lb_Pago_SerBac.Name = "Lb_Pago_SerBac";
            this.Lb_Pago_SerBac.Size = new System.Drawing.Size(89, 34);
            this.Lb_Pago_SerBac.TabIndex = 88;
            this.Lb_Pago_SerBac.Text = "Pago";
            // 
            // Panel_TeclaN_SerBac
            // 
            this.Panel_TeclaN_SerBac.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel_TeclaN_SerBac.BorderColor = System.Drawing.Color.Silver;
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_Borrar_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_Coma_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N0_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N9_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N8_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N7_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N6_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N5_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N4_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N3_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N2_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_N1_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_Res_Desc_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_Desc_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_Res_SubT_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_SubT_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_Crédito_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_Efectivo_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_ResT_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btb_Cantidad_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_Total_SerBac);
            this.Panel_TeclaN_SerBac.Controls.Add(this.Btn_Des_SerBac);
            this.Panel_TeclaN_SerBac.ForeColor = System.Drawing.Color.Gainsboro;
            this.Panel_TeclaN_SerBac.Location = new System.Drawing.Point(57, 168);
            this.Panel_TeclaN_SerBac.Name = "Panel_TeclaN_SerBac";
            this.Panel_TeclaN_SerBac.PanelColor = System.Drawing.Color.Empty;
            this.Panel_TeclaN_SerBac.ShadowDept = 2;
            this.Panel_TeclaN_SerBac.ShadowTopLeftVisible = false;
            this.Panel_TeclaN_SerBac.Size = new System.Drawing.Size(571, 559);
            this.Panel_TeclaN_SerBac.TabIndex = 86;
            // 
            // Btn_Borrar_SerBac
            // 
            this.Btn_Borrar_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Borrar_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Borrar_SerBac.Image")));
            this.Btn_Borrar_SerBac.ImageActive = null;
            this.Btn_Borrar_SerBac.Location = new System.Drawing.Point(212, 373);
            this.Btn_Borrar_SerBac.Name = "Btn_Borrar_SerBac";
            this.Btn_Borrar_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_Borrar_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Borrar_SerBac.TabIndex = 82;
            this.Btn_Borrar_SerBac.TabStop = false;
            this.Btn_Borrar_SerBac.Zoom = 10;
            // 
            // Btn_Coma_SerBac
            // 
            this.Btn_Coma_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Coma_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Coma_SerBac.Image")));
            this.Btn_Coma_SerBac.ImageActive = null;
            this.Btn_Coma_SerBac.Location = new System.Drawing.Point(24, 373);
            this.Btn_Coma_SerBac.Name = "Btn_Coma_SerBac";
            this.Btn_Coma_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_Coma_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Coma_SerBac.TabIndex = 81;
            this.Btn_Coma_SerBac.TabStop = false;
            this.Btn_Coma_SerBac.Zoom = 10;
            // 
            // Btn_N0_SerBac
            // 
            this.Btn_N0_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N0_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N0_SerBac.Image")));
            this.Btn_N0_SerBac.ImageActive = null;
            this.Btn_N0_SerBac.Location = new System.Drawing.Point(118, 373);
            this.Btn_N0_SerBac.Name = "Btn_N0_SerBac";
            this.Btn_N0_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N0_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N0_SerBac.TabIndex = 80;
            this.Btn_N0_SerBac.TabStop = false;
            this.Btn_N0_SerBac.Zoom = 10;
            // 
            // Btn_N9_SerBac
            // 
            this.Btn_N9_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N9_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N9_SerBac.Image")));
            this.Btn_N9_SerBac.ImageActive = null;
            this.Btn_N9_SerBac.Location = new System.Drawing.Point(212, 280);
            this.Btn_N9_SerBac.Name = "Btn_N9_SerBac";
            this.Btn_N9_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N9_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N9_SerBac.TabIndex = 79;
            this.Btn_N9_SerBac.TabStop = false;
            this.Btn_N9_SerBac.Zoom = 10;
            // 
            // Btn_N8_SerBac
            // 
            this.Btn_N8_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N8_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N8_SerBac.Image")));
            this.Btn_N8_SerBac.ImageActive = null;
            this.Btn_N8_SerBac.Location = new System.Drawing.Point(118, 280);
            this.Btn_N8_SerBac.Name = "Btn_N8_SerBac";
            this.Btn_N8_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N8_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N8_SerBac.TabIndex = 78;
            this.Btn_N8_SerBac.TabStop = false;
            this.Btn_N8_SerBac.Zoom = 10;
            // 
            // Btn_N7_SerBac
            // 
            this.Btn_N7_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N7_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N7_SerBac.Image")));
            this.Btn_N7_SerBac.ImageActive = null;
            this.Btn_N7_SerBac.Location = new System.Drawing.Point(24, 280);
            this.Btn_N7_SerBac.Name = "Btn_N7_SerBac";
            this.Btn_N7_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N7_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N7_SerBac.TabIndex = 77;
            this.Btn_N7_SerBac.TabStop = false;
            this.Btn_N7_SerBac.Zoom = 10;
            // 
            // Btn_N6_SerBac
            // 
            this.Btn_N6_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N6_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N6_SerBac.Image")));
            this.Btn_N6_SerBac.ImageActive = null;
            this.Btn_N6_SerBac.Location = new System.Drawing.Point(212, 187);
            this.Btn_N6_SerBac.Name = "Btn_N6_SerBac";
            this.Btn_N6_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N6_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N6_SerBac.TabIndex = 76;
            this.Btn_N6_SerBac.TabStop = false;
            this.Btn_N6_SerBac.Zoom = 10;
            // 
            // Btn_N5_SerBac
            // 
            this.Btn_N5_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N5_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N5_SerBac.Image")));
            this.Btn_N5_SerBac.ImageActive = null;
            this.Btn_N5_SerBac.Location = new System.Drawing.Point(118, 187);
            this.Btn_N5_SerBac.Name = "Btn_N5_SerBac";
            this.Btn_N5_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N5_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N5_SerBac.TabIndex = 75;
            this.Btn_N5_SerBac.TabStop = false;
            this.Btn_N5_SerBac.Zoom = 10;
            // 
            // Btn_N4_SerBac
            // 
            this.Btn_N4_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N4_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N4_SerBac.Image")));
            this.Btn_N4_SerBac.ImageActive = null;
            this.Btn_N4_SerBac.Location = new System.Drawing.Point(24, 187);
            this.Btn_N4_SerBac.Name = "Btn_N4_SerBac";
            this.Btn_N4_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N4_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N4_SerBac.TabIndex = 74;
            this.Btn_N4_SerBac.TabStop = false;
            this.Btn_N4_SerBac.Zoom = 10;
            // 
            // Btn_N3_SerBac
            // 
            this.Btn_N3_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N3_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N3_SerBac.Image")));
            this.Btn_N3_SerBac.ImageActive = null;
            this.Btn_N3_SerBac.Location = new System.Drawing.Point(212, 94);
            this.Btn_N3_SerBac.Name = "Btn_N3_SerBac";
            this.Btn_N3_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N3_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N3_SerBac.TabIndex = 73;
            this.Btn_N3_SerBac.TabStop = false;
            this.Btn_N3_SerBac.Zoom = 10;
            // 
            // Btn_N2_SerBac
            // 
            this.Btn_N2_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N2_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N2_SerBac.Image")));
            this.Btn_N2_SerBac.ImageActive = null;
            this.Btn_N2_SerBac.Location = new System.Drawing.Point(118, 94);
            this.Btn_N2_SerBac.Name = "Btn_N2_SerBac";
            this.Btn_N2_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N2_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N2_SerBac.TabIndex = 72;
            this.Btn_N2_SerBac.TabStop = false;
            this.Btn_N2_SerBac.Zoom = 10;
            // 
            // Btn_N1_SerBac
            // 
            this.Btn_N1_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N1_SerBac.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N1_SerBac.Image")));
            this.Btn_N1_SerBac.ImageActive = null;
            this.Btn_N1_SerBac.Location = new System.Drawing.Point(24, 94);
            this.Btn_N1_SerBac.Name = "Btn_N1_SerBac";
            this.Btn_N1_SerBac.Size = new System.Drawing.Size(94, 93);
            this.Btn_N1_SerBac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N1_SerBac.TabIndex = 71;
            this.Btn_N1_SerBac.TabStop = false;
            this.Btn_N1_SerBac.Zoom = 10;
            // 
            // Btn_Res_Desc_SerBac
            // 
            this.Btn_Res_Desc_SerBac.AutoSize = true;
            this.Btn_Res_Desc_SerBac.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Res_Desc_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Res_Desc_SerBac.Location = new System.Drawing.Point(450, 373);
            this.Btn_Res_Desc_SerBac.Name = "Btn_Res_Desc_SerBac";
            this.Btn_Res_Desc_SerBac.Size = new System.Drawing.Size(79, 22);
            this.Btn_Res_Desc_SerBac.TabIndex = 70;
            this.Btn_Res_Desc_SerBac.Text = "TOTAL";
            // 
            // Btn_Desc_SerBac
            // 
            this.Btn_Desc_SerBac.AutoSize = true;
            this.Btn_Desc_SerBac.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Desc_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Desc_SerBac.Location = new System.Drawing.Point(312, 373);
            this.Btn_Desc_SerBac.Name = "Btn_Desc_SerBac";
            this.Btn_Desc_SerBac.Size = new System.Drawing.Size(140, 22);
            this.Btn_Desc_SerBac.TabIndex = 69;
            this.Btn_Desc_SerBac.Text = "DESCUENTO :";
            // 
            // Btn_Res_SubT_SerBac
            // 
            this.Btn_Res_SubT_SerBac.AutoSize = true;
            this.Btn_Res_SubT_SerBac.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Res_SubT_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Res_SubT_SerBac.Location = new System.Drawing.Point(450, 332);
            this.Btn_Res_SubT_SerBac.Name = "Btn_Res_SubT_SerBac";
            this.Btn_Res_SubT_SerBac.Size = new System.Drawing.Size(79, 22);
            this.Btn_Res_SubT_SerBac.TabIndex = 68;
            this.Btn_Res_SubT_SerBac.Text = "TOTAL";
            // 
            // Btn_SubT_SerBac
            // 
            this.Btn_SubT_SerBac.AutoSize = true;
            this.Btn_SubT_SerBac.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_SubT_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_SubT_SerBac.Location = new System.Drawing.Point(312, 332);
            this.Btn_SubT_SerBac.Name = "Btn_SubT_SerBac";
            this.Btn_SubT_SerBac.Size = new System.Drawing.Size(132, 22);
            this.Btn_SubT_SerBac.TabIndex = 67;
            this.Btn_SubT_SerBac.Text = "SUB TOTAL :";
            // 
            // Btn_Crédito_SerBac
            // 
            this.Btn_Crédito_SerBac.ActiveBorderThickness = 2;
            this.Btn_Crédito_SerBac.ActiveCornerRadius = 30;
            this.Btn_Crédito_SerBac.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Crédito_SerBac.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_SerBac.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_SerBac.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_SerBac.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Crédito_SerBac.BackgroundImage")));
            this.Btn_Crédito_SerBac.ButtonText = "Crédito";
            this.Btn_Crédito_SerBac.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Crédito_SerBac.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Crédito_SerBac.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_SerBac.IdleBorderThickness = 2;
            this.Btn_Crédito_SerBac.IdleCornerRadius = 20;
            this.Btn_Crédito_SerBac.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Crédito_SerBac.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_SerBac.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_SerBac.Location = new System.Drawing.Point(243, 479);
            this.Btn_Crédito_SerBac.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Crédito_SerBac.Name = "Btn_Crédito_SerBac";
            this.Btn_Crédito_SerBac.Size = new System.Drawing.Size(196, 59);
            this.Btn_Crédito_SerBac.TabIndex = 23;
            this.Btn_Crédito_SerBac.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Efectivo_SerBac
            // 
            this.Btn_Efectivo_SerBac.ActiveBorderThickness = 2;
            this.Btn_Efectivo_SerBac.ActiveCornerRadius = 30;
            this.Btn_Efectivo_SerBac.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Efectivo_SerBac.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_SerBac.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_SerBac.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_SerBac.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Efectivo_SerBac.BackgroundImage")));
            this.Btn_Efectivo_SerBac.ButtonText = "Efectivo";
            this.Btn_Efectivo_SerBac.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Efectivo_SerBac.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Efectivo_SerBac.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_SerBac.IdleBorderThickness = 2;
            this.Btn_Efectivo_SerBac.IdleCornerRadius = 20;
            this.Btn_Efectivo_SerBac.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Efectivo_SerBac.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_SerBac.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_SerBac.Location = new System.Drawing.Point(24, 479);
            this.Btn_Efectivo_SerBac.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Efectivo_SerBac.Name = "Btn_Efectivo_SerBac";
            this.Btn_Efectivo_SerBac.Size = new System.Drawing.Size(196, 59);
            this.Btn_Efectivo_SerBac.TabIndex = 21;
            this.Btn_Efectivo_SerBac.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_ResT_SerBac
            // 
            this.Btn_ResT_SerBac.AutoSize = true;
            this.Btn_ResT_SerBac.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_ResT_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_ResT_SerBac.Location = new System.Drawing.Point(202, 22);
            this.Btn_ResT_SerBac.Name = "Btn_ResT_SerBac";
            this.Btn_ResT_SerBac.Size = new System.Drawing.Size(157, 43);
            this.Btn_ResT_SerBac.TabIndex = 66;
            this.Btn_ResT_SerBac.Text = "TOTAL";
            // 
            // Btb_Cantidad_SerBac
            // 
            this.Btb_Cantidad_SerBac.ActiveBorderThickness = 2;
            this.Btb_Cantidad_SerBac.ActiveCornerRadius = 30;
            this.Btb_Cantidad_SerBac.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btb_Cantidad_SerBac.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_SerBac.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_SerBac.BackColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_SerBac.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btb_Cantidad_SerBac.BackgroundImage")));
            this.Btb_Cantidad_SerBac.ButtonText = "Cantidad";
            this.Btb_Cantidad_SerBac.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btb_Cantidad_SerBac.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btb_Cantidad_SerBac.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_SerBac.IdleBorderThickness = 2;
            this.Btb_Cantidad_SerBac.IdleCornerRadius = 20;
            this.Btb_Cantidad_SerBac.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btb_Cantidad_SerBac.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_SerBac.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_SerBac.Location = new System.Drawing.Point(347, 128);
            this.Btb_Cantidad_SerBac.Margin = new System.Windows.Forms.Padding(5);
            this.Btb_Cantidad_SerBac.Name = "Btb_Cantidad_SerBac";
            this.Btb_Cantidad_SerBac.Size = new System.Drawing.Size(196, 59);
            this.Btb_Cantidad_SerBac.TabIndex = 22;
            this.Btb_Cantidad_SerBac.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Total_SerBac
            // 
            this.Btn_Total_SerBac.AutoSize = true;
            this.Btn_Total_SerBac.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Total_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Total_SerBac.Location = new System.Drawing.Point(16, 22);
            this.Btn_Total_SerBac.Name = "Btn_Total_SerBac";
            this.Btn_Total_SerBac.Size = new System.Drawing.Size(180, 43);
            this.Btn_Total_SerBac.TabIndex = 19;
            this.Btn_Total_SerBac.Text = "TOTAL :";
            // 
            // Btn_Des_SerBac
            // 
            this.Btn_Des_SerBac.ActiveBorderThickness = 2;
            this.Btn_Des_SerBac.ActiveCornerRadius = 30;
            this.Btn_Des_SerBac.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Des_SerBac.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_SerBac.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_SerBac.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_SerBac.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Des_SerBac.BackgroundImage")));
            this.Btn_Des_SerBac.ButtonText = "Descuento";
            this.Btn_Des_SerBac.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Des_SerBac.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Des_SerBac.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_SerBac.IdleBorderThickness = 2;
            this.Btn_Des_SerBac.IdleCornerRadius = 20;
            this.Btn_Des_SerBac.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Des_SerBac.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_SerBac.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_SerBac.Location = new System.Drawing.Point(347, 197);
            this.Btn_Des_SerBac.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Des_SerBac.Name = "Btn_Des_SerBac";
            this.Btn_Des_SerBac.Size = new System.Drawing.Size(196, 59);
            this.Btn_Des_SerBac.TabIndex = 20;
            this.Btn_Des_SerBac.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lb_Deposito_SerBac
            // 
            this.Lb_Deposito_SerBac.AutoSize = true;
            this.Lb_Deposito_SerBac.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Deposito_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Deposito_SerBac.Location = new System.Drawing.Point(99, 96);
            this.Lb_Deposito_SerBac.Name = "Lb_Deposito_SerBac";
            this.Lb_Deposito_SerBac.Size = new System.Drawing.Size(150, 34);
            this.Lb_Deposito_SerBac.TabIndex = 87;
            this.Lb_Deposito_SerBac.Text = "Deposito";
            // 
            // PB_Retiro_SerBac
            // 
            this.PB_Retiro_SerBac.BackColor = System.Drawing.Color.Transparent;
            this.PB_Retiro_SerBac.BorderThickness = 1;
            this.PB_Retiro_SerBac.Checked = true;
            this.PB_Retiro_SerBac.Location = new System.Drawing.Point(286, 94);
            this.PB_Retiro_SerBac.Name = "PB_Retiro_SerBac";
            this.PB_Retiro_SerBac.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.PB_Retiro_SerBac.OutlineColorUnchecked = System.Drawing.Color.Teal;
            this.PB_Retiro_SerBac.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.PB_Retiro_SerBac.Size = new System.Drawing.Size(29, 29);
            this.PB_Retiro_SerBac.TabIndex = 93;
            this.PB_Retiro_SerBac.Text = null;
            // 
            // Lb_Retiro_SerBac
            // 
            this.Lb_Retiro_SerBac.AutoSize = true;
            this.Lb_Retiro_SerBac.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Retiro_SerBac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Retiro_SerBac.Location = new System.Drawing.Point(324, 96);
            this.Lb_Retiro_SerBac.Name = "Lb_Retiro_SerBac";
            this.Lb_Retiro_SerBac.Size = new System.Drawing.Size(106, 34);
            this.Lb_Retiro_SerBac.TabIndex = 92;
            this.Lb_Retiro_SerBac.Text = "Retiro";
            // 
            // Servicios_Bac
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(684, 765);
            this.Controls.Add(this.PB_Retiro_SerBac);
            this.Controls.Add(this.Lb_Retiro_SerBac);
            this.Controls.Add(this.Lb_Deposito_SerBac);
            this.Controls.Add(this.RB_Deposito_SerBac);
            this.Controls.Add(this.RB_Pago_SerBac);
            this.Controls.Add(this.Lb_Bac_SerBac);
            this.Controls.Add(this.Lb_Pago_SerBac);
            this.Controls.Add(this.Panel_TeclaN_SerBac);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Servicios_Bac";
            this.Text = "Servicios_Bac";
            this.Panel_TeclaN_SerBac.ResumeLayout(false);
            this.Panel_TeclaN_SerBac.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_SerBac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_SerBac)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.UI.WinForms.BunifuRadioButton RB_Deposito_SerBac;
        private Bunifu.UI.WinForms.BunifuRadioButton RB_Pago_SerBac;
        private Guna.UI.WinForms.GunaLabel Lb_Bac_SerBac;
        private Guna.UI.WinForms.GunaLabel Lb_Pago_SerBac;
        private Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel Panel_TeclaN_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Borrar_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Coma_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N0_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N9_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N8_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N7_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N6_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N5_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N4_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N3_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N2_SerBac;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N1_SerBac;
        private Guna.UI.WinForms.GunaLabel Btn_Res_Desc_SerBac;
        private Guna.UI.WinForms.GunaLabel Btn_Desc_SerBac;
        private Guna.UI.WinForms.GunaLabel Btn_Res_SubT_SerBac;
        private Guna.UI.WinForms.GunaLabel Btn_SubT_SerBac;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Crédito_SerBac;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Efectivo_SerBac;
        private Guna.UI.WinForms.GunaLabel Btn_ResT_SerBac;
        private Bunifu.Framework.UI.BunifuThinButton2 Btb_Cantidad_SerBac;
        private Guna.UI.WinForms.GunaLabel Btn_Total_SerBac;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Des_SerBac;
        private Guna.UI.WinForms.GunaLabel Lb_Deposito_SerBac;
        private Bunifu.UI.WinForms.BunifuRadioButton PB_Retiro_SerBac;
        private Guna.UI.WinForms.GunaLabel Lb_Retiro_SerBac;
    }
}