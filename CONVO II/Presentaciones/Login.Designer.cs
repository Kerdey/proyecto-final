﻿namespace CONVO_II.Presentaciones
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties5 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties6 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties7 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties8 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            this.Usuario = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Passwork = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Bordeado_Login = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.Panel_Login = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Btn_OcultarPass_Login = new System.Windows.Forms.PictureBox();
            this.Btn_MostrarPass_Login = new System.Windows.Forms.PictureBox();
            this.Btn_Salir_Lodin = new System.Windows.Forms.PictureBox();
            this.Txt_Usu_Login = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Txt_Pass_Login = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Btn_Ingresar_Login = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Lb_Pass_Login = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Lb_Usu_Login = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Btn_Minimizar_Login = new System.Windows.Forms.PictureBox();
            this.bunifuMaterialTextbox1 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Passw = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.BL = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.Mov_Login = new Bunifu.UI.WinForms.BunifuFormDock();
            this.Panel_Login.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_OcultarPass_Login)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_MostrarPass_Login)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Salir_Lodin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Minimizar_Login)).BeginInit();
            this.SuspendLayout();
            // 
            // Usuario
            // 
            this.Usuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Usuario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Usuario.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Usuario.ForeColor = System.Drawing.Color.Gainsboro;
            this.Usuario.HintForeColor = System.Drawing.Color.Gainsboro;
            this.Usuario.HintText = "";
            this.Usuario.isPassword = false;
            this.Usuario.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Usuario.LineIdleColor = System.Drawing.Color.Gainsboro;
            this.Usuario.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Usuario.LineThickness = 3;
            this.Usuario.Location = new System.Drawing.Point(533, 274);
            this.Usuario.Margin = new System.Windows.Forms.Padding(4);
            this.Usuario.Name = "Usuario";
            this.Usuario.Size = new System.Drawing.Size(338, 45);
            this.Usuario.TabIndex = 15;
            this.Usuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // Passwork
            // 
            this.Passwork.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Passwork.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Passwork.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Passwork.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Passwork.ForeColor = System.Drawing.Color.Gainsboro;
            this.Passwork.HintForeColor = System.Drawing.Color.Gainsboro;
            this.Passwork.HintText = "";
            this.Passwork.isPassword = true;
            this.Passwork.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Passwork.LineIdleColor = System.Drawing.Color.Gainsboro;
            this.Passwork.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Passwork.LineThickness = 3;
            this.Passwork.Location = new System.Drawing.Point(533, 408);
            this.Passwork.Margin = new System.Windows.Forms.Padding(4);
            this.Passwork.Name = "Passwork";
            this.Passwork.Size = new System.Drawing.Size(338, 45);
            this.Passwork.TabIndex = 16;
            this.Passwork.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // Bordeado_Login
            // 
            this.Bordeado_Login.ElipseRadius = 30;
            this.Bordeado_Login.TargetControl = this.Panel_Login;
            // 
            // Panel_Login
            // 
            this.Panel_Login.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Panel_Login.BackgroundImage")));
            this.Panel_Login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Panel_Login.Controls.Add(this.pictureBox1);
            this.Panel_Login.Controls.Add(this.Btn_OcultarPass_Login);
            this.Panel_Login.Controls.Add(this.Btn_MostrarPass_Login);
            this.Panel_Login.Controls.Add(this.Btn_Salir_Lodin);
            this.Panel_Login.Controls.Add(this.Txt_Usu_Login);
            this.Panel_Login.Controls.Add(this.Txt_Pass_Login);
            this.Panel_Login.Controls.Add(this.Btn_Ingresar_Login);
            this.Panel_Login.Controls.Add(this.Lb_Pass_Login);
            this.Panel_Login.Controls.Add(this.Lb_Usu_Login);
            this.Panel_Login.Controls.Add(this.Btn_Minimizar_Login);
            this.Panel_Login.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Login.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.Panel_Login.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.Panel_Login.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(11)))), ((int)(((byte)(12)))));
            this.Panel_Login.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.Panel_Login.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(11)))), ((int)(((byte)(12)))));
            this.Panel_Login.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Panel_Login.Location = new System.Drawing.Point(0, 0);
            this.Panel_Login.Name = "Panel_Login";
            this.Panel_Login.Quality = 10;
            this.Panel_Login.Size = new System.Drawing.Size(555, 733);
            this.Panel_Login.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(192, 87);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(198, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // Btn_OcultarPass_Login
            // 
            this.Btn_OcultarPass_Login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_OcultarPass_Login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(20)))));
            this.Btn_OcultarPass_Login.Image = ((System.Drawing.Image)(resources.GetObject("Btn_OcultarPass_Login.Image")));
            this.Btn_OcultarPass_Login.Location = new System.Drawing.Point(388, 500);
            this.Btn_OcultarPass_Login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_OcultarPass_Login.Name = "Btn_OcultarPass_Login";
            this.Btn_OcultarPass_Login.Size = new System.Drawing.Size(36, 32);
            this.Btn_OcultarPass_Login.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_OcultarPass_Login.TabIndex = 39;
            this.Btn_OcultarPass_Login.TabStop = false;
            this.Btn_OcultarPass_Login.Click += new System.EventHandler(this.Btn_OcultarPass_Login_Click);
            // 
            // Btn_MostrarPass_Login
            // 
            this.Btn_MostrarPass_Login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_MostrarPass_Login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(20)))));
            this.Btn_MostrarPass_Login.Image = ((System.Drawing.Image)(resources.GetObject("Btn_MostrarPass_Login.Image")));
            this.Btn_MostrarPass_Login.Location = new System.Drawing.Point(389, 500);
            this.Btn_MostrarPass_Login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_MostrarPass_Login.Name = "Btn_MostrarPass_Login";
            this.Btn_MostrarPass_Login.Size = new System.Drawing.Size(36, 32);
            this.Btn_MostrarPass_Login.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_MostrarPass_Login.TabIndex = 40;
            this.Btn_MostrarPass_Login.TabStop = false;
            this.Btn_MostrarPass_Login.Click += new System.EventHandler(this.Btn_MostrarPass_Login_Click);
            // 
            // Btn_Salir_Lodin
            // 
            this.Btn_Salir_Lodin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Salir_Lodin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Salir_Lodin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Salir_Lodin.Image")));
            this.Btn_Salir_Lodin.Location = new System.Drawing.Point(483, 27);
            this.Btn_Salir_Lodin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Salir_Lodin.Name = "Btn_Salir_Lodin";
            this.Btn_Salir_Lodin.Size = new System.Drawing.Size(32, 31);
            this.Btn_Salir_Lodin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Salir_Lodin.TabIndex = 38;
            this.Btn_Salir_Lodin.TabStop = false;
            this.Btn_Salir_Lodin.Click += new System.EventHandler(this.Btn_Salir_Lodin_Click);
            // 
            // Txt_Usu_Login
            // 
            this.Txt_Usu_Login.AcceptsReturn = false;
            this.Txt_Usu_Login.AcceptsTab = false;
            this.Txt_Usu_Login.AnimationSpeed = 200;
            this.Txt_Usu_Login.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Usu_Login.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Usu_Login.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Usu_Login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Usu_Login.BackgroundImage")));
            this.Txt_Usu_Login.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(85)))), ((int)(((byte)(115)))));
            this.Txt_Usu_Login.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Usu_Login.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.Txt_Usu_Login.BorderColorIdle = System.Drawing.Color.Silver;
            this.Txt_Usu_Login.BorderRadius = 9;
            this.Txt_Usu_Login.BorderThickness = 3;
            this.Txt_Usu_Login.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Usu_Login.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Usu_Login.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Usu_Login.DefaultText = "";
            this.Txt_Usu_Login.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(20)))));
            this.Txt_Usu_Login.ForeColor = System.Drawing.Color.Gainsboro;
            this.Txt_Usu_Login.HideSelection = true;
            this.Txt_Usu_Login.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Usu_Login.IconLeft")));
            this.Txt_Usu_Login.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Usu_Login.IconPadding = 10;
            this.Txt_Usu_Login.IconRight = null;
            this.Txt_Usu_Login.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Usu_Login.Lines = new string[0];
            this.Txt_Usu_Login.Location = new System.Drawing.Point(106, 357);
            this.Txt_Usu_Login.MaxLength = 32767;
            this.Txt_Usu_Login.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Usu_Login.Modified = false;
            this.Txt_Usu_Login.Multiline = false;
            this.Txt_Usu_Login.Name = "Txt_Usu_Login";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(85)))), ((int)(((byte)(115)))));
            stateProperties1.FillColor = System.Drawing.Color.Empty;
            stateProperties1.ForeColor = System.Drawing.Color.Empty;
            stateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Usu_Login.OnActiveState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.Empty;
            stateProperties2.FillColor = System.Drawing.Color.White;
            stateProperties2.ForeColor = System.Drawing.Color.Empty;
            stateProperties2.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Usu_Login.OnDisabledState = stateProperties2;
            stateProperties3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(20)))));
            stateProperties3.FillColor = System.Drawing.Color.Empty;
            stateProperties3.ForeColor = System.Drawing.Color.Empty;
            stateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Usu_Login.OnHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.Silver;
            stateProperties4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(20)))));
            stateProperties4.ForeColor = System.Drawing.Color.Gainsboro;
            stateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Usu_Login.OnIdleState = stateProperties4;
            this.Txt_Usu_Login.PasswordChar = '\0';
            this.Txt_Usu_Login.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Usu_Login.PlaceholderText = "Usuario";
            this.Txt_Usu_Login.ReadOnly = false;
            this.Txt_Usu_Login.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Usu_Login.SelectedText = "";
            this.Txt_Usu_Login.SelectionLength = 0;
            this.Txt_Usu_Login.SelectionStart = 0;
            this.Txt_Usu_Login.ShortcutsEnabled = true;
            this.Txt_Usu_Login.Size = new System.Drawing.Size(333, 52);
            this.Txt_Usu_Login.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Usu_Login.TabIndex = 14;
            this.Txt_Usu_Login.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Usu_Login.TextMarginBottom = 0;
            this.Txt_Usu_Login.TextMarginLeft = 5;
            this.Txt_Usu_Login.TextMarginTop = 0;
            this.Txt_Usu_Login.TextPlaceholder = "Usuario";
            this.Txt_Usu_Login.UseSystemPasswordChar = false;
            this.Txt_Usu_Login.WordWrap = true;
            this.Txt_Usu_Login.TextChanged += new System.EventHandler(this.Txt_Usu_Login_TextChanged);
            this.Txt_Usu_Login.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Usu_Login_KeyPress);
            // 
            // Txt_Pass_Login
            // 
            this.Txt_Pass_Login.AcceptsReturn = false;
            this.Txt_Pass_Login.AcceptsTab = false;
            this.Txt_Pass_Login.AnimationSpeed = 200;
            this.Txt_Pass_Login.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Pass_Login.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Pass_Login.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Pass_Login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Pass_Login.BackgroundImage")));
            this.Txt_Pass_Login.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(85)))), ((int)(((byte)(115)))));
            this.Txt_Pass_Login.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Pass_Login.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.Txt_Pass_Login.BorderColorIdle = System.Drawing.Color.Silver;
            this.Txt_Pass_Login.BorderRadius = 9;
            this.Txt_Pass_Login.BorderThickness = 3;
            this.Txt_Pass_Login.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Pass_Login.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Pass_Login.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Pass_Login.DefaultText = "";
            this.Txt_Pass_Login.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(20)))));
            this.Txt_Pass_Login.ForeColor = System.Drawing.Color.Gainsboro;
            this.Txt_Pass_Login.HideSelection = true;
            this.Txt_Pass_Login.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Pass_Login.IconLeft")));
            this.Txt_Pass_Login.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Pass_Login.IconPadding = 10;
            this.Txt_Pass_Login.IconRight = null;
            this.Txt_Pass_Login.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Pass_Login.Lines = new string[0];
            this.Txt_Pass_Login.Location = new System.Drawing.Point(106, 490);
            this.Txt_Pass_Login.MaxLength = 32767;
            this.Txt_Pass_Login.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Pass_Login.Modified = false;
            this.Txt_Pass_Login.Multiline = false;
            this.Txt_Pass_Login.Name = "Txt_Pass_Login";
            stateProperties5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(85)))), ((int)(((byte)(115)))));
            stateProperties5.FillColor = System.Drawing.Color.Empty;
            stateProperties5.ForeColor = System.Drawing.Color.Empty;
            stateProperties5.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Pass_Login.OnActiveState = stateProperties5;
            stateProperties6.BorderColor = System.Drawing.Color.Empty;
            stateProperties6.FillColor = System.Drawing.Color.White;
            stateProperties6.ForeColor = System.Drawing.Color.Empty;
            stateProperties6.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Pass_Login.OnDisabledState = stateProperties6;
            stateProperties7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(20)))));
            stateProperties7.FillColor = System.Drawing.Color.Empty;
            stateProperties7.ForeColor = System.Drawing.Color.Empty;
            stateProperties7.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Pass_Login.OnHoverState = stateProperties7;
            stateProperties8.BorderColor = System.Drawing.Color.Silver;
            stateProperties8.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(20)))));
            stateProperties8.ForeColor = System.Drawing.Color.Gainsboro;
            stateProperties8.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Pass_Login.OnIdleState = stateProperties8;
            this.Txt_Pass_Login.PasswordChar = '*';
            this.Txt_Pass_Login.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Pass_Login.PlaceholderText = "password";
            this.Txt_Pass_Login.ReadOnly = false;
            this.Txt_Pass_Login.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Pass_Login.SelectedText = "";
            this.Txt_Pass_Login.SelectionLength = 0;
            this.Txt_Pass_Login.SelectionStart = 0;
            this.Txt_Pass_Login.ShortcutsEnabled = true;
            this.Txt_Pass_Login.Size = new System.Drawing.Size(333, 52);
            this.Txt_Pass_Login.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Pass_Login.TabIndex = 13;
            this.Txt_Pass_Login.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Pass_Login.TextMarginBottom = 0;
            this.Txt_Pass_Login.TextMarginLeft = 5;
            this.Txt_Pass_Login.TextMarginTop = 0;
            this.Txt_Pass_Login.TextPlaceholder = "password";
            this.Txt_Pass_Login.UseSystemPasswordChar = false;
            this.Txt_Pass_Login.WordWrap = true;
            this.Txt_Pass_Login.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Pass_Login_KeyPress);
            // 
            // Btn_Ingresar_Login
            // 
            this.Btn_Ingresar_Login.ActiveBorderThickness = 2;
            this.Btn_Ingresar_Login.ActiveCornerRadius = 30;
            this.Btn_Ingresar_Login.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(20)))));
            this.Btn_Ingresar_Login.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Ingresar_Login.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Ingresar_Login.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Ingresar_Login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Ingresar_Login.BackgroundImage")));
            this.Btn_Ingresar_Login.ButtonText = "INGRESAR";
            this.Btn_Ingresar_Login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Ingresar_Login.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ingresar_Login.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Ingresar_Login.IdleBorderThickness = 2;
            this.Btn_Ingresar_Login.IdleCornerRadius = 20;
            this.Btn_Ingresar_Login.IdleFillColor = System.Drawing.Color.Transparent;
            this.Btn_Ingresar_Login.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Ingresar_Login.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Ingresar_Login.Location = new System.Drawing.Point(183, 600);
            this.Btn_Ingresar_Login.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Ingresar_Login.Name = "Btn_Ingresar_Login";
            this.Btn_Ingresar_Login.Size = new System.Drawing.Size(196, 59);
            this.Btn_Ingresar_Login.TabIndex = 11;
            this.Btn_Ingresar_Login.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Btn_Ingresar_Login.Click += new System.EventHandler(this.Btn_Ingresar_Login_Click);
            this.Btn_Ingresar_Login.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Btn_Ingresar_Login_KeyPress);
            // 
            // Lb_Pass_Login
            // 
            this.Lb_Pass_Login.AutoSize = true;
            this.Lb_Pass_Login.BackColor = System.Drawing.Color.Transparent;
            this.Lb_Pass_Login.Font = new System.Drawing.Font("Lucida Bright", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Pass_Login.ForeColor = System.Drawing.Color.Gainsboro;
            this.Lb_Pass_Login.Location = new System.Drawing.Point(53, 438);
            this.Lb_Pass_Login.Name = "Lb_Pass_Login";
            this.Lb_Pass_Login.Size = new System.Drawing.Size(186, 25);
            this.Lb_Pass_Login.TabIndex = 8;
            this.Lb_Pass_Login.Text = "CONTRASEÑA :";
            // 
            // Lb_Usu_Login
            // 
            this.Lb_Usu_Login.AutoSize = true;
            this.Lb_Usu_Login.BackColor = System.Drawing.Color.Transparent;
            this.Lb_Usu_Login.Font = new System.Drawing.Font("Lucida Bright", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Usu_Login.ForeColor = System.Drawing.Color.Gainsboro;
            this.Lb_Usu_Login.Location = new System.Drawing.Point(53, 312);
            this.Lb_Usu_Login.Name = "Lb_Usu_Login";
            this.Lb_Usu_Login.Size = new System.Drawing.Size(129, 25);
            this.Lb_Usu_Login.TabIndex = 7;
            this.Lb_Usu_Login.Text = "USUARIO :";
            // 
            // Btn_Minimizar_Login
            // 
            this.Btn_Minimizar_Login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Minimizar_Login.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Minimizar_Login.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Minimizar_Login.Image")));
            this.Btn_Minimizar_Login.Location = new System.Drawing.Point(441, 27);
            this.Btn_Minimizar_Login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Minimizar_Login.Name = "Btn_Minimizar_Login";
            this.Btn_Minimizar_Login.Size = new System.Drawing.Size(36, 31);
            this.Btn_Minimizar_Login.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Minimizar_Login.TabIndex = 6;
            this.Btn_Minimizar_Login.TabStop = false;
            this.Btn_Minimizar_Login.Click += new System.EventHandler(this.Minimizar_Click);
            // 
            // bunifuMaterialTextbox1
            // 
            this.bunifuMaterialTextbox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.bunifuMaterialTextbox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMaterialTextbox1.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuMaterialTextbox1.ForeColor = System.Drawing.Color.Gainsboro;
            this.bunifuMaterialTextbox1.HintForeColor = System.Drawing.Color.Gainsboro;
            this.bunifuMaterialTextbox1.HintText = "";
            this.bunifuMaterialTextbox1.isPassword = false;
            this.bunifuMaterialTextbox1.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.bunifuMaterialTextbox1.LineIdleColor = System.Drawing.Color.Gainsboro;
            this.bunifuMaterialTextbox1.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.bunifuMaterialTextbox1.LineThickness = 3;
            this.bunifuMaterialTextbox1.Location = new System.Drawing.Point(533, 408);
            this.bunifuMaterialTextbox1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMaterialTextbox1.Name = "bunifuMaterialTextbox1";
            this.bunifuMaterialTextbox1.Size = new System.Drawing.Size(338, 45);
            this.bunifuMaterialTextbox1.TabIndex = 16;
            this.bunifuMaterialTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // Passw
            // 
            this.Passw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Passw.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Passw.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Passw.ForeColor = System.Drawing.Color.Gainsboro;
            this.Passw.HintForeColor = System.Drawing.Color.Gainsboro;
            this.Passw.HintText = "";
            this.Passw.isPassword = false;
            this.Passw.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Passw.LineIdleColor = System.Drawing.Color.Gainsboro;
            this.Passw.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Passw.LineThickness = 3;
            this.Passw.Location = new System.Drawing.Point(533, 429);
            this.Passw.Margin = new System.Windows.Forms.Padding(4);
            this.Passw.Name = "Passw";
            this.Passw.Size = new System.Drawing.Size(338, 45);
            this.Passw.TabIndex = 18;
            this.Passw.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // BL
            // 
            this.BL.ElipseRadius = 30;
            this.BL.TargetControl = this;
            // 
            // Mov_Login
            // 
            this.Mov_Login.AllowFormDragging = true;
            this.Mov_Login.AllowFormDropShadow = true;
            this.Mov_Login.AllowFormResizing = true;
            this.Mov_Login.AllowHidingBottomRegion = true;
            this.Mov_Login.AllowOpacityChangesWhileDragging = false;
            this.Mov_Login.BorderOptions.BottomBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Login.BorderOptions.BottomBorder.BorderThickness = 1;
            this.Mov_Login.BorderOptions.BottomBorder.ShowBorder = true;
            this.Mov_Login.BorderOptions.LeftBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Login.BorderOptions.LeftBorder.BorderThickness = 1;
            this.Mov_Login.BorderOptions.LeftBorder.ShowBorder = true;
            this.Mov_Login.BorderOptions.RightBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Login.BorderOptions.RightBorder.BorderThickness = 1;
            this.Mov_Login.BorderOptions.RightBorder.ShowBorder = true;
            this.Mov_Login.BorderOptions.TopBorder.BorderColor = System.Drawing.Color.Silver;
            this.Mov_Login.BorderOptions.TopBorder.BorderThickness = 1;
            this.Mov_Login.BorderOptions.TopBorder.ShowBorder = true;
            this.Mov_Login.ContainerControl = this;
            this.Mov_Login.DockingIndicatorsColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(215)))), ((int)(((byte)(233)))));
            this.Mov_Login.DockingIndicatorsOpacity = 0.5D;
            this.Mov_Login.DockingOptions.DockAll = true;
            this.Mov_Login.DockingOptions.DockBottomLeft = true;
            this.Mov_Login.DockingOptions.DockBottomRight = true;
            this.Mov_Login.DockingOptions.DockFullScreen = true;
            this.Mov_Login.DockingOptions.DockLeft = true;
            this.Mov_Login.DockingOptions.DockRight = true;
            this.Mov_Login.DockingOptions.DockTopLeft = true;
            this.Mov_Login.DockingOptions.DockTopRight = true;
            this.Mov_Login.FormDraggingOpacity = 0.9D;
            this.Mov_Login.ParentForm = this;
            this.Mov_Login.ShowCursorChanges = true;
            this.Mov_Login.ShowDockingIndicators = true;
            this.Mov_Login.TitleBarOptions.AllowFormDragging = true;
            this.Mov_Login.TitleBarOptions.BunifuFormDock = this.Mov_Login;
            this.Mov_Login.TitleBarOptions.DoubleClickToExpandWindow = false;
            this.Mov_Login.TitleBarOptions.TitleBarControl = this.Panel_Login;
            this.Mov_Login.TitleBarOptions.UseBackColorOnDockingIndicators = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 733);
            this.Controls.Add(this.Panel_Login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Panel_Login.ResumeLayout(false);
            this.Panel_Login.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_OcultarPass_Login)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_MostrarPass_Login)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Salir_Lodin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Minimizar_Login)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuElipse Bordeado_Login;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Usuario;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Passwork;
        private Bunifu.Framework.UI.BunifuMaterialTextbox bunifuMaterialTextbox1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Passw;
        private Bunifu.Framework.UI.BunifuGradientPanel Panel_Login;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Ingresar_Login;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Pass_Login;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Usu_Login;
        private System.Windows.Forms.PictureBox Btn_Minimizar_Login;
        private Bunifu.Framework.UI.BunifuElipse BL;
        private Bunifu.UI.WinForms.BunifuFormDock Mov_Login;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Pass_Login;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Usu_Login;
        private System.Windows.Forms.PictureBox Btn_Salir_Lodin;
        private System.Windows.Forms.PictureBox Btn_OcultarPass_Login;
        private System.Windows.Forms.PictureBox Btn_MostrarPass_Login;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}