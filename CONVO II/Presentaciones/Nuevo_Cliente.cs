﻿using System.Windows.Forms;

namespace CONVO_II.Presentaciones
{
    public partial class Nuevo_Cliente : Form
    {
        public Nuevo_Cliente()
        {
            InitializeComponent();
        }

        private void Txt_PrimNom_NCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_SegNom_NCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_PrimApe_NCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_SegApe_NCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Cédula_NCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Número por favor");
                e.Handled = true;                                                                                                                       //Para que no deje q aun con el mensaje se escriban letras
                return;
            }
        }

        private void Txt_Movil_NCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Número por favor");
                e.Handled = true;                                                                                                                       //Para que no deje q aun con el mensaje se escriban letras
                return;
            }
        }

        private void Salir_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }
    }
}
