﻿using Pulpería_Lovo.CAPA_DATOS.Coneccion;
using System;
using System.Data;
using System.Windows.Forms;

namespace Pulpería_Lovo.Presentaciones
{
    public partial class Nuevo_Producto : Form
    {
        public Nuevo_Producto()
        {
            InitializeComponent();
        }

        private void Salir_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btn_Cancelar_Cate_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        //Método para solo Números 
        private void Txt_PrecioVts_NP_KeyPress(object sender, KeyPressEventArgs e)
        {                                                                                                                                                           //Condicional para que solo acepte números según el Cód. ASCII
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Número por favor");
                e.Handled = true;                                                                                                                       //Para que no deje q aun con el mensaje se escriban letras
                return;
            }
        }

        private void Txt_Stock_NP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Numeros por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_NomProd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Marca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Categ_Np_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Proveedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Fecha_Regis_NP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 91 && e.KeyChar <= 96))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Fecha_Ven_NP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 46) || (e.KeyChar >= 58 && e.KeyChar <= 96))
            {
                MessageBox.Show("Solo numeros y / por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Descrip_NP_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            //{
            //    MessageBox.Show("Solo Letras por favor, Gracias");
            //    e.Handled = true;
            //    return;
            //}
        }

        private void Nuevo_Producto_Load(object sender, EventArgs e)
        {
            llenarTodoLoNecesario();
        }

        private void llenarTodoLoNecesario()
        {
            Productos objProducto = new Productos();
            ComboBox_Cate_NP.DataSource = objProducto.ListCate();
            ComboBox_Cate_NP.DisplayMember = "Categoria";
            ComboBox_Cate_NP.ValueMember = "ID";
            ComboBox_Cate_NP.StartIndex = -1;
            DataTable Prov = new DataTable();
            Prov.Columns.AddRange(new DataColumn[2] { new DataColumn("Id", typeof(int)),
               new DataColumn("Alias", typeof(string)) });
            foreach (DataRow dt in objProducto.ListProveedorN().Rows)
            { Prov.Rows.Add(Prov.Rows.Count + 1, dt[1]); }
            foreach (DataRow dt in objProducto.ListProveedorJ().Rows)
            { Prov.Rows.Add(Prov.Rows.Count + 1, dt[1]); }
            ComboBox_Prove_NPorv.DataSource = Prov;
            ComboBox_Prove_NPorv.DisplayMember = "Alias";
            ComboBox_Prove_NPorv.ValueMember = "ID";
            ComboBox_Prove_NPorv.StartIndex = -1;


            DateTime now = DateTime.Today;
            txt_Fecha_Regis_NP.Text = now.ToString("dd/MM/yyyy");
            txt_Fecha_Regis_NP.Enabled = false;
        }

    }
}
