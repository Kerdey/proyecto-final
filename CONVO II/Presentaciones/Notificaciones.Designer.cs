﻿namespace CONVO_II.Presentaciones
{
    partial class Notificaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lb_Alerta_Noti = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Ok_Correcto_Noti = new Guna.UI.WinForms.GunaGradientButton();
            this.SuspendLayout();
            // 
            // Lb_Alerta_Noti
            // 
            this.Lb_Alerta_Noti.AutoSize = true;
            this.Lb_Alerta_Noti.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Alerta_Noti.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Alerta_Noti.Location = new System.Drawing.Point(136, 185);
            this.Lb_Alerta_Noti.Name = "Lb_Alerta_Noti";
            this.Lb_Alerta_Noti.Size = new System.Drawing.Size(86, 27);
            this.Lb_Alerta_Noti.TabIndex = 4;
            this.Lb_Alerta_Noti.Text = "Alerta";
            // 
            // Btn_Ok_Correcto_Noti
            // 
            this.Btn_Ok_Correcto_Noti.AnimationHoverSpeed = 0.07F;
            this.Btn_Ok_Correcto_Noti.AnimationSpeed = 0.03F;
            this.Btn_Ok_Correcto_Noti.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Ok_Correcto_Noti.BaseColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Ok_Correcto_Noti.BaseColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.Btn_Ok_Correcto_Noti.BorderColor = System.Drawing.Color.Black;
            this.Btn_Ok_Correcto_Noti.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Btn_Ok_Correcto_Noti.FocusedColor = System.Drawing.Color.Empty;
            this.Btn_Ok_Correcto_Noti.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ok_Correcto_Noti.ForeColor = System.Drawing.Color.White;
            this.Btn_Ok_Correcto_Noti.Image = null;
            this.Btn_Ok_Correcto_Noti.ImageSize = new System.Drawing.Size(20, 20);
            this.Btn_Ok_Correcto_Noti.Location = new System.Drawing.Point(99, 256);
            this.Btn_Ok_Correcto_Noti.Name = "Btn_Ok_Correcto_Noti";
            this.Btn_Ok_Correcto_Noti.OnHoverBaseColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Ok_Correcto_Noti.OnHoverBaseColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.Btn_Ok_Correcto_Noti.OnHoverBorderColor = System.Drawing.Color.Black;
            this.Btn_Ok_Correcto_Noti.OnHoverForeColor = System.Drawing.Color.White;
            this.Btn_Ok_Correcto_Noti.OnHoverImage = null;
            this.Btn_Ok_Correcto_Noti.OnPressedColor = System.Drawing.Color.Black;
            this.Btn_Ok_Correcto_Noti.Radius = 10;
            this.Btn_Ok_Correcto_Noti.Size = new System.Drawing.Size(160, 42);
            this.Btn_Ok_Correcto_Noti.TabIndex = 3;
            this.Btn_Ok_Correcto_Noti.Text = "OK";
            this.Btn_Ok_Correcto_Noti.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Btn_Ok_Correcto_Noti.Click += new System.EventHandler(this.Btn_Ok_Correcto_Noti_Click);
            this.Btn_Ok_Correcto_Noti.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Btn_Ok_Correcto_Noti_KeyPress);
            // 
            // Notificaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 330);
            this.Controls.Add(this.Lb_Alerta_Noti);
            this.Controls.Add(this.Btn_Ok_Correcto_Noti);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Notificaciones";
            this.Text = "Notificaciones";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI.WinForms.GunaLabel Lb_Alerta_Noti;
        private Guna.UI.WinForms.GunaGradientButton Btn_Ok_Correcto_Noti;
    }
}