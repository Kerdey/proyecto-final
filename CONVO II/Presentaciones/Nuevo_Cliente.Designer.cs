﻿namespace CONVO_II.Presentaciones
{
    partial class Nuevo_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Nuevo_Cliente));
            this.Lb_SegApe_NCliente = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_SegApe_NCliente = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_PrimApe_NCliente = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_PrimApe_NCliente = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_SegNom_NCliente = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Txt_SegNom_NCliente = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_PrimNom_NCliente = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_PrimNom_NCliente = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Cancelar_NCliente = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Guardar_NCliente = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Guardar_NCliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Cancelar_NCliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Lb_tipoCliente_NCliente = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Cédula_NCliente = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Lb_Movil_NCliente = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Movil_NCliente = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Mov_NuevoCliente = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.PS_Principal = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.Btn_Salir_Prin = new System.Windows.Forms.PictureBox();
            this.Lb_NueClie_NC = new Guna.UI.WinForms.GunaLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_NCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_NCliente)).BeginInit();
            this.PS_Principal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Salir_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Lb_SegApe_NCliente
            // 
            this.Lb_SegApe_NCliente.AutoSize = true;
            this.Lb_SegApe_NCliente.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_SegApe_NCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_SegApe_NCliente.Location = new System.Drawing.Point(406, 141);
            this.Lb_SegApe_NCliente.Name = "Lb_SegApe_NCliente";
            this.Lb_SegApe_NCliente.Size = new System.Drawing.Size(201, 23);
            this.Lb_SegApe_NCliente.TabIndex = 83;
            this.Lb_SegApe_NCliente.Text = "Segundo Apellido :";
            // 
            // txt_SegApe_NCliente
            // 
            this.txt_SegApe_NCliente.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SegApe_NCliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SegApe_NCliente.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SegApe_NCliente.HintForeColor = System.Drawing.Color.Empty;
            this.txt_SegApe_NCliente.HintText = "";
            this.txt_SegApe_NCliente.isPassword = false;
            this.txt_SegApe_NCliente.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_SegApe_NCliente.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_SegApe_NCliente.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_SegApe_NCliente.LineThickness = 3;
            this.txt_SegApe_NCliente.Location = new System.Drawing.Point(610, 132);
            this.txt_SegApe_NCliente.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_SegApe_NCliente.Name = "txt_SegApe_NCliente";
            this.txt_SegApe_NCliente.Size = new System.Drawing.Size(186, 36);
            this.txt_SegApe_NCliente.TabIndex = 82;
            this.txt_SegApe_NCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_SegApe_NCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_SegApe_NCliente_KeyPress);
            // 
            // Lb_PrimApe_NCliente
            // 
            this.Lb_PrimApe_NCliente.AutoSize = true;
            this.Lb_PrimApe_NCliente.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_PrimApe_NCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_PrimApe_NCliente.Location = new System.Drawing.Point(18, 141);
            this.Lb_PrimApe_NCliente.Name = "Lb_PrimApe_NCliente";
            this.Lb_PrimApe_NCliente.Size = new System.Drawing.Size(180, 23);
            this.Lb_PrimApe_NCliente.TabIndex = 81;
            this.Lb_PrimApe_NCliente.Text = "Primer Apellido :";
            // 
            // txt_PrimApe_NCliente
            // 
            this.txt_PrimApe_NCliente.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_PrimApe_NCliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PrimApe_NCliente.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_PrimApe_NCliente.HintForeColor = System.Drawing.Color.Empty;
            this.txt_PrimApe_NCliente.HintText = "";
            this.txt_PrimApe_NCliente.isPassword = false;
            this.txt_PrimApe_NCliente.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_PrimApe_NCliente.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_PrimApe_NCliente.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_PrimApe_NCliente.LineThickness = 3;
            this.txt_PrimApe_NCliente.Location = new System.Drawing.Point(202, 132);
            this.txt_PrimApe_NCliente.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_PrimApe_NCliente.Name = "txt_PrimApe_NCliente";
            this.txt_PrimApe_NCliente.Size = new System.Drawing.Size(186, 36);
            this.txt_PrimApe_NCliente.TabIndex = 80;
            this.txt_PrimApe_NCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_PrimApe_NCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_PrimApe_NCliente_KeyPress);
            // 
            // Lb_SegNom_NCliente
            // 
            this.Lb_SegNom_NCliente.AutoSize = true;
            this.Lb_SegNom_NCliente.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_SegNom_NCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_SegNom_NCliente.Location = new System.Drawing.Point(406, 93);
            this.Lb_SegNom_NCliente.Name = "Lb_SegNom_NCliente";
            this.Lb_SegNom_NCliente.Size = new System.Drawing.Size(198, 23);
            this.Lb_SegNom_NCliente.TabIndex = 79;
            this.Lb_SegNom_NCliente.Text = "Segundo Nombre :";
            // 
            // Txt_SegNom_NCliente
            // 
            this.Txt_SegNom_NCliente.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_SegNom_NCliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_SegNom_NCliente.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Txt_SegNom_NCliente.HintForeColor = System.Drawing.Color.Empty;
            this.Txt_SegNom_NCliente.HintText = "";
            this.Txt_SegNom_NCliente.isPassword = false;
            this.Txt_SegNom_NCliente.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_SegNom_NCliente.LineIdleColor = System.Drawing.Color.DimGray;
            this.Txt_SegNom_NCliente.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.Txt_SegNom_NCliente.LineThickness = 3;
            this.Txt_SegNom_NCliente.Location = new System.Drawing.Point(610, 82);
            this.Txt_SegNom_NCliente.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Txt_SegNom_NCliente.Name = "Txt_SegNom_NCliente";
            this.Txt_SegNom_NCliente.Size = new System.Drawing.Size(186, 36);
            this.Txt_SegNom_NCliente.TabIndex = 78;
            this.Txt_SegNom_NCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_SegNom_NCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_SegNom_NCliente_KeyPress);
            // 
            // Lb_PrimNom_NCliente
            // 
            this.Lb_PrimNom_NCliente.AutoSize = true;
            this.Lb_PrimNom_NCliente.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_PrimNom_NCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_PrimNom_NCliente.Location = new System.Drawing.Point(18, 93);
            this.Lb_PrimNom_NCliente.Name = "Lb_PrimNom_NCliente";
            this.Lb_PrimNom_NCliente.Size = new System.Drawing.Size(177, 23);
            this.Lb_PrimNom_NCliente.TabIndex = 77;
            this.Lb_PrimNom_NCliente.Text = "Primer Nombre :";
            // 
            // txt_PrimNom_NCliente
            // 
            this.txt_PrimNom_NCliente.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_PrimNom_NCliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PrimNom_NCliente.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_PrimNom_NCliente.HintForeColor = System.Drawing.Color.Empty;
            this.txt_PrimNom_NCliente.HintText = "";
            this.txt_PrimNom_NCliente.isPassword = false;
            this.txt_PrimNom_NCliente.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_PrimNom_NCliente.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_PrimNom_NCliente.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_PrimNom_NCliente.LineThickness = 3;
            this.txt_PrimNom_NCliente.Location = new System.Drawing.Point(202, 82);
            this.txt_PrimNom_NCliente.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_PrimNom_NCliente.Name = "txt_PrimNom_NCliente";
            this.txt_PrimNom_NCliente.Size = new System.Drawing.Size(186, 36);
            this.txt_PrimNom_NCliente.TabIndex = 76;
            this.txt_PrimNom_NCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_PrimNom_NCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_PrimNom_NCliente_KeyPress);
            // 
            // Lb_Cancelar_NCliente
            // 
            this.Lb_Cancelar_NCliente.AutoSize = true;
            this.Lb_Cancelar_NCliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Cancelar_NCliente.Location = new System.Drawing.Point(412, 313);
            this.Lb_Cancelar_NCliente.Name = "Lb_Cancelar_NCliente";
            this.Lb_Cancelar_NCliente.Size = new System.Drawing.Size(95, 22);
            this.Lb_Cancelar_NCliente.TabIndex = 87;
            this.Lb_Cancelar_NCliente.Text = "Cancelar";
            // 
            // Lb_Guardar_NCliente
            // 
            this.Lb_Guardar_NCliente.AutoSize = true;
            this.Lb_Guardar_NCliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Guardar_NCliente.Location = new System.Drawing.Point(314, 313);
            this.Lb_Guardar_NCliente.Name = "Lb_Guardar_NCliente";
            this.Lb_Guardar_NCliente.Size = new System.Drawing.Size(90, 22);
            this.Lb_Guardar_NCliente.TabIndex = 86;
            this.Lb_Guardar_NCliente.Text = "Guardar";
            // 
            // Btn_Guardar_NCliente
            // 
            this.Btn_Guardar_NCliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Guardar_NCliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Guardar_NCliente.Image")));
            this.Btn_Guardar_NCliente.ImageActive = null;
            this.Btn_Guardar_NCliente.Location = new System.Drawing.Point(326, 251);
            this.Btn_Guardar_NCliente.Name = "Btn_Guardar_NCliente";
            this.Btn_Guardar_NCliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Guardar_NCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Guardar_NCliente.TabIndex = 85;
            this.Btn_Guardar_NCliente.TabStop = false;
            this.Btn_Guardar_NCliente.Zoom = 10;
            // 
            // Btn_Cancelar_NCliente
            // 
            this.Btn_Cancelar_NCliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Cancelar_NCliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Cancelar_NCliente.Image")));
            this.Btn_Cancelar_NCliente.ImageActive = null;
            this.Btn_Cancelar_NCliente.Location = new System.Drawing.Point(427, 251);
            this.Btn_Cancelar_NCliente.Name = "Btn_Cancelar_NCliente";
            this.Btn_Cancelar_NCliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Cancelar_NCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Cancelar_NCliente.TabIndex = 84;
            this.Btn_Cancelar_NCliente.TabStop = false;
            this.Btn_Cancelar_NCliente.Zoom = 10;
            // 
            // Lb_tipoCliente_NCliente
            // 
            this.Lb_tipoCliente_NCliente.AutoSize = true;
            this.Lb_tipoCliente_NCliente.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_tipoCliente_NCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_tipoCliente_NCliente.Location = new System.Drawing.Point(18, 190);
            this.Lb_tipoCliente_NCliente.Name = "Lb_tipoCliente_NCliente";
            this.Lb_tipoCliente_NCliente.Size = new System.Drawing.Size(92, 23);
            this.Lb_tipoCliente_NCliente.TabIndex = 95;
            this.Lb_tipoCliente_NCliente.Text = "Cédula :";
            // 
            // txt_Cédula_NCliente
            // 
            this.txt_Cédula_NCliente.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Cédula_NCliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Cédula_NCliente.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Cédula_NCliente.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Cédula_NCliente.HintText = "";
            this.txt_Cédula_NCliente.isPassword = false;
            this.txt_Cédula_NCliente.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Cédula_NCliente.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Cédula_NCliente.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Cédula_NCliente.LineThickness = 3;
            this.txt_Cédula_NCliente.Location = new System.Drawing.Point(125, 182);
            this.txt_Cédula_NCliente.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txt_Cédula_NCliente.Name = "txt_Cédula_NCliente";
            this.txt_Cédula_NCliente.Size = new System.Drawing.Size(190, 31);
            this.txt_Cédula_NCliente.TabIndex = 96;
            this.txt_Cédula_NCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Cédula_NCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Cédula_NCliente_KeyPress);
            // 
            // Lb_Movil_NCliente
            // 
            this.Lb_Movil_NCliente.AutoSize = true;
            this.Lb_Movil_NCliente.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Movil_NCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Movil_NCliente.Location = new System.Drawing.Point(406, 190);
            this.Lb_Movil_NCliente.Name = "Lb_Movil_NCliente";
            this.Lb_Movil_NCliente.Size = new System.Drawing.Size(77, 23);
            this.Lb_Movil_NCliente.TabIndex = 98;
            this.Lb_Movil_NCliente.Text = "Movil :";
            // 
            // txt_Movil_NCliente
            // 
            this.txt_Movil_NCliente.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Movil_NCliente.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Movil_NCliente.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Movil_NCliente.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Movil_NCliente.HintText = "";
            this.txt_Movil_NCliente.isPassword = false;
            this.txt_Movil_NCliente.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.txt_Movil_NCliente.LineIdleColor = System.Drawing.Color.DimGray;
            this.txt_Movil_NCliente.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(120)))), ((int)(((byte)(5)))));
            this.txt_Movil_NCliente.LineThickness = 3;
            this.txt_Movil_NCliente.Location = new System.Drawing.Point(489, 179);
            this.txt_Movil_NCliente.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_Movil_NCliente.Name = "txt_Movil_NCliente";
            this.txt_Movil_NCliente.Size = new System.Drawing.Size(171, 36);
            this.txt_Movil_NCliente.TabIndex = 97;
            this.txt_Movil_NCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Movil_NCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Movil_NCliente_KeyPress);
            // 
            // Mov_NuevoCliente
            // 
            this.Mov_NuevoCliente.Fixed = true;
            this.Mov_NuevoCliente.Horizontal = true;
            this.Mov_NuevoCliente.Vertical = true;
            // 
            // PS_Principal
            // 
            this.PS_Principal.BackColor = System.Drawing.Color.Gainsboro;
            this.PS_Principal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PS_Principal.BackgroundImage")));
            this.PS_Principal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PS_Principal.Controls.Add(this.pictureBox1);
            this.PS_Principal.Controls.Add(this.Lb_NueClie_NC);
            this.PS_Principal.Controls.Add(this.Btn_Salir_Prin);
            this.PS_Principal.Dock = System.Windows.Forms.DockStyle.Top;
            this.PS_Principal.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.PS_Principal.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(11)))), ((int)(((byte)(12)))));
            this.PS_Principal.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.PS_Principal.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.PS_Principal.Location = new System.Drawing.Point(0, 0);
            this.PS_Principal.Name = "PS_Principal";
            this.PS_Principal.Quality = 10;
            this.PS_Principal.Size = new System.Drawing.Size(816, 51);
            this.PS_Principal.TabIndex = 99;
            // 
            // Btn_Salir_Prin
            // 
            this.Btn_Salir_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Salir_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Salir_Prin.ErrorImage = null;
            this.Btn_Salir_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Salir_Prin.Image")));
            this.Btn_Salir_Prin.Location = new System.Drawing.Point(767, 11);
            this.Btn_Salir_Prin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Salir_Prin.Name = "Btn_Salir_Prin";
            this.Btn_Salir_Prin.Size = new System.Drawing.Size(29, 31);
            this.Btn_Salir_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btn_Salir_Prin.TabIndex = 4;
            this.Btn_Salir_Prin.TabStop = false;
            // 
            // Lb_NueClie_NC
            // 
            this.Lb_NueClie_NC.AutoSize = true;
            this.Lb_NueClie_NC.BackColor = System.Drawing.Color.Transparent;
            this.Lb_NueClie_NC.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_NueClie_NC.ForeColor = System.Drawing.Color.Gainsboro;
            this.Lb_NueClie_NC.Location = new System.Drawing.Point(65, 15);
            this.Lb_NueClie_NC.Name = "Lb_NueClie_NC";
            this.Lb_NueClie_NC.Size = new System.Drawing.Size(153, 23);
            this.Lb_NueClie_NC.TabIndex = 87;
            this.Lb_NueClie_NC.Text = "Nuevo Cliente";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(13, 8);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 37);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 100;
            this.pictureBox1.TabStop = false;
            // 
            // Nuevo_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(816, 362);
            this.Controls.Add(this.PS_Principal);
            this.Controls.Add(this.Lb_Movil_NCliente);
            this.Controls.Add(this.txt_Movil_NCliente);
            this.Controls.Add(this.txt_Cédula_NCliente);
            this.Controls.Add(this.Lb_tipoCliente_NCliente);
            this.Controls.Add(this.Lb_Cancelar_NCliente);
            this.Controls.Add(this.Lb_Guardar_NCliente);
            this.Controls.Add(this.Btn_Guardar_NCliente);
            this.Controls.Add(this.Btn_Cancelar_NCliente);
            this.Controls.Add(this.Lb_SegApe_NCliente);
            this.Controls.Add(this.txt_SegApe_NCliente);
            this.Controls.Add(this.Lb_PrimApe_NCliente);
            this.Controls.Add(this.txt_PrimApe_NCliente);
            this.Controls.Add(this.Lb_SegNom_NCliente);
            this.Controls.Add(this.Txt_SegNom_NCliente);
            this.Controls.Add(this.Lb_PrimNom_NCliente);
            this.Controls.Add(this.txt_PrimNom_NCliente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Nuevo_Cliente";
            this.Text = "Nuevo_Cliente";
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_NCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cancelar_NCliente)).EndInit();
            this.PS_Principal.ResumeLayout(false);
            this.PS_Principal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Salir_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_SegApe_NCliente;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_SegApe_NCliente;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_PrimApe_NCliente;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_PrimApe_NCliente;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_SegNom_NCliente;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Txt_SegNom_NCliente;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_PrimNom_NCliente;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_PrimNom_NCliente;
        private Guna.UI.WinForms.GunaLabel Lb_Cancelar_NCliente;
        private Guna.UI.WinForms.GunaLabel Lb_Guardar_NCliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Guardar_NCliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Cancelar_NCliente;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_tipoCliente_NCliente;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Cédula_NCliente;
        private Bunifu.Framework.UI.BunifuCustomLabel Lb_Movil_NCliente;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Movil_NCliente;
        private Bunifu.Framework.UI.BunifuDragControl Mov_NuevoCliente;
        private Bunifu.Framework.UI.BunifuGradientPanel PS_Principal;
        private Guna.UI.WinForms.GunaLabel Lb_NueClie_NC;
        private System.Windows.Forms.PictureBox Btn_Salir_Prin;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}