﻿using System;
using System.Windows.Forms;

namespace CONVO_II.Presentaciones
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Btn_Salir_Lodin_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Btn_Ingresar_Login_Click(object sender, EventArgs e)
        {
            Form F = new Principal();
            //Form F = new ProgressBar();
            F.Show();
            this.Hide();
        }

        private void Txt_Usu_Login_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 64) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Letras por favor, Gracias");
                e.Handled = true;
                return;
            }
        }

        private void Txt_Pass_Login_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.Hide();
                Form F = new Principal();
                F.Show();
            }
        }
        private void Btn_OcultarPass_Login_Click(object sender, EventArgs e)
        {
            Btn_OcultarPass_Login.Visible = false;                                          //Se oculta el Boton de ocultar
            Btn_MostrarPass_Login.Visible = true;                                          //Se muestra el Boton de mostrar
        }

        private void Btn_MostrarPass_Login_Click(object sender, EventArgs e)
        {
            Btn_OcultarPass_Login.Visible = true;                                           //Se muestra el Boton de Ocultar
            Btn_MostrarPass_Login.Visible = false;                                         //Se oculta el Boton de mostrar
        }

        private void Btn_Ingresar_Login_KeyPress(object sender, KeyPressEventArgs e)
        {
                       
        }

        private void Txt_Usu_Login_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
