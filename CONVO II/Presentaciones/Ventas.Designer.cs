﻿namespace CONVO_II.Presentaciones
{
    partial class Ventas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ventas));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties5 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties6 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties7 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties8 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            this.panel_ventas = new System.Windows.Forms.Panel();
            this.Btn_Eliminar_Vnts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Eliminar_Vts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Panel_TecladoNumerico_Vts = new Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel();
            this.Lb_ResDes_Vts = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Des_Vts = new Guna.UI.WinForms.GunaLabel();
            this.Panel_TecladoNumerico_Vnts = new Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel();
            this.gunaLabel1 = new Guna.UI.WinForms.GunaLabel();
            this.gunaLabel2 = new Guna.UI.WinForms.GunaLabel();
            this.Btn_ResSubT_Vnts = new Guna.UI.WinForms.GunaLabel();
            this.Lb_SubT_Vnts = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Crédito_Vnts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Efectivo_Vnts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Lb_ResT_Vnts = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Cantidad_Vnts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Lb_Total_Vnts = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Dec_Vnts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Borrar_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Coma_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N0_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N9_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N8_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N7_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N6_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N5_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N4_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N3_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N2_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N1_Vnts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Lb_ResSubT_Vts = new Guna.UI.WinForms.GunaLabel();
            this.Lb_SubTotal_Vts = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Credito_Vts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Efectivo_Vts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Lb_Resultado = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Cantidad_Vts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Lb_Total_Vts = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Descuento_Vts = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Borrar_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Coma_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N0_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N9_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N8_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N7_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N6_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N5_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N4_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N3_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N2_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N1_Vts = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Buscar_Vnts = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Tabla_Ventas_Vts = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.Txt_Buscar_Vts = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.panel_ventas.SuspendLayout();
            this.Panel_TecladoNumerico_Vts.SuspendLayout();
            this.Panel_TecladoNumerico_Vnts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_Vnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_Vts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Ventas_Vts)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_ventas
            // 
            this.panel_ventas.Controls.Add(this.Btn_Eliminar_Vnts);
            this.panel_ventas.Controls.Add(this.Btn_Eliminar_Vts);
            this.panel_ventas.Controls.Add(this.Panel_TecladoNumerico_Vts);
            this.panel_ventas.Controls.Add(this.Btn_Buscar_Vnts);
            this.panel_ventas.Controls.Add(this.Tabla_Ventas_Vts);
            this.panel_ventas.Controls.Add(this.Txt_Buscar_Vts);
            this.panel_ventas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_ventas.Location = new System.Drawing.Point(0, 0);
            this.panel_ventas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel_ventas.Name = "panel_ventas";
            this.panel_ventas.Size = new System.Drawing.Size(1304, 708);
            this.panel_ventas.TabIndex = 0;
            // 
            // Btn_Eliminar_Vnts
            // 
            this.Btn_Eliminar_Vnts.ActiveBorderThickness = 2;
            this.Btn_Eliminar_Vnts.ActiveCornerRadius = 30;
            this.Btn_Eliminar_Vnts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Eliminar_Vnts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vnts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vnts.BackColor = System.Drawing.Color.Silver;
            this.Btn_Eliminar_Vnts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar_Vnts.BackgroundImage")));
            this.Btn_Eliminar_Vnts.ButtonText = "Eliminar";
            this.Btn_Eliminar_Vnts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Eliminar_Vnts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Eliminar_Vnts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vnts.IdleBorderThickness = 2;
            this.Btn_Eliminar_Vnts.IdleCornerRadius = 20;
            this.Btn_Eliminar_Vnts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Eliminar_Vnts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vnts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vnts.Location = new System.Drawing.Point(35, 606);
            this.Btn_Eliminar_Vnts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Eliminar_Vnts.Name = "Btn_Eliminar_Vnts";
            this.Btn_Eliminar_Vnts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Eliminar_Vnts.TabIndex = 76;
            this.Btn_Eliminar_Vnts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Eliminar_Vts
            // 
            this.Btn_Eliminar_Vts.ActiveBorderThickness = 2;
            this.Btn_Eliminar_Vts.ActiveCornerRadius = 30;
            this.Btn_Eliminar_Vts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Eliminar_Vts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vts.BackColor = System.Drawing.Color.Silver;
            this.Btn_Eliminar_Vts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar_Vts.BackgroundImage")));
            this.Btn_Eliminar_Vts.ButtonText = "Eliminar";
            this.Btn_Eliminar_Vts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Eliminar_Vts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Eliminar_Vts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vts.IdleBorderThickness = 2;
            this.Btn_Eliminar_Vts.IdleCornerRadius = 20;
            this.Btn_Eliminar_Vts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Eliminar_Vts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Vts.Location = new System.Drawing.Point(35, 606);
            this.Btn_Eliminar_Vts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Eliminar_Vts.Name = "Btn_Eliminar_Vts";
            this.Btn_Eliminar_Vts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Eliminar_Vts.TabIndex = 77;
            this.Btn_Eliminar_Vts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel_TecladoNumerico_Vts
            // 
            this.Panel_TecladoNumerico_Vts.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel_TecladoNumerico_Vts.BorderColor = System.Drawing.Color.Silver;
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Lb_ResDes_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Lb_Des_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Panel_TecladoNumerico_Vnts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Lb_ResSubT_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Lb_SubTotal_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_Credito_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_Efectivo_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Lb_Resultado);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_Cantidad_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Lb_Total_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_Descuento_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_Borrar_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_Coma_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N0_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N9_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N8_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N7_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N6_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N5_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N4_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N3_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N2_Vts);
            this.Panel_TecladoNumerico_Vts.Controls.Add(this.Btn_N1_Vts);
            this.Panel_TecladoNumerico_Vts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Panel_TecladoNumerico_Vts.Location = new System.Drawing.Point(695, 106);
            this.Panel_TecladoNumerico_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Panel_TecladoNumerico_Vts.Name = "Panel_TecladoNumerico_Vts";
            this.Panel_TecladoNumerico_Vts.PanelColor = System.Drawing.Color.Empty;
            this.Panel_TecladoNumerico_Vts.ShadowDept = 2;
            this.Panel_TecladoNumerico_Vts.ShadowTopLeftVisible = false;
            this.Panel_TecladoNumerico_Vts.Size = new System.Drawing.Size(575, 559);
            this.Panel_TecladoNumerico_Vts.TabIndex = 75;
            // 
            // Lb_ResDes_Vts
            // 
            this.Lb_ResDes_Vts.AutoSize = true;
            this.Lb_ResDes_Vts.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_ResDes_Vts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_ResDes_Vts.Location = new System.Drawing.Point(456, 373);
            this.Lb_ResDes_Vts.Name = "Lb_ResDes_Vts";
            this.Lb_ResDes_Vts.Size = new System.Drawing.Size(79, 22);
            this.Lb_ResDes_Vts.TabIndex = 70;
            this.Lb_ResDes_Vts.Text = "TOTAL";
            // 
            // Lb_Des_Vts
            // 
            this.Lb_Des_Vts.AutoSize = true;
            this.Lb_Des_Vts.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Des_Vts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Des_Vts.Location = new System.Drawing.Point(317, 373);
            this.Lb_Des_Vts.Name = "Lb_Des_Vts";
            this.Lb_Des_Vts.Size = new System.Drawing.Size(140, 22);
            this.Lb_Des_Vts.TabIndex = 69;
            this.Lb_Des_Vts.Text = "DESCUENTO :";
            // 
            // Panel_TecladoNumerico_Vnts
            // 
            this.Panel_TecladoNumerico_Vnts.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel_TecladoNumerico_Vnts.BorderColor = System.Drawing.Color.Silver;
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.gunaLabel1);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.gunaLabel2);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_ResSubT_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Lb_SubT_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_Crédito_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_Efectivo_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Lb_ResT_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_Cantidad_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Lb_Total_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_Dec_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_Borrar_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_Coma_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N0_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N9_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N8_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N7_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N6_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N5_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N4_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N3_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N2_Vnts);
            this.Panel_TecladoNumerico_Vnts.Controls.Add(this.Btn_N1_Vnts);
            this.Panel_TecladoNumerico_Vnts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Panel_TecladoNumerico_Vnts.Location = new System.Drawing.Point(0, 0);
            this.Panel_TecladoNumerico_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Panel_TecladoNumerico_Vnts.Name = "Panel_TecladoNumerico_Vnts";
            this.Panel_TecladoNumerico_Vnts.PanelColor = System.Drawing.Color.Empty;
            this.Panel_TecladoNumerico_Vnts.ShadowDept = 2;
            this.Panel_TecladoNumerico_Vnts.ShadowTopLeftVisible = false;
            this.Panel_TecladoNumerico_Vnts.Size = new System.Drawing.Size(575, 559);
            this.Panel_TecladoNumerico_Vnts.TabIndex = 18;
            // 
            // gunaLabel1
            // 
            this.gunaLabel1.AutoSize = true;
            this.gunaLabel1.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gunaLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.gunaLabel1.Location = new System.Drawing.Point(451, 373);
            this.gunaLabel1.Name = "gunaLabel1";
            this.gunaLabel1.Size = new System.Drawing.Size(79, 22);
            this.gunaLabel1.TabIndex = 70;
            this.gunaLabel1.Text = "TOTAL";
            // 
            // gunaLabel2
            // 
            this.gunaLabel2.AutoSize = true;
            this.gunaLabel2.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gunaLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.gunaLabel2.Location = new System.Drawing.Point(312, 373);
            this.gunaLabel2.Name = "gunaLabel2";
            this.gunaLabel2.Size = new System.Drawing.Size(140, 22);
            this.gunaLabel2.TabIndex = 69;
            this.gunaLabel2.Text = "DESCUENTO :";
            // 
            // Btn_ResSubT_Vnts
            // 
            this.Btn_ResSubT_Vnts.AutoSize = true;
            this.Btn_ResSubT_Vnts.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_ResSubT_Vnts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_ResSubT_Vnts.Location = new System.Drawing.Point(456, 332);
            this.Btn_ResSubT_Vnts.Name = "Btn_ResSubT_Vnts";
            this.Btn_ResSubT_Vnts.Size = new System.Drawing.Size(79, 22);
            this.Btn_ResSubT_Vnts.TabIndex = 68;
            this.Btn_ResSubT_Vnts.Text = "TOTAL";
            // 
            // Lb_SubT_Vnts
            // 
            this.Lb_SubT_Vnts.AutoSize = true;
            this.Lb_SubT_Vnts.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_SubT_Vnts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_SubT_Vnts.Location = new System.Drawing.Point(317, 332);
            this.Lb_SubT_Vnts.Name = "Lb_SubT_Vnts";
            this.Lb_SubT_Vnts.Size = new System.Drawing.Size(132, 22);
            this.Lb_SubT_Vnts.TabIndex = 67;
            this.Lb_SubT_Vnts.Text = "SUB TOTAL :";
            // 
            // Btn_Crédito_Vnts
            // 
            this.Btn_Crédito_Vnts.ActiveBorderThickness = 2;
            this.Btn_Crédito_Vnts.ActiveCornerRadius = 30;
            this.Btn_Crédito_Vnts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Crédito_Vnts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Vnts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Vnts.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Vnts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Crédito_Vnts.BackgroundImage")));
            this.Btn_Crédito_Vnts.ButtonText = "Crédito";
            this.Btn_Crédito_Vnts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Crédito_Vnts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Crédito_Vnts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Vnts.IdleBorderThickness = 2;
            this.Btn_Crédito_Vnts.IdleCornerRadius = 20;
            this.Btn_Crédito_Vnts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Crédito_Vnts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Vnts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Vnts.Location = new System.Drawing.Point(243, 479);
            this.Btn_Crédito_Vnts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Crédito_Vnts.Name = "Btn_Crédito_Vnts";
            this.Btn_Crédito_Vnts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Crédito_Vnts.TabIndex = 23;
            this.Btn_Crédito_Vnts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Efectivo_Vnts
            // 
            this.Btn_Efectivo_Vnts.ActiveBorderThickness = 2;
            this.Btn_Efectivo_Vnts.ActiveCornerRadius = 30;
            this.Btn_Efectivo_Vnts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Efectivo_Vnts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vnts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vnts.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vnts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Efectivo_Vnts.BackgroundImage")));
            this.Btn_Efectivo_Vnts.ButtonText = "Efectivo";
            this.Btn_Efectivo_Vnts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Efectivo_Vnts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Efectivo_Vnts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vnts.IdleBorderThickness = 2;
            this.Btn_Efectivo_Vnts.IdleCornerRadius = 20;
            this.Btn_Efectivo_Vnts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Efectivo_Vnts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vnts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vnts.Location = new System.Drawing.Point(24, 479);
            this.Btn_Efectivo_Vnts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Efectivo_Vnts.Name = "Btn_Efectivo_Vnts";
            this.Btn_Efectivo_Vnts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Efectivo_Vnts.TabIndex = 21;
            this.Btn_Efectivo_Vnts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lb_ResT_Vnts
            // 
            this.Lb_ResT_Vnts.AutoSize = true;
            this.Lb_ResT_Vnts.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_ResT_Vnts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_ResT_Vnts.Location = new System.Drawing.Point(203, 22);
            this.Lb_ResT_Vnts.Name = "Lb_ResT_Vnts";
            this.Lb_ResT_Vnts.Size = new System.Drawing.Size(157, 43);
            this.Lb_ResT_Vnts.TabIndex = 66;
            this.Lb_ResT_Vnts.Text = "TOTAL";
            // 
            // Btn_Cantidad_Vnts
            // 
            this.Btn_Cantidad_Vnts.ActiveBorderThickness = 2;
            this.Btn_Cantidad_Vnts.ActiveCornerRadius = 30;
            this.Btn_Cantidad_Vnts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Cantidad_Vnts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vnts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vnts.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vnts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Cantidad_Vnts.BackgroundImage")));
            this.Btn_Cantidad_Vnts.ButtonText = "Cantidad";
            this.Btn_Cantidad_Vnts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Cantidad_Vnts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Cantidad_Vnts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vnts.IdleBorderThickness = 2;
            this.Btn_Cantidad_Vnts.IdleCornerRadius = 20;
            this.Btn_Cantidad_Vnts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Cantidad_Vnts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vnts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vnts.Location = new System.Drawing.Point(347, 128);
            this.Btn_Cantidad_Vnts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Cantidad_Vnts.Name = "Btn_Cantidad_Vnts";
            this.Btn_Cantidad_Vnts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Cantidad_Vnts.TabIndex = 22;
            this.Btn_Cantidad_Vnts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lb_Total_Vnts
            // 
            this.Lb_Total_Vnts.AutoSize = true;
            this.Lb_Total_Vnts.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Total_Vnts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Total_Vnts.Location = new System.Drawing.Point(16, 22);
            this.Lb_Total_Vnts.Name = "Lb_Total_Vnts";
            this.Lb_Total_Vnts.Size = new System.Drawing.Size(180, 43);
            this.Lb_Total_Vnts.TabIndex = 19;
            this.Lb_Total_Vnts.Text = "TOTAL :";
            // 
            // Btn_Dec_Vnts
            // 
            this.Btn_Dec_Vnts.ActiveBorderThickness = 2;
            this.Btn_Dec_Vnts.ActiveCornerRadius = 30;
            this.Btn_Dec_Vnts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Dec_Vnts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Dec_Vnts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Dec_Vnts.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Dec_Vnts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Dec_Vnts.BackgroundImage")));
            this.Btn_Dec_Vnts.ButtonText = "Descuento";
            this.Btn_Dec_Vnts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Dec_Vnts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Dec_Vnts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Dec_Vnts.IdleBorderThickness = 2;
            this.Btn_Dec_Vnts.IdleCornerRadius = 20;
            this.Btn_Dec_Vnts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Dec_Vnts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Dec_Vnts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Dec_Vnts.Location = new System.Drawing.Point(347, 197);
            this.Btn_Dec_Vnts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Dec_Vnts.Name = "Btn_Dec_Vnts";
            this.Btn_Dec_Vnts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Dec_Vnts.TabIndex = 20;
            this.Btn_Dec_Vnts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Borrar_Vnts
            // 
            this.Btn_Borrar_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Borrar_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Borrar_Vnts.Image")));
            this.Btn_Borrar_Vnts.ImageActive = null;
            this.Btn_Borrar_Vnts.Location = new System.Drawing.Point(212, 373);
            this.Btn_Borrar_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Borrar_Vnts.Name = "Btn_Borrar_Vnts";
            this.Btn_Borrar_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_Borrar_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Borrar_Vnts.TabIndex = 65;
            this.Btn_Borrar_Vnts.TabStop = false;
            this.Btn_Borrar_Vnts.Zoom = 10;
            // 
            // Btn_Coma_Vnts
            // 
            this.Btn_Coma_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Coma_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Coma_Vnts.Image")));
            this.Btn_Coma_Vnts.ImageActive = null;
            this.Btn_Coma_Vnts.Location = new System.Drawing.Point(24, 373);
            this.Btn_Coma_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Coma_Vnts.Name = "Btn_Coma_Vnts";
            this.Btn_Coma_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_Coma_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Coma_Vnts.TabIndex = 64;
            this.Btn_Coma_Vnts.TabStop = false;
            this.Btn_Coma_Vnts.Zoom = 10;
            // 
            // Btn_N0_Vnts
            // 
            this.Btn_N0_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N0_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N0_Vnts.Image")));
            this.Btn_N0_Vnts.ImageActive = null;
            this.Btn_N0_Vnts.Location = new System.Drawing.Point(117, 373);
            this.Btn_N0_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N0_Vnts.Name = "Btn_N0_Vnts";
            this.Btn_N0_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N0_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N0_Vnts.TabIndex = 63;
            this.Btn_N0_Vnts.TabStop = false;
            this.Btn_N0_Vnts.Zoom = 10;
            // 
            // Btn_N9_Vnts
            // 
            this.Btn_N9_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N9_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N9_Vnts.Image")));
            this.Btn_N9_Vnts.ImageActive = null;
            this.Btn_N9_Vnts.Location = new System.Drawing.Point(212, 281);
            this.Btn_N9_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N9_Vnts.Name = "Btn_N9_Vnts";
            this.Btn_N9_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N9_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N9_Vnts.TabIndex = 62;
            this.Btn_N9_Vnts.TabStop = false;
            this.Btn_N9_Vnts.Zoom = 10;
            // 
            // Btn_N8_Vnts
            // 
            this.Btn_N8_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N8_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N8_Vnts.Image")));
            this.Btn_N8_Vnts.ImageActive = null;
            this.Btn_N8_Vnts.Location = new System.Drawing.Point(117, 281);
            this.Btn_N8_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N8_Vnts.Name = "Btn_N8_Vnts";
            this.Btn_N8_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N8_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N8_Vnts.TabIndex = 61;
            this.Btn_N8_Vnts.TabStop = false;
            this.Btn_N8_Vnts.Zoom = 10;
            // 
            // Btn_N7_Vnts
            // 
            this.Btn_N7_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N7_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N7_Vnts.Image")));
            this.Btn_N7_Vnts.ImageActive = null;
            this.Btn_N7_Vnts.Location = new System.Drawing.Point(24, 281);
            this.Btn_N7_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N7_Vnts.Name = "Btn_N7_Vnts";
            this.Btn_N7_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N7_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N7_Vnts.TabIndex = 60;
            this.Btn_N7_Vnts.TabStop = false;
            this.Btn_N7_Vnts.Zoom = 10;
            // 
            // Btn_N6_Vnts
            // 
            this.Btn_N6_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N6_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N6_Vnts.Image")));
            this.Btn_N6_Vnts.ImageActive = null;
            this.Btn_N6_Vnts.Location = new System.Drawing.Point(212, 187);
            this.Btn_N6_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N6_Vnts.Name = "Btn_N6_Vnts";
            this.Btn_N6_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N6_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N6_Vnts.TabIndex = 59;
            this.Btn_N6_Vnts.TabStop = false;
            this.Btn_N6_Vnts.Zoom = 10;
            // 
            // Btn_N5_Vnts
            // 
            this.Btn_N5_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N5_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N5_Vnts.Image")));
            this.Btn_N5_Vnts.ImageActive = null;
            this.Btn_N5_Vnts.Location = new System.Drawing.Point(117, 187);
            this.Btn_N5_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N5_Vnts.Name = "Btn_N5_Vnts";
            this.Btn_N5_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N5_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N5_Vnts.TabIndex = 58;
            this.Btn_N5_Vnts.TabStop = false;
            this.Btn_N5_Vnts.Zoom = 10;
            // 
            // Btn_N4_Vnts
            // 
            this.Btn_N4_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N4_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N4_Vnts.Image")));
            this.Btn_N4_Vnts.ImageActive = null;
            this.Btn_N4_Vnts.Location = new System.Drawing.Point(24, 187);
            this.Btn_N4_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N4_Vnts.Name = "Btn_N4_Vnts";
            this.Btn_N4_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N4_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N4_Vnts.TabIndex = 57;
            this.Btn_N4_Vnts.TabStop = false;
            this.Btn_N4_Vnts.Zoom = 10;
            // 
            // Btn_N3_Vnts
            // 
            this.Btn_N3_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N3_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N3_Vnts.Image")));
            this.Btn_N3_Vnts.ImageActive = null;
            this.Btn_N3_Vnts.Location = new System.Drawing.Point(212, 94);
            this.Btn_N3_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N3_Vnts.Name = "Btn_N3_Vnts";
            this.Btn_N3_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N3_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N3_Vnts.TabIndex = 56;
            this.Btn_N3_Vnts.TabStop = false;
            this.Btn_N3_Vnts.Zoom = 10;
            // 
            // Btn_N2_Vnts
            // 
            this.Btn_N2_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N2_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N2_Vnts.Image")));
            this.Btn_N2_Vnts.ImageActive = null;
            this.Btn_N2_Vnts.Location = new System.Drawing.Point(117, 94);
            this.Btn_N2_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N2_Vnts.Name = "Btn_N2_Vnts";
            this.Btn_N2_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N2_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N2_Vnts.TabIndex = 55;
            this.Btn_N2_Vnts.TabStop = false;
            this.Btn_N2_Vnts.Zoom = 10;
            // 
            // Btn_N1_Vnts
            // 
            this.Btn_N1_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N1_Vnts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N1_Vnts.Image")));
            this.Btn_N1_Vnts.ImageActive = null;
            this.Btn_N1_Vnts.Location = new System.Drawing.Point(24, 94);
            this.Btn_N1_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N1_Vnts.Name = "Btn_N1_Vnts";
            this.Btn_N1_Vnts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N1_Vnts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N1_Vnts.TabIndex = 54;
            this.Btn_N1_Vnts.TabStop = false;
            this.Btn_N1_Vnts.Zoom = 10;
            this.Btn_N1_Vnts.Click += new System.EventHandler(this.Btn_N1_Vnts_Click);
            // 
            // Lb_ResSubT_Vts
            // 
            this.Lb_ResSubT_Vts.AutoSize = true;
            this.Lb_ResSubT_Vts.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_ResSubT_Vts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_ResSubT_Vts.Location = new System.Drawing.Point(451, 332);
            this.Lb_ResSubT_Vts.Name = "Lb_ResSubT_Vts";
            this.Lb_ResSubT_Vts.Size = new System.Drawing.Size(79, 22);
            this.Lb_ResSubT_Vts.TabIndex = 68;
            this.Lb_ResSubT_Vts.Text = "TOTAL";
            // 
            // Lb_SubTotal_Vts
            // 
            this.Lb_SubTotal_Vts.AutoSize = true;
            this.Lb_SubTotal_Vts.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_SubTotal_Vts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_SubTotal_Vts.Location = new System.Drawing.Point(312, 332);
            this.Lb_SubTotal_Vts.Name = "Lb_SubTotal_Vts";
            this.Lb_SubTotal_Vts.Size = new System.Drawing.Size(132, 22);
            this.Lb_SubTotal_Vts.TabIndex = 67;
            this.Lb_SubTotal_Vts.Text = "SUB TOTAL :";
            // 
            // Btn_Credito_Vts
            // 
            this.Btn_Credito_Vts.ActiveBorderThickness = 2;
            this.Btn_Credito_Vts.ActiveCornerRadius = 30;
            this.Btn_Credito_Vts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Credito_Vts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Credito_Vts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Credito_Vts.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Credito_Vts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Credito_Vts.BackgroundImage")));
            this.Btn_Credito_Vts.ButtonText = "Crédito";
            this.Btn_Credito_Vts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Credito_Vts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Credito_Vts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Credito_Vts.IdleBorderThickness = 2;
            this.Btn_Credito_Vts.IdleCornerRadius = 20;
            this.Btn_Credito_Vts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Credito_Vts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Credito_Vts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Credito_Vts.Location = new System.Drawing.Point(243, 479);
            this.Btn_Credito_Vts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Credito_Vts.Name = "Btn_Credito_Vts";
            this.Btn_Credito_Vts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Credito_Vts.TabIndex = 23;
            this.Btn_Credito_Vts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Efectivo_Vts
            // 
            this.Btn_Efectivo_Vts.ActiveBorderThickness = 2;
            this.Btn_Efectivo_Vts.ActiveCornerRadius = 30;
            this.Btn_Efectivo_Vts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Efectivo_Vts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vts.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Efectivo_Vts.BackgroundImage")));
            this.Btn_Efectivo_Vts.ButtonText = "Efectivo";
            this.Btn_Efectivo_Vts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Efectivo_Vts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Efectivo_Vts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vts.IdleBorderThickness = 2;
            this.Btn_Efectivo_Vts.IdleCornerRadius = 20;
            this.Btn_Efectivo_Vts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Efectivo_Vts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Vts.Location = new System.Drawing.Point(24, 479);
            this.Btn_Efectivo_Vts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Efectivo_Vts.Name = "Btn_Efectivo_Vts";
            this.Btn_Efectivo_Vts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Efectivo_Vts.TabIndex = 21;
            this.Btn_Efectivo_Vts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lb_Resultado
            // 
            this.Lb_Resultado.AutoSize = true;
            this.Lb_Resultado.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Resultado.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Resultado.Location = new System.Drawing.Point(203, 22);
            this.Lb_Resultado.Name = "Lb_Resultado";
            this.Lb_Resultado.Size = new System.Drawing.Size(157, 43);
            this.Lb_Resultado.TabIndex = 66;
            this.Lb_Resultado.Text = "TOTAL";
            // 
            // Btn_Cantidad_Vts
            // 
            this.Btn_Cantidad_Vts.ActiveBorderThickness = 2;
            this.Btn_Cantidad_Vts.ActiveCornerRadius = 30;
            this.Btn_Cantidad_Vts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Cantidad_Vts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vts.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Cantidad_Vts.BackgroundImage")));
            this.Btn_Cantidad_Vts.ButtonText = "Cantidad";
            this.Btn_Cantidad_Vts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Cantidad_Vts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Cantidad_Vts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vts.IdleBorderThickness = 2;
            this.Btn_Cantidad_Vts.IdleCornerRadius = 20;
            this.Btn_Cantidad_Vts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Cantidad_Vts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Cantidad_Vts.Location = new System.Drawing.Point(347, 128);
            this.Btn_Cantidad_Vts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Cantidad_Vts.Name = "Btn_Cantidad_Vts";
            this.Btn_Cantidad_Vts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Cantidad_Vts.TabIndex = 22;
            this.Btn_Cantidad_Vts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lb_Total_Vts
            // 
            this.Lb_Total_Vts.AutoSize = true;
            this.Lb_Total_Vts.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Total_Vts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Total_Vts.Location = new System.Drawing.Point(16, 22);
            this.Lb_Total_Vts.Name = "Lb_Total_Vts";
            this.Lb_Total_Vts.Size = new System.Drawing.Size(180, 43);
            this.Lb_Total_Vts.TabIndex = 19;
            this.Lb_Total_Vts.Text = "TOTAL :";
            // 
            // Btn_Descuento_Vts
            // 
            this.Btn_Descuento_Vts.ActiveBorderThickness = 2;
            this.Btn_Descuento_Vts.ActiveCornerRadius = 30;
            this.Btn_Descuento_Vts.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Descuento_Vts.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Descuento_Vts.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Descuento_Vts.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Descuento_Vts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Descuento_Vts.BackgroundImage")));
            this.Btn_Descuento_Vts.ButtonText = "Descuento";
            this.Btn_Descuento_Vts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Descuento_Vts.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Descuento_Vts.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Descuento_Vts.IdleBorderThickness = 2;
            this.Btn_Descuento_Vts.IdleCornerRadius = 20;
            this.Btn_Descuento_Vts.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Descuento_Vts.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Descuento_Vts.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Descuento_Vts.Location = new System.Drawing.Point(347, 197);
            this.Btn_Descuento_Vts.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Btn_Descuento_Vts.Name = "Btn_Descuento_Vts";
            this.Btn_Descuento_Vts.Size = new System.Drawing.Size(196, 59);
            this.Btn_Descuento_Vts.TabIndex = 20;
            this.Btn_Descuento_Vts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Borrar_Vts
            // 
            this.Btn_Borrar_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Borrar_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Borrar_Vts.Image")));
            this.Btn_Borrar_Vts.ImageActive = null;
            this.Btn_Borrar_Vts.Location = new System.Drawing.Point(212, 373);
            this.Btn_Borrar_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Borrar_Vts.Name = "Btn_Borrar_Vts";
            this.Btn_Borrar_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_Borrar_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Borrar_Vts.TabIndex = 65;
            this.Btn_Borrar_Vts.TabStop = false;
            this.Btn_Borrar_Vts.Zoom = 10;
            // 
            // Btn_Coma_Vts
            // 
            this.Btn_Coma_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Coma_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Coma_Vts.Image")));
            this.Btn_Coma_Vts.ImageActive = null;
            this.Btn_Coma_Vts.Location = new System.Drawing.Point(24, 373);
            this.Btn_Coma_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Coma_Vts.Name = "Btn_Coma_Vts";
            this.Btn_Coma_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_Coma_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Coma_Vts.TabIndex = 64;
            this.Btn_Coma_Vts.TabStop = false;
            this.Btn_Coma_Vts.Zoom = 10;
            // 
            // Btn_N0_Vts
            // 
            this.Btn_N0_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N0_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N0_Vts.Image")));
            this.Btn_N0_Vts.ImageActive = null;
            this.Btn_N0_Vts.Location = new System.Drawing.Point(117, 373);
            this.Btn_N0_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N0_Vts.Name = "Btn_N0_Vts";
            this.Btn_N0_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N0_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N0_Vts.TabIndex = 63;
            this.Btn_N0_Vts.TabStop = false;
            this.Btn_N0_Vts.Zoom = 10;
            // 
            // Btn_N9_Vts
            // 
            this.Btn_N9_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N9_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N9_Vts.Image")));
            this.Btn_N9_Vts.ImageActive = null;
            this.Btn_N9_Vts.Location = new System.Drawing.Point(212, 281);
            this.Btn_N9_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N9_Vts.Name = "Btn_N9_Vts";
            this.Btn_N9_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N9_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N9_Vts.TabIndex = 62;
            this.Btn_N9_Vts.TabStop = false;
            this.Btn_N9_Vts.Zoom = 10;
            // 
            // Btn_N8_Vts
            // 
            this.Btn_N8_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N8_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N8_Vts.Image")));
            this.Btn_N8_Vts.ImageActive = null;
            this.Btn_N8_Vts.Location = new System.Drawing.Point(117, 281);
            this.Btn_N8_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N8_Vts.Name = "Btn_N8_Vts";
            this.Btn_N8_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N8_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N8_Vts.TabIndex = 61;
            this.Btn_N8_Vts.TabStop = false;
            this.Btn_N8_Vts.Zoom = 10;
            // 
            // Btn_N7_Vts
            // 
            this.Btn_N7_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N7_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N7_Vts.Image")));
            this.Btn_N7_Vts.ImageActive = null;
            this.Btn_N7_Vts.Location = new System.Drawing.Point(24, 281);
            this.Btn_N7_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N7_Vts.Name = "Btn_N7_Vts";
            this.Btn_N7_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N7_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N7_Vts.TabIndex = 60;
            this.Btn_N7_Vts.TabStop = false;
            this.Btn_N7_Vts.Zoom = 10;
            // 
            // Btn_N6_Vts
            // 
            this.Btn_N6_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N6_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N6_Vts.Image")));
            this.Btn_N6_Vts.ImageActive = null;
            this.Btn_N6_Vts.Location = new System.Drawing.Point(212, 187);
            this.Btn_N6_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N6_Vts.Name = "Btn_N6_Vts";
            this.Btn_N6_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N6_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N6_Vts.TabIndex = 59;
            this.Btn_N6_Vts.TabStop = false;
            this.Btn_N6_Vts.Zoom = 10;
            // 
            // Btn_N5_Vts
            // 
            this.Btn_N5_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N5_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N5_Vts.Image")));
            this.Btn_N5_Vts.ImageActive = null;
            this.Btn_N5_Vts.Location = new System.Drawing.Point(117, 187);
            this.Btn_N5_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N5_Vts.Name = "Btn_N5_Vts";
            this.Btn_N5_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N5_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N5_Vts.TabIndex = 58;
            this.Btn_N5_Vts.TabStop = false;
            this.Btn_N5_Vts.Zoom = 10;
            // 
            // Btn_N4_Vts
            // 
            this.Btn_N4_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N4_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N4_Vts.Image")));
            this.Btn_N4_Vts.ImageActive = null;
            this.Btn_N4_Vts.Location = new System.Drawing.Point(24, 187);
            this.Btn_N4_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N4_Vts.Name = "Btn_N4_Vts";
            this.Btn_N4_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N4_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N4_Vts.TabIndex = 57;
            this.Btn_N4_Vts.TabStop = false;
            this.Btn_N4_Vts.Zoom = 10;
            // 
            // Btn_N3_Vts
            // 
            this.Btn_N3_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N3_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N3_Vts.Image")));
            this.Btn_N3_Vts.ImageActive = null;
            this.Btn_N3_Vts.Location = new System.Drawing.Point(212, 94);
            this.Btn_N3_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N3_Vts.Name = "Btn_N3_Vts";
            this.Btn_N3_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N3_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N3_Vts.TabIndex = 56;
            this.Btn_N3_Vts.TabStop = false;
            this.Btn_N3_Vts.Zoom = 10;
            // 
            // Btn_N2_Vts
            // 
            this.Btn_N2_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N2_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N2_Vts.Image")));
            this.Btn_N2_Vts.ImageActive = null;
            this.Btn_N2_Vts.Location = new System.Drawing.Point(117, 94);
            this.Btn_N2_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N2_Vts.Name = "Btn_N2_Vts";
            this.Btn_N2_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N2_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N2_Vts.TabIndex = 55;
            this.Btn_N2_Vts.TabStop = false;
            this.Btn_N2_Vts.Zoom = 10;
            // 
            // Btn_N1_Vts
            // 
            this.Btn_N1_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N1_Vts.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N1_Vts.Image")));
            this.Btn_N1_Vts.ImageActive = null;
            this.Btn_N1_Vts.Location = new System.Drawing.Point(24, 94);
            this.Btn_N1_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_N1_Vts.Name = "Btn_N1_Vts";
            this.Btn_N1_Vts.Size = new System.Drawing.Size(93, 94);
            this.Btn_N1_Vts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N1_Vts.TabIndex = 54;
            this.Btn_N1_Vts.TabStop = false;
            this.Btn_N1_Vts.Zoom = 10;
            // 
            // Btn_Buscar_Vnts
            // 
            this.Btn_Buscar_Vnts.AcceptsReturn = false;
            this.Btn_Buscar_Vnts.AcceptsTab = false;
            this.Btn_Buscar_Vnts.AnimationSpeed = 200;
            this.Btn_Buscar_Vnts.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Btn_Buscar_Vnts.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Btn_Buscar_Vnts.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Buscar_Vnts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Buscar_Vnts.BackgroundImage")));
            this.Btn_Buscar_Vnts.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Buscar_Vnts.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Btn_Buscar_Vnts.BorderColorHover = System.Drawing.Color.DimGray;
            this.Btn_Buscar_Vnts.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Buscar_Vnts.BorderRadius = 9;
            this.Btn_Buscar_Vnts.BorderThickness = 3;
            this.Btn_Buscar_Vnts.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Btn_Buscar_Vnts.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Btn_Buscar_Vnts.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Buscar_Vnts.DefaultText = "";
            this.Btn_Buscar_Vnts.FillColor = System.Drawing.Color.Gainsboro;
            this.Btn_Buscar_Vnts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Buscar_Vnts.HideSelection = true;
            this.Btn_Buscar_Vnts.IconLeft = ((System.Drawing.Image)(resources.GetObject("Btn_Buscar_Vnts.IconLeft")));
            this.Btn_Buscar_Vnts.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Btn_Buscar_Vnts.IconPadding = 10;
            this.Btn_Buscar_Vnts.IconRight = null;
            this.Btn_Buscar_Vnts.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Btn_Buscar_Vnts.Lines = new string[0];
            this.Btn_Buscar_Vnts.Location = new System.Drawing.Point(35, 43);
            this.Btn_Buscar_Vnts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Buscar_Vnts.MaxLength = 32767;
            this.Btn_Buscar_Vnts.MinimumSize = new System.Drawing.Size(1, 1);
            this.Btn_Buscar_Vnts.Modified = false;
            this.Btn_Buscar_Vnts.Multiline = false;
            this.Btn_Buscar_Vnts.Name = "Btn_Buscar_Vnts";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties1.FillColor = System.Drawing.Color.Empty;
            stateProperties1.ForeColor = System.Drawing.Color.Empty;
            stateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Btn_Buscar_Vnts.OnActiveState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.Empty;
            stateProperties2.FillColor = System.Drawing.Color.White;
            stateProperties2.ForeColor = System.Drawing.Color.Empty;
            stateProperties2.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Btn_Buscar_Vnts.OnDisabledState = stateProperties2;
            stateProperties3.BorderColor = System.Drawing.Color.DimGray;
            stateProperties3.FillColor = System.Drawing.Color.Empty;
            stateProperties3.ForeColor = System.Drawing.Color.Empty;
            stateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Btn_Buscar_Vnts.OnHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties4.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Btn_Buscar_Vnts.OnIdleState = stateProperties4;
            this.Btn_Buscar_Vnts.PasswordChar = '\0';
            this.Btn_Buscar_Vnts.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Btn_Buscar_Vnts.PlaceholderText = "Buscar";
            this.Btn_Buscar_Vnts.ReadOnly = false;
            this.Btn_Buscar_Vnts.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Btn_Buscar_Vnts.SelectedText = "";
            this.Btn_Buscar_Vnts.SelectionLength = 0;
            this.Btn_Buscar_Vnts.SelectionStart = 0;
            this.Btn_Buscar_Vnts.ShortcutsEnabled = true;
            this.Btn_Buscar_Vnts.Size = new System.Drawing.Size(357, 52);
            this.Btn_Buscar_Vnts.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Btn_Buscar_Vnts.TabIndex = 72;
            this.Btn_Buscar_Vnts.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Btn_Buscar_Vnts.TextMarginBottom = 0;
            this.Btn_Buscar_Vnts.TextMarginLeft = 5;
            this.Btn_Buscar_Vnts.TextMarginTop = 0;
            this.Btn_Buscar_Vnts.TextPlaceholder = "Buscar";
            this.Btn_Buscar_Vnts.UseSystemPasswordChar = false;
            this.Btn_Buscar_Vnts.WordWrap = true;
            // 
            // Tabla_Ventas_Vts
            // 
            this.Tabla_Ventas_Vts.AllowCustomTheming = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Ventas_Vts.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Tabla_Ventas_Vts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Tabla_Ventas_Vts.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Tabla_Ventas_Vts.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.Tabla_Ventas_Vts.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Tabla_Ventas_Vts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Tabla_Ventas_Vts.ColumnHeadersHeight = 40;
            this.Tabla_Ventas_Vts.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.Tabla_Ventas_Vts.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Ventas_Vts.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Ventas_Vts.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Ventas_Vts.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.Tabla_Ventas_Vts.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.Tabla_Ventas_Vts.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Ventas_Vts.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Ventas_Vts.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Ventas_Vts.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.Tabla_Ventas_Vts.CurrentTheme.HeaderStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            this.Tabla_Ventas_Vts.CurrentTheme.HeaderStyle.SelectionForeColor = System.Drawing.Color.White;
            this.Tabla_Ventas_Vts.CurrentTheme.Name = null;
            this.Tabla_Ventas_Vts.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.Tabla_Ventas_Vts.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Ventas_Vts.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Ventas_Vts.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Ventas_Vts.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Tabla_Ventas_Vts.DefaultCellStyle = dataGridViewCellStyle3;
            this.Tabla_Ventas_Vts.EnableHeadersVisualStyles = false;
            this.Tabla_Ventas_Vts.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Ventas_Vts.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Ventas_Vts.HeaderBgColor = System.Drawing.Color.Empty;
            this.Tabla_Ventas_Vts.HeaderForeColor = System.Drawing.Color.White;
            this.Tabla_Ventas_Vts.Location = new System.Drawing.Point(35, 174);
            this.Tabla_Ventas_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Tabla_Ventas_Vts.Name = "Tabla_Ventas_Vts";
            this.Tabla_Ventas_Vts.RowHeadersVisible = false;
            this.Tabla_Ventas_Vts.RowHeadersWidth = 51;
            this.Tabla_Ventas_Vts.RowTemplate.Height = 40;
            this.Tabla_Ventas_Vts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Tabla_Ventas_Vts.Size = new System.Drawing.Size(629, 404);
            this.Tabla_Ventas_Vts.TabIndex = 74;
            this.Tabla_Ventas_Vts.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            // 
            // Txt_Buscar_Vts
            // 
            this.Txt_Buscar_Vts.AcceptsReturn = false;
            this.Txt_Buscar_Vts.AcceptsTab = false;
            this.Txt_Buscar_Vts.AnimationSpeed = 200;
            this.Txt_Buscar_Vts.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Buscar_Vts.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Buscar_Vts.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Buscar_Vts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Vts.BackgroundImage")));
            this.Txt_Buscar_Vts.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Buscar_Vts.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Buscar_Vts.BorderColorHover = System.Drawing.Color.DimGray;
            this.Txt_Buscar_Vts.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Buscar_Vts.BorderRadius = 9;
            this.Txt_Buscar_Vts.BorderThickness = 3;
            this.Txt_Buscar_Vts.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Buscar_Vts.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Vts.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buscar_Vts.DefaultText = "";
            this.Txt_Buscar_Vts.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Buscar_Vts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Txt_Buscar_Vts.HideSelection = true;
            this.Txt_Buscar_Vts.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Vts.IconLeft")));
            this.Txt_Buscar_Vts.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Vts.IconPadding = 10;
            this.Txt_Buscar_Vts.IconRight = null;
            this.Txt_Buscar_Vts.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Vts.Lines = new string[0];
            this.Txt_Buscar_Vts.Location = new System.Drawing.Point(35, 43);
            this.Txt_Buscar_Vts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txt_Buscar_Vts.MaxLength = 32767;
            this.Txt_Buscar_Vts.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Buscar_Vts.Modified = false;
            this.Txt_Buscar_Vts.Multiline = false;
            this.Txt_Buscar_Vts.Name = "Txt_Buscar_Vts";
            stateProperties5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties5.FillColor = System.Drawing.Color.Empty;
            stateProperties5.ForeColor = System.Drawing.Color.Empty;
            stateProperties5.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Vts.OnActiveState = stateProperties5;
            stateProperties6.BorderColor = System.Drawing.Color.Empty;
            stateProperties6.FillColor = System.Drawing.Color.White;
            stateProperties6.ForeColor = System.Drawing.Color.Empty;
            stateProperties6.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Vts.OnDisabledState = stateProperties6;
            stateProperties7.BorderColor = System.Drawing.Color.DimGray;
            stateProperties7.FillColor = System.Drawing.Color.Empty;
            stateProperties7.ForeColor = System.Drawing.Color.Empty;
            stateProperties7.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Vts.OnHoverState = stateProperties7;
            stateProperties8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties8.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties8.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Vts.OnIdleState = stateProperties8;
            this.Txt_Buscar_Vts.PasswordChar = '\0';
            this.Txt_Buscar_Vts.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Vts.PlaceholderText = "Buscar";
            this.Txt_Buscar_Vts.ReadOnly = false;
            this.Txt_Buscar_Vts.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Buscar_Vts.SelectedText = "";
            this.Txt_Buscar_Vts.SelectionLength = 0;
            this.Txt_Buscar_Vts.SelectionStart = 0;
            this.Txt_Buscar_Vts.ShortcutsEnabled = true;
            this.Txt_Buscar_Vts.Size = new System.Drawing.Size(357, 52);
            this.Txt_Buscar_Vts.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Buscar_Vts.TabIndex = 73;
            this.Txt_Buscar_Vts.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Buscar_Vts.TextMarginBottom = 0;
            this.Txt_Buscar_Vts.TextMarginLeft = 5;
            this.Txt_Buscar_Vts.TextMarginTop = 0;
            this.Txt_Buscar_Vts.TextPlaceholder = "Buscar";
            this.Txt_Buscar_Vts.UseSystemPasswordChar = false;
            this.Txt_Buscar_Vts.WordWrap = true;
            // 
            // Ventas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1304, 708);
            this.Controls.Add(this.panel_ventas);
            this.ForeColor = System.Drawing.Color.Gainsboro;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Ventas";
            this.Text = "Ventas";
            this.panel_ventas.ResumeLayout(false);
            this.Panel_TecladoNumerico_Vts.ResumeLayout(false);
            this.Panel_TecladoNumerico_Vts.PerformLayout();
            this.Panel_TecladoNumerico_Vnts.ResumeLayout(false);
            this.Panel_TecladoNumerico_Vnts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_Vnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_Vts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Ventas_Vts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_ventas;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Eliminar_Vnts;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Eliminar_Vts;
        private Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel Panel_TecladoNumerico_Vts;
        private Guna.UI.WinForms.GunaLabel Lb_ResDes_Vts;
        private Guna.UI.WinForms.GunaLabel Lb_Des_Vts;
        private Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel Panel_TecladoNumerico_Vnts;
        private Guna.UI.WinForms.GunaLabel gunaLabel1;
        private Guna.UI.WinForms.GunaLabel gunaLabel2;
        private Guna.UI.WinForms.GunaLabel Btn_ResSubT_Vnts;
        private Guna.UI.WinForms.GunaLabel Lb_SubT_Vnts;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Crédito_Vnts;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Efectivo_Vnts;
        private Guna.UI.WinForms.GunaLabel Lb_ResT_Vnts;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Cantidad_Vnts;
        private Guna.UI.WinForms.GunaLabel Lb_Total_Vnts;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Dec_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Borrar_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Coma_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N0_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N9_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N8_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N7_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N6_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N5_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N4_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N3_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N2_Vnts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N1_Vnts;
        private Guna.UI.WinForms.GunaLabel Lb_ResSubT_Vts;
        private Guna.UI.WinForms.GunaLabel Lb_SubTotal_Vts;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Credito_Vts;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Efectivo_Vts;
        private Guna.UI.WinForms.GunaLabel Lb_Resultado;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Cantidad_Vts;
        private Guna.UI.WinForms.GunaLabel Lb_Total_Vts;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Descuento_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Borrar_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Coma_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N0_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N9_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N8_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N7_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N6_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N5_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N4_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N3_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N2_Vts;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N1_Vts;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Btn_Buscar_Vnts;
        private Bunifu.UI.WinForms.BunifuDataGridView Tabla_Ventas_Vts;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Buscar_Vts;
    }
}