﻿namespace Pulpería_Lovo.Presentaciones
{
    partial class Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Clientes));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            this.Lb_Editar_Cliente = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Nuevo_Cliente = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Buscar_Cliente = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Eliminar_Cliente = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Guardar_Cliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Nuevo_Cliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Eliminar_Cliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Editar_Cliente = new Bunifu.Framework.UI.BunifuImageButton();
            this.Lb_ListClientes_ = new Guna.UI.WinForms.GunaLabel();
            this.Txt_Buscar_Cliente = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Tabla_Cliente = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Cliente)).BeginInit();
            this.SuspendLayout();
            // 
            // Lb_Editar_Cliente
            // 
            this.Lb_Editar_Cliente.AutoSize = true;
            this.Lb_Editar_Cliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Editar_Cliente.Location = new System.Drawing.Point(774, 107);
            this.Lb_Editar_Cliente.Name = "Lb_Editar_Cliente";
            this.Lb_Editar_Cliente.Size = new System.Drawing.Size(67, 22);
            this.Lb_Editar_Cliente.TabIndex = 101;
            this.Lb_Editar_Cliente.Text = "Editar";
            // 
            // Lb_Nuevo_Cliente
            // 
            this.Lb_Nuevo_Cliente.AutoSize = true;
            this.Lb_Nuevo_Cliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nuevo_Cliente.Location = new System.Drawing.Point(680, 107);
            this.Lb_Nuevo_Cliente.Name = "Lb_Nuevo_Cliente";
            this.Lb_Nuevo_Cliente.Size = new System.Drawing.Size(73, 22);
            this.Lb_Nuevo_Cliente.TabIndex = 100;
            this.Lb_Nuevo_Cliente.Text = "Nuevo";
            // 
            // Lb_Buscar_Cliente
            // 
            this.Lb_Buscar_Cliente.AutoSize = true;
            this.Lb_Buscar_Cliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Buscar_Cliente.Location = new System.Drawing.Point(426, 107);
            this.Lb_Buscar_Cliente.Name = "Lb_Buscar_Cliente";
            this.Lb_Buscar_Cliente.Size = new System.Drawing.Size(75, 22);
            this.Lb_Buscar_Cliente.TabIndex = 99;
            this.Lb_Buscar_Cliente.Text = "Buscar";
            // 
            // Lb_Eliminar_Cliente
            // 
            this.Lb_Eliminar_Cliente.AutoSize = true;
            this.Lb_Eliminar_Cliente.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Eliminar_Cliente.Location = new System.Drawing.Point(856, 107);
            this.Lb_Eliminar_Cliente.Name = "Lb_Eliminar_Cliente";
            this.Lb_Eliminar_Cliente.Size = new System.Drawing.Size(88, 22);
            this.Lb_Eliminar_Cliente.TabIndex = 98;
            this.Lb_Eliminar_Cliente.Text = "Eliminar";
            // 
            // Btn_Guardar_Cliente
            // 
            this.Btn_Guardar_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Guardar_Cliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Guardar_Cliente.Image")));
            this.Btn_Guardar_Cliente.ImageActive = null;
            this.Btn_Guardar_Cliente.Location = new System.Drawing.Point(434, 42);
            this.Btn_Guardar_Cliente.Name = "Btn_Guardar_Cliente";
            this.Btn_Guardar_Cliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Guardar_Cliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Guardar_Cliente.TabIndex = 97;
            this.Btn_Guardar_Cliente.TabStop = false;
            this.Btn_Guardar_Cliente.Zoom = 10;
            // 
            // Btn_Nuevo_Cliente
            // 
            this.Btn_Nuevo_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Nuevo_Cliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Nuevo_Cliente.Image")));
            this.Btn_Nuevo_Cliente.ImageActive = null;
            this.Btn_Nuevo_Cliente.Location = new System.Drawing.Point(687, 42);
            this.Btn_Nuevo_Cliente.Name = "Btn_Nuevo_Cliente";
            this.Btn_Nuevo_Cliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Nuevo_Cliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Nuevo_Cliente.TabIndex = 96;
            this.Btn_Nuevo_Cliente.TabStop = false;
            this.Btn_Nuevo_Cliente.Zoom = 10;
            // 
            // Btn_Eliminar_Cliente
            // 
            this.Btn_Eliminar_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Eliminar_Cliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar_Cliente.Image")));
            this.Btn_Eliminar_Cliente.ImageActive = null;
            this.Btn_Eliminar_Cliente.Location = new System.Drawing.Point(868, 42);
            this.Btn_Eliminar_Cliente.Name = "Btn_Eliminar_Cliente";
            this.Btn_Eliminar_Cliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Eliminar_Cliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Eliminar_Cliente.TabIndex = 95;
            this.Btn_Eliminar_Cliente.TabStop = false;
            this.Btn_Eliminar_Cliente.Zoom = 10;
            // 
            // Btn_Editar_Cliente
            // 
            this.Btn_Editar_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Editar_Cliente.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Editar_Cliente.Image")));
            this.Btn_Editar_Cliente.ImageActive = null;
            this.Btn_Editar_Cliente.Location = new System.Drawing.Point(777, 42);
            this.Btn_Editar_Cliente.Name = "Btn_Editar_Cliente";
            this.Btn_Editar_Cliente.Size = new System.Drawing.Size(62, 60);
            this.Btn_Editar_Cliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Editar_Cliente.TabIndex = 94;
            this.Btn_Editar_Cliente.TabStop = false;
            this.Btn_Editar_Cliente.Zoom = 10;
            // 
            // Lb_ListClientes_
            // 
            this.Lb_ListClientes_.AutoSize = true;
            this.Lb_ListClientes_.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_ListClientes_.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_ListClientes_.Location = new System.Drawing.Point(35, 162);
            this.Lb_ListClientes_.Name = "Lb_ListClientes_";
            this.Lb_ListClientes_.Size = new System.Drawing.Size(207, 27);
            this.Lb_ListClientes_.TabIndex = 93;
            this.Lb_ListClientes_.Text = "Lista de Clientes";
            // 
            // Txt_Buscar_Cliente
            // 
            this.Txt_Buscar_Cliente.AcceptsReturn = false;
            this.Txt_Buscar_Cliente.AcceptsTab = false;
            this.Txt_Buscar_Cliente.AnimationSpeed = 200;
            this.Txt_Buscar_Cliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Buscar_Cliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Buscar_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Buscar_Cliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cliente.BackgroundImage")));
            this.Txt_Buscar_Cliente.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Cliente.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Buscar_Cliente.BorderColorHover = System.Drawing.Color.DimGray;
            this.Txt_Buscar_Cliente.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Cliente.BorderRadius = 9;
            this.Txt_Buscar_Cliente.BorderThickness = 3;
            this.Txt_Buscar_Cliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Buscar_Cliente.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cliente.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buscar_Cliente.DefaultText = "";
            this.Txt_Buscar_Cliente.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Buscar_Cliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Cliente.HideSelection = true;
            this.Txt_Buscar_Cliente.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cliente.IconLeft")));
            this.Txt_Buscar_Cliente.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cliente.IconPadding = 10;
            this.Txt_Buscar_Cliente.IconRight = null;
            this.Txt_Buscar_Cliente.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cliente.Lines = new string[0];
            this.Txt_Buscar_Cliente.Location = new System.Drawing.Point(40, 50);
            this.Txt_Buscar_Cliente.MaxLength = 32767;
            this.Txt_Buscar_Cliente.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Buscar_Cliente.Modified = false;
            this.Txt_Buscar_Cliente.Multiline = false;
            this.Txt_Buscar_Cliente.Name = "Txt_Buscar_Cliente";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties1.FillColor = System.Drawing.Color.Empty;
            stateProperties1.ForeColor = System.Drawing.Color.Empty;
            stateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cliente.OnActiveState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.Empty;
            stateProperties2.FillColor = System.Drawing.Color.White;
            stateProperties2.ForeColor = System.Drawing.Color.Empty;
            stateProperties2.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cliente.OnDisabledState = stateProperties2;
            stateProperties3.BorderColor = System.Drawing.Color.DimGray;
            stateProperties3.FillColor = System.Drawing.Color.Empty;
            stateProperties3.ForeColor = System.Drawing.Color.Empty;
            stateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cliente.OnHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties4.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cliente.OnIdleState = stateProperties4;
            this.Txt_Buscar_Cliente.PasswordChar = '\0';
            this.Txt_Buscar_Cliente.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cliente.PlaceholderText = "Buscar";
            this.Txt_Buscar_Cliente.ReadOnly = false;
            this.Txt_Buscar_Cliente.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Buscar_Cliente.SelectedText = "";
            this.Txt_Buscar_Cliente.SelectionLength = 0;
            this.Txt_Buscar_Cliente.SelectionStart = 0;
            this.Txt_Buscar_Cliente.ShortcutsEnabled = true;
            this.Txt_Buscar_Cliente.Size = new System.Drawing.Size(357, 52);
            this.Txt_Buscar_Cliente.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Buscar_Cliente.TabIndex = 92;
            this.Txt_Buscar_Cliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Buscar_Cliente.TextMarginBottom = 0;
            this.Txt_Buscar_Cliente.TextMarginLeft = 5;
            this.Txt_Buscar_Cliente.TextMarginTop = 0;
            this.Txt_Buscar_Cliente.TextPlaceholder = "Buscar";
            this.Txt_Buscar_Cliente.UseSystemPasswordChar = false;
            this.Txt_Buscar_Cliente.WordWrap = true;
            // 
            // Tabla_Cliente
            // 
            this.Tabla_Cliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Tabla_Cliente.Location = new System.Drawing.Point(40, 217);
            this.Tabla_Cliente.Name = "Tabla_Cliente";
            this.Tabla_Cliente.RowHeadersWidth = 51;
            this.Tabla_Cliente.RowTemplate.Height = 24;
            this.Tabla_Cliente.Size = new System.Drawing.Size(916, 466);
            this.Tabla_Cliente.TabIndex = 113;
            // 
            // Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1002, 727);
            this.Controls.Add(this.Tabla_Cliente);
            this.Controls.Add(this.Lb_Editar_Cliente);
            this.Controls.Add(this.Lb_Nuevo_Cliente);
            this.Controls.Add(this.Lb_Buscar_Cliente);
            this.Controls.Add(this.Lb_Eliminar_Cliente);
            this.Controls.Add(this.Btn_Guardar_Cliente);
            this.Controls.Add(this.Btn_Nuevo_Cliente);
            this.Controls.Add(this.Btn_Eliminar_Cliente);
            this.Controls.Add(this.Btn_Editar_Cliente);
            this.Controls.Add(this.Lb_ListClientes_);
            this.Controls.Add(this.Txt_Buscar_Cliente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Clientes";
            this.Text = "Clientes";
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Cliente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI.WinForms.GunaLabel Lb_Editar_Cliente;
        private Guna.UI.WinForms.GunaLabel Lb_Nuevo_Cliente;
        private Guna.UI.WinForms.GunaLabel Lb_Buscar_Cliente;
        private Guna.UI.WinForms.GunaLabel Lb_Eliminar_Cliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Guardar_Cliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Nuevo_Cliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Eliminar_Cliente;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Editar_Cliente;
        private Guna.UI.WinForms.GunaLabel Lb_ListClientes_;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Buscar_Cliente;
        private System.Windows.Forms.DataGridView Tabla_Cliente;
    }
}