﻿namespace CONVO_II.Presentaciones
{
    partial class Notificación_Correcta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Notificación_Correcta));
            this.Redondar_Noti_C = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.Mov_Noti = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.Transición_Noti = new Bunifu.Framework.UI.BunifuFormFadeTransition(this.components);
            this.Chek_Correcto_NotiCo = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.Chek_Correcto = new System.Windows.Forms.Timer(this.components);
            this.Btn_Ok_Correcto_NotiCor = new Guna.UI.WinForms.GunaGradientButton();
            this.Lb_Correcto_NotiCor = new Guna.UI.WinForms.GunaLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Chek_Correcto_NotiCo)).BeginInit();
            this.SuspendLayout();
            // 
            // Redondar_Noti_C
            // 
            this.Redondar_Noti_C.ElipseRadius = 20;
            this.Redondar_Noti_C.TargetControl = this;
            // 
            // Mov_Noti
            // 
            this.Mov_Noti.Fixed = true;
            this.Mov_Noti.Horizontal = true;
            this.Mov_Noti.TargetControl = this;
            this.Mov_Noti.Vertical = true;
            // 
            // Transición_Noti
            // 
            this.Transición_Noti.Delay = 1;
            this.Transición_Noti.TransitionEnd += new System.EventHandler(this.Transición_Noti_TransitionEnd);
            // 
            // Chek_Correcto_NotiCo
            // 
            this.Chek_Correcto_NotiCo.AllowFocused = false;
            this.Chek_Correcto_NotiCo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Chek_Correcto_NotiCo.BackColor = System.Drawing.Color.White;
            this.Chek_Correcto_NotiCo.BorderRadius = 50;
            this.Chek_Correcto_NotiCo.Image = ((System.Drawing.Image)(resources.GetObject("Chek_Correcto_NotiCo.Image")));
            this.Chek_Correcto_NotiCo.IsCircle = true;
            this.Chek_Correcto_NotiCo.Location = new System.Drawing.Point(87, 12);
            this.Chek_Correcto_NotiCo.Name = "Chek_Correcto_NotiCo";
            this.Chek_Correcto_NotiCo.Size = new System.Drawing.Size(175, 175);
            this.Chek_Correcto_NotiCo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Chek_Correcto_NotiCo.TabIndex = 0;
            this.Chek_Correcto_NotiCo.TabStop = false;
            this.Chek_Correcto_NotiCo.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            // 
            // Chek_Correcto
            // 
            this.Chek_Correcto.Enabled = true;
            this.Chek_Correcto.Interval = 4000;
            this.Chek_Correcto.Tick += new System.EventHandler(this.Chek_Correcto_Tick);
            // 
            // Btn_Ok_Correcto_NotiCor
            // 
            this.Btn_Ok_Correcto_NotiCor.AnimationHoverSpeed = 0.07F;
            this.Btn_Ok_Correcto_NotiCor.AnimationSpeed = 0.03F;
            this.Btn_Ok_Correcto_NotiCor.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Ok_Correcto_NotiCor.BaseColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Ok_Correcto_NotiCor.BaseColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.Btn_Ok_Correcto_NotiCor.BorderColor = System.Drawing.Color.Black;
            this.Btn_Ok_Correcto_NotiCor.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Btn_Ok_Correcto_NotiCor.FocusedColor = System.Drawing.Color.Empty;
            this.Btn_Ok_Correcto_NotiCor.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ok_Correcto_NotiCor.ForeColor = System.Drawing.Color.White;
            this.Btn_Ok_Correcto_NotiCor.Image = null;
            this.Btn_Ok_Correcto_NotiCor.ImageSize = new System.Drawing.Size(20, 20);
            this.Btn_Ok_Correcto_NotiCor.Location = new System.Drawing.Point(87, 275);
            this.Btn_Ok_Correcto_NotiCor.Name = "Btn_Ok_Correcto_NotiCor";
            this.Btn_Ok_Correcto_NotiCor.OnHoverBaseColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Ok_Correcto_NotiCor.OnHoverBaseColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(39)))), ((int)(((byte)(43)))));
            this.Btn_Ok_Correcto_NotiCor.OnHoverBorderColor = System.Drawing.Color.Black;
            this.Btn_Ok_Correcto_NotiCor.OnHoverForeColor = System.Drawing.Color.White;
            this.Btn_Ok_Correcto_NotiCor.OnHoverImage = null;
            this.Btn_Ok_Correcto_NotiCor.OnPressedColor = System.Drawing.Color.Black;
            this.Btn_Ok_Correcto_NotiCor.Radius = 10;
            this.Btn_Ok_Correcto_NotiCor.Size = new System.Drawing.Size(160, 42);
            this.Btn_Ok_Correcto_NotiCor.TabIndex = 1;
            this.Btn_Ok_Correcto_NotiCor.Text = "OK";
            this.Btn_Ok_Correcto_NotiCor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Btn_Ok_Correcto_NotiCor.Click += new System.EventHandler(this.Btn_Ok_Correcto_Noti_Click);
            // 
            // Lb_Correcto_NotiCor
            // 
            this.Lb_Correcto_NotiCor.AutoSize = true;
            this.Lb_Correcto_NotiCor.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Correcto_NotiCor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Lb_Correcto_NotiCor.Location = new System.Drawing.Point(60, 218);
            this.Lb_Correcto_NotiCor.Name = "Lb_Correcto_NotiCor";
            this.Lb_Correcto_NotiCor.Size = new System.Drawing.Size(218, 27);
            this.Lb_Correcto_NotiCor.TabIndex = 2;
            this.Lb_Correcto_NotiCor.Text = "Acción Realizada";
            // 
            // Notificación_Correcta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(343, 350);
            this.Controls.Add(this.Lb_Correcto_NotiCor);
            this.Controls.Add(this.Btn_Ok_Correcto_NotiCor);
            this.Controls.Add(this.Chek_Correcto_NotiCo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Notificación_Correcta";
            this.Opacity = 0.5D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Notificación_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Chek_Correcto_NotiCo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse Redondar_Noti_C;
        private Bunifu.Framework.UI.BunifuDragControl Mov_Noti;
        private Bunifu.Framework.UI.BunifuFormFadeTransition Transición_Noti;
        private Bunifu.UI.WinForms.BunifuPictureBox Chek_Correcto_NotiCo;
        private System.Windows.Forms.Timer Chek_Correcto;
        private Guna.UI.WinForms.GunaGradientButton Btn_Ok_Correcto_NotiCor;
        private Guna.UI.WinForms.GunaLabel Lb_Correcto_NotiCor;
    }
}