﻿namespace CONVO_II.Presentaciones
{
    partial class Compras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Compras));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties9 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties10 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties11 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties12 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Btn_AgregarProd_Compra = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Eliminar_Compras = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Panel_TecladoNumerico_Comp = new Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel();
            this.Btn_Res_Desc_Compras = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Desc_Compras = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Res_SubT_Compras = new Guna.UI.WinForms.GunaLabel();
            this.Btn_SubT_Compras = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Crédito_Compras = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Efectivo_Compras = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_ResT_Compras = new Guna.UI.WinForms.GunaLabel();
            this.Btb_Cantidad_Compras = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Total_Compras = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Des_Compras = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Btn_Buscar_Compras = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Tabla_Compras_Comp = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.Btn_Borrar_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Coma_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N0_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N9_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N8_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N7_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N6_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N5_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N4_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N3_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N2_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_N1_Compras = new Bunifu.Framework.UI.BunifuImageButton();
            this.Panel_TecladoNumerico_Comp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Compras_Comp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_Compras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_Compras)).BeginInit();
            this.SuspendLayout();
            // 
            // Btn_AgregarProd_Compra
            // 
            this.Btn_AgregarProd_Compra.ActiveBorderThickness = 2;
            this.Btn_AgregarProd_Compra.ActiveCornerRadius = 30;
            this.Btn_AgregarProd_Compra.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_AgregarProd_Compra.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_AgregarProd_Compra.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_AgregarProd_Compra.BackColor = System.Drawing.Color.Silver;
            this.Btn_AgregarProd_Compra.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_AgregarProd_Compra.BackgroundImage")));
            this.Btn_AgregarProd_Compra.ButtonText = "Agregar Producto";
            this.Btn_AgregarProd_Compra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_AgregarProd_Compra.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_AgregarProd_Compra.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_AgregarProd_Compra.IdleBorderThickness = 2;
            this.Btn_AgregarProd_Compra.IdleCornerRadius = 20;
            this.Btn_AgregarProd_Compra.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_AgregarProd_Compra.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_AgregarProd_Compra.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_AgregarProd_Compra.Location = new System.Drawing.Point(263, 625);
            this.Btn_AgregarProd_Compra.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_AgregarProd_Compra.Name = "Btn_AgregarProd_Compra";
            this.Btn_AgregarProd_Compra.Size = new System.Drawing.Size(226, 59);
            this.Btn_AgregarProd_Compra.TabIndex = 76;
            this.Btn_AgregarProd_Compra.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Eliminar_Compras
            // 
            this.Btn_Eliminar_Compras.ActiveBorderThickness = 2;
            this.Btn_Eliminar_Compras.ActiveCornerRadius = 30;
            this.Btn_Eliminar_Compras.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Eliminar_Compras.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Compras.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Compras.BackColor = System.Drawing.Color.Silver;
            this.Btn_Eliminar_Compras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar_Compras.BackgroundImage")));
            this.Btn_Eliminar_Compras.ButtonText = "Eliminar";
            this.Btn_Eliminar_Compras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Eliminar_Compras.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Eliminar_Compras.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Compras.IdleBorderThickness = 2;
            this.Btn_Eliminar_Compras.IdleCornerRadius = 20;
            this.Btn_Eliminar_Compras.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Eliminar_Compras.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Compras.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Eliminar_Compras.Location = new System.Drawing.Point(39, 625);
            this.Btn_Eliminar_Compras.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Eliminar_Compras.Name = "Btn_Eliminar_Compras";
            this.Btn_Eliminar_Compras.Size = new System.Drawing.Size(196, 59);
            this.Btn_Eliminar_Compras.TabIndex = 75;
            this.Btn_Eliminar_Compras.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel_TecladoNumerico_Comp
            // 
            this.Panel_TecladoNumerico_Comp.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel_TecladoNumerico_Comp.BorderColor = System.Drawing.Color.Silver;
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_Borrar_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_Coma_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N0_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N9_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N8_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N7_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N6_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N5_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N4_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N3_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N2_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_N1_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_Res_Desc_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_Desc_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_Res_SubT_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_SubT_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_Crédito_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_Efectivo_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_ResT_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btb_Cantidad_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_Total_Compras);
            this.Panel_TecladoNumerico_Comp.Controls.Add(this.Btn_Des_Compras);
            this.Panel_TecladoNumerico_Comp.ForeColor = System.Drawing.Color.Gainsboro;
            this.Panel_TecladoNumerico_Comp.Location = new System.Drawing.Point(711, 125);
            this.Panel_TecladoNumerico_Comp.Name = "Panel_TecladoNumerico_Comp";
            this.Panel_TecladoNumerico_Comp.PanelColor = System.Drawing.Color.Empty;
            this.Panel_TecladoNumerico_Comp.ShadowDept = 2;
            this.Panel_TecladoNumerico_Comp.ShadowTopLeftVisible = false;
            this.Panel_TecladoNumerico_Comp.Size = new System.Drawing.Size(571, 559);
            this.Panel_TecladoNumerico_Comp.TabIndex = 74;
            // 
            // Btn_Res_Desc_Compras
            // 
            this.Btn_Res_Desc_Compras.AutoSize = true;
            this.Btn_Res_Desc_Compras.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Res_Desc_Compras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Res_Desc_Compras.Location = new System.Drawing.Point(450, 373);
            this.Btn_Res_Desc_Compras.Name = "Btn_Res_Desc_Compras";
            this.Btn_Res_Desc_Compras.Size = new System.Drawing.Size(79, 22);
            this.Btn_Res_Desc_Compras.TabIndex = 70;
            this.Btn_Res_Desc_Compras.Text = "TOTAL";
            // 
            // Btn_Desc_Compras
            // 
            this.Btn_Desc_Compras.AutoSize = true;
            this.Btn_Desc_Compras.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Desc_Compras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Desc_Compras.Location = new System.Drawing.Point(312, 373);
            this.Btn_Desc_Compras.Name = "Btn_Desc_Compras";
            this.Btn_Desc_Compras.Size = new System.Drawing.Size(140, 22);
            this.Btn_Desc_Compras.TabIndex = 69;
            this.Btn_Desc_Compras.Text = "DESCUENTO :";
            // 
            // Btn_Res_SubT_Compras
            // 
            this.Btn_Res_SubT_Compras.AutoSize = true;
            this.Btn_Res_SubT_Compras.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Res_SubT_Compras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Res_SubT_Compras.Location = new System.Drawing.Point(450, 332);
            this.Btn_Res_SubT_Compras.Name = "Btn_Res_SubT_Compras";
            this.Btn_Res_SubT_Compras.Size = new System.Drawing.Size(79, 22);
            this.Btn_Res_SubT_Compras.TabIndex = 68;
            this.Btn_Res_SubT_Compras.Text = "TOTAL";
            // 
            // Btn_SubT_Compras
            // 
            this.Btn_SubT_Compras.AutoSize = true;
            this.Btn_SubT_Compras.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_SubT_Compras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_SubT_Compras.Location = new System.Drawing.Point(312, 332);
            this.Btn_SubT_Compras.Name = "Btn_SubT_Compras";
            this.Btn_SubT_Compras.Size = new System.Drawing.Size(132, 22);
            this.Btn_SubT_Compras.TabIndex = 67;
            this.Btn_SubT_Compras.Text = "SUB TOTAL :";
            // 
            // Btn_Crédito_Compras
            // 
            this.Btn_Crédito_Compras.ActiveBorderThickness = 2;
            this.Btn_Crédito_Compras.ActiveCornerRadius = 30;
            this.Btn_Crédito_Compras.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Crédito_Compras.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Compras.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Compras.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Compras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Crédito_Compras.BackgroundImage")));
            this.Btn_Crédito_Compras.ButtonText = "Crédito";
            this.Btn_Crédito_Compras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Crédito_Compras.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Crédito_Compras.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Compras.IdleBorderThickness = 2;
            this.Btn_Crédito_Compras.IdleCornerRadius = 20;
            this.Btn_Crédito_Compras.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Crédito_Compras.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Compras.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Crédito_Compras.Location = new System.Drawing.Point(243, 479);
            this.Btn_Crédito_Compras.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Crédito_Compras.Name = "Btn_Crédito_Compras";
            this.Btn_Crédito_Compras.Size = new System.Drawing.Size(196, 59);
            this.Btn_Crédito_Compras.TabIndex = 23;
            this.Btn_Crédito_Compras.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Efectivo_Compras
            // 
            this.Btn_Efectivo_Compras.ActiveBorderThickness = 2;
            this.Btn_Efectivo_Compras.ActiveCornerRadius = 30;
            this.Btn_Efectivo_Compras.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Efectivo_Compras.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Compras.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Compras.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Compras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Efectivo_Compras.BackgroundImage")));
            this.Btn_Efectivo_Compras.ButtonText = "Efectivo";
            this.Btn_Efectivo_Compras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Efectivo_Compras.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Efectivo_Compras.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Compras.IdleBorderThickness = 2;
            this.Btn_Efectivo_Compras.IdleCornerRadius = 20;
            this.Btn_Efectivo_Compras.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Efectivo_Compras.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Compras.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Efectivo_Compras.Location = new System.Drawing.Point(24, 479);
            this.Btn_Efectivo_Compras.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Efectivo_Compras.Name = "Btn_Efectivo_Compras";
            this.Btn_Efectivo_Compras.Size = new System.Drawing.Size(196, 59);
            this.Btn_Efectivo_Compras.TabIndex = 21;
            this.Btn_Efectivo_Compras.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_ResT_Compras
            // 
            this.Btn_ResT_Compras.AutoSize = true;
            this.Btn_ResT_Compras.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_ResT_Compras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_ResT_Compras.Location = new System.Drawing.Point(202, 22);
            this.Btn_ResT_Compras.Name = "Btn_ResT_Compras";
            this.Btn_ResT_Compras.Size = new System.Drawing.Size(157, 43);
            this.Btn_ResT_Compras.TabIndex = 66;
            this.Btn_ResT_Compras.Text = "TOTAL";
            // 
            // Btb_Cantidad_Compras
            // 
            this.Btb_Cantidad_Compras.ActiveBorderThickness = 2;
            this.Btb_Cantidad_Compras.ActiveCornerRadius = 30;
            this.Btb_Cantidad_Compras.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btb_Cantidad_Compras.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Compras.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Compras.BackColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Compras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btb_Cantidad_Compras.BackgroundImage")));
            this.Btb_Cantidad_Compras.ButtonText = "Cantidad";
            this.Btb_Cantidad_Compras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btb_Cantidad_Compras.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btb_Cantidad_Compras.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Compras.IdleBorderThickness = 2;
            this.Btb_Cantidad_Compras.IdleCornerRadius = 20;
            this.Btb_Cantidad_Compras.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btb_Cantidad_Compras.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Compras.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btb_Cantidad_Compras.Location = new System.Drawing.Point(347, 128);
            this.Btb_Cantidad_Compras.Margin = new System.Windows.Forms.Padding(5);
            this.Btb_Cantidad_Compras.Name = "Btb_Cantidad_Compras";
            this.Btb_Cantidad_Compras.Size = new System.Drawing.Size(196, 59);
            this.Btb_Cantidad_Compras.TabIndex = 22;
            this.Btb_Cantidad_Compras.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Total_Compras
            // 
            this.Btn_Total_Compras.AutoSize = true;
            this.Btn_Total_Compras.Font = new System.Drawing.Font("Lucida Bright", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Total_Compras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Btn_Total_Compras.Location = new System.Drawing.Point(16, 22);
            this.Btn_Total_Compras.Name = "Btn_Total_Compras";
            this.Btn_Total_Compras.Size = new System.Drawing.Size(180, 43);
            this.Btn_Total_Compras.TabIndex = 19;
            this.Btn_Total_Compras.Text = "TOTAL :";
            // 
            // Btn_Des_Compras
            // 
            this.Btn_Des_Compras.ActiveBorderThickness = 2;
            this.Btn_Des_Compras.ActiveCornerRadius = 30;
            this.Btn_Des_Compras.ActiveFillColor = System.Drawing.Color.Teal;
            this.Btn_Des_Compras.ActiveForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Compras.ActiveLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Compras.BackColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Compras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Des_Compras.BackgroundImage")));
            this.Btn_Des_Compras.ButtonText = "Descuento";
            this.Btn_Des_Compras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Des_Compras.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Des_Compras.ForeColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Compras.IdleBorderThickness = 2;
            this.Btn_Des_Compras.IdleCornerRadius = 20;
            this.Btn_Des_Compras.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(47)))));
            this.Btn_Des_Compras.IdleForecolor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Compras.IdleLineColor = System.Drawing.Color.Gainsboro;
            this.Btn_Des_Compras.Location = new System.Drawing.Point(347, 197);
            this.Btn_Des_Compras.Margin = new System.Windows.Forms.Padding(5);
            this.Btn_Des_Compras.Name = "Btn_Des_Compras";
            this.Btn_Des_Compras.Size = new System.Drawing.Size(196, 59);
            this.Btn_Des_Compras.TabIndex = 20;
            this.Btn_Des_Compras.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_Buscar_Compras
            // 
            this.Btn_Buscar_Compras.AcceptsReturn = false;
            this.Btn_Buscar_Compras.AcceptsTab = false;
            this.Btn_Buscar_Compras.AnimationSpeed = 200;
            this.Btn_Buscar_Compras.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Btn_Buscar_Compras.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Btn_Buscar_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Buscar_Compras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Buscar_Compras.BackgroundImage")));
            this.Btn_Buscar_Compras.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Buscar_Compras.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Btn_Buscar_Compras.BorderColorHover = System.Drawing.Color.DimGray;
            this.Btn_Buscar_Compras.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Buscar_Compras.BorderRadius = 9;
            this.Btn_Buscar_Compras.BorderThickness = 3;
            this.Btn_Buscar_Compras.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Btn_Buscar_Compras.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Btn_Buscar_Compras.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Buscar_Compras.DefaultText = "";
            this.Btn_Buscar_Compras.FillColor = System.Drawing.Color.Gainsboro;
            this.Btn_Buscar_Compras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.Btn_Buscar_Compras.HideSelection = true;
            this.Btn_Buscar_Compras.IconLeft = ((System.Drawing.Image)(resources.GetObject("Btn_Buscar_Compras.IconLeft")));
            this.Btn_Buscar_Compras.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Btn_Buscar_Compras.IconPadding = 10;
            this.Btn_Buscar_Compras.IconRight = null;
            this.Btn_Buscar_Compras.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Btn_Buscar_Compras.Lines = new string[0];
            this.Btn_Buscar_Compras.Location = new System.Drawing.Point(39, 48);
            this.Btn_Buscar_Compras.MaxLength = 32767;
            this.Btn_Buscar_Compras.MinimumSize = new System.Drawing.Size(1, 1);
            this.Btn_Buscar_Compras.Modified = false;
            this.Btn_Buscar_Compras.Multiline = false;
            this.Btn_Buscar_Compras.Name = "Btn_Buscar_Compras";
            stateProperties9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties9.FillColor = System.Drawing.Color.Empty;
            stateProperties9.ForeColor = System.Drawing.Color.Empty;
            stateProperties9.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Btn_Buscar_Compras.OnActiveState = stateProperties9;
            stateProperties10.BorderColor = System.Drawing.Color.Empty;
            stateProperties10.FillColor = System.Drawing.Color.White;
            stateProperties10.ForeColor = System.Drawing.Color.Empty;
            stateProperties10.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Btn_Buscar_Compras.OnDisabledState = stateProperties10;
            stateProperties11.BorderColor = System.Drawing.Color.DimGray;
            stateProperties11.FillColor = System.Drawing.Color.Empty;
            stateProperties11.ForeColor = System.Drawing.Color.Empty;
            stateProperties11.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Btn_Buscar_Compras.OnHoverState = stateProperties11;
            stateProperties12.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties12.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            stateProperties12.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Btn_Buscar_Compras.OnIdleState = stateProperties12;
            this.Btn_Buscar_Compras.PasswordChar = '\0';
            this.Btn_Buscar_Compras.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Btn_Buscar_Compras.PlaceholderText = "Buscar";
            this.Btn_Buscar_Compras.ReadOnly = false;
            this.Btn_Buscar_Compras.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Btn_Buscar_Compras.SelectedText = "";
            this.Btn_Buscar_Compras.SelectionLength = 0;
            this.Btn_Buscar_Compras.SelectionStart = 0;
            this.Btn_Buscar_Compras.ShortcutsEnabled = true;
            this.Btn_Buscar_Compras.Size = new System.Drawing.Size(357, 52);
            this.Btn_Buscar_Compras.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Btn_Buscar_Compras.TabIndex = 73;
            this.Btn_Buscar_Compras.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Btn_Buscar_Compras.TextMarginBottom = 0;
            this.Btn_Buscar_Compras.TextMarginLeft = 5;
            this.Btn_Buscar_Compras.TextMarginTop = 0;
            this.Btn_Buscar_Compras.TextPlaceholder = "Buscar";
            this.Btn_Buscar_Compras.UseSystemPasswordChar = false;
            this.Btn_Buscar_Compras.WordWrap = true;
            // 
            // Tabla_Compras_Comp
            // 
            this.Tabla_Compras_Comp.AllowCustomTheming = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Compras_Comp.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.Tabla_Compras_Comp.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Tabla_Compras_Comp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Tabla_Compras_Comp.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.Tabla_Compras_Comp.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Tabla_Compras_Comp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.Tabla_Compras_Comp.ColumnHeadersHeight = 40;
            this.Tabla_Compras_Comp.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.Tabla_Compras_Comp.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Compras_Comp.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Compras_Comp.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Compras_Comp.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.Tabla_Compras_Comp.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.Tabla_Compras_Comp.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Compras_Comp.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Compras_Comp.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Compras_Comp.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.Tabla_Compras_Comp.CurrentTheme.HeaderStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            this.Tabla_Compras_Comp.CurrentTheme.HeaderStyle.SelectionForeColor = System.Drawing.Color.White;
            this.Tabla_Compras_Comp.CurrentTheme.Name = null;
            this.Tabla_Compras_Comp.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.Tabla_Compras_Comp.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.Tabla_Compras_Comp.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.Tabla_Compras_Comp.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.Tabla_Compras_Comp.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Tabla_Compras_Comp.DefaultCellStyle = dataGridViewCellStyle9;
            this.Tabla_Compras_Comp.EnableHeadersVisualStyles = false;
            this.Tabla_Compras_Comp.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.Tabla_Compras_Comp.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.Tabla_Compras_Comp.HeaderBgColor = System.Drawing.Color.Empty;
            this.Tabla_Compras_Comp.HeaderForeColor = System.Drawing.Color.White;
            this.Tabla_Compras_Comp.Location = new System.Drawing.Point(39, 187);
            this.Tabla_Compras_Comp.Name = "Tabla_Compras_Comp";
            this.Tabla_Compras_Comp.RowHeadersVisible = false;
            this.Tabla_Compras_Comp.RowHeadersWidth = 51;
            this.Tabla_Compras_Comp.RowTemplate.Height = 40;
            this.Tabla_Compras_Comp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Tabla_Compras_Comp.Size = new System.Drawing.Size(630, 404);
            this.Tabla_Compras_Comp.TabIndex = 77;
            this.Tabla_Compras_Comp.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            // 
            // Btn_Borrar_Compras
            // 
            this.Btn_Borrar_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Borrar_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Borrar_Compras.Image")));
            this.Btn_Borrar_Compras.ImageActive = null;
            this.Btn_Borrar_Compras.Location = new System.Drawing.Point(212, 373);
            this.Btn_Borrar_Compras.Name = "Btn_Borrar_Compras";
            this.Btn_Borrar_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_Borrar_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Borrar_Compras.TabIndex = 82;
            this.Btn_Borrar_Compras.TabStop = false;
            this.Btn_Borrar_Compras.Zoom = 10;
            // 
            // Btn_Coma_Compras
            // 
            this.Btn_Coma_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Coma_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Coma_Compras.Image")));
            this.Btn_Coma_Compras.ImageActive = null;
            this.Btn_Coma_Compras.Location = new System.Drawing.Point(24, 373);
            this.Btn_Coma_Compras.Name = "Btn_Coma_Compras";
            this.Btn_Coma_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_Coma_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Coma_Compras.TabIndex = 81;
            this.Btn_Coma_Compras.TabStop = false;
            this.Btn_Coma_Compras.Zoom = 10;
            // 
            // Btn_N0_Compras
            // 
            this.Btn_N0_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N0_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N0_Compras.Image")));
            this.Btn_N0_Compras.ImageActive = null;
            this.Btn_N0_Compras.Location = new System.Drawing.Point(118, 373);
            this.Btn_N0_Compras.Name = "Btn_N0_Compras";
            this.Btn_N0_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N0_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N0_Compras.TabIndex = 80;
            this.Btn_N0_Compras.TabStop = false;
            this.Btn_N0_Compras.Zoom = 10;
            // 
            // Btn_N9_Compras
            // 
            this.Btn_N9_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N9_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N9_Compras.Image")));
            this.Btn_N9_Compras.ImageActive = null;
            this.Btn_N9_Compras.Location = new System.Drawing.Point(212, 280);
            this.Btn_N9_Compras.Name = "Btn_N9_Compras";
            this.Btn_N9_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N9_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N9_Compras.TabIndex = 79;
            this.Btn_N9_Compras.TabStop = false;
            this.Btn_N9_Compras.Zoom = 10;
            // 
            // Btn_N8_Compras
            // 
            this.Btn_N8_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N8_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N8_Compras.Image")));
            this.Btn_N8_Compras.ImageActive = null;
            this.Btn_N8_Compras.Location = new System.Drawing.Point(118, 280);
            this.Btn_N8_Compras.Name = "Btn_N8_Compras";
            this.Btn_N8_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N8_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N8_Compras.TabIndex = 78;
            this.Btn_N8_Compras.TabStop = false;
            this.Btn_N8_Compras.Zoom = 10;
            // 
            // Btn_N7_Compras
            // 
            this.Btn_N7_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N7_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N7_Compras.Image")));
            this.Btn_N7_Compras.ImageActive = null;
            this.Btn_N7_Compras.Location = new System.Drawing.Point(24, 280);
            this.Btn_N7_Compras.Name = "Btn_N7_Compras";
            this.Btn_N7_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N7_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N7_Compras.TabIndex = 77;
            this.Btn_N7_Compras.TabStop = false;
            this.Btn_N7_Compras.Zoom = 10;
            // 
            // Btn_N6_Compras
            // 
            this.Btn_N6_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N6_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N6_Compras.Image")));
            this.Btn_N6_Compras.ImageActive = null;
            this.Btn_N6_Compras.Location = new System.Drawing.Point(212, 187);
            this.Btn_N6_Compras.Name = "Btn_N6_Compras";
            this.Btn_N6_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N6_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N6_Compras.TabIndex = 76;
            this.Btn_N6_Compras.TabStop = false;
            this.Btn_N6_Compras.Zoom = 10;
            // 
            // Btn_N5_Compras
            // 
            this.Btn_N5_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N5_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N5_Compras.Image")));
            this.Btn_N5_Compras.ImageActive = null;
            this.Btn_N5_Compras.Location = new System.Drawing.Point(118, 187);
            this.Btn_N5_Compras.Name = "Btn_N5_Compras";
            this.Btn_N5_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N5_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N5_Compras.TabIndex = 75;
            this.Btn_N5_Compras.TabStop = false;
            this.Btn_N5_Compras.Zoom = 10;
            // 
            // Btn_N4_Compras
            // 
            this.Btn_N4_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N4_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N4_Compras.Image")));
            this.Btn_N4_Compras.ImageActive = null;
            this.Btn_N4_Compras.Location = new System.Drawing.Point(24, 187);
            this.Btn_N4_Compras.Name = "Btn_N4_Compras";
            this.Btn_N4_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N4_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N4_Compras.TabIndex = 74;
            this.Btn_N4_Compras.TabStop = false;
            this.Btn_N4_Compras.Zoom = 10;
            // 
            // Btn_N3_Compras
            // 
            this.Btn_N3_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N3_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N3_Compras.Image")));
            this.Btn_N3_Compras.ImageActive = null;
            this.Btn_N3_Compras.Location = new System.Drawing.Point(212, 94);
            this.Btn_N3_Compras.Name = "Btn_N3_Compras";
            this.Btn_N3_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N3_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N3_Compras.TabIndex = 73;
            this.Btn_N3_Compras.TabStop = false;
            this.Btn_N3_Compras.Zoom = 10;
            // 
            // Btn_N2_Compras
            // 
            this.Btn_N2_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N2_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N2_Compras.Image")));
            this.Btn_N2_Compras.ImageActive = null;
            this.Btn_N2_Compras.Location = new System.Drawing.Point(118, 94);
            this.Btn_N2_Compras.Name = "Btn_N2_Compras";
            this.Btn_N2_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N2_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N2_Compras.TabIndex = 72;
            this.Btn_N2_Compras.TabStop = false;
            this.Btn_N2_Compras.Zoom = 10;
            // 
            // Btn_N1_Compras
            // 
            this.Btn_N1_Compras.BackColor = System.Drawing.Color.Transparent;
            this.Btn_N1_Compras.Image = ((System.Drawing.Image)(resources.GetObject("Btn_N1_Compras.Image")));
            this.Btn_N1_Compras.ImageActive = null;
            this.Btn_N1_Compras.Location = new System.Drawing.Point(24, 94);
            this.Btn_N1_Compras.Name = "Btn_N1_Compras";
            this.Btn_N1_Compras.Size = new System.Drawing.Size(94, 93);
            this.Btn_N1_Compras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_N1_Compras.TabIndex = 71;
            this.Btn_N1_Compras.TabStop = false;
            this.Btn_N1_Compras.Zoom = 10;
            // 
            // Compras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1316, 723);
            this.Controls.Add(this.Tabla_Compras_Comp);
            this.Controls.Add(this.Btn_AgregarProd_Compra);
            this.Controls.Add(this.Btn_Eliminar_Compras);
            this.Controls.Add(this.Panel_TecladoNumerico_Comp);
            this.Controls.Add(this.Btn_Buscar_Compras);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Compras";
            this.Text = "Compras";
            this.Panel_TecladoNumerico_Comp.ResumeLayout(false);
            this.Panel_TecladoNumerico_Comp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Compras_Comp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Borrar_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Coma_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N0_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N9_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N8_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N7_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N6_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N5_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N4_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N3_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N2_Compras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_N1_Compras)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 Btn_AgregarProd_Compra;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Eliminar_Compras;
        private Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel Panel_TecladoNumerico_Comp;
        private Guna.UI.WinForms.GunaLabel Btn_Res_Desc_Compras;
        private Guna.UI.WinForms.GunaLabel Btn_Desc_Compras;
        private Guna.UI.WinForms.GunaLabel Btn_Res_SubT_Compras;
        private Guna.UI.WinForms.GunaLabel Btn_SubT_Compras;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Crédito_Compras;
        private Guna.UI.WinForms.GunaLabel Btn_ResT_Compras;
        private Bunifu.Framework.UI.BunifuThinButton2 Btb_Cantidad_Compras;
        private Guna.UI.WinForms.GunaLabel Btn_Total_Compras;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Des_Compras;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Btn_Buscar_Compras;
        private Bunifu.UI.WinForms.BunifuDataGridView Tabla_Compras_Comp;
        private Bunifu.Framework.UI.BunifuThinButton2 Btn_Efectivo_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Borrar_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Coma_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N0_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N9_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N8_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N7_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N6_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N5_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N4_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N3_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N2_Compras;
        private Bunifu.Framework.UI.BunifuImageButton Btn_N1_Compras;
    }
}