﻿namespace Pulpería_Lovo.Presentaciones
{
    partial class backups
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(backups));
            this.Panel_PrincipalTop = new System.Windows.Forms.Panel();
            this.Btn_Maximizar_Prin = new System.Windows.Forms.PictureBox();
            this.Btn_Restaurar_Prin = new System.Windows.Forms.PictureBox();
            this.Btn_Minimizar_Prin = new System.Windows.Forms.PictureBox();
            this.Btn_Salir_Prin = new System.Windows.Forms.PictureBox();
            this.Lb_Menú_Prin = new System.Windows.Forms.Label();
            this.Btn_Menu_Prin = new System.Windows.Forms.PictureBox();
            this.panel_Menu = new System.Windows.Forms.Panel();
            this.MarcaAgua_Logo_Prin = new System.Windows.Forms.PictureBox();
            this.Lb_Fecha_Prin = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Hora_Prin = new Guna.UI.WinForms.GunaLabel();
            this.Panel_PrincipalTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Maximizar_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Restaurar_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Minimizar_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Salir_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Menu_Prin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MarcaAgua_Logo_Prin)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel_PrincipalTop
            // 
            this.Panel_PrincipalTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Maximizar_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Restaurar_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Minimizar_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Salir_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Lb_Menú_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.Btn_Menu_Prin);
            this.Panel_PrincipalTop.Controls.Add(this.panel_Menu);
            this.Panel_PrincipalTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_PrincipalTop.Location = new System.Drawing.Point(0, 0);
            this.Panel_PrincipalTop.Margin = new System.Windows.Forms.Padding(2);
            this.Panel_PrincipalTop.Name = "Panel_PrincipalTop";
            this.Panel_PrincipalTop.Size = new System.Drawing.Size(1236, 65);
            this.Panel_PrincipalTop.TabIndex = 4;
            // 
            // Btn_Maximizar_Prin
            // 
            this.Btn_Maximizar_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Maximizar_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Maximizar_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Maximizar_Prin.Image")));
            this.Btn_Maximizar_Prin.Location = new System.Drawing.Point(1163, 22);
            this.Btn_Maximizar_Prin.Margin = new System.Windows.Forms.Padding(2);
            this.Btn_Maximizar_Prin.Name = "Btn_Maximizar_Prin";
            this.Btn_Maximizar_Prin.Size = new System.Drawing.Size(27, 25);
            this.Btn_Maximizar_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Maximizar_Prin.TabIndex = 3;
            this.Btn_Maximizar_Prin.TabStop = false;
            // 
            // Btn_Restaurar_Prin
            // 
            this.Btn_Restaurar_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Restaurar_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Restaurar_Prin.Image")));
            this.Btn_Restaurar_Prin.Location = new System.Drawing.Point(1163, 22);
            this.Btn_Restaurar_Prin.Margin = new System.Windows.Forms.Padding(2);
            this.Btn_Restaurar_Prin.Name = "Btn_Restaurar_Prin";
            this.Btn_Restaurar_Prin.Size = new System.Drawing.Size(27, 25);
            this.Btn_Restaurar_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Restaurar_Prin.TabIndex = 3;
            this.Btn_Restaurar_Prin.TabStop = false;
            this.Btn_Restaurar_Prin.Visible = false;
            // 
            // Btn_Minimizar_Prin
            // 
            this.Btn_Minimizar_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Minimizar_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Minimizar_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Minimizar_Prin.Image")));
            this.Btn_Minimizar_Prin.Location = new System.Drawing.Point(1132, 22);
            this.Btn_Minimizar_Prin.Margin = new System.Windows.Forms.Padding(2);
            this.Btn_Minimizar_Prin.Name = "Btn_Minimizar_Prin";
            this.Btn_Minimizar_Prin.Size = new System.Drawing.Size(27, 25);
            this.Btn_Minimizar_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Minimizar_Prin.TabIndex = 3;
            this.Btn_Minimizar_Prin.TabStop = false;
            // 
            // Btn_Salir_Prin
            // 
            this.Btn_Salir_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Salir_Prin.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Salir_Prin.ErrorImage = null;
            this.Btn_Salir_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Salir_Prin.Image")));
            this.Btn_Salir_Prin.Location = new System.Drawing.Point(1195, 22);
            this.Btn_Salir_Prin.Margin = new System.Windows.Forms.Padding(2);
            this.Btn_Salir_Prin.Name = "Btn_Salir_Prin";
            this.Btn_Salir_Prin.Size = new System.Drawing.Size(22, 25);
            this.Btn_Salir_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btn_Salir_Prin.TabIndex = 4;
            this.Btn_Salir_Prin.TabStop = false;
            // 
            // Lb_Menú_Prin
            // 
            this.Lb_Menú_Prin.AutoSize = true;
            this.Lb_Menú_Prin.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Menú_Prin.ForeColor = System.Drawing.Color.White;
            this.Lb_Menú_Prin.Location = new System.Drawing.Point(68, 26);
            this.Lb_Menú_Prin.Name = "Lb_Menú_Prin";
            this.Lb_Menú_Prin.Size = new System.Drawing.Size(67, 22);
            this.Lb_Menú_Prin.TabIndex = 3;
            this.Lb_Menú_Prin.Text = "MENU";
            this.Lb_Menú_Prin.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Btn_Menu_Prin
            // 
            this.Btn_Menu_Prin.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Menu_Prin.Image")));
            this.Btn_Menu_Prin.Location = new System.Drawing.Point(21, 26);
            this.Btn_Menu_Prin.Margin = new System.Windows.Forms.Padding(2);
            this.Btn_Menu_Prin.Name = "Btn_Menu_Prin";
            this.Btn_Menu_Prin.Size = new System.Drawing.Size(27, 25);
            this.Btn_Menu_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Menu_Prin.TabIndex = 2;
            this.Btn_Menu_Prin.TabStop = false;
            // 
            // panel_Menu
            // 
            this.panel_Menu.Location = new System.Drawing.Point(2, 70);
            this.panel_Menu.Margin = new System.Windows.Forms.Padding(2);
            this.panel_Menu.Name = "panel_Menu";
            this.panel_Menu.Size = new System.Drawing.Size(225, 666);
            this.panel_Menu.TabIndex = 1;
            // 
            // MarcaAgua_Logo_Prin
            // 
            this.MarcaAgua_Logo_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MarcaAgua_Logo_Prin.Image = ((System.Drawing.Image)(resources.GetObject("MarcaAgua_Logo_Prin.Image")));
            this.MarcaAgua_Logo_Prin.Location = new System.Drawing.Point(496, 153);
            this.MarcaAgua_Logo_Prin.Margin = new System.Windows.Forms.Padding(2);
            this.MarcaAgua_Logo_Prin.Name = "MarcaAgua_Logo_Prin";
            this.MarcaAgua_Logo_Prin.Size = new System.Drawing.Size(406, 469);
            this.MarcaAgua_Logo_Prin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MarcaAgua_Logo_Prin.TabIndex = 9;
            this.MarcaAgua_Logo_Prin.TabStop = false;
            // 
            // Lb_Fecha_Prin
            // 
            this.Lb_Fecha_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lb_Fecha_Prin.AutoSize = true;
            this.Lb_Fecha_Prin.Font = new System.Drawing.Font("Lucida Bright", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Fecha_Prin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Fecha_Prin.Location = new System.Drawing.Point(335, 598);
            this.Lb_Fecha_Prin.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Fecha_Prin.Name = "Lb_Fecha_Prin";
            this.Lb_Fecha_Prin.Size = new System.Drawing.Size(83, 27);
            this.Lb_Fecha_Prin.TabIndex = 8;
            this.Lb_Fecha_Prin.Text = "Fecha";
            // 
            // Lb_Hora_Prin
            // 
            this.Lb_Hora_Prin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lb_Hora_Prin.AutoSize = true;
            this.Lb_Hora_Prin.Font = new System.Drawing.Font("Lucida Bright", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Hora_Prin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Hora_Prin.Location = new System.Drawing.Point(407, 634);
            this.Lb_Hora_Prin.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lb_Hora_Prin.Name = "Lb_Hora_Prin";
            this.Lb_Hora_Prin.Size = new System.Drawing.Size(80, 31);
            this.Lb_Hora_Prin.TabIndex = 7;
            this.Lb_Hora_Prin.Text = "Hora";
            // 
            // backups
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1236, 818);
            this.Controls.Add(this.MarcaAgua_Logo_Prin);
            this.Controls.Add(this.Lb_Fecha_Prin);
            this.Controls.Add(this.Lb_Hora_Prin);
            this.Controls.Add(this.Panel_PrincipalTop);
            this.Name = "backups";
            this.Text = "backups";
            this.Panel_PrincipalTop.ResumeLayout(false);
            this.Panel_PrincipalTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Maximizar_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Restaurar_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Minimizar_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Salir_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Menu_Prin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MarcaAgua_Logo_Prin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel Panel_PrincipalTop;
        private System.Windows.Forms.PictureBox Btn_Maximizar_Prin;
        private System.Windows.Forms.PictureBox Btn_Restaurar_Prin;
        private System.Windows.Forms.PictureBox Btn_Minimizar_Prin;
        private System.Windows.Forms.PictureBox Btn_Salir_Prin;
        private System.Windows.Forms.Label Lb_Menú_Prin;
        private System.Windows.Forms.PictureBox Btn_Menu_Prin;
        private System.Windows.Forms.Panel panel_Menu;
        private System.Windows.Forms.PictureBox MarcaAgua_Logo_Prin;
        private Guna.UI.WinForms.GunaLabel Lb_Fecha_Prin;
        private Guna.UI.WinForms.GunaLabel Lb_Hora_Prin;
    }
}