﻿namespace Pulpería_Lovo.Presentaciones
{
    partial class Cátalogo_Prod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cátalogo_Prod));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            this.Txt_Buscar_Cata = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Lb_Cáta_Prod_Cata = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Nuevo_Cáta = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Eliminar_Cáta = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Editar_Cáta = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Guardar_Cáta = new Bunifu.Framework.UI.BunifuImageButton();
            this.Lb_Eliminar_CátaPro = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Buscar_CátaProd = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Nuevo_CátaProd = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Editar_CátaProd = new Guna.UI.WinForms.GunaLabel();
            this.Tabla_Productos = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Cáta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Cáta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Cáta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Cáta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Productos)).BeginInit();
            this.SuspendLayout();
            // 
            // Txt_Buscar_Cata
            // 
            this.Txt_Buscar_Cata.AcceptsReturn = false;
            this.Txt_Buscar_Cata.AcceptsTab = false;
            this.Txt_Buscar_Cata.AnimationSpeed = 200;
            this.Txt_Buscar_Cata.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Buscar_Cata.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Buscar_Cata.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Buscar_Cata.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cata.BackgroundImage")));
            this.Txt_Buscar_Cata.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Cata.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Buscar_Cata.BorderColorHover = System.Drawing.Color.DimGray;
            this.Txt_Buscar_Cata.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Cata.BorderRadius = 9;
            this.Txt_Buscar_Cata.BorderThickness = 3;
            this.Txt_Buscar_Cata.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Buscar_Cata.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cata.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buscar_Cata.DefaultText = "";
            this.Txt_Buscar_Cata.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Buscar_Cata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Cata.HideSelection = true;
            this.Txt_Buscar_Cata.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Cata.IconLeft")));
            this.Txt_Buscar_Cata.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cata.IconPadding = 10;
            this.Txt_Buscar_Cata.IconRight = null;
            this.Txt_Buscar_Cata.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Cata.Lines = new string[0];
            this.Txt_Buscar_Cata.Location = new System.Drawing.Point(40, 37);
            this.Txt_Buscar_Cata.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txt_Buscar_Cata.MaxLength = 32767;
            this.Txt_Buscar_Cata.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Buscar_Cata.Modified = false;
            this.Txt_Buscar_Cata.Multiline = false;
            this.Txt_Buscar_Cata.Name = "Txt_Buscar_Cata";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties1.FillColor = System.Drawing.Color.Empty;
            stateProperties1.ForeColor = System.Drawing.Color.Empty;
            stateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cata.OnActiveState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.Empty;
            stateProperties2.FillColor = System.Drawing.Color.White;
            stateProperties2.ForeColor = System.Drawing.Color.Empty;
            stateProperties2.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cata.OnDisabledState = stateProperties2;
            stateProperties3.BorderColor = System.Drawing.Color.DimGray;
            stateProperties3.FillColor = System.Drawing.Color.Empty;
            stateProperties3.ForeColor = System.Drawing.Color.Empty;
            stateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cata.OnHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties4.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Cata.OnIdleState = stateProperties4;
            this.Txt_Buscar_Cata.PasswordChar = '\0';
            this.Txt_Buscar_Cata.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Cata.PlaceholderText = "Buscar";
            this.Txt_Buscar_Cata.ReadOnly = false;
            this.Txt_Buscar_Cata.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Buscar_Cata.SelectedText = "";
            this.Txt_Buscar_Cata.SelectionLength = 0;
            this.Txt_Buscar_Cata.SelectionStart = 0;
            this.Txt_Buscar_Cata.ShortcutsEnabled = true;
            this.Txt_Buscar_Cata.Size = new System.Drawing.Size(357, 52);
            this.Txt_Buscar_Cata.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Buscar_Cata.TabIndex = 15;
            this.Txt_Buscar_Cata.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Buscar_Cata.TextMarginBottom = 0;
            this.Txt_Buscar_Cata.TextMarginLeft = 5;
            this.Txt_Buscar_Cata.TextMarginTop = 0;
            this.Txt_Buscar_Cata.TextPlaceholder = "Buscar";
            this.Txt_Buscar_Cata.UseSystemPasswordChar = false;
            this.Txt_Buscar_Cata.WordWrap = true;
            // 
            // Lb_Cáta_Prod_Cata
            // 
            this.Lb_Cáta_Prod_Cata.AutoSize = true;
            this.Lb_Cáta_Prod_Cata.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Cáta_Prod_Cata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_Cáta_Prod_Cata.Location = new System.Drawing.Point(35, 154);
            this.Lb_Cáta_Prod_Cata.Name = "Lb_Cáta_Prod_Cata";
            this.Lb_Cáta_Prod_Cata.Size = new System.Drawing.Size(284, 27);
            this.Lb_Cáta_Prod_Cata.TabIndex = 16;
            this.Lb_Cáta_Prod_Cata.Text = "Cátalogo de Productos";
            // 
            // Btn_Nuevo_Cáta
            // 
            this.Btn_Nuevo_Cáta.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Nuevo_Cáta.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Nuevo_Cáta.Image")));
            this.Btn_Nuevo_Cáta.ImageActive = null;
            this.Btn_Nuevo_Cáta.Location = new System.Drawing.Point(677, 30);
            this.Btn_Nuevo_Cáta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Nuevo_Cáta.Name = "Btn_Nuevo_Cáta";
            this.Btn_Nuevo_Cáta.Size = new System.Drawing.Size(61, 60);
            this.Btn_Nuevo_Cáta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Nuevo_Cáta.TabIndex = 37;
            this.Btn_Nuevo_Cáta.TabStop = false;
            this.Btn_Nuevo_Cáta.Zoom = 10;
            this.Btn_Nuevo_Cáta.Click += new System.EventHandler(this.Btn_Nuevo_Cáta_Click);
            // 
            // Btn_Eliminar_Cáta
            // 
            this.Btn_Eliminar_Cáta.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Eliminar_Cáta.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar_Cáta.Image")));
            this.Btn_Eliminar_Cáta.ImageActive = null;
            this.Btn_Eliminar_Cáta.Location = new System.Drawing.Point(859, 30);
            this.Btn_Eliminar_Cáta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Eliminar_Cáta.Name = "Btn_Eliminar_Cáta";
            this.Btn_Eliminar_Cáta.Size = new System.Drawing.Size(61, 60);
            this.Btn_Eliminar_Cáta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Eliminar_Cáta.TabIndex = 36;
            this.Btn_Eliminar_Cáta.TabStop = false;
            this.Btn_Eliminar_Cáta.Zoom = 10;
            // 
            // Btn_Editar_Cáta
            // 
            this.Btn_Editar_Cáta.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Editar_Cáta.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Editar_Cáta.Image")));
            this.Btn_Editar_Cáta.ImageActive = null;
            this.Btn_Editar_Cáta.Location = new System.Drawing.Point(768, 30);
            this.Btn_Editar_Cáta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Editar_Cáta.Name = "Btn_Editar_Cáta";
            this.Btn_Editar_Cáta.Size = new System.Drawing.Size(61, 60);
            this.Btn_Editar_Cáta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Editar_Cáta.TabIndex = 35;
            this.Btn_Editar_Cáta.TabStop = false;
            this.Btn_Editar_Cáta.Zoom = 10;
            // 
            // Btn_Guardar_Cáta
            // 
            this.Btn_Guardar_Cáta.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Guardar_Cáta.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Guardar_Cáta.Image")));
            this.Btn_Guardar_Cáta.ImageActive = null;
            this.Btn_Guardar_Cáta.Location = new System.Drawing.Point(435, 30);
            this.Btn_Guardar_Cáta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Guardar_Cáta.Name = "Btn_Guardar_Cáta";
            this.Btn_Guardar_Cáta.Size = new System.Drawing.Size(61, 60);
            this.Btn_Guardar_Cáta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Guardar_Cáta.TabIndex = 41;
            this.Btn_Guardar_Cáta.TabStop = false;
            this.Btn_Guardar_Cáta.Zoom = 10;
            // 
            // Lb_Eliminar_CátaPro
            // 
            this.Lb_Eliminar_CátaPro.AutoSize = true;
            this.Lb_Eliminar_CátaPro.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Eliminar_CátaPro.Location = new System.Drawing.Point(847, 94);
            this.Lb_Eliminar_CátaPro.Name = "Lb_Eliminar_CátaPro";
            this.Lb_Eliminar_CátaPro.Size = new System.Drawing.Size(88, 22);
            this.Lb_Eliminar_CátaPro.TabIndex = 88;
            this.Lb_Eliminar_CátaPro.Text = "Eliminar";
            // 
            // Lb_Buscar_CátaProd
            // 
            this.Lb_Buscar_CátaProd.AutoSize = true;
            this.Lb_Buscar_CátaProd.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Buscar_CátaProd.Location = new System.Drawing.Point(427, 94);
            this.Lb_Buscar_CátaProd.Name = "Lb_Buscar_CátaProd";
            this.Lb_Buscar_CátaProd.Size = new System.Drawing.Size(75, 22);
            this.Lb_Buscar_CátaProd.TabIndex = 89;
            this.Lb_Buscar_CátaProd.Text = "Buscar";
            // 
            // Lb_Nuevo_CátaProd
            // 
            this.Lb_Nuevo_CátaProd.AutoSize = true;
            this.Lb_Nuevo_CátaProd.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nuevo_CátaProd.Location = new System.Drawing.Point(671, 94);
            this.Lb_Nuevo_CátaProd.Name = "Lb_Nuevo_CátaProd";
            this.Lb_Nuevo_CátaProd.Size = new System.Drawing.Size(73, 22);
            this.Lb_Nuevo_CátaProd.TabIndex = 90;
            this.Lb_Nuevo_CátaProd.Text = "Nuevo";
            // 
            // Lb_Editar_CátaProd
            // 
            this.Lb_Editar_CátaProd.AutoSize = true;
            this.Lb_Editar_CátaProd.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Editar_CátaProd.Location = new System.Drawing.Point(765, 94);
            this.Lb_Editar_CátaProd.Name = "Lb_Editar_CátaProd";
            this.Lb_Editar_CátaProd.Size = new System.Drawing.Size(67, 22);
            this.Lb_Editar_CátaProd.TabIndex = 91;
            this.Lb_Editar_CátaProd.Text = "Editar";
            // 
            // Tabla_Productos
            // 
            this.Tabla_Productos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Tabla_Productos.Location = new System.Drawing.Point(40, 251);
            this.Tabla_Productos.Name = "Tabla_Productos";
            this.Tabla_Productos.RowHeadersWidth = 51;
            this.Tabla_Productos.RowTemplate.Height = 24;
            this.Tabla_Productos.Size = new System.Drawing.Size(916, 466);
            this.Tabla_Productos.TabIndex = 113;
            // 
            // Cátalogo_Prod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1006, 759);
            this.Controls.Add(this.Tabla_Productos);
            this.Controls.Add(this.Lb_Editar_CátaProd);
            this.Controls.Add(this.Lb_Nuevo_CátaProd);
            this.Controls.Add(this.Lb_Buscar_CátaProd);
            this.Controls.Add(this.Lb_Eliminar_CátaPro);
            this.Controls.Add(this.Btn_Guardar_Cáta);
            this.Controls.Add(this.Btn_Nuevo_Cáta);
            this.Controls.Add(this.Btn_Eliminar_Cáta);
            this.Controls.Add(this.Btn_Editar_Cáta);
            this.Controls.Add(this.Lb_Cáta_Prod_Cata);
            this.Controls.Add(this.Txt_Buscar_Cata);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Cátalogo_Prod";
            this.Text = "Catalogo";
            this.Load += new System.EventHandler(this.Cátalogo_Prod_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Catalogo_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Cáta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Cáta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Cáta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Cáta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Productos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Buscar_Cata;
        private Guna.UI.WinForms.GunaLabel Lb_Cáta_Prod_Cata;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Nuevo_Cáta;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Eliminar_Cáta;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Editar_Cáta;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Guardar_Cáta;
        private Guna.UI.WinForms.GunaLabel Lb_Eliminar_CátaPro;
        private Guna.UI.WinForms.GunaLabel Lb_Buscar_CátaProd;
        private Guna.UI.WinForms.GunaLabel Lb_Nuevo_CátaProd;
        private Guna.UI.WinForms.GunaLabel Lb_Editar_CátaProd;
        private System.Windows.Forms.DataGridView Tabla_Productos;
    }
}