﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CONVO_II.Presentaciones;

namespace CONVO_II.Presentaciones
{
    public partial class ProgressBar : Form
    {
        public ProgressBar()
        {
            InitializeComponent();
        }

        int Aux = 1;
        private void Control_Progress_Tick(object sender, EventArgs e)
        {
          if (Progress_Circ.Value == 90)
            {
                Aux = -1;       //Se reduce
                Progress_Circ.animationIterval = 6;             //Intervalos en lo que el progressbar se contrae
            }

         else

         if(Progress_Circ.Value == 10)
            {
                Aux = +1;       //Se Expande
                Progress_Circ.animationIterval = 4;
            }

            Progress_Circ.Value += Aux;

        }
    }
}
