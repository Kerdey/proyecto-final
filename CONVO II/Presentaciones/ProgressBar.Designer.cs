﻿
namespace CONVO_II.Presentaciones
{
    partial class ProgressBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgressBar));
            this.Progress_Circ = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.Control_Progress = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // Progress_Circ
            // 
            this.Progress_Circ.animated = true;
            this.Progress_Circ.animationIterval = 1;
            this.Progress_Circ.animationSpeed = 1;
            this.Progress_Circ.BackColor = System.Drawing.Color.Transparent;
            this.Progress_Circ.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Progress_Circ.BackgroundImage")));
            this.Progress_Circ.Font = new System.Drawing.Font("Lucida Bright", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Progress_Circ.ForeColor = System.Drawing.Color.Gainsboro;
            this.Progress_Circ.LabelVisible = true;
            this.Progress_Circ.LineProgressThickness = 8;
            this.Progress_Circ.LineThickness = 5;
            this.Progress_Circ.Location = new System.Drawing.Point(32, 20);
            this.Progress_Circ.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.Progress_Circ.MaxValue = 100;
            this.Progress_Circ.Name = "Progress_Circ";
            this.Progress_Circ.ProgressBackColor = System.Drawing.Color.Gainsboro;
            this.Progress_Circ.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(56)))), ((int)(((byte)(76)))));
            this.Progress_Circ.Size = new System.Drawing.Size(178, 178);
            this.Progress_Circ.TabIndex = 0;
            this.Progress_Circ.Value = 0;
            // 
            // Control_Progress
            // 
            this.Control_Progress.Enabled = true;
            this.Control_Progress.Interval = 140;
            this.Control_Progress.Tick += new System.EventHandler(this.Control_Progress_Tick);
            // 
            // ProgressBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(256, 231);
            this.Controls.Add(this.Progress_Circ);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ProgressBar";
            this.Opacity = 0.3D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "backups";
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCircleProgressbar Progress_Circ;
        private System.Windows.Forms.Timer Control_Progress;
    }
}