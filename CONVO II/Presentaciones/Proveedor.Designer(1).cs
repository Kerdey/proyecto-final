﻿namespace Pulpería_Lovo.Presentaciones
{
    partial class Proveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Proveedor));
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            this.Lb_Editar_Proveedor = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Nuevo_Proveedor = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Buscar_Proveedor = new Guna.UI.WinForms.GunaLabel();
            this.Lb_Eliminar_Proveedor = new Guna.UI.WinForms.GunaLabel();
            this.Btn_Guardar_Proveedor = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Nuevo_Proveedor = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Eliminar_Proveedor = new Bunifu.Framework.UI.BunifuImageButton();
            this.Btn_Editar_Proveedor = new Bunifu.Framework.UI.BunifuImageButton();
            this.Lb_ListProveedor = new Guna.UI.WinForms.GunaLabel();
            this.Txt_Buscar_Proveedor = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.Tabla_Proveedor = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Proveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Proveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Proveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Proveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Proveedor)).BeginInit();
            this.SuspendLayout();
            // 
            // Lb_Editar_Proveedor
            // 
            this.Lb_Editar_Proveedor.AutoSize = true;
            this.Lb_Editar_Proveedor.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Editar_Proveedor.Location = new System.Drawing.Point(768, 104);
            this.Lb_Editar_Proveedor.Name = "Lb_Editar_Proveedor";
            this.Lb_Editar_Proveedor.Size = new System.Drawing.Size(67, 22);
            this.Lb_Editar_Proveedor.TabIndex = 111;
            this.Lb_Editar_Proveedor.Text = "Editar";
            // 
            // Lb_Nuevo_Proveedor
            // 
            this.Lb_Nuevo_Proveedor.AutoSize = true;
            this.Lb_Nuevo_Proveedor.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nuevo_Proveedor.Location = new System.Drawing.Point(674, 104);
            this.Lb_Nuevo_Proveedor.Name = "Lb_Nuevo_Proveedor";
            this.Lb_Nuevo_Proveedor.Size = new System.Drawing.Size(73, 22);
            this.Lb_Nuevo_Proveedor.TabIndex = 110;
            this.Lb_Nuevo_Proveedor.Text = "Nuevo";
            // 
            // Lb_Buscar_Proveedor
            // 
            this.Lb_Buscar_Proveedor.AutoSize = true;
            this.Lb_Buscar_Proveedor.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Buscar_Proveedor.Location = new System.Drawing.Point(420, 104);
            this.Lb_Buscar_Proveedor.Name = "Lb_Buscar_Proveedor";
            this.Lb_Buscar_Proveedor.Size = new System.Drawing.Size(75, 22);
            this.Lb_Buscar_Proveedor.TabIndex = 109;
            this.Lb_Buscar_Proveedor.Text = "Buscar";
            // 
            // Lb_Eliminar_Proveedor
            // 
            this.Lb_Eliminar_Proveedor.AutoSize = true;
            this.Lb_Eliminar_Proveedor.Font = new System.Drawing.Font("Lucida Bright", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Eliminar_Proveedor.Location = new System.Drawing.Point(850, 104);
            this.Lb_Eliminar_Proveedor.Name = "Lb_Eliminar_Proveedor";
            this.Lb_Eliminar_Proveedor.Size = new System.Drawing.Size(88, 22);
            this.Lb_Eliminar_Proveedor.TabIndex = 108;
            this.Lb_Eliminar_Proveedor.Text = "Eliminar";
            // 
            // Btn_Guardar_Proveedor
            // 
            this.Btn_Guardar_Proveedor.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Guardar_Proveedor.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Guardar_Proveedor.Image")));
            this.Btn_Guardar_Proveedor.ImageActive = null;
            this.Btn_Guardar_Proveedor.Location = new System.Drawing.Point(428, 39);
            this.Btn_Guardar_Proveedor.Name = "Btn_Guardar_Proveedor";
            this.Btn_Guardar_Proveedor.Size = new System.Drawing.Size(62, 60);
            this.Btn_Guardar_Proveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Guardar_Proveedor.TabIndex = 107;
            this.Btn_Guardar_Proveedor.TabStop = false;
            this.Btn_Guardar_Proveedor.Zoom = 10;
            // 
            // Btn_Nuevo_Proveedor
            // 
            this.Btn_Nuevo_Proveedor.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Nuevo_Proveedor.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Nuevo_Proveedor.Image")));
            this.Btn_Nuevo_Proveedor.ImageActive = null;
            this.Btn_Nuevo_Proveedor.Location = new System.Drawing.Point(681, 39);
            this.Btn_Nuevo_Proveedor.Name = "Btn_Nuevo_Proveedor";
            this.Btn_Nuevo_Proveedor.Size = new System.Drawing.Size(62, 60);
            this.Btn_Nuevo_Proveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Nuevo_Proveedor.TabIndex = 106;
            this.Btn_Nuevo_Proveedor.TabStop = false;
            this.Btn_Nuevo_Proveedor.Zoom = 10;
            // 
            // Btn_Eliminar_Proveedor
            // 
            this.Btn_Eliminar_Proveedor.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Eliminar_Proveedor.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar_Proveedor.Image")));
            this.Btn_Eliminar_Proveedor.ImageActive = null;
            this.Btn_Eliminar_Proveedor.Location = new System.Drawing.Point(862, 39);
            this.Btn_Eliminar_Proveedor.Name = "Btn_Eliminar_Proveedor";
            this.Btn_Eliminar_Proveedor.Size = new System.Drawing.Size(62, 60);
            this.Btn_Eliminar_Proveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Eliminar_Proveedor.TabIndex = 105;
            this.Btn_Eliminar_Proveedor.TabStop = false;
            this.Btn_Eliminar_Proveedor.Zoom = 10;
            // 
            // Btn_Editar_Proveedor
            // 
            this.Btn_Editar_Proveedor.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Editar_Proveedor.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Editar_Proveedor.Image")));
            this.Btn_Editar_Proveedor.ImageActive = null;
            this.Btn_Editar_Proveedor.Location = new System.Drawing.Point(771, 39);
            this.Btn_Editar_Proveedor.Name = "Btn_Editar_Proveedor";
            this.Btn_Editar_Proveedor.Size = new System.Drawing.Size(62, 60);
            this.Btn_Editar_Proveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Editar_Proveedor.TabIndex = 104;
            this.Btn_Editar_Proveedor.TabStop = false;
            this.Btn_Editar_Proveedor.Zoom = 10;
            // 
            // Lb_ListProveedor
            // 
            this.Lb_ListProveedor.AutoSize = true;
            this.Lb_ListProveedor.Font = new System.Drawing.Font("Lucida Bright", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_ListProveedor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Lb_ListProveedor.Location = new System.Drawing.Point(29, 159);
            this.Lb_ListProveedor.Name = "Lb_ListProveedor";
            this.Lb_ListProveedor.Size = new System.Drawing.Size(333, 34);
            this.Lb_ListProveedor.TabIndex = 103;
            this.Lb_ListProveedor.Text = "Lista de Proveedores";
            // 
            // Txt_Buscar_Proveedor
            // 
            this.Txt_Buscar_Proveedor.AcceptsReturn = false;
            this.Txt_Buscar_Proveedor.AcceptsTab = false;
            this.Txt_Buscar_Proveedor.AnimationSpeed = 200;
            this.Txt_Buscar_Proveedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Txt_Buscar_Proveedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Txt_Buscar_Proveedor.BackColor = System.Drawing.Color.Transparent;
            this.Txt_Buscar_Proveedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Proveedor.BackgroundImage")));
            this.Txt_Buscar_Proveedor.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Proveedor.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.Txt_Buscar_Proveedor.BorderColorHover = System.Drawing.Color.DimGray;
            this.Txt_Buscar_Proveedor.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Proveedor.BorderRadius = 9;
            this.Txt_Buscar_Proveedor.BorderThickness = 3;
            this.Txt_Buscar_Proveedor.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Txt_Buscar_Proveedor.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Proveedor.DefaultFont = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buscar_Proveedor.DefaultText = "";
            this.Txt_Buscar_Proveedor.FillColor = System.Drawing.Color.Gainsboro;
            this.Txt_Buscar_Proveedor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            this.Txt_Buscar_Proveedor.HideSelection = true;
            this.Txt_Buscar_Proveedor.IconLeft = ((System.Drawing.Image)(resources.GetObject("Txt_Buscar_Proveedor.IconLeft")));
            this.Txt_Buscar_Proveedor.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Proveedor.IconPadding = 10;
            this.Txt_Buscar_Proveedor.IconRight = null;
            this.Txt_Buscar_Proveedor.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.Txt_Buscar_Proveedor.Lines = new string[0];
            this.Txt_Buscar_Proveedor.Location = new System.Drawing.Point(34, 47);
            this.Txt_Buscar_Proveedor.MaxLength = 32767;
            this.Txt_Buscar_Proveedor.MinimumSize = new System.Drawing.Size(1, 1);
            this.Txt_Buscar_Proveedor.Modified = false;
            this.Txt_Buscar_Proveedor.Multiline = false;
            this.Txt_Buscar_Proveedor.Name = "Txt_Buscar_Proveedor";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties1.FillColor = System.Drawing.Color.Empty;
            stateProperties1.ForeColor = System.Drawing.Color.Empty;
            stateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Proveedor.OnActiveState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.Empty;
            stateProperties2.FillColor = System.Drawing.Color.White;
            stateProperties2.ForeColor = System.Drawing.Color.Empty;
            stateProperties2.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Proveedor.OnDisabledState = stateProperties2;
            stateProperties3.BorderColor = System.Drawing.Color.DimGray;
            stateProperties3.FillColor = System.Drawing.Color.Empty;
            stateProperties3.ForeColor = System.Drawing.Color.Empty;
            stateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Proveedor.OnHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties4.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(37)))), ((int)(((byte)(43)))));
            stateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.Txt_Buscar_Proveedor.OnIdleState = stateProperties4;
            this.Txt_Buscar_Proveedor.PasswordChar = '\0';
            this.Txt_Buscar_Proveedor.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.Txt_Buscar_Proveedor.PlaceholderText = "Buscar";
            this.Txt_Buscar_Proveedor.ReadOnly = false;
            this.Txt_Buscar_Proveedor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Txt_Buscar_Proveedor.SelectedText = "";
            this.Txt_Buscar_Proveedor.SelectionLength = 0;
            this.Txt_Buscar_Proveedor.SelectionStart = 0;
            this.Txt_Buscar_Proveedor.ShortcutsEnabled = true;
            this.Txt_Buscar_Proveedor.Size = new System.Drawing.Size(357, 52);
            this.Txt_Buscar_Proveedor.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.Txt_Buscar_Proveedor.TabIndex = 102;
            this.Txt_Buscar_Proveedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_Buscar_Proveedor.TextMarginBottom = 0;
            this.Txt_Buscar_Proveedor.TextMarginLeft = 5;
            this.Txt_Buscar_Proveedor.TextMarginTop = 0;
            this.Txt_Buscar_Proveedor.TextPlaceholder = "Buscar";
            this.Txt_Buscar_Proveedor.UseSystemPasswordChar = false;
            this.Txt_Buscar_Proveedor.WordWrap = true;
            // 
            // Tabla_Proveedor
            // 
            this.Tabla_Proveedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Tabla_Proveedor.Location = new System.Drawing.Point(34, 219);
            this.Tabla_Proveedor.Name = "Tabla_Proveedor";
            this.Tabla_Proveedor.RowHeadersWidth = 51;
            this.Tabla_Proveedor.RowTemplate.Height = 24;
            this.Tabla_Proveedor.Size = new System.Drawing.Size(916, 466);
            this.Tabla_Proveedor.TabIndex = 112;
            // 
            // Proveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(998, 728);
            this.Controls.Add(this.Tabla_Proveedor);
            this.Controls.Add(this.Lb_Editar_Proveedor);
            this.Controls.Add(this.Lb_Nuevo_Proveedor);
            this.Controls.Add(this.Lb_Buscar_Proveedor);
            this.Controls.Add(this.Lb_Eliminar_Proveedor);
            this.Controls.Add(this.Btn_Guardar_Proveedor);
            this.Controls.Add(this.Btn_Nuevo_Proveedor);
            this.Controls.Add(this.Btn_Eliminar_Proveedor);
            this.Controls.Add(this.Btn_Editar_Proveedor);
            this.Controls.Add(this.Lb_ListProveedor);
            this.Controls.Add(this.Txt_Buscar_Proveedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Proveedor";
            this.Text = " ";
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Guardar_Proveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Nuevo_Proveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Eliminar_Proveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Editar_Proveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabla_Proveedor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI.WinForms.GunaLabel Lb_Editar_Proveedor;
        private Guna.UI.WinForms.GunaLabel Lb_Nuevo_Proveedor;
        private Guna.UI.WinForms.GunaLabel Lb_Buscar_Proveedor;
        private Guna.UI.WinForms.GunaLabel Lb_Eliminar_Proveedor;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Guardar_Proveedor;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Nuevo_Proveedor;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Eliminar_Proveedor;
        private Bunifu.Framework.UI.BunifuImageButton Btn_Editar_Proveedor;
        private Guna.UI.WinForms.GunaLabel Lb_ListProveedor;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox Txt_Buscar_Proveedor;
        private System.Windows.Forms.DataGridView Tabla_Proveedor;
    }
}