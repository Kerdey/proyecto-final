﻿using System.Windows.Forms;

namespace CONVO_II.Presentaciones
{
    public partial class Notificación_Correcta : Form
    {
        public Notificación_Correcta()
        {
            InitializeComponent();
        }

        private void Notificación_Load(object sender, System.EventArgs e)
        {
            Transición_Noti.ShowAsyc(this);                                                     //Animacion para q notificación aparezca desvaneciendose
        }

        private void Transición_Noti_TransitionEnd(object sender, System.EventArgs e)
        {
            Chek_Correcto.Start();
            Chek_Correcto_NotiCo.Enabled = true;
        }

        private void Chek_Correcto_Tick(object sender, System.EventArgs e)
        {
            Chek_Correcto_NotiCo.Enabled = false;
            Chek_Correcto.Stop();
        }

        private void Btn_Ok_Correcto_Noti_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }
    }
}
