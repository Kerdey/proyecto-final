﻿using CONVO_II.CAPA_DATOS.Coneccion;
using System;
using System.Windows.Forms;

namespace CONVO_II.Presentaciones
{
    public partial class Categoría : Form
    {
        public Categoría()
        {
            InitializeComponent();
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btn_Cancelar_Cate_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Categoría_Load(object sender, EventArgs e)
        {
            Productos prod = new Productos();
            Tabla_Categoría.DataSource = prod.ListCate();
            Tabla_Categoría.AutoGenerateColumns = true;
        }
    }
}
