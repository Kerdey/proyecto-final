﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CONVO_II.CAPA_DATOS
{
    class UnidadMedida
    {
        private int id;
        private string nombre;

        public UnidadMedida(int id, string nombre)
        {
            //aqui tambien
            this.id = id;
            this.nombre = nombre;
        }

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }


    }
}
