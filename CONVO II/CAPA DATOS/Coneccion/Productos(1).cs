﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pulpería_Lovo.CAPA_DATOS.Coneccion
{
    class Productos
    {
        private SqlConnection c = new SqlConnection();
        private Coneccion_DB conect = new Coneccion_DB();
        private SqlCommand cmd = new SqlCommand();
        private SqlDataReader ReadRows;

        public Productos()
        {
            c = conect.AbrirConecion();
            if (c.State == ConnectionState.Open)
            {
                MessageBox.Show("abierto");
            }
            else
            {
                MessageBox.Show("Cerrado");
            }
        }

        public DataTable ListCate()
        {
            DataTable table = new DataTable();
            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "ListarCategorias";
            cmd.CommandType = CommandType.StoredProcedure;
            ReadRows = cmd.ExecuteReader();
            table.Load(ReadRows);
            ReadRows.Close();
            conect.CerrarConeccion();
            return table;
        }

        public DataTable ListProd()
        {
            DataTable table = new DataTable();
            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "ListProductos";
            cmd.CommandType = CommandType.StoredProcedure;
            ReadRows = cmd.ExecuteReader();
            table.Load(ReadRows);
            ReadRows.Close();
            conect.CerrarConeccion();
            return table;
        }

        public DataTable ListProveedorJ()
        {
            DataTable table = new DataTable();
            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "dbo.GetAnyProvj";
            cmd.CommandType = CommandType.StoredProcedure;
            ReadRows = cmd.ExecuteReader();
            table.Load(ReadRows);
            ReadRows.Close();
            conect.CerrarConeccion();
            return table;
        }

        public DataTable ListProveedorN()
        {
            DataTable table = new DataTable();
            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "dbo.GetAnyProvN";
            cmd.CommandType = CommandType.StoredProcedure;
            ReadRows = cmd.ExecuteReader();
            table.Load(ReadRows);
            ReadRows.Close();
            conect.CerrarConeccion();
            return table;
        }


        public void InsertarProducto(string cod, string nombreprod, string descp, string marca, float precio, float stock, string unid, int cat, string prov, DateTime fechavenc)
        {

            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@CodBar", SqlDbType.NVarChar);
            param[0].Value = cod;
            param[1] = new SqlParameter("@NombreProducto", SqlDbType.NVarChar);
            param[1].Value = nombreprod;
            param[2] = new SqlParameter("@descripcion", SqlDbType.NVarChar);
            param[2].Value = descp;
            param[3] = new SqlParameter("@Marca", SqlDbType.NVarChar);
            param[3].Value = marca;
            param[4] = new SqlParameter("@Idc", SqlDbType.Int);
            param[4].Value = cat;
            param[5] = new SqlParameter("@Stock", SqlDbType.Float);
            param[5].Value = stock;
            param[6] = new SqlParameter("@UnidadVenta", SqlDbType.NVarChar);
            param[6].Value = unid;
            param[7] = new SqlParameter("@PrecioVenta", SqlDbType.Float);
            param[7].Value = precio;
            param[8] = new SqlParameter("@FechaVencimiento", SqlDbType.DateTime);
            param[8].Value = fechavenc;
            param[9] = new SqlParameter("@ProveedorJ", SqlDbType.Int);
            param[9].Value = BuscarProveedorj(prov);
            param[9] = new SqlParameter("@ProveedorJ", SqlDbType.Int);
            param[9].Value = BuscarProveedorj(prov);




            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "Agregar_Producto";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddRange(param);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            da.Fill(ds);

        }

        public int BuscarProveedorj(string nameprov)
        {
            int id;
            try
            {
                cmd.Connection = conect.AbrirConecion();
                cmd.CommandText = "BuscarProvJ";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Search", nameprov);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataRow dr = dt.Rows[0];
                id = Convert.ToInt32(dr["Id_proveedor"].ToString());
            }
            catch (Exception)
            {
                id = 0;
            }

            return id;
        }

        public int BuscarProveedorN(string nameprov)
        {
            int id;
            try
            {
                cmd.Connection = conect.AbrirConecion();
                cmd.CommandText = "BuscarProvJ";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Search", nameprov);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataRow dr = dt.Rows[0];
                id = Convert.ToInt32(dr["Id_proveedor"].ToString());
            }
            catch (Exception)
            {
                id = 0;
            }

            return id;
        }

    }
}
