﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulpería_Lovo.CAPA_DATOS.Coneccion
{
    class Coneccion_DB
    {
        private static string Cadena_Coneccion = "Data Source=KERYMSGARCIA;Initial Catalog=PulperiaLovo;User ID=Admin;Password=Admin123";

        private SqlConnection Con = new SqlConnection(Cadena_Coneccion);

        public SqlConnection AbrirConecion()
        {
            if (Con.State == ConnectionState.Closed)
            {
                Con.Open();

            }
            return Con;
        }



        public SqlConnection CerrarConeccion()
        {
            if (Con.State == ConnectionState.Open)
            {
                Con.Close();

            }
            return Con;
        }



    }
}
