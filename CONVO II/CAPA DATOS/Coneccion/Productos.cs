﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONVO_II.CAPA_DATOS.Coneccion
{
    class Productos
    {
        private SqlConnection c = new SqlConnection();
        

        public Productos()
        {
        //    Coneccion_DB conect = new Coneccion_DB();
        // SqlCommand cmd = new SqlCommand();
        // SqlDataReader ReadRows;
        //c = conect.AbrirConecion();
        //    if (c.State == ConnectionState.Open)
        //    {
        //        MessageBox.Show("abierto");
        //    }
        //    else
        //    {
        //        MessageBox.Show("Cerrado");
        //    }
        }

        public DataTable ListCate()
        {
            Coneccion_DB conect = new Coneccion_DB();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader ReadRows;
            DataTable table = new DataTable();
            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "ListarCategorias";
            cmd.CommandType = CommandType.StoredProcedure;
            ReadRows = cmd.ExecuteReader();
            table.Load(ReadRows);
            ReadRows.Close();
            conect.CerrarConeccion();
            return table;
        }

        public DataTable ListProd()
        {
            Coneccion_DB conect = new Coneccion_DB();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader ReadRows;
            DataTable table = new DataTable();
            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "ListProductos";
            cmd.CommandType = CommandType.StoredProcedure;
            ReadRows = cmd.ExecuteReader();
            table.Load(ReadRows);
            ReadRows.Close();
            conect.CerrarConeccion();
            return table;
        }

        public DataTable ListProveedorJ()
        {
            Coneccion_DB conect = new Coneccion_DB();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader ReadRows;
            DataTable table = new DataTable();
            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "dbo.GetAnyProvj";
            cmd.CommandType = CommandType.StoredProcedure;
            ReadRows = cmd.ExecuteReader();
            table.Load(ReadRows);
            ReadRows.Close();
            conect.CerrarConeccion();
            return table;
        }

        public DataTable ListProveedorN()
        {
            Coneccion_DB conect = new Coneccion_DB();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader ReadRows;
            DataTable table = new DataTable();
            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "dbo.GetAnyProvN";
            cmd.CommandType = CommandType.StoredProcedure;
            ReadRows = cmd.ExecuteReader();
            table.Load(ReadRows);
            ReadRows.Close();
            conect.CerrarConeccion();
            return table;
        }


        public bool InsertarProducto(string cod, string nombreprod, string descp, string marca, float precio, float stock, string unid, int cat, string prov, DateTime fechavenc)
        {
            bool state;
            Coneccion_DB conect = new Coneccion_DB();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader ReadRows;
            try {

                //MessageBox.Show("insert");
            cmd.Connection = conect.AbrirConecion();
            cmd.CommandText = "Agregar_Producto";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CodBar",cod);
                cmd.Parameters.AddWithValue("@NombreProducto",nombreprod);
                cmd.Parameters.AddWithValue("@descripcion",descp);
                cmd.Parameters.AddWithValue("@Marca",marca);
                cmd.Parameters.AddWithValue("@Idc",cat);
                cmd.Parameters.AddWithValue("@Stock",stock);
                cmd.Parameters.AddWithValue("@UnidadVenta",unid);
                cmd.Parameters.AddWithValue("@PrecioVenta",precio);
                cmd.Parameters.AddWithValue("@FechaVencimiento",fechavenc);
                cmd.Parameters.AddWithValue("@ProveedorJ", BuscarProveedorj(prov));
                cmd.Parameters.AddWithValue("@ProveedorN", BuscarProveedorN(prov));
                cmd.ExecuteNonQuery();
            
               // ReadRows.Close();
                conect.CerrarConeccion();
                state = true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                state = false ;
            }

            return state;

        }

        public int BuscarProveedorj(string nameprov)
        {
            Coneccion_DB conect = new Coneccion_DB();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader ReadRows;
            int id;
            try
            {
                cmd.Connection = conect.AbrirConecion();
                cmd.CommandText = "BuscarProvJ";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Search", nameprov);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataRow dr = dt.Rows[0];
                id = Convert.ToInt32(dr["Id_proveedor"].ToString());
                cmd.Parameters.Clear();
                conect.CerrarConeccion();
            }
            catch (Exception)
            {
                id = 2;
            }

            return id;
        }

        public int BuscarProveedorN(string nameprov)
        {
            Coneccion_DB conect = new Coneccion_DB();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader ReadRows;
            int id;
            try
            {
                cmd.Connection = conect.AbrirConecion();
                cmd.CommandText = "BuscarProvN";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Search", nameprov);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataRow dr = dt.Rows[0];
                id = Convert.ToInt32(dr["Id_proveedor"].ToString());
                //ReadRows.Close();
                cmd.Parameters.Clear();
                conect.CerrarConeccion();
            }
            catch (Exception)
            {
                id = 1;
            }

            return id;
        }

    }
}
